public with sharing class CardExtension{
    public string cardgenStatus{get;set;}
    public string cardauthStatus{get;set;}
    public string cardfinStatus{get;set;}
    public Date ExpiryDate{get;set;}
    public Integer WrongPinAttempts{get;set;}
    
    public List<Card__c>Cardlist;
   // public string cardNumbertoCHK;//css-1710
    public CardExtension(ApexPages.StandardController stdController)
    {
        Cardlist=[SELECT id,name,Institution_Id__c,Card_Number_Truncated__c,Financial_Account__c,PrimaryCardFlag__c,Financial_Account__r.Account_Number__c,people__c,People__r.SSN__c from card__c where id=:stdController.getID()];
       // cardNumbertoCHK=Cardlist[0].Card_Number_Truncated__c;//css-1710
        if(Cardlist[0].PrimaryCardFlag__c==false)//css-1710[START]
        {
           id finid=Cardlist[0].Financial_Account__c;
           Cardlist=new List<Card__c>();
           Cardlist=[SELECT id,name,Institution_Id__c,Card_Number_Truncated__c,Financial_Account__c,PrimaryCardFlag__c,Financial_Account__r.Account_Number__c,people__c,People__r.SSN__c from card__c where Financial_Account__c=:finid];    
        }//css-1710[END]
    }
    public void doCall()
    {   try{
        if(Cardlist!=null && Cardlist.size()>0 && Cardlist[0].People__c!=null && Cardlist[0].People__r.SSN__c!=null && Cardlist[0].People__r.SSN__c!='' && Cardlist[0].Institution_Id__c!=null ){
            system.debug('-------> GetCustomerOutput '+Cardlist[0].Institution_Id__c);
            system.debug('-------> GetCustomerOutput '+Cardlist[0].People__r.SSN__c);
            GetCustomerOutput output= CSSServiceHelper.getCustomerDetails(integer.valueof(Cardlist[0].Institution_Id__c),Cardlist[0].People__r.SSN__c,'Y','Y');//'0000000061'
            if(output!=null && output.FinancialAccounts!=null){
                for(GetCustomerOutput.CustomerFinancialAccount eachFA : output.FinancialAccounts)
                {
                    if(eachFA.AccountNo==Cardlist[0].Financial_Account__r.Account_Number__c)
                    {system.debug('==>>'+Cardlist[0].Financial_Account__r.Account_Number__c);
                        for(GetCustomerOutput.CardType eachCard : eachFA.Cards)
                        {    
                           
                                cardgenStatus=eachCard.StGen;
                                system.debug('----->stat01'+ cardgenStatus);
                                cardfinStatus=eachCard.StFin;
                                system.debug('----->stat02'+ cardfinStatus);
                                cardauthStatus=eachCard.StAuth;
                                system.debug('----->stat03'+ cardauthStatus);
                                ExpiryDate=eachCard.ExpiryDate;
                                WrongPinAttempts=eachCard.WrongPinAttempts;
                                break; 
                           
                         }   
                            break;
                    }
                }
            }
        } 
        } 
        
        catch(DMLException e)   {
            StatusLogHelper.logSalesforceError('getCustomerDetails', 'E005', 'getCustomerDetails: ' + integer.valueof(Cardlist[0].Institution_Id__c)+Cardlist[0].People__r.SSN__c, e, false);
        }
        catch(CalloutException e)   {
            StatusLogHelper.logSalesforceError('getCustomerDetails', 'E001', 'getCustomerDetails: ' + integer.valueof(Cardlist[0].Institution_Id__c)+Cardlist[0].People__r.SSN__c, e, false);
        }
        catch(Exception e)   {
            StatusLogHelper.logSalesforceError('getCustomerDetails', 'E004', 'getCustomerDetails: ' + integer.valueof(Cardlist[0].Institution_Id__c)+Cardlist[0].People__r.SSN__c, e, false);
        }
    }
}