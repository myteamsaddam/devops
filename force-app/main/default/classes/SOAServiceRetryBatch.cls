global class SOAServiceRetryBatch implements Database.Batchable<sObject>,Database.AllowsCallouts,Database.Stateful{

Datetime starttime = system.now();
global Database.QueryLocator start(Database.BatchableContext BC){
      return Database.getQueryLocator('select Number_of_Attempt__c,Requested_Agent__c,Response_Status__c,Service_name__c,Service_request_body__c from SOAServiceRetryLog__c where Response_Status__c!=\'Success\' AND Number_of_Attempt__c<6');
   }

   global void execute(Database.BatchableContext BC, List<sObject> scope){
       
       for(sObject s : scope)
       {
           SOAServiceRetryLog__c soaRetry = (SOAServiceRetryLog__c)s;
           if(soaRetry.Service_name__c=='UpdateCustomerNameAddress')
           {
                UpdateCustomerAddressInput UpdateCustomerAddressInputInstance = (UpdateCustomerAddressInput)JSON.deserialize(soaRetry.Service_request_body__c,UpdateCustomerAddressInput.class);
                Service svc = new Service(ServiceHelper.updateCustomerNameAddress);
                UpdateCustomerAddressOutput output = (UpdateCustomerAddressOutput)svc.callout(UpdateCustomerAddressInputInstance); 
                system.debug('UpdateCustomerNameAddress output list form-->'+output);
                if(output==null || output.Code!='0')//service call failed logging request >> Modified by Shameel from 0 to '0'
                SOAServiceRetryLogOpsController.assignDataSOAServiceRetryLog(soaRetry.id,soaRetry.Number_of_Attempt__c+1,soaRetry.Requested_Agent__c,'Failed','UpdateCustomerNameAddress',JSON.serialize(UpdateCustomerAddressInputInstance));
                else
                SOAServiceRetryLogOpsController.assignDataSOAServiceRetryLog(soaRetry.id,soaRetry.Number_of_Attempt__c+1,soaRetry.Requested_Agent__c,'Success','UpdateCustomerNameAddress',JSON.serialize(UpdateCustomerAddressInputInstance));
           }
           if(soaRetry.Service_name__c=='UpdateCustomerData')
           {
                UpdateCustomerDataInput UpdateCustomerDataInputInstance = (UpdateCustomerDataInput)JSON.deserialize(soaRetry.Service_request_body__c,UpdateCustomerDataInput.class);
                Service svc = new Service(ServiceHelper.UpdateCustomerData);
                UpdateCustomerDataOutput output = (UpdateCustomerDataOutput)svc.callout(UpdateCustomerDataInputInstance); 
                system.debug('UpdateCustomer output form-->'+output);
                if(output==null || output.UCDR.Code!='0')//service call failed logging request
                SOAServiceRetryLogOpsController.assignDataSOAServiceRetryLog(soaRetry.id,soaRetry.Number_of_Attempt__c+1,soaRetry.Requested_Agent__c,'Failed','UpdateCustomerData',JSON.serialize(UpdateCustomerDataInputInstance));
                else
                SOAServiceRetryLogOpsController.assignDataSOAServiceRetryLog(soaRetry.id,soaRetry.Number_of_Attempt__c+1,soaRetry.Requested_Agent__c,'Success','UpdateCustomerData',JSON.serialize(UpdateCustomerDataInputInstance));
           }
           if(soaRetry.Service_name__c=='UpdateConsentData')
           {
                UpdateConsentServiceInput updateConsentServiceInputInstance = (UpdateConsentServiceInput)JSON.deserialize(soaRetry.Service_request_body__c,UpdateConsentServiceInput.class);
                Service svc = new Service(ServiceHelper.UpdateConsent);
                UpdateConsentServiceOutput output = (UpdateConsentServiceOutput)svc.callout(updateConsentServiceInputInstance); 
                system.debug('UpdateConsentData output form-->'+output);
                if(output==null || output.Code!='0')//service call failed logging request
                SOAServiceRetryLogOpsController.assignDataSOAServiceRetryLog(soaRetry.id,soaRetry.Number_of_Attempt__c+1,soaRetry.Requested_Agent__c,'Failed','UpdateConsentData',JSON.serialize(updateConsentServiceInputInstance));
                else
                SOAServiceRetryLogOpsController.assignDataSOAServiceRetryLog(soaRetry.id,soaRetry.Number_of_Attempt__c+1,soaRetry.Requested_Agent__c,'Success','UpdateConsentData',JSON.serialize(updateConsentServiceInputInstance));
           }
       }
       SOAServiceRetryLogOpsController.insertDataSOAServiceRetryLog();//insert retry logs if any
     
    }

   global void finish(Database.BatchableContext BC){
       Common_Settings__c comset = Common_Settings__c.getValues('SOAServiceRetryBatch');
       Integer intervalafter = (Integer)comset.Retry_batch_interval_in_hours__c*60;
       SOAServiceRetryBatch SOAServiceRetryBatchinstance = new SOAServiceRetryBatch();
       Long minutesFromNow = (system.now().getTime()-starttime.getTime())/60000;
       if(!Test.isRunningTest())
       System.scheduleBatch(SOAServiceRetryBatchinstance, 'SOAServiceRetryBatch1', intervalafter -((Integer)minutesFromNow), 10);
   }

}