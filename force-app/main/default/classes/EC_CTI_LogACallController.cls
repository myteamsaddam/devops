/**********************************************************************
Name: EC_CTI_LogACallController
=======================================================================
Purpose: This class is used for CTI functionality in salesforce  User Story -SFD-1254

======================================================================= 
History
-----------------------------------------------------------------------         
Version     Author                   Date              Detail
1.0         Saddaam Hussain         9-Jul-19       Initial Version

**********************************************************************/

Public class EC_CTI_LogACallController {
    /**********************************************************************
*   Author: Saddam Hussain
*   Date:     9-Jul-19
*   User Story : SFD-1254
*   Param: None
*   Return: result message    
*   Description: In this method we are trying to update CTI task, creating the memo and also making the callout 
* 				 to prime to create memo.
* @Revision(s): [Date] - [Change Reference] - [Changed By] - [Description]   

**********************************************************************/
    
    
    @AuraEnabled
    Public Static String updateCtiTask(String finId, String cardId, Id custId, String category,
                                       String description, String caseType, Boolean memoFlag, 
                                       Boolean callFlag, String saveAtmp, String saved, 
                                       String reason, String offer
                                      ) {
                                          String message = '';
                                          try {
                                              List<Task> taskList = [SELECT Id, Description, WhatId, WhoId,Comments__c, 
                                                                     CallType, Call_Type__c, Account_Serno__c,Card_Serno__c,
                                                                     Category__c,Customer_Serno__c,MAH_Serno__c,Type 
                                                                     FROM Task 
                                                                     WHERE (CreatedById = :UserInfo.getUserId() 
                                                                            AND CreatedDate = Today 
                                                                            AND WhoId =: custId) 
                                                                     AND (Status =: EC_StaticConstant.TASK_COMPLETE_STATUS 
                                                                          AND Comments__c = '' 
                                                                          AND CallType != null) 
                                                                     ORDER BY 
                                                                     CreatedDate DESC Limit 1];
                                              system.debug('----ctiTask----'+taskList);
                                              List<Card__c> cardList = [SELECT Id,Card_Serno__c,People_Serno__c,
                                                                        Financial_Account_Serno__c,GeneralStatus__c,
                                                                        People__r.Account.Customer_Serno__c, People__r.SSN__c, 
                                                                        People__r.Institution_Id__c 
                                                                        FROM Card__c 
                                                                        WHERE People__c =: custId 
                                                                        AND Id =: cardId];
                                              system.debug('---cardlist--'+cardList.size());
                                              system.debug('--TaskList--'+taskList.size());
                                              system.debug('-callFlag--'+callFlag);
                                              system.debug('---cate---'+String.isNotBlank(category));
                                              
                                              If (taskList.size() > 0 
                                                  && memoFlag == false) {
                                                      message = System.Label.Activity_not_available_msg;
                                                  }
                                              
                                              system.debug('--category-'+category);
                                              If ((memoFlag == true 
                                                   || callFlag == true
                                                  ) 
                                                  && String.isBlank(category)
                                                 ) {
                                                     message = System.Label.Category_not_select_msg;
                                                 }
                                              
                                              if ((memoFlag == true 
                                                   || callFlag == true
                                                  ) 
                                                  && (finId == EC_StaticConstant.NONE_VALUE 
                                                      || String.isBlank(finId)
                                                     )
                                                 ) {
                                                     message = System.Label.Financial_Value;
                                                 }
                                              
                                              If ( category == EC_StaticConstant.CATEGORY_ACCOUNT_CLOSURE 
                                                  && ( memoFlag == true 
                                                      || callFlag == true
                                                     ) 
                                                  && ( String.isBlank(saveAtmp) 
                                                      || String.isBlank(saved) 
                                                      || String.isBlank(reason) 
                                                      || String.isBlank(offer) 
                                                      || finId == EC_StaticConstant.OTHER_VALUE
                                                      || finId == EC_StaticConstant.NONE_VALUE
                                                      || String.isBlank(finId)
                                                     )
                                                 ) {
                                                     if (String.isBlank(saveAtmp))
                                                         message = EC_StaticConstant.RETENTION_SAVEATTEMPT;//'Please Select the Save Attempt';
                                                     if (String.isBlank(saved))
                                                         message = EC_StaticConstant.RETENTION_SAVED;//'Please Select the Saved';
                                                     if (String.isBlank(offer))
                                                         message = EC_StaticConstant.RETENTION_OFFER;//'Please Select the Offer';
                                                     if (String.isBlank(reason))
                                                         message = EC_StaticConstant.RETENTION_REASON;//'Please Select the Reason';
                                                     if ( finId == EC_StaticConstant.OTHER_VALUE 
                                                         || finId == EC_StaticConstant.NONE_VALUE
                                                         || String.isBlank(finId)
                                                        )
                                                         message = System.Label.Financial_Value;
                                                     
                                                     return message;
                                                 }
                                              
                                              List<Contact> contactList = [SELECT Id,Institution_Id__c,SSN__c FROM Contact WHERE Id=:custId];
                                              system.debug('-conlst--'+contactList);
                                              if ( contactList.size() > 0 
                                                  && contactList[0].Institution_Id__c!=null 
                                                  && (memoFlag == true 
                                                      || callFlag == true
                                                     )
                                                 ) {
                                                     // Below method is used to make the callout to Prime to create memo.
                                                     CustomerMemoExportDataOutput outputmemoexportdata 
                                                         = CSSServiceHelper.updateCustomerMemo('NewMemo','PersonSSN',
                                                                                               contactList[0].SSN__c,
                                                                                               'css',
                                                                                               description,String.valueOf(contactList[0].Institution_Id__c));
                                                     system.debug('--outputmemoexportdata---'+outputmemoexportdata);
                                                 }
                                              
                                              
                                              if ((cardList.size() > 0 
                                                   || cardId == EC_StaticConstant.OTHER_VALUE
                                                  ) 
                                                  && taskList.size() > 0 
                                                  && callFlag == true 
                                                  && String.isNotBlank(category)
                                                 ) {
                                                     
                                                     taskList[0].Description = description;
                                                     taskList[0].Comments__c = (description != null 
                                                                                && description.length() > 255 ?
                                                                                description.substring(0,254) : description
                                                                               );
                                                     taskList[0].Subject = EC_StaticConstant.TASK_CALL_SUBJECT; 
                                                     taskList[0].Type = caseType;
                                                     taskList[0].Call_Type__c = (taskList[0].CallType != null ?
                                                                                 taskList[0].CallType : 'N/A'
                                                                                );   
                                                     taskList[0].Category__c = category;
                                                     
                                                     if (cardId != EC_StaticConstant.OTHER_VALUE) {
                                                         taskList[0].Account_Serno__c = cardList[0].Financial_Account_Serno__c;
                                                         taskList[0].Customer_Serno__c = cardList[0].People_Serno__c;
                                                         taskList[0].Card_Serno__c = cardList[0].Card_Serno__c;
                                                         taskList[0].MAH_Serno__c = cardList[0].People__r.Account.Customer_Serno__c;
                                                     }
                                                     
                                                     if (taskList[0].CallType == EC_StaticConstant.TASK_INBOUND_CALLTYPE 
                                                         || taskList[0].CallType == EC_StaticConstant.TASK_OUTBOUND_CALLTYPE
                                                        )
                                                     {
                                                         upsert taskList;
                                                         message = System.Label.Activity_update_msg;
                                                     } 
                                                 }
                                              
                                              if ( (String.isNotBlank(finId) 
                                                    && finId != EC_StaticConstant.NONE_VALUE
                                                   ) 
                                                  && (memoFlag == true 
                                                      && description != null 
                                                      && String.isNotBlank(category)
                                                     )
                                                 ) {
                                                     //This below if condition used to create the completed Task.
                                                     
                                                     if(memoFlag == true){
                                                         Task taskObj = new Task();
                                                         taskObj.Subject = EC_StaticConstant.TASK_MEMO_SUBJECT; 
                                                         taskObj.Category__c = category;
                                                         taskObj.Type = caseType;
                                                         taskObj.Status = EC_StaticConstant.TASK_COMPLETE_STATUS;
                                                         taskObj.Description = description;
                                                         taskObj.Comments__c = (description != null 
                                                                                && description.length()>255 ?
                                                                                description.substring(0,254) : description);
                                                         taskObj.WhoId = CustId;
                                                         if (finId != EC_StaticConstant.OTHER_VALUE) {	
                                                             taskObj.Account_Serno__c = cardList[0].Financial_Account_Serno__c;
                                                             taskObj.Customer_Serno__c = cardList[0].People_Serno__c;
                                                             taskObj.Card_Serno__c = cardList[0].Card_Serno__c;
                                                             taskObj.MAH_Serno__c = cardList[0].People__r.Account.Customer_Serno__c;
                                                         }
                                                         insert taskObj;
                                                         message = System.Label.Activity_Insert_msg;
                                                     }
                                                 }
                                             
                                              If ( (String.isNotBlank(finId) 
                                                    && finId != EC_StaticConstant.NONE_VALUE
                                                   )
                                                  && category == EC_StaticConstant.CATEGORY_ACCOUNT_CLOSURE
                                                  && (memoFlag == true 
                                                      || callFlag == true
                                                     ) 
                                                  && (String.isNotBlank(saveAtmp) 
                                                      || String.isNotBlank(saved) 
                                                      || String.isNotBlank(reason) 
                                                      || String.isNotBlank(offer)
                                                     )
                                                 ) {
                                                     Retention_Response__c retentionObj = new Retention_Response__c();
                                                     retentionObj.Financial_Account__c = finId;
                                                     retentionObj.Person__c = custId;
                                                     retentionObj.Save_Attempt__c = saveAtmp;
                                                     retentionObj.Account_Retained__c = saved;
                                                     retentionObj.Account_Closed__c = (saved == EC_StaticConstant.YES_VALUE ? 
                                                                                       EC_StaticConstant.NO_VALUE:EC_StaticConstant.YES_VALUE);
                                                     retentionObj.Reason_for_Account_Closure__c = reason;
                                                     retentionObj.Offer__c = offer;
                                                     
                                                     insert retentionObj;
                                                 }
                                              
                                          } catch(Exception exp) {
                                              StatusLogHelper.logSalesforceError('EC_CTI_LogACallController', 'E005', 'updateCtiTask', exp, false); // Class,method
                                          }
                                          
                                          system.debug('----message----'+message);
                                          return message;
                                      }
    
    /**********************************************************************
*   Author: Saddam Hussain
*   Date:     12/05/2020
*   User Story : SFD-1426
*   Param: None
*   Return: picklist map    
*   Description: In this method we are trying to get the category__c field picklist values from the Case object.
*   @Revision(s): [Date] - [Change Reference] - [Changed By] - [Description]   

**********************************************************************/
    
    @AuraEnabled
    public static Map<String, String> getCategory(){
        Map<String, String> pickListMap = new Map<String, String>();
        //get Case Category__c Field Describe
        Schema.DescribeFieldResult fieldResult = Case.Category__c.getDescribe();
        //get Case Category__c Picklist Values
        List<Schema.PicklistEntry> pList = fieldResult.getPicklistValues();
        for (Schema.PicklistEntry p: pList) {
            //Put Picklist Value & Label in Map
            pickListMap.put(p.getValue(), p.getLabel());
        }
        return pickListMap;
    }
    
    /**********************************************************************
*   Author: Saddam Hussain
*   Date:     12/05/2020
*   User Story : SFD-1426
*   Param: None
*   Return: picklist map    
*   Description: In this method we are trying to get the type field picklist values from the Case object.
*   @Revision(s): [Date] - [Change Reference] - [Changed By] - [Description]   

**********************************************************************/
    
    @AuraEnabled
    public static Map<String, String> getType(){
        Map<String, String> typePickListMap = new Map<String, String>();
        //get Case Category__c Field Describe
        Schema.DescribeFieldResult fieldResult = Case.Type.getDescribe();
        //get Case Category__c Picklist Values
        List<Schema.PicklistEntry> pList = fieldResult.getPicklistValues();
        for (Schema.PicklistEntry p: pList) {
            //Put Picklist Value & Label in Map
            typePickListMap.put(p.getValue(), p.getLabel());
        }
        return typePickListMap;
    }
    
    
    /**********************************************************************
*   Author: Saddam Hussain
*   Date:     9-Jul-19
*   User Story : SFD-1254
*   Param: None
*   Return: boolean    
*   Description: In this method we are trying to update CTI task when change the customer name
*   @Revision(s): [Date] - [Change Reference] - [Changed By] - [Description]   

**********************************************************************/
    
    @AuraEnabled
    Public Static Boolean updateCustomerName(Id whoId, Id conId) {
        Boolean flag = false;
        try {
            List<Task> taskList = [SELECT Id, WhoId FROM Task 
                                   WHERE WhoId =: conId 
                                   AND (Status =: EC_StaticConstant.TASK_COMPLETE_STATUS 
                                        AND Comments__c = '') ORDER BY CreatedDate DESC Limit 1];
            system.debug('-----task-----'+taskList.size());
            
            if (taskList.size() > 0) {
                taskList[0].WhoId = whoId;
                upsert taskList;
                flag = true;
            } else {
                flag = false;
            }
            system.debug('---after-update----'+taskList);
        } catch(Exception exp) {
            StatusLogHelper.logSalesforceError('EC_CTI_LogACallController', 'E005', 'updateCustomerName', exp, false); // Class,method
        }
        return flag;
    }
    
    /**********************************************************************
*   Author: Saddam Hussain
*   Date:     9-Jul-19
*   User Story : SFD-1254
*   Param: None
*   Return: boolean    
*   Description: In this method we are trying to get the callType from task object.
*   @Revision(s): [Date] - [Change Reference] - [Changed By] - [Description]   

**********************************************************************/
    
    @AuraEnabled
    Public Static String getCallType(Id custId) {
        List<Task> taskList = [SELECT Id, CallType FROM Task 
                               WHERE WhoId =: custId 
                               AND CallType != '' 
                               AND Comments__c = '' 
                               AND (CreatedDate = Today 
                                    AND CreatedById = :UserInfo.getUserId()) 
                               ORDER BY CreatedDate DESC Limit 1];
        if (taskList.size() > 0) {
            return taskList[0].CallType;
        }
        return null;
    }
    
    /**********************************************************************
*   Author: Saddam Hussain
*   Date:     9-Jul-19
*   User Story : SFD-1254
*   Param: None
*   Return: boolean    
*   Description: In this method we are trying to get the Financial Account record for customer.
*   @Revision(s): [Date] - [Change Reference] - [Changed By] - [Description]   

**********************************************************************/
    
    @AuraEnabled
    Public Static Map<String, String> getFinancialDetails(Id custId) {
        String finId;
        Map<String, String> optionMap =  new Map<String, String>();
        Set<Id> financialAccIdSet =new Set<Id>();
        List<Card__c> cardList = [SELECT Id, Name, People__c, Financial_Account__c FROM Card__c 
                                  WHERE People__c=:custId 
                                  AND Financial_Account__c!=null 
                                  ORDER BY CreatedDate DESC Limit 50000];
        for(Card__c card:cardList) {
            financialAccIdSet.add(card.Financial_Account__c);
        }
        
        List<Financial_Account__c> financialAccountList = [SELECT Id, Name, Customer__c, Account_Number__c,
                                                           Account_Serno__c,Product__c,Product__r.name 
                                                           FROM Financial_Account__c 
                                                           WHERE Id IN:financialAccIdSet 
                                                           ORDER BY CreatedDate DESC Limit 50000];
        
        if (financialAccountList.size() == 1) {
            optionMap.put(financialAccountList[0].Id,financialAccountList[0].Account_Number__c+' : '
                          +(financialAccountList[0].Product__r.name!=null?financialAccountList[0].Product__r.name:'N/A'));
        } else {
            for(Financial_Account__c fa : financialAccountList) {
                optionMap.put(EC_StaticConstant.NONE_VALUE,EC_StaticConstant.NONE_VALUE);
                optionMap.put(fa.Id,fa.Account_Number__c+' : '+(fa.Product__r.name!=null?fa.Product__r.name:'N/A'));
            }
        }
        optionMap.put(EC_StaticConstant.OTHER_VALUE,EC_StaticConstant.OTHER_VALUE);
        return optionMap;
    }
    
    /**********************************************************************
*   Author: Saddam Hussain
*   Date:     9-Jul-19
*   User Story : SFD-1254
*   Param: None
*   Return: boolean    
*   Description: In this method we are trying to get the card record related to Financial Account.

**********************************************************************/
    
    @AuraEnabled
    Public Static Map<String, String> getCardDetails(String financialVal, Id custId) {
        Map<String, String> cardMap = new Map<String, String>();
        List<Card__c> cardList = [SELECT Id, Name, People__c, Financial_Account__c,
                                  Card_Number_Truncated__c,Product__r.name,GeneralStatus__c 
                                  FROM Card__c 
                                  WHERE People__c=:custId 
                                  AND Financial_Account__c=:financialVal 
                                  ORDER BY ExpiryDate__C DESC Limit 50000];
        system.debug('------card----'+cardList.size());        
        system.debug('---financialVal-'+financialVal);
        
        if (cardList.size() == 1) {
            cardMap.put(cardList[0].Id,cardList[0].Card_Number_Truncated__c+' : '
                        +(cardList[0].GeneralStatus__c!=null?cardList[0].GeneralStatus__c:'N/A'));
        } else {
            for(Card__c card:cardList) {
                cardMap.put(card.id , card.Card_Number_Truncated__c+' : '+(card.GeneralStatus__c!=null?card.GeneralStatus__c:'N/A'));
            }
        }
        if (financialVal == EC_StaticConstant.OTHER_VALUE) {
            cardMap.put(EC_StaticConstant.OTHER_VALUE,EC_StaticConstant.OTHER_VALUE); 
        }
        if (financialVal == EC_StaticConstant.NONE_VALUE) {
            cardMap.put(EC_StaticConstant.NONE_VALUE, EC_StaticConstant.NONE_VALUE);
        }
        return cardMap;
    }
    
    //public static DisableCustomerTabService service = new DisableCustomerTabService();
    /*@AuraEnabled 
public static boolean disableCustomer(String CustId){
Boolean flag;
List<Task> tsklist = [SELECT Id,WhoId,Comments__c, 
CallType FROM Task 
WHERE (CreatedById = :UserInfo.getUserId() 
AND CreatedDate = Today AND WhoId =: CustId) 
AND (Status = 'Completed' 
AND Comments__c = '' AND CallType != null)
ORDER BY CreatedDate DESC Limit 1];
if(!tsklist.isEmpty()){
flag = true;
}else{
flag = false;
}
return flag;
}*/
    
    Public String ssn {set;get;}
    Public String custId{set;get;}
    Public String custName{set;get;}
    Public Case caseObj {set;get;}
    Public Task taskObj {set;get;}
    Public Id tskId {set;get;}
    
    // Constructor..
    Public EC_CTI_LogACallController(){
        caseObj = new Case();
        taskObj = new Task();
    }
    
    /**********************************************************************
*   Author: Saddam Hussain
*   Date:     9-Jul-19
*   User Story : SFD-1254
*   Param: None
*   Return: None    
*   Description: In this method we are trying to search the customer by SSN and update the created inbound task
with the found customer.

**********************************************************************/
    
    Public Pagereference redirctcustomer() {
        List<Task> taskList = [SELECT Id, WhoId, Comments__c, 
                               Category__c, Type 
                               FROM Task 
                               WHERE CreatedById = :UserInfo.getUserId() 
                               AND CreatedDate = Today 
                               AND Comments__c = '' 
                               ORDER BY CreatedDate DESC Limit 1];
        system.debug('----tslist---'+taskList.size());
        if (ssn != null) {
            Blob hash = Crypto.generateDigest('MD5', Blob.valueOf(Ssn));
            string encryptSSN=EncodingUtil.convertToHex(hash);
            List<Contact> contactList = [SELECT Id,Name FROM Contact WHERE Encrypted_SSN__c = :encryptSSN];
            system.debug('----conlist----'+contactList.size());
            if (contactList.size() > 0 
                && taskList.size() > 0
               ) {
                   taskList[0].WhoId = contactList[0].id;
                   upsert taskList;
                   CustId = contactList[0].id;
                   CustName = contactList[0].name;
               } else if (contactList.isEmpty()){
                   CustId = System.Label.No_Customer_msg;
               } else if (!contactList.isEmpty() 
                          && taskList.isEmpty()
                         ) {
                             CustId = System.Label.Activity_not_available_msg;
                         }
            //return null;
        }
        
        
        if (String.isNotBlank(ssn) 
            || ssn.isNumeric() == false
           ) {
               system.debug('--Ssn--12----'+ssn+'-check--'+ssn.isNumeric());
               if(!ssn.isNumeric())
                   custId=System.Label.SSN_not_Numaric_msg;//'SSN should be Numaric value';
               
               if(String.isBlank(ssn))
                   custId=	System.Label.No_SSN_msg;//'Please enter SSN';
               
               return null;
           }
        return null;  
    }
    
    /**********************************************************************
*   Author: Saddam Hussain
*   Date:     9-Jul-19
*   User Story : SFD-1254
*   Param: None
*   Return: None    
*   Description: In this method we are trying to update the created inbound task
with the found customer in the above method.

**********************************************************************/
    
    Public Pagereference updateActivity() {
        system.debug('-------case--'+caseObj.Type+'-----task---'+taskObj.Description+'---ctgy---'+caseObj.Category__c);
        List<Task> taskList = [SELECT Id, WhoId, Comments__c, 
                               Category__c, Type, Subject 
                               FROM Task 
                               WHERE CreatedById = :UserInfo.getUserId() 
                               AND CreatedDate = Today 
                               AND Comments__c = '' 
                               AND WhoId = '' 
                               ORDER BY CreatedDate DESC Limit 1];
        system.debug('----tslist---'+taskList);
        system.debug('---cat-'+caseObj.Category__c);
        if ((caseObj.Category__c == EC_StaticConstant.NONE_VAL 
             && !taskList.isEmpty()
            ) 
            || taskList.isEmpty()
           ) {
               if (caseObj.Category__c == EC_StaticConstant.NONE_VAL 
                   && !taskList.isEmpty()
                  )
                   custId=System.Label.Category_not_select_msg;
               if (taskList.isEmpty())
                   custId = System.Label.Activity_not_available_msg;
               return null;
           }
        
        if ((caseObj.Category__c != EC_StaticConstant.NONE_VAL 
             || taskObj.Description != null 
             || caseObj.Type != null
            ) 
            && !taskList.isEmpty()
           ) {
               taskList[0].Category__c = caseObj.Category__c;
               taskList[0].Comments__c = taskObj.Description;
               taskList[0].Comments__c = (taskObj.Description != null 
                                          && taskObj.Description.length()>255 ?
                                          taskObj.Description.substring(0,254) : taskObj.Description);
               taskList[0].Type = caseObj.Type;
               taskList[0].Subject = EC_StaticConstant.TASK_CALL_SUBJECT;
               update taskList;
               tskId = taskList[0].Id;
               custId = System.Label.Activity_update_msg;
           } 
        
        return null;
    }
}