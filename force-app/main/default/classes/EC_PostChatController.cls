public class EC_PostChatController {
    
    Public Boolean flag {set;get;}
    Public String chatky {set;get;}
    Public String fedval {set;get;}
    
    Public EC_PostChatController(){
        flag = false;
    }
    
    public PageReference updateCustFeedBack() {
        chatky = ApexPages.currentPage().getParameters().get('chatkey');
        fedval = ApexPages.currentPage().getParameters().get('feedback');
        system.debug('---chatky--'+chatky+'---'+fedval);
        
        LiveChatTranscript lctlist = [Select id, Customer_Feedback__c from LiveChatTranscript where chatkey =: chatky];
        if(lctlist != null){
            lctlist.Customer_Feedback__c = fedval;
            update lctlist;
            flag = true;
        }
        return null;
    }
}