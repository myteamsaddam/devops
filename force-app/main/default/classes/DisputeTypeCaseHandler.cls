/*
Created By: Srinivas D
Created Date: 5th-Feb-2019
Created for Jira Ticket:SFD-984
Calling from: 'DisputeTypeCaseCreation' Trigger
Last Modified By: Srinivas D
Last Modified Date: 27th-Feb-2019
*/
public class DisputeTypeCaseHandler {
    
    // caseListToCreate list is to store new Cases to insert
    public List<Case> caseListToCreate = new List<Case>();
    // Below caseMap is store Dispute Cases created on Today. CardserNo as key and Case as Value
    public Map<String,Case> caseMap = new Map<String,Case>();
    public Map<String,Case> caseDispMap = new Map<String,Case>();
    public Map<id,Case> caseIdMap = new Map<Id,Case>();
    public List<String> cardSerNolist = new List<String>();
    public Map<String,Card__c> cardMap = new Map<String,Card__c>();
    // Below Map is to hold Queue Data
    public Map<String,Id> queueMap = new Map<String,Id>();
    public List<Case> newCasesList = new List<Case>();
    public List<DCMS_Transaction__c> updateTrans = new List<DCMS_Transaction__c>();
    // Below fraudCaseList to hold new FraudCases to insert
    public List<DCMS_FraudCase__c> fraudCaseList = new List<DCMS_FraudCase__c>(); 
    public Map<String,String> qcsMap = new Map<String,String>();
    public Map<String,String> qcsMap1 = new Map<String,String>();
    
    public List<DCMS_Decision__c> decisionStatusCS; 
    public List<DCMS_CaseStatus__c> caseStatusCS;
    public List<DCMS_FraudRing__c> fraudRingCS;
    
    public Map<String,Decimal> decisionStatusMap = new Map<String,Decimal>();
    public Map<String,Decimal> caseStatusMap = new Map<String,Decimal>();
    public Map<String,Decimal> fraudRingMap = new Map<String,Decimal>();
    
    //Constructor 
    public void DisputeTypeCaseHandler(){
        
        //Getting Custom Settings Data
        decisionStatusCS=DCMS_Decision__c.getall().values();
        caseStatusCS=DCMS_CaseStatus__c.getall().values();
        fraudRingCS=DCMS_FraudRing__c.getall().values();
        
        for(DCMS_Decision__c desCS : decisionStatusCS){
            decisionStatusMap.put(desCS.Name,desCS.DecisionId__c);
        }
        
        for(DCMS_CaseStatus__c stsCS : caseStatusCS){
            caseStatusMap.put(stsCS.Name,stsCS.CaseStatusId__c);
        }
        
        for(DCMS_FraudRing__c frdCS : fraudRingCS){
            fraudRingMap.put(frdCS.Name,frdCS.FraudRingId__c);
        }
        for(DCMS_Queue_Settings__c qcset : DCMS_Queue_Settings__c.getall().values()){
            qcsMap.put(qcset.Name,qcset.Queue_Name__c);
        }
        System.debug('qcsMap with out SOQL'+qcsMap1);
        
        // Below code is to get DCMS_Queue_Settings__c data
        /*  for(DCMS_Queue_Settings__c qcset : [Select Name,Dispute_Type__c,Institutionid__c,Queue_Name__c from DCMS_Queue_Settings__c]){
qcsMap.put(qcset.Name,qcset.Queue_Name__c);
} */
        System.debug('qcsMap with SOQL'+qcsMap);
        // Below for loop is to store Queue details into Map
        for(Group grpqueue : [select Id,Name,DeveloperName from Group where Type = 'Queue']){
            queueMap.put(grpqueue.DeveloperName,grpqueue.Id);
        }
    }
    // Below method is to create Case records based on Dispute Type field on DCMS_Transaction__c Object
    public void createDisputeCases (List<DCMS_Transaction__c> transList) {
        try{
            system.debug('---transList-'+transList.size());
            DisputeTypeCaseHandler();
            System.debug('transList============>'+transList);
            List<DCMS_Transaction__c> transactionsList = [Select id,Txn_CardSerno__c,DisputeType__c,MerchName__c,PCI_Dss_Token__c,BillingAmount__c,Institutionid__c,CaseNumber__c,CaseNumber__r.Card_SerNo__c,CaseNumber__r.Dispute_Type__c from DCMS_Transaction__c where Id IN: transList];
            List<DCMS_FraudCase__c> fCaseList = new List<DCMS_FraudCase__c>();
            List<Decimal> tranCardSernosList = new List<Decimal>();
            // Below for loop is to get all cardserNo from DCMS Transaction Object
            for(DCMS_Transaction__c tran : transList){
                String cardSNo = String.valueOf(tran.Txn_CardSerno__c);
                System.debug('cardSNo============>'+cardSNo);
                if(cardSNo != null){
                    String ss=cardSNo.removeEnd('.0');
                    System.debug('sss============>'+ss);
                    cardSerNolist.add(ss);
                    tranCardSernosList.add(Decimal.valueOf(ss));
                }
            }
            system.debug('cardSerNolist=====>>>>>>'+cardSerNolist);
            Map<Id,DCMS_Transaction__c> oldTransList = new  Map<Id,DCMS_Transaction__c>([Select id,Txn_CardSerno__c,MerchName__c,DisputeType__c,PCI_Dss_Token__c,BillingAmount__c,Institutionid__c,CaseNumber__c,CaseNumber__r.Card_SerNo__c,CaseNumber__r.Dispute_Type__c,CaseNumber__r.Status from DCMS_Transaction__c where Txn_CardSerno__c IN: tranCardSernosList Limit 50000]);
            
            Map<String,DCMS_Transaction__c> tranMerchMap = new Map<String,DCMS_Transaction__c>();
            for(DCMS_Transaction__c oldtxn : oldTransList.values()){
                String temp =oldtxn.Txn_CardSerno__c+oldtxn.MerchName__c;
                tranMerchMap.put(temp,oldtxn);
            }
            
            
            // Below for loop is store Dispute Cases created on Today. CardserNo as key and Case as Value
            for(Case cas : [Select id,Description,Card_SerNo__c,Status,Card__r.Financial_Account__r.Account_Serno__c,Fraud_Ring__c,Decision_Status__c,Dispute_Type__c,Institutionid__c,Gross_Amount__c,Card_Number_Masked__c,Pci_Dss_Token__c,CreatedDate,Contact.SerNo__c,Contact.Institution_Id__c,(Select id,Institutionid__c from DCMS_Transactios__r),(Select id,TotalFraudAmt__c,TotalAtRisk__c,NumberOfTransactions__c from DCMS_FraudCases__r) from Case where Dispute_Type__c!='' AND (Status='In Progress' OR Status='New') limit 50000]){
                caseMap.put(cas.Card_SerNo__c,cas);
                caseIdMap.put(cas.id,cas);
                String keyParam= cas.Dispute_Type__c+cas.Card_SerNo__c;
                caseDispMap.put(keyParam,cas);
            }
            system.debug('====caseMap=====>>>>>>'+caseMap);
            
            // Below for loop is to get Card details into Map<CardSerNo,Card>
            for(Card__c crd : [select id,Card_Serno__c,People__c,People__r.Institution_Id__c from Card__c where Card_Serno__c IN: cardSerNolist]){
                cardMap.put(crd.Card_Serno__c,crd);
            }
            system.debug('cardMap=====>>>>>>'+cardMap);
            // Below code is to create new Dispute cases if all conditions met
            for(DCMS_Transaction__c tran : transactionsList){
                System.debug('--- Dispute Type----> '+tran.CaseNumber__c);
                Case cs = new Case();
                if(tran.CaseNumber__c != null){
                    if(caseMap.get(tran.CaseNumber__r.Card_SerNo__c) != null){
                        tran.Institutionid__c = caseIdMap.get(tran.CaseNumber__r.Id).Institutionid__c;
                        tran.DisputeType__c = caseIdMap.get(tran.CaseNumber__r.Id).Dispute_Type__c;
                        System.debug('account serno '+caseIdMap.get(tran.CaseNumber__r.Id).Card__r.Financial_Account__r.Account_Serno__c);
                        if(caseIdMap.get(tran.CaseNumber__r.Id).Card__r.Financial_Account__r.Account_Serno__c != null){
                            tran.Account_Serno__c = Decimal.valueOf(caseIdMap.get(tran.CaseNumber__r.Id).Card__r.Financial_Account__r.Account_Serno__c);
                        }
                        System.debug('tran Case Dispute_Type__c ====>> ' +caseIdMap.get(tran.CaseNumber__r.Id).Dispute_Type__c);   
                        if(caseIdMap.get(tran.CaseNumber__r.Id).Card_SerNo__c != null){
                            tran.Txn_CardSerno__c = Decimal.valueOf(caseIdMap.get(tran.CaseNumber__r.Id).Card_SerNo__c);
                            tran.Truncated_Card_Number__c = caseIdMap.get(tran.CaseNumber__r.Id).Card_Number_Masked__c;
                        }
                        cs.Gross_Amount__c = caseIdMap.get(tran.CaseNumber__r.Id).Gross_Amount__c+tran.BillingAmount__c;
                        System.debug('tran Case 111 Dispute_Type__c ====>>'+caseIdMap.get(tran.CaseNumber__r.Id).DCMS_FraudCases__r.size());
                        if(caseIdMap.get(tran.CaseNumber__r.Id).DCMS_FraudCases__r.size() > 0){
                            System.debug('tran Case Dispute_Type__c ====>> '+caseIdMap.get(tran.CaseNumber__r.Id).DCMS_FraudCases__r.size());
                            caseIdMap.get(tran.CaseNumber__r.Id).DCMS_FraudCases__r[0].TotalAtRisk__c= caseIdMap.get(tran.CaseNumber__r.Id).DCMS_FraudCases__r[0].TotalAtRisk__c+tran.BillingAmount__c;
                            caseIdMap.get(tran.CaseNumber__r.Id).DCMS_FraudCases__r[0].TotalFraudAmt__c= caseIdMap.get(tran.CaseNumber__r.Id).DCMS_FraudCases__r[0].TotalFraudAmt__c+tran.BillingAmount__c;
                            caseIdMap.get(tran.CaseNumber__r.Id).DCMS_FraudCases__r[0].NumberOfTransactions__c= caseIdMap.get(tran.CaseNumber__r.Id).DCMS_Transactios__r.size();
                            fCaseList.add(caseIdMap.get(tran.CaseNumber__r.id).DCMS_FraudCases__r[0]);
                        }
                        updateTrans.add(tran);
                    }
                }
                else{
                    if(tran.DisputeType__c != null){
                        String cardSerNo = String.valueOf(tran.Txn_CardSerno__c);
                        System.debug('--cardNum-----> '+cardSerNo);
                        //Case cs = new Case();
                        System.debug('--caseMap---cardNum-----> '+caseMap.get(cardSerNo));
                        if(caseMap.get(cardSerNo) != null && tran.CaseNumber__c == null){
                            String keyval=tran.DisputeType__c+tran.Txn_CardSerno__c;
                            //system.debug('Case Disp Type -->'+caseDispMap.get(keyval).Dispute_Type__c);
                            system.debug('Case cardSerNo -->'+caseMap.get(cardSerNo).Card_SerNo__c);
                            system.debug('tran.DisputeType__c -->'+tran.DisputeType__c);
                            system.debug('Tran cardSerNo -->'+tran.Txn_CardSerno__c);
                            system.debug('Case Status -->'+caseMap.get(cardSerNo).Status);
                            
                            if(tran.DisputeType__c == '10: credit'){
                                System.debug('new Tran Disp Type== '+tranMerchMap.get(tran.MerchName__c));
                                String temp =tran.Txn_CardSerno__c+tran.MerchName__c;
                                System.debug('temp== '+temp);
                                System.debug('temp1== '+tranMerchMap.get(temp));
                                System.debug('temp2== '+(tranMerchMap.get(temp).CaseNumber__r.Status == 'In Progress' || tranMerchMap.get(temp).CaseNumber__r.Status == 'New'));
                                if(tranMerchMap.get(temp) != null && (tranMerchMap.get(temp).CaseNumber__r.Status == 'In Progress' || tranMerchMap.get(temp).CaseNumber__r.Status == 'New')){
                                    System.debug('new Tran Case based on MerchName== '+tranMerchMap.get(temp).CaseNumber__c);
                                    tran.CaseNumber__c = tranMerchMap.get(temp).CaseNumber__c;
                                    updateTrans.add(tran);
                                }
                                else{
                                    System.debug('===MerchName not Matched---'+caseMap.get(cardSerNo).Id);
                                    tran.CaseNumber__c = caseMap.get(cardSerNo).Id;
                                    updateTrans.add(tran);
                                }
                                /*  for(DCMS_Transaction__c oldtran : oldTransList.values()){
System.debug('----outside if loop old tran Cardserno----'+oldtran.Txn_CardSerno__c);
System.debug('----outside if loop  1st Condition----'+(tran.Txn_CardSerno__c == oldtran.Txn_CardSerno__c));
System.debug('----outside if  2nd Condition----'+(tran.MerchName__c == oldtran.MerchName__c));
if((tran.Txn_CardSerno__c == oldtran.Txn_CardSerno__c) && (tran.DisputeType__c == oldtran.DisputeType__c || tran.MerchName__c == oldtran.MerchName__c) && (oldtran.CaseNumber__r.Status == 'In Progress' || oldtran.CaseNumber__r.Status == 'New')){
tran.CaseNumber__c = oldtran.CaseNumber__c;
updateTrans.add(tran);
System.debug('Old Tran Case Number== '+oldtran.CaseNumber__c);
break;
}
else if((tran.Txn_CardSerno__c == oldtran.Txn_CardSerno__c) && (oldtran.CaseNumber__r.Status == 'In Progress' || oldtran.CaseNumber__r.Status == 'New')){
System.debug('==else if== ');
tran.CaseNumber__c = oldtran.CaseNumber__c;
updateTrans.add(tran);
break;
}
} */
                            }
                            
                            // if(caseDispMap.get(keyval) != null){
                            else if(caseDispMap.get(keyval) != null && (tran.DisputeType__c == caseDispMap.get(keyval).Dispute_Type__c) && (tran.Txn_CardSerno__c == Decimal.valueOf(caseDispMap.get(keyval).Card_SerNo__c)) && (cardSerNo == caseMap.get(cardSerNo).Card_SerNo__c) && (caseMap.get(cardSerNo).Status == 'In Progress' || caseMap.get(cardSerNo).Status == 'New')){
                                System.debug('--inside- if----> '); 
                                // Here we need to update the Case Id in Transaction Record.
                                System.debug('keyval===>>'+keyval);
                                tran.CaseNumber__c = caseDispMap.get(keyval).Id;
                                updateTrans.add(tran);
                                if(caseDispMap.get(keyval).DCMS_FraudCases__r.size() > 0){
                                    caseDispMap.get(keyval).DCMS_FraudCases__r[0].TotalAtRisk__c= caseDispMap.get(keyval).DCMS_FraudCases__r[0].TotalAtRisk__c+tran.BillingAmount__c;
                                    caseDispMap.get(keyval).DCMS_FraudCases__r[0].TotalFraudAmt__c= caseDispMap.get(keyval).DCMS_FraudCases__r[0].TotalFraudAmt__c+tran.BillingAmount__c;
                                    caseDispMap.get(keyval).DCMS_FraudCases__r[0].NumberOfTransactions__c= caseDispMap.get(keyval).DCMS_Transactios__r.size();
                                    fCaseList.add(caseDispMap.get(keyval).DCMS_FraudCases__r[0]);
                                }
                            }
                            // }  
                            else if(tran.CaseNumber__c == null){
                                System.debug(' ---inner If else-->>>>>> '+tran.CaseNumber__c);
                                
                                cs.Department__c='Fraud and Chargeback';
                                cs.Category__c='Dispute';
                                cs.Dispute_Type__c=tran.DisputeType__c;
                                cs.Card__c = cardMap.get(cardSerNo).Id;
                                cs.ContactId = cardMap.get(cardSerNo).People__c;
                                cs.Institutionid__c = tran.Institutionid__c;
                                //cs.Gross_Amount__c = tran.BillingAmount__c;
                                cs.Pci_Dss_Token__c = String.valueOf(tran.PCI_Dss_Token__c);
                                System.debug('BillingAmount===>>>> '+tran.BillingAmount__c);
                                cs.Origin = 'SOA';
                                
                                if(tran.Institutionid__c == 1)
                                    cs.Country__c ='NO';
                                if(tran.Institutionid__c == 2)
                                    cs.Country__c ='SE';
                                if(tran.Institutionid__c == 3)
                                    cs.Country__c ='DK';
                                
                                if(tran.DisputeType__c != null && tran.Institutionid__c != null){
                                    String temp= tran.DisputeType__c+tran.Institutionid__c;
                                    if(queueMap.get(qcsMap.get(temp)) != null){
                                        cs.OwnerId = queueMap.get(qcsMap.get(temp));
                                    }
                                }
                                caseListToCreate.add(cs);
                            }
                        }
                        else{
                            System.debug('-- inside else -----> '); 
                            // Case cs = new Case();
                            
                            cs.Department__c='Fraud and Chargeback';
                            cs.Category__c='Dispute';
                            cs.Dispute_Type__c=tran.DisputeType__c;
                            System.debug('======cardMap1111====='+cardMap.get(cardSerNo));
                            if(cardMap.get(cardSerNo) != Null){
                                System.debug('======cardMap1111222');
                                cs.Card__c = cardMap.get(cardSerNo).Id;
                                cs.ContactId = cardMap.get(cardSerNo).People__c;
                            }
                            cs.Institutionid__c = tran.Institutionid__c;
                            //cs.Gross_Amount__c = tran.BillingAmount__c;
                            System.debug('===============BillingAmount===>>>> '+tran.BillingAmount__c);
                            cs.Pci_Dss_Token__c = String.valueOf(tran.PCI_Dss_Token__c);
                            cs.Origin = 'SOA';
                            
                            if(tran.Institutionid__c == 1)
                                cs.Country__c ='NO';
                            if(tran.Institutionid__c == 2)
                                cs.Country__c ='SE';
                            if(tran.Institutionid__c == 3)
                                cs.Country__c ='DK';
                            
                            if(tran.DisputeType__c != null && tran.Institutionid__c != null){
                                String temp= tran.DisputeType__c+tran.Institutionid__c;
                                if(queueMap.get(qcsMap.get(temp)) != null){
                                    cs.OwnerId = queueMap.get(qcsMap.get(temp));
                                }
                            }
                            caseListToCreate.add(cs);
                        }
                    } 
                }
                
            }
            System.debug(' ---caseListToCreate-->>>> '+caseListToCreate.size()+'--list--'+caseListToCreate);
            // Below code to insert new Case records if list size is not null.
            if(caseListToCreate.size() >0){
                insert caseListToCreate;
                System.debug(' ---caseListToCreate-->>>> '+caseListToCreate);
            }
            
            //List<Case> newCasesList =[Select id,Card_SerNo__c,Dispute_Type__c from Case where createdDate=TODAY AND Id IN:caseListToCreate];
            //Below Code is to link Case on DCMS_Transaction object  
            if(caseListToCreate.size() > 0){
                newCasesList =[Select id,Description,Card_SerNo__c,Dispute_Type__c,Institutionid__c,Gross_Amount__c,Card_Number_Masked__c,Pci_Dss_Token__c,CreatedDate,Contact.SerNo__c,Contact.Institution_Id__c,(Select id,Institutionid__c,BillingAmount__c from DCMS_Transactios__r),(Select id,TotalFraudAmt__c,TotalAtRisk__c from DCMS_FraudCases__r) from Case where Id IN:caseListToCreate];
                for(DCMS_Transaction__c tran : transactionsList){
                    for(Case cas : newCasesList){
                        String cardSerNo = String.valueOf(tran.Txn_CardSerno__c);
                        //  System.debug(' ---cardSerNo-->>>> '+cardSerNo);
                        System.debug(' ---case serNo -->>>> '+cas.Card_SerNo__c);
                        if(tran.DisputeType__c == cas.Dispute_Type__c && cardSerNo == cas.Card_SerNo__c){
                            tran.CaseNumber__c = cas.Id;
                            System.debug(' ---tran.CaseNumber-->>>> '+tran.CaseNumber__c);
                            updateTrans.add(tran);
                            if(cas.DCMS_FraudCases__r.size() > 0){
                                cas.DCMS_FraudCases__r[0].TotalAtRisk__c = tran.BillingAmount__c;
                                fCaseList.add(cas.DCMS_FraudCases__r[0]);
                            }
                        }
                    }
                }
            }
            system.debug('--updateTrans.size--'+updateTrans.size());
            // Update Transaction Records if Transaction records are there in list. 
            if(updateTrans.size() > 0){
                update updateTrans;
            }
            system.debug('--updateTrans--'+updateTrans);
            system.debug('---fCaseList----'+fCaseList.size());
            if(fCaseList.size() > 0){
                update fCaseList;
            }
        }catch(Exception exp){
            StatusLogHelper.logSalesforceError('disputeTypeCaseHandler', 'E004', 'Error in createDisputeCases method ' , exp, false);
        }
    }
    // Below method will be called from UpdateCaseHandlingTime when Case records are updated 
    public void updateFraudCases(Map<id,Case> dispCases , Map<Id,Case> oldCases){
        try{
            //Calling Constructor to get QueueMap data here
            DisputeTypeCaseHandler();
            //Declaring fraudCasesToupdateList to update fraudCases
            List<DCMS_FraudCase__c> fraudCasesToupdateList = new List<DCMS_FraudCase__c>();
            
            List<DCMS_Transaction__c> transToupdateList = new List<DCMS_Transaction__c>();
            List<Case> CasesToupdateList = new List<Case>();
            //Below Code is to get DCMS_FraudCase__c records related to updated Cases.  
            Map<Id,DCMS_FraudCase__c> fCaseMap = new  Map<Id,DCMS_FraudCase__c>();
            for(DCMS_FraudCase__c fcase : [Select id,Description__c,CaseCloseDate__c,DisputeType__c,Case_Number__c from DCMS_FraudCase__c where Case_Number__c IN:dispCases.keyset()]){
                fCaseMap.put(fcase.Case_Number__c,fcase);
            }
            System.debug('fCaseMap--->> '+fCaseMap);
            Map<Id,Case> casesMap = new Map<Id,Case>([Select id,Description,Card_SerNo__c,Gross_Amount__c,Dispute_Type__c,Institutionid__c,Card_Number_Masked__c,Pci_Dss_Token__c,CreatedDate,Contact.SerNo__c,Contact.Institution_Id__c,(Select id,Institutionid__c,BillingAmount__c,DisputeType__c from DCMS_Transactios__r) from Case where Id IN:dispCases.keyset()]);
            System.debug('casesMap--->> '+casesMap);
            for(Case cas : dispCases.values()){
                if(cas.Dispute_Type__c != null && cas.Institutionid__c != null){
                    String temp= cas.Dispute_Type__c+cas.Institutionid__c;
                    System.debug('temp===>> '+temp);
                    if(queueMap.get(qcsMap.get(temp)) != null && (oldCases.get(cas.Id).Dispute_Type__c!=dispCases.get(cas.Id).Dispute_Type__c)){
                        System.debug('queueMap===>> '+queueMap.get(qcsMap.get(temp)));
                        cas.OwnerId = queueMap.get(qcsMap.get(temp));
                        // CasesToupdateList.add(cas);
                    }           
                }
                // cas.Gross_Amount__c=0.0;          
                //Below code to assign values from Case to fraudCase 
                if(fCaseMap.get(cas.id) != null){ 
                    // cas.Gross_Amount__c=0.0;
                    fCaseMap.get(cas.id).DisputeType__c= cas.Dispute_Type__c;
                    fCaseMap.get(cas.id).Description__c= cas.Description;
                    fCaseMap.get(cas.id).Institutionid__c= cas.Institutionid__c;
                    fCaseMap.get(cas.id).DecisionId__c = decisionStatusMap.get(cas.Decision_Status__c);
                    fCaseMap.get(cas.id).CaseStatusId__c = caseStatusMap.get(cas.Status);
                    fCaseMap.get(cas.id).FraudRingId__c = fraudRingMap.get(cas.Fraud_Ring__c);
                    fCaseMap.get(cas.id).NumberOfTransactions__c= casesMap.get(cas.Id).DCMS_Transactios__r.size();
                    for(DCMS_Transaction__c tr : casesMap.get(cas.Id).DCMS_Transactios__r){
                        //   cas.Gross_Amount__c=cas.Gross_Amount__c+tr.BillingAmount__c;
                        //  fCaseMap.get(cas.id).TotalAtRisk__c= cas.Gross_Amount__c;
                    }
                    fraudCasesToupdateList.add(fCaseMap.get(cas.id));
                }
                
                //Update Transaction Records when Case is updated
                for(DCMS_Transaction__c casTran : casesMap.get(cas.Id).DCMS_Transactios__r){
                    System.debug('Case Dispute===>>>> Srini===>> '+ cas.Dispute_Type__c);
                    casTran.DisputeType__c = cas.Dispute_Type__c;
                    transToupdateList.add(casTran);
                }
                
            }
            
            if(transToupdateList.size() > 0){
                update transToupdateList;
            }
            
            if(CasesToupdateList.size() > 0){
                // update CasesToupdateList;
            }
            //update fraudCasesToupdateList when case records updated and fraudCasesToupdateList size is grater than zero
            if(fraudCasesToupdateList.size() > 0){
                update fraudCasesToupdateList;
            }
        }catch(Exception exp){
            StatusLogHelper.logSalesforceError('disputeTypeCaseHandler', 'E004', 'Error in updateFraudCases method ' , exp, false);
        }
    }
    public void updateGrossAmount(List<DCMS_Transaction__c> delTransList){
        try{
            Set<Id> caseIdSet = new Set<Id>();
            //List<Id> caseIdSet = new List<Id>();
            for(DCMS_Transaction__c trans : delTransList){
                caseIdSet.add(trans.CaseNumber__c);
            } 
            system.debug('---caseIdSet--'+caseIdSet);
            Map<id,Case> caseMap = new Map<id,Case>([Select id, Gross_Amount__c,(Select id,Institutionid__c,BillingAmount__c from DCMS_Transactios__r),(Select id,TotalFraudAmt__c,TotalAtRisk__c from DCMS_FraudCases__r) from Case where Id IN: caseIdSet]);
            system.debug('--------caseMap-----'+caseMap);
            Set<Case> casesToUpdate = new Set<Case>();
            Set<DCMS_FraudCase__c> fcasesToUpdate = new Set<DCMS_FraudCase__c>();
            List<Case> casesToUpdate1 = new List<Case>();
            List<DCMS_FraudCase__c> fcasesToUpdate1 = new List<DCMS_FraudCase__c>();
            for(DCMS_Transaction__c trans1 : delTransList){
                if(caseMap.get(trans1.CaseNumber__c) !=null){
                    caseMap.get(trans1.CaseNumber__c).Gross_Amount__c = caseMap.get(trans1.CaseNumber__c).Gross_Amount__c-trans1.BillingAmount__c;
                    casesToUpdate1.add(caseMap.get(trans1.CaseNumber__c));
                    System.debug('fCase Size '+caseMap.get(trans1.CaseNumber__c).DCMS_FraudCases__r.size());
                    if(caseMap.get(trans1.CaseNumber__c).DCMS_FraudCases__r.size() > 0){
                        caseMap.get(trans1.CaseNumber__c).DCMS_FraudCases__r[0].TotalAtRisk__c = caseMap.get(trans1.CaseNumber__c).DCMS_FraudCases__r[0].TotalAtRisk__c - trans1.BillingAmount__c;
                        caseMap.get(trans1.CaseNumber__c).DCMS_FraudCases__r[0].TotalFraudAmt__c = caseMap.get(trans1.CaseNumber__c).DCMS_FraudCases__r[0].TotalFraudAmt__c - trans1.BillingAmount__c;
                        caseMap.get(trans1.CaseNumber__c).DCMS_FraudCases__r[0].NumberOfTransactions__c = caseMap.get(trans1.CaseNumber__c).DCMS_Transactios__r.size();
                        
                        fcasesToUpdate1.add(caseMap.get(trans1.CaseNumber__c).DCMS_FraudCases__r[0]);
                    }
                }
            }
            
            casesToUpdate.addAll(casesToUpdate1);
            casesToUpdate1.clear();
            casesToUpdate1.addAll(casesToUpdate);
            if(casesToUpdate1.size() > 0){
                update casesToUpdate1;
            }
            fcasesToUpdate.addAll(fcasesToUpdate1);
            fcasesToUpdate1.clear();
            fcasesToUpdate1.addAll(fcasesToUpdate);
            if(fcasesToUpdate.size() > 0){
                update fcasesToUpdate1;
            }
        }catch(Exception exp){
            StatusLogHelper.logSalesforceError('disputeTypeCaseHandler', 'E004', 'Error in updateGrossAmount method ' , exp, false);
        }
    }
    public void createFraudCases(List<Case> newCaseList){
        //Calling Constructor to get QueueMap data here
        try{
            DisputeTypeCaseHandler();
            List<Case> csList = [Select id,CreatedBy.Name,Description,Card_SerNo__c,Card__c,Card__r.Financial_Account__r.Account_Serno__c,Status,Fraud_Ring__c,Decision_Status__c,Dispute_Type__c,Institutionid__c,Gross_Amount__c,Card_Number_Masked__c,Pci_Dss_Token__c,CreatedDate,Contact.SerNo__c,Contact.Institution_Id__c,(Select id,Institutionid__c,BillingAmount__c from DCMS_Transactios__r),(Select id,TotalFraudAmt__c,TotalAtRisk__c,NumberOfTransactions__c from DCMS_FraudCases__r) from Case where Id in:newCaseList];
            List<DCMS_FraudCase__c> fraudCaseList = new List<DCMS_FraudCase__c>(); 
            system.debug('--csList---'+csList.size());
            for(Case dtcase : csList){
                if(dtcase.Dispute_Type__c != Null){
                    DCMS_FraudCase__c fraudCase = new DCMS_FraudCase__c();
                    fraudCase.Case_Number__c = dtcase.Id;
                    fraudCase.TruncatedCardNumber__c = dtcase.Card_Number_Masked__c;
                    if(dtcase.Card_SerNo__c != null){
                        fraudCase.Card_Serno__c = Decimal.valueOf(dtcase.Card_SerNo__c);
                    }
                    system.debug('--Account_Serno__c-----'+dtcase.Card__r.Financial_Account__r.Account_Serno__c);
                    if(dtcase.Card__r.Financial_Account__r.Account_Serno__c != null || dtcase.Card__r.Financial_Account__r.Account_Serno__c != ''){
                        fraudCase.AccountSerno__c = Decimal.valueOf(dtcase.Card__r.Financial_Account__r.Account_Serno__c);
                    }
                    fraudCase.Institutionid__c = dtcase.Institutionid__c; //dtcase.Contact.Institution_Id__c;
                    fraudCase.CaseOpenDate__c = Date.valueOf(dtcase.CreatedDate);
                    fraudCase.Description__c = dtcase.Description;
                    fraudCase.DisputeType__c = dtcase.Dispute_Type__c;
                    fraudCase.AssignedUser__c =dtcase.createdby.Name; //'Salesforce Support';
                    fraudCase.DecisionId__c = decisionStatusMap.get(dtcase.Decision_Status__c);
                    fraudCase.CaseStatusId__c = caseStatusMap.get(dtcase.Status);
                    fraudCase.FraudRingId__c = fraudRingMap.get(dtcase.Fraud_Ring__c);
                    fraudCase.FraudWO__c = 0;
                    fraudCase.TotalCb__c = 0;
                    fraudCase.NonFraudWO__c = 0;
                    fraudCase.TotalAtRisk__c = dtcase.Gross_Amount__c; //dtcase.DCMS_Transactios__r[0].BillingAmount__c;
                    fraudCase.TotalFraudAmt__c = dtcase.Gross_Amount__c;
                    if(dtcase.ContactId != null){
                        fraudCase.Peopleserno__c = Decimal.valueOf(dtcase.Contact.SerNo__c);
                    }
                    fraudCase.Pcidsstoken__c = dtcase.Pci_Dss_Token__c;
                    fraudCase.NumberOfTransactions__c= dtcase.DCMS_Transactios__r.size();
                    
                    fraudCaseList.add(fraudCase);
                }
            }
            
            if(fraudCaseList.size() > 0){
                insert fraudCaseList;
            }
            system.debug('---fraudCaseList--'+fraudCaseList);
        } catch(Exception exp){
            StatusLogHelper.logSalesforceError('disputeTypeCaseHandler', 'E004', 'Error in createFraudCases method ' , exp, false);
        }
    }
}