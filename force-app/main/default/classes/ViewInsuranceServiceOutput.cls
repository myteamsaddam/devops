/*
----------------------------------------------------------------------------------------
Author    Date         Description
Shameel   16/12/2016   Output class to implement SOA service using Integration framework.
----------------------------------------------------------------------------------------
*/
public class ViewInsuranceServiceOutput{

    public SubscribedInsurance  SubscribedInsuranceList;
    public SubscribeToInsurance SubscribeToInsuranceList;
     
    public class SubscribeToInsurance {
    
        public ViewInsurancesResponseBody[] ViewInsurancesResponseBodyList;
    }
    
    public class SubscribedInsurance {
    
        public ViewInsurancesResponseBody[] ViewInsurancesResponseBodyList;
    }
    

    public class ViewInsurancesResponseBody {
    
        public String InsuranceCode;
        public String Description;
        public String LogAction;
    
    }
    //To Cover Test Class 100%
    public void test()
    {
        
    }
}