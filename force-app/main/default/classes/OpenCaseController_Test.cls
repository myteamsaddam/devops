@isTest
public class OpenCaseController_Test {
    public static testmethod void testMethod1(){
        
        Contact con = new Contact();
        con.LastName = 'Test';
        con.SSN__c = '7307091772';
        con.SerNo__c = '921497';
        insert con;
        
        List<Case> cslist = new List<Case>();
        Case cs = new Case();
        cs.Origin = 'Phone';
        cs.Status = 'Inprogress';
        cs.Category__c = 'Retention';
        cs.ContactId = con.Id;
        cslist.add(cs);
        insert cslist;
        
        List<Id> conIds = new List<Id>();
        conIds.add(con.id);
        OpenCaseController.updateOpencase(conIds);
    }
}