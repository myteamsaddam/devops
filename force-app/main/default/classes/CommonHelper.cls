public without sharing class CommonHelper{

public static string displayMessage(string code)
{
  
  return ErrorCodes__c.getInstance(code)!=null?ErrorCodes__c.getInstance(code).UIDisplayMessage__c:ErrorCodes__c.getInstance('defaultError').UIDisplayMessage__c;
}

}