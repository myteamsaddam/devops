@IsTest(seeallData=true)
public class UpdateCaseHandlingTime_Test{
     
    static testmethod void testMethod1(){
        test.startTest();  
        Account accountObj=new Account(
        Name='Test Account', 
        Institution_Id__c=2, 
        Customer_Serno__c='1123123'
        );
        insert accountObj;

        Blob hash = Crypto.generateDigest('MD5', Blob.valueOf('0000000061'));
        string encryptSSN=EncodingUtil.convertToHex(hash);
        Contact contactObj =new Contact(
        LastName='Test Contact', 
        FirstName='First Test Contact',
        SerNo__c='2123123', 
        Institution_Id__c=2, 
        SSN__c='0000000061',
        Encrypted_SSN__c=encryptSSN,
        AccountId=accountObj.Id,
        Phone='123123123', 
        Fax='98789879', 
        MobilePhone='98789879', 
        HomePhone='123123123', 
        Email='testemail@test.com'
        );
        insert contactObj;

        Product_Custom__c prodobj=new Product_Custom__c(
        Name='Test Product',
        Pserno__c='56789'
        );
        insert prodobj;

        Financial_Account__c finAccObj=new Financial_Account__c(
        Account_Number__c='12w23eeeeedd', 
        Customer__c=accountObj.Id, 
        Account_Serno__c='Test31231', 
        Institution_Id__c=2,
        Customer_Serno__c=contactObj.SerNo__c, 
        Product__c=prodobj.Id,
        Product_Serno__c=prodobj.Pserno__c
        );
        insert finAccObj;

        Card__c cardObj=new Card__c(
        People__c=contactObj.Id,
        PrimaryCardFlag__c=true,
        Financial_Account__c=finAccObj.Id, 
        Card_Serno__c='524858519', 
        Card_Number_Truncated__c='7136785481583561', 
        Financial_Account_Serno__c=finAccObj.Id, 
        People_Serno__c=contactObj.Id,
        Prod_Serno__c=prodobj.Pserno__c, 
        Product__c=prodobj.Id,
        Institution_Id__c=2
        );
        insert cardObj;

        Case caseObj=new Case(
        ContactId=contactObj.Id, 
        AccountId=accountObj.Id, 
        Card__c=cardObj.Id,
        Financial_Account__c=finAccObj.Id, 
        Category__c='Account closure', 
        Origin='Web',
        status='Closed',
        Description='Kundens personnummer (10 siffror utan streck) 0000000061'
        );
        insert caseObj;
        
        
        
        
        test.stopTest();
    }
    
    static testmethod void testMethod2(){
        test.startTest();  
        Account accountObj=new Account(
        Name='Test Account', 
        Institution_Id__c=2, 
        Customer_Serno__c='1123123'
        );
        insert accountObj;

        Blob hash = Crypto.generateDigest('MD5', Blob.valueOf('0000000061'));
        string encryptSSN=EncodingUtil.convertToHex(hash);
        Contact contactObj =new Contact(
        LastName='Test Contact', 
        FirstName='First Test Contact',
        SerNo__c='2123123', 
        Institution_Id__c=2, 
        SSN__c='0000000061',
        Encrypted_SSN__c=encryptSSN,
        AccountId=accountObj.Id,
        Phone='123123123', 
        Fax='98789879', 
        MobilePhone='98789879', 
        HomePhone='123123123', 
        Email='testemail@test.com'
        );
        insert contactObj;

        Product_Custom__c prodobj=new Product_Custom__c(
        Name='Test Product',
        Pserno__c='56789'
        );
        insert prodobj;

        Financial_Account__c finAccObj=new Financial_Account__c(
        Account_Number__c='12w23eeeeedd', 
        Customer__c=accountObj.Id, 
        Account_Serno__c='Test3123126', 
        Institution_Id__c=2,
        Customer_Serno__c=contactObj.SerNo__c, 
        Product__c=prodobj.Id,
        Product_Serno__c=prodobj.Pserno__c
        );
        insert finAccObj;

        Card__c cardObj=new Card__c(
        People__c=contactObj.Id,
        PrimaryCardFlag__c=true,
        Financial_Account__c=finAccObj.Id, 
        Card_Serno__c='524858519', 
        Card_Number_Truncated__c='7136785481583561', 
        Financial_Account_Serno__c=finAccObj.Id, 
        People_Serno__c=contactObj.Id,
        Prod_Serno__c=prodobj.Pserno__c, 
        Product__c=prodobj.Id,
        Institution_Id__c=2
        );
        insert cardObj;

        Case caseObj1=new Case(
        ContactId=contactObj.Id, 
        AccountId=accountObj.Id, 
        Card__c=cardObj.Id,
        Financial_Account__c=finAccObj.Id, 
        Category__c='Account closure', 
        Origin='Web',
        status='In Progress',
        Description='Kundens personnummer (10 siffror utan streck) 0000000061'
        );
        insert caseObj1;
        
        
        Case caseObj=new Case(
        ContactId=contactObj.Id, 
        AccountId=accountObj.Id, 
        Card__c=cardObj.Id,
        Financial_Account__c=finAccObj.Id, 
        Category__c='Account closure', 
        Origin='Web',
        status='In Progress',
        ParentId=caseObj1.id,
        Description='Kundens personnummer (10 siffror utan streck) 0000000061'
        );
        insert caseObj;
        
        caseObj.status = 'Closed';
        
        update caseObj;
        
        
        test.stopTest();
    }
    
        Static testmethod void testMethod3(){
        test.startTest();  
        Account accountObj=new Account(
        Name='Test Account', 
        Institution_Id__c=2, 
        Customer_Serno__c='1123123'
        );
        insert accountObj;

        Blob hash = Crypto.generateDigest('MD5', Blob.valueOf('0000000061'));
        string encryptSSN=EncodingUtil.convertToHex(hash);
        Contact contactObj =new Contact(
        LastName='Test Contact', 
        FirstName='First Test Contact',
        SerNo__c='2123123', 
        Institution_Id__c=2, 
        SSN__c='0000000061',
        Encrypted_SSN__c=encryptSSN,
        AccountId=accountObj.Id,
        Phone='123123123', 
        Fax='98789879', 
        MobilePhone='98789879', 
        HomePhone='123123123', 
        Email='testemail@test.com'
        );
        insert contactObj;

        Product_Custom__c prodobj=new Product_Custom__c(
        Name='Test Product',
        Pserno__c='56789'
        );
        insert prodobj;

        Financial_Account__c finAccObj=new Financial_Account__c(
        Account_Number__c='12w23eeeeedd', 
        Customer__c=accountObj.Id, 
        Account_Serno__c='Test3163123', 
        Institution_Id__c=2,
        Customer_Serno__c=contactObj.SerNo__c, 
        Product__c=prodobj.Id,
        Product_Serno__c=prodobj.Pserno__c
        );
        insert finAccObj;

        Card__c cardObj=new Card__c(
        People__c=contactObj.Id,
        PrimaryCardFlag__c=true,
        Financial_Account__c=finAccObj.Id, 
        Card_Serno__c='524858519', 
        Card_Number_Truncated__c='7136785481583561', 
        Financial_Account_Serno__c=finAccObj.Id, 
        People_Serno__c=contactObj.Id,
        Prod_Serno__c=prodobj.Pserno__c, 
        Product__c=prodobj.Id,
        Institution_Id__c=2
        );
        insert cardObj;

        
        
        
        Case caseObj=new Case(
        ContactId=contactObj.Id, 
        AccountId=accountObj.Id, 
        Card__c=cardObj.Id,
        Financial_Account__c=finAccObj.Id, 
        Category__c='Account closure', 
        Origin='Web',
        status='Closed',
        Description='Kundens personnummer (10 siffror utan streck) 0000000061'
        //ParentId=caseObj1.id
        );
        insert caseObj;
        
       
        
        
        test.stopTest();
    }
    
    Static testmethod void testMethod4(){
        test.startTest();  
        Account accountObj=new Account(
        Name='Test Account', 
        Institution_Id__c=2, 
        Customer_Serno__c='1123123'
        );
        insert accountObj;

        Blob hash = Crypto.generateDigest('MD5', Blob.valueOf('0000000061'));
        string encryptSSN=EncodingUtil.convertToHex(hash);
        Contact contactObj =new Contact(
        LastName='Test Contact', 
        FirstName='First Test Contact',
        SerNo__c='2123123', 
        Institution_Id__c=2, 
        SSN__c='0000000061',
        Encrypted_SSN__c=encryptSSN,
        AccountId=accountObj.Id,
        Phone='123123123', 
        Fax='98789879', 
        MobilePhone='98789879', 
        HomePhone='123123123', 
        Email='testemail@test.com'
        );
        insert contactObj;

        Product_Custom__c prodobj=new Product_Custom__c(
        Name='Test Product',
        Pserno__c='56789'
        );
        insert prodobj;

        Financial_Account__c finAccObj=new Financial_Account__c(
        Account_Number__c='12w23eeeeedd', 
        Customer__c=accountObj.Id, 
        Account_Serno__c='Test3123023', 
        Institution_Id__c=2,
        Customer_Serno__c=contactObj.SerNo__c, 
        Product__c=prodobj.Id,
        Product_Serno__c=prodobj.Pserno__c
        );
        insert finAccObj;

        Card__c cardObj=new Card__c(
        People__c=contactObj.Id,
        PrimaryCardFlag__c=true,
        Financial_Account__c=finAccObj.Id, 
        Card_Serno__c='524858519', 
        Card_Number_Truncated__c='7136785481583561', 
        Financial_Account_Serno__c=finAccObj.Id, 
        People_Serno__c=contactObj.Id,
        Prod_Serno__c=prodobj.Pserno__c, 
        Product__c=prodobj.Id,
        Institution_Id__c=2
        );
        insert cardObj;

        
        
        
        Case caseObj=new Case(
        ContactId=null, 
        AccountId=accountObj.Id, 
        Card__c=cardObj.Id,
        Financial_Account__c=finAccObj.Id, 
        Category__c='Account closure', 
        Origin='Web',
        status='Closed'
        //ParentId=caseObj1.id
        );
        insert caseObj;
        
        caseObj.status='Closed';
        update caseObj;
       
        
        
        test.stopTest();
    }
    
    Static testmethod void testMethod5(){
        test.startTest();  
        Account accountObj=new Account(
        Name='Test Account', 
        Institution_Id__c=2, 
        Customer_Serno__c='1123123'
        );
        insert accountObj;

        Blob hash = Crypto.generateDigest('MD5', Blob.valueOf('0000000061'));
        string encryptSSN=EncodingUtil.convertToHex(hash);
        Contact contactObj =new Contact(
        LastName='Test Contact', 
        FirstName='First Test Contact',
        SerNo__c='2123123', 
        Institution_Id__c=2, 
        SSN__c='0000000061',
        Encrypted_SSN__c=encryptSSN,
        AccountId=accountObj.Id,
        Phone='123123123', 
        Fax='98789879', 
        MobilePhone='98789879', 
        HomePhone='123123123', 
        Email='testemail@test.com'
        );
        insert contactObj;

        Product_Custom__c prodobj=new Product_Custom__c(
        Name='Test Product',
        Pserno__c='56789'
        );
        insert prodobj;

        Financial_Account__c finAccObj=new Financial_Account__c(
        Account_Number__c='12w23eeeeedd', 
        Customer__c=accountObj.Id, 
        Account_Serno__c='Test3923123', 
        Institution_Id__c=2,
        Customer_Serno__c=contactObj.SerNo__c, 
        Product__c=prodobj.Id,
        Product_Serno__c=prodobj.Pserno__c
        );
        insert finAccObj;

        Card__c cardObj=new Card__c(
        People__c=contactObj.Id,
        PrimaryCardFlag__c=true,
        Financial_Account__c=finAccObj.Id, 
        Card_Serno__c='524858519', 
        Card_Number_Truncated__c='7136785481583561', 
        Financial_Account_Serno__c=finAccObj.Id, 
        People_Serno__c=contactObj.Id,
        Prod_Serno__c=prodobj.Pserno__c, 
        Product__c=prodobj.Id,
        Institution_Id__c=2
        );
        insert cardObj;

        
        
        
        Case caseObj=new Case(
        ContactId=contactObj.id, 
        AccountId=accountObj.Id, 
        Card__c=cardObj.Id,
        Financial_Account__c=null,
        Category__c='Account closure', 
        Origin='Web',
        status='Closed',
        Description='Kundens personnummer (10 siffror utan streck)000000061'
        //ParentId=caseObj1.id
        );
        insert caseObj;
        
       
        
        
        test.stopTest();
    }
    
}