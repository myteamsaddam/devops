public class UpdateConsentServiceCallout extends SoapIO{
   
    SOAConsentRequest.UpdateConsentDataRequestBody updateConsentDataRequestBodyInstance;
    UpdateConsentServiceInput input;
    
    public  void convertInputToRequest(){
    
        input = (UpdateConsentServiceInput)serviceInput;
        updateConsentDataRequestBodyInstance = new SOAConsentRequest.UpdateConsentDataRequestBody();
        updateConsentDataRequestBodyInstance.Institution_ID=input.InstitutionId;
        updateConsentDataRequestBodyInstance.ConsentEntityID=input.ConsentEntityId;
        updateConsentDataRequestBodyInstance.ConsentEntityID_Type=input.ConsentEntityIdType;
       // updateConsentDataRequestBodyInstance.Consent_Code=input.ConsentCode;
       // updateConsentDataRequestBodyInstance.Value=input.Value;
        updateConsentDataRequestBodyInstance.Consent_Code = new List<SOAConsentRequest.Consent_Code_type>();
        if(input.ConsentCodeValue!=null)
        {
            for(integer i=0;i<input.ConsentCodeValue.size();i++)
            {
                SOAConsentRequest.Consent_Code_type consentType = new SOAConsentRequest.Consent_Code_type();
                consentType.code=input.ConsentCodeValue[i].code;
                consentType.value=input.ConsentCodeValue[i].value;
                updateConsentDataRequestBodyInstance.Consent_Code.add(consentType);
            }
        }
        updateConsentDataRequestBodyInstance.DateTimeStamp=input.DateTimeStamp;
        updateConsentDataRequestBodyInstance.UpdatedBy=input.UpdatedBy;
        updateConsentDataRequestBodyInstance.Source=input.Source;

    }
    
    public  override object invokeWebserviceCallout(SoapRequest soapRequest){
        
        SOAConsentTypelib.headerType HeadInstance=new SOAConsentTypelib.headerType();
        HeadInstance.MsgId='SFDC consuming Update Consent SOA service. Time Stamp=>'+system.now();
        HeadInstance.CorrelationId=Common_Settings__c.getValues('CorrelationId').Common_Value__c;
        HeadInstance.RequestorId=Common_Settings__c.getValues('RequestorId').Common_Value__c;
        HeadInstance.SystemId=Common_Settings__c.getValues('SystemId').Common_Value__c;
        HeadInstance.InstitutionId=string.valueof(input.InstitutionId);
        SOAConsentInvoke.x_xsoap_CSSServiceESB_CSSServicePT   invokeInstance= new SOAConsentInvoke.x_xsoap_CSSServiceESB_CSSServicePT ();
        invokeInstance.timeout_x=30000;
         // Added 12/13/2016
        ServiceSettings__c serviceObj = ServiceSettings__c.getValues('UpdateConsent');
        if(serviceObj!=null && serviceObj.EndPoint__c!='' && serviceObj.EndPoint__c!=null)
        invokeInstance.endpoint_x = serviceObj.EndPoint__c;
        // Added 12/13/2016
        
        //String username = 'evry_access';
        //String password = '9oKuwQioQ4';
        
        String username = serviceObj.Username__c; //'css_soa';
        String password = serviceObj.Password__c;//'P1%gruzA';
        
        Blob headerValue = Blob.valueOf(username + ':' + password);
        String authorizationHeader = 'BASIC ' +EncodingUtil.base64Encode(headerValue);
        
        invokeInstance.inputHttpHeaders_x = new Map<String, String>();
        invokeInstance.inputHttpHeaders_x.put('Authorization', authorizationHeader);
        
        SOAConsentSecurityHeader.UsernameToken creds=new SOAConsentSecurityHeader.UsernameToken();
        //creds.Username='evry_access';
        //creds.Password='9oKuwQioQ4';
        
        creds.Username=serviceObj.Username__c; //'css_soa';
        creds.Password=serviceObj.Password__c;//'P1%gruzA';
        
        SOAConsentSecurityHeader.Security_element security_ele=new SOAConsentSecurityHeader.Security_element();
        security_ele.UsernameToken=creds;
        invokeInstance.SecurityPart=security_ele;
        SOAConsentResponse.UpdateConsentDataResponse_element   updateConsentDataResponse_element  ;
        updateConsentDataResponse_element =  invokeInstance.UpdateConsentData(HeadInstance,updateConsentDataRequestBodyInstance);
        system.debug('updateConsentDataResponse_element -->'+updateConsentDataResponse_element);
        return updateConsentDataResponse_element;
     }  
        
     public override object convertResponseToOutput(object response, map<string, string> responseHeader, ServiceStatus serviceStatus){
       if(response==null)
           return null;
       
       SOAConsentResponse.UpdateConsentDataResponse_element updateConsentDataResponse_elementInstance = (SOAConsentResponse.UpdateConsentDataResponse_element)response;
       SOAConsentResponse.UpdateConsentDataResponseRecordType updateConsentDataResponseRecordInstance =updateConsentDataResponse_elementInstance.UpdateConsentDataResponseRecord;
      
       system.debug('***updateConsentDataResponseRecordInstance***'+updateConsentDataResponseRecordInstance);
       
       UpdateConsentServiceOutput updateConsentServiceOutput = new UpdateConsentServiceOutput();
       updateConsentServiceOutput.Code=updateConsentDataResponseRecordInstance.Code;
       updateConsentServiceOutput.Description=updateConsentDataResponseRecordInstance.Description;
      
      return updateConsentServiceOutput;
    }        
}