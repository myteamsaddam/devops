/**********************************************************************
Name: EC_ContactTriggerHandler
=======================================================================
Purpose: This trigger handler class is created on Contact object to handle all the contact related logics. User Story SFD-1431

======================================================================= 
History
-----------------------------------------------------------------------         
Version     Author                   Date              Detail
1.0         Saddaam Hussain         21-May-2020       Initial Version

**********************************************************************/
public with sharing class EC_ContactTriggerHandler {
    /**********************************************************************
*   Author: Saddam Hussain
*   Date:     21-May-2020
*   User Story : SFD-1431
*   Param: None
*   Return: None    
*   Description: In this method we are trying to update Mobile number in SaveDesk record when related parent
contact Mobile number updated.
* @Revision(s): [Date] - [Change Reference] - [Changed By] - [Description]   

**********************************************************************/
    
    public static void updateSavedeskPhone(List<Contact> contactList, Map<Id, Contact> contactOldMap) {
        Set<Id> contactIdSet = new Set<Id>();
        Map<Id, Contact> contactMap = new Map<Id, Contact>();
        for(Contact conObj : contactList) {
            Contact oldConObj = contactOldMap.get(conObj.Id);
            if(conObj.MobilePhone != oldConObj.MobilePhone) {
                contactIdSet.add(conObj.Id);
                contactMap.put(conObj.id,conObj);
            }
        }
        
        if(contactIdSet.size() > 0) {
            List<SaveDeskQueue__c> updateSaveDeskList = new List<SaveDeskQueue__c>();
            for(SaveDeskQueue__c sdObj : [SELECT Id,Phone_No__c, Customer_Name__c 
                                          FROM SaveDeskQueue__c
                                          WHERE Customer_Name__c IN :contactIdSet]
               ) {
                   
                   Contact conObje = contactMap.get(sdObj.Customer_Name__c);
                   if(conObje.MobilePhone != null) {
                       sdObj.Phone_No__c = conObje.MobilePhone;
                       updateSaveDeskList.add(sdObj);
                   }
                   
               }
            if(updateSaveDeskList.size() > 0) {
                EC_StaticConstant.SAVEDESK_ISRECURSIVE = true;
                update updateSaveDeskList;
            }
        }
    }
    
    /**********************************************************************
*   Author: Saddam Hussain
*   Date:     21-May-2020
*   User Story : SFD-1431
*   Param: None
*   Return: None    
*   Description: In this method we are trying to Encrypt the SSN and trim mobile number if start with '0' before contact record insert.
* @Revision(s): [Date] - [Change Reference] - [Changed By] - [Description]   

**********************************************************************/
    public static void updateEncryptSsn(List<Contact> contactList, Map<Id, Contact> contactOldMap, Boolean isInsert) {
        if(ContactTriggerControl.ContactTriggerFlag) {
            for(Contact conObj : contactList) {
                //below logic to update the encrypt ssn field
                if(conObj.SSN__c != null
                   && (isInsert == true
                       || conObj.SSN__c != contactOldMap.get(conObj.Id).SSN__c
                      )
                  ) {
                      Blob hash = Crypto.generateDigest('MD5', Blob.valueOf(conObj.SSN__c)); 
                      conObj.Encrypted_SSN__c=EncodingUtil.convertToHex(hash);
                      system.debug('***conObj.Encrypted_SSN__c***'+conObj.Encrypted_SSN__c);
                  }
                
                //below logic to update the Email field
                system.debug('---isInsert--'+isInsert);
                if(conObj.customer_email__C != null
                   && String.isNotBlank(conObj.customer_email__C)
                   && (isInsert == true
                       || conObj.customer_email__C != contactOldMap.get(conObj.Id).customer_email__C
                      )
                  ) {
                      conObj.Email  = conObj.customer_email__C;
                  }
                //below logic to update the Mobile phone field
                if(conObj.MobilePhone != null
                   && String.isNotBlank(conObj.MobilePhone)
                  ) {
                      conObj.MobilePhone = (conObj.MobilePhone.trim().startsWith('0') ? 
                                            conObj.MobilePhone.trim().replaceFirst('^0+', '') : conObj.MobilePhone);
                  }
            }
        }
    }
    
    /**********************************************************************
*   Author: Saddam Hussain
*   Date:     21-May-2020
*   User Story : SFD-1431
*   Param: None
*   Return: None    
*   Description: In this method we are trying to VIP__c field on Financial Account object.
* @Revision(s): [Date] - [Change Reference] - [Changed By] - [Description]   

**********************************************************************/
    public static void updateVIPOnFinancialAccount(List<Contact> contactList, Map<Id, Contact> contactOldMap) {
        Set<Id> contactIdSet = new Set<Id>();
        Set<Id> financialIdSet = new Set<Id>();
        for(Contact conObj : contactList) {
            Contact conOldObj = contactOldMap.get(conObj.Id);
            if(conObj.VIP__c != conOldObj.VIP__c) {
                contactIdSet.add(conObj.Id);
            }
        }
        Map<Id, Set<Id>> contactFinancialMap = new Map<Id, Set<Id>>();
        if(contactIdSet.size() > 0) {
            for(Card__c cardObj : [SELECT id, people__c,Financial_Account__c 
                                   FROM Card__c 
                                   WHERE people__c IN: contactIdSet 
                                   AND PrimaryCardFlag__c=true]
               ) {
                   if(!contactFinancialMap.containsKey(cardObj.People__c)) {
                       financialIdSet.add(cardObj.Financial_Account__c);
                       contactFinancialMap.put(cardObj.People__c,financialIdSet);
                   } else {
                       financialIdSet = contactFinancialMap.get(cardObj.People__c);
                       financialIdSet.add(cardObj.Financial_Account__c);
                       contactFinancialMap.put(cardObj.People__c,financialIdSet);
                   }
                   financialIdSet.add(cardObj.Financial_Account__c);
               }
        }
        
        Map<Id,Financial_Account__c> financialMap= new Map<Id,Financial_Account__c>();
        List<Financial_Account__c> financialList = new List<Financial_Account__c>();
        if(financialIdSet.size() > 0) {
            for(Financial_Account__c finObj : [SELECT Id, VIP__c 
                                               FROM Financial_Account__c 
                                               WHERE Id IN : financialIdSet]
               ) {
                   financialMap.put(finObj.Id,finObj); 
               }
        }
        if (contactFinancialMap.size() > 0
            && financialMap.size() > 0
           ) {
               for(Contact conObj : contactList) {
                   Contact oldContactObj = contactOldMap.get(conObj.Id);
                   if(conObj.VIP__c != oldContactObj.VIP__c) {
                       if(contactFinancialMap.containsKey(conObj.Id)) {
                           for(id finIds : contactFinancialMap.get(conObj.id)) {
                               if (financialMap.containsKey(finIds)) {
                                   Financial_Account__c finAccObj=financialMap.get(finIds);
                                   finAccObj.VIP__c = conObj.VIP__c;
                                   financialList.add(finAccObj);   
                               }
                           }
                       }
                   }
               }
           }
        if(financialList.size() > 0) {
            system.debug('--financialList-'+financialList);
            update financialList;
        }
    }
}