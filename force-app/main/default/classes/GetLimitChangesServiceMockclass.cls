@isTest
global class GetLimitChangesServiceMockclass implements WebServiceMock {
   global void doInvoke(
           Object stub,
           Object request,
           Map<String, Object> response,
           String endpoint,
           String soapAction,
           String requestName,
           String responseNS,
           String responseName,
           String responseType) {
       SOAPCSSServiceResponse.GetLimitChangesResponse_element respEle =new SOAPCSSServiceResponse.GetLimitChangesResponse_element();
       respEle.GetLimitChangesResponseRecord=new SOAPCSSServiceResponse.getLimitChangesResponseRecordType();
       SOAPCSSServiceResponse.getLimitChangesResponseRecordType  glcr = new SOAPCSSServiceResponse.getLimitChangesResponseRecordType();
       date mydate = date.parse('2016-09-17');
       glcr.AccountNo='AccountNo';
       glcr.From_x=mydate;
       
       SOAPCSSServiceResponse.limitChangeType[] limitChangeList =new List<SOAPCSSServiceResponse.limitChangeType>();
       for(integer i=0;i<6;i++)
       {
       SOAPCSSServiceResponse.limitChangeType limitChange = new SOAPCSSServiceResponse.limitChangeType();
       limitChange.Date_x = mydate; 
       limitChange.OldLimit = 'OldLimit'+i;
       limitChange.NewLimit = 'NewLimit'+i;
       limitChangeList.add(limitChange);
       }
       glcr.LimitChange=limitChangeList;
       respEle.GetLimitChangesResponseRecord=glcr;
       response.put('response_x', respEle);
    }
}