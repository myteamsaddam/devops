public without sharing class RESTService extends BASEService {
  
    HttpRequest request = new HttpRequest();
    HttpResponse response = new HttpResponse();
    Http http = new Http();
    
    private void prepareServiceRequest(){
      if(serviceSettings.Timeout__c != null) request.setTimeout(integer.ValueOf(serviceSettings.Timeout__c));
      if(serviceSettings.Operation__c != null) request.setMethod(serviceSettings.Operation__c);
      if(serviceSettings.Compressed__c) request.setCompressed(serviceSettings.Compressed__c);
      if(serviceSettings.CertificateName__c != null) request.setClientCertificateName(serviceSettings.CertificateName__c);  
          
      if(certificate != null) request.setClientCertificate(certificate, serviceSettings.CertificatePassword__c);
        
        if(requestHeader != null && !requestHeader.isEmpty()){
          Set<string> keys = requestHeader.keySet();
          for(string key : keys){
            request.setHeader(key, requestHeader.get(key));
          }          
        }
        
        string jsonString;
        if(requestBody != null){
            if(requestBody instanceof string){
                jsonString = (string)requestBody;}
            else {
                jsonString = JSON.serializePretty(requestBody);}            
        }        
        
        if(!serviceSettings.EndPointParameters__c){
            request.setEndpoint(serviceEndpoint);
            request.setBody(jsonString);
        }
        else
            request.setEndpoint(serviceEndpoint + jsonString);
        
        requestBody = jsonString;        
        System.debug('Request Body : ' + requestBody);
    }
   
    private void execute(){
        try{
          system.debug('Request : ' + request);
          system.debug('Request get Body: ' + request.getEndpoint());
            response = http.send(request); 
            system.debug('Response : ' + response);
        }
        catch(Exception ex) {
            system.debug('Interface Error : '+ex.getMessage());
            throw ex;
        }
    } 
    
    public void processServiceResponse(){
      responseBody = response.getBody();
        System.Debug('Response Body : ' + responseBody);
        
        string jsonResponse = (string)responseBody;
        system.debug('@@jsonResponse' + jsonResponse);
        //TODO
        //Set Response Headers
        
        if(response.getStatusCode() != 200 && response.getStatusCode() != 201){
            serviceStatus.status = 'I';
            serviceStatus.errorCode = string.valueOf(response.getStatusCode());
            serviceStatus.errorDescription = response.getStatus();            
            /*if(jsonResponse != null){
                XmlStreamReader xsr = new XmlStreamReader(jsonResponse);
                //TODO
                //Parse the XML for the error code and description
            }*/                   
            try{
                system.debug('Final JSON to deserialize :' + jsonResponse);
                responseBody = JSON.deserialize(jsonResponse, Type.forName(serviceSettings.OutputClass__c));
            }
            catch(Exception e){
                //DO NOT DO ANYTHING
            }
        }
        else if(serviceSettings.OutputClass__c != null && responseBody != null && responseBody instanceof string && serviceStatus.status == 'S'){           
            //jsonResponse = jsonResponse.replace('"system":', '"system_x":');
            //jsonResponse = jsonResponse.replace('"@type":', '"type_x":');
            //jsonResponse = jsonResponse.replace('@xmlns:', '');
            system.debug('Final JSON to deserialize :' + jsonResponse);
            system.debug('serviceSettings.OutputClass__c :' + serviceSettings.OutputClass__c);
            system.debug('Type.forName(serviceSettings.OutputClass__c) :' + Type.forName(serviceSettings.OutputClass__c));
            responseBody = JSON.deserialize(jsonResponse, Type.forName(serviceSettings.OutputClass__c));  
            system.debug('Final JSON deserialized :' + responseBody);
        }
    }

}