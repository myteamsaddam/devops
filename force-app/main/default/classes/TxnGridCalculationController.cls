Public class TxnGridCalculationController {
    
    Public void getFlagAmnts(Map<Id, DCMS_Transaction__c> txnlist, Map<Id, DCMS_Transaction__c> oldtxns){
        try{
            List<Id> CsId = new List<Id>();
            List<DCMS_Transaction__c> transactionsList = [Select id,BillingAmount__c,Chargeback__c,Representment__c,Arbitration__c,WriteOff__c,FraudWriteOff__c,CH_Liable__c,Recovery__c, CaseNumber__c from DCMS_Transaction__c where Id IN: txnlist.keyset()];
            system.debug('----txnlist---'+txnlist);
            for(DCMS_Transaction__c txn : transactionsList){
                CsId.add(txn.CaseNumber__c);
            }
            system.debug('-----case----'+CsId);
            
            if(!CsId.isEmpty()){
                List<DCMS_Transaction__c> txnsList = [Select id,BillingAmount__c,Chargeback__c,Representment__c,Arbitration__c,WriteOff__c,FraudWriteOff__c,CH_Liable__c,Recovery__c, CaseNumber__c from DCMS_Transaction__c where CaseNumber__c = :CsId AND (Chargeback__c = true OR Representment__c = true OR Arbitration__c = true OR WriteOff__c = true OR FraudWriteOff__c = true OR Recovery__c = true OR CH_Liable__c = true)];
                List<DCMS_FraudCase__c> fruadlist = [Select id, TotalCb__c,FraudWO__c,NonFraudWO__c,TotalFraudAmt__c,TotalRefund__c,TotalToCh__c,TotalAtRisk__c from DCMS_FraudCase__c where Case_Number__c = :CsId ORDER BY CreatedDate DESC];
                Case cse = [Select Id, Gross_Amount__c from Case where Id IN: CsId];
                
                system.debug('-----fruadlist----'+fruadlist.size());
                system.debug('-----txn-size----'+transactionsList.size());
                
                if(fruadlist[0] != Null){
                    for(DCMS_Transaction__c txn : transactionsList){
                        if((txn.Chargeback__c != oldtxns.get(txn.id).Chargeback__c) && txn.Chargeback__c == true){
                            system.debug('----Chargeback__c-true----');
                            fruadlist[0].TotalCb__c = fruadlist[0].TotalCb__c + txn.BillingAmount__c;
                            fruadlist[0].TotalAtRisk__c =  fruadlist[0].TotalFraudAmt__c - fruadlist[0].TotalRefund__c - fruadlist[0].TotalCb__c - fruadlist[0].FraudWO__c - fruadlist[0].NonFraudWO__c;
                        } 
                        if((txn.Chargeback__c != oldtxns.get(txn.id).Chargeback__c) && txn.Chargeback__c == false) {
                            system.debug('-----else-condition-----');
                            fruadlist[0].TotalCb__c = fruadlist[0].TotalCb__c - txn.BillingAmount__c;
                            fruadlist[0].TotalAtRisk__c =  fruadlist[0].TotalFraudAmt__c - fruadlist[0].TotalRefund__c - fruadlist[0].TotalCb__c - fruadlist[0].FraudWO__c - fruadlist[0].NonFraudWO__c;
                        }
                        
                        if((txn.Representment__c != oldtxns.get(txn.id).Representment__c) && txn.Representment__c == true){
                            system.debug('----Representment__c-true----');
                            fruadlist[0].TotalCb__c = fruadlist[0].TotalCb__c - txn.BillingAmount__c;
                            fruadlist[0].TotalAtRisk__c =  fruadlist[0].TotalFraudAmt__c - fruadlist[0].TotalRefund__c - fruadlist[0].TotalCb__c - fruadlist[0].FraudWO__c - fruadlist[0].NonFraudWO__c;
                        } 
                        if((txn.Representment__c != oldtxns.get(txn.id).Representment__c) && txn.Representment__c == false) {
                            system.debug('-----else-condition-rep-----');
                            fruadlist[0].TotalCb__c = fruadlist[0].TotalCb__c + txn.BillingAmount__c;
                            fruadlist[0].TotalAtRisk__c =  fruadlist[0].TotalFraudAmt__c - fruadlist[0].TotalRefund__c - fruadlist[0].TotalCb__c - fruadlist[0].FraudWO__c - fruadlist[0].NonFraudWO__c;
                        }
                        
                        if((txn.Arbitration__c != oldtxns.get(txn.id).Arbitration__c) && txn.Arbitration__c == true){
                            system.debug('----Arbitration__c-true----');
                            fruadlist[0].TotalCb__c = fruadlist[0].TotalCb__c + txn.BillingAmount__c;
                            fruadlist[0].TotalAtRisk__c =  fruadlist[0].TotalFraudAmt__c - fruadlist[0].TotalRefund__c - fruadlist[0].TotalCb__c - fruadlist[0].FraudWO__c - fruadlist[0].NonFraudWO__c;
                        } 
                        if((txn.Arbitration__c != oldtxns.get(txn.id).Arbitration__c) && txn.Arbitration__c == false) {
                            system.debug('-----else-condition-Arb----');
                            fruadlist[0].TotalCb__c = fruadlist[0].TotalCb__c - txn.BillingAmount__c;
                            fruadlist[0].TotalAtRisk__c =  fruadlist[0].TotalFraudAmt__c - fruadlist[0].TotalRefund__c - fruadlist[0].TotalCb__c - fruadlist[0].FraudWO__c - fruadlist[0].NonFraudWO__c;
                            
                        }
                        
                        if((txn.FraudWriteOff__c != oldtxns.get(txn.id).FraudWriteOff__c) && txn.FraudWriteOff__c == true){
                            system.debug('----FraudWriteOff-true----');
                            fruadlist[0].FraudWO__c = fruadlist[0].FraudWO__c + txn.BillingAmount__c;
                            fruadlist[0].TotalAtRisk__c =  fruadlist[0].TotalFraudAmt__c - fruadlist[0].TotalRefund__c - fruadlist[0].TotalCb__c - fruadlist[0].FraudWO__c - fruadlist[0].NonFraudWO__c;
                        }
                        
                        if((txn.FraudWriteOff__c != oldtxns.get(txn.id).FraudWriteOff__c) && txn.FraudWriteOff__c == false){
                            fruadlist[0].FraudWO__c = fruadlist[0].FraudWO__c - txn.BillingAmount__c;
                            fruadlist[0].TotalAtRisk__c =  fruadlist[0].TotalFraudAmt__c - fruadlist[0].TotalRefund__c - fruadlist[0].TotalCb__c - fruadlist[0].FraudWO__c - fruadlist[0].NonFraudWO__c;
                        }
                        
                        if((txn.WriteOff__c != oldtxns.get(txn.id).WriteOff__c) && txn.WriteOff__c == true){
                            system.debug('----FraudWriteOff-true----');
                            fruadlist[0].NonFraudWO__c = fruadlist[0].NonFraudWO__c + txn.BillingAmount__c;
                            fruadlist[0].TotalAtRisk__c =  fruadlist[0].TotalFraudAmt__c - fruadlist[0].TotalRefund__c - fruadlist[0].TotalCb__c - fruadlist[0].FraudWO__c - fruadlist[0].NonFraudWO__c;
                        }
                        
                        if((txn.WriteOff__c != oldtxns.get(txn.id).WriteOff__c) && txn.WriteOff__c == false){
                            system.debug('----FraudWriteOff-true----');
                            fruadlist[0].NonFraudWO__c = fruadlist[0].NonFraudWO__c - txn.BillingAmount__c;
                            fruadlist[0].TotalAtRisk__c =  fruadlist[0].TotalFraudAmt__c - fruadlist[0].TotalRefund__c - fruadlist[0].TotalCb__c - fruadlist[0].FraudWO__c - fruadlist[0].NonFraudWO__c;
                        }
                        
                        /*if((txn.CH_Liable__c != oldtxns.get(txn.id).CH_Liable__c) && txn.CH_Liable__c == true){
fruadlist[0].TotalToCh__c = fruadlist[0].TotalToCh__c + txn.BillingAmount__c;
fruadlist[0].TotalAtRisk__c =  fruadlist[0].TotalFraudAmt__c - fruadlist[0].TotalRefund__c - fruadlist[0].TotalCb__c - fruadlist[0].FraudWO__c - fruadlist[0].NonFraudWO__c - fruadlist[0].TotalToCh__c;
}

if((txn.CH_Liable__c != oldtxns.get(txn.id).CH_Liable__c) && txn.CH_Liable__c == false){
fruadlist[0].TotalToCh__c = fruadlist[0].TotalToCh__c - txn.BillingAmount__c;
fruadlist[0].TotalAtRisk__c =  fruadlist[0].TotalFraudAmt__c - fruadlist[0].TotalRefund__c - fruadlist[0].TotalCb__c - fruadlist[0].FraudWO__c - fruadlist[0].NonFraudWO__c - fruadlist[0].TotalToCh__c;
}*/
                        if((txn.Recovery__c != oldtxns.get(txn.id).Recovery__c) && txn.Recovery__c == true){
                            fruadlist[0].TotalFraudAmt__c = fruadlist[0].TotalFraudAmt__c - txn.BillingAmount__c;
                            fruadlist[0].TotalRefund__c = fruadlist[0].TotalRefund__c + txn.BillingAmount__c;
                            fruadlist[0].TotalAtRisk__c =  fruadlist[0].TotalFraudAmt__c - fruadlist[0].TotalRefund__c - fruadlist[0].TotalCb__c - fruadlist[0].FraudWO__c - fruadlist[0].NonFraudWO__c;
                            if(cse != null){
                                cse.Gross_Amount__c = cse.Gross_Amount__c - txn.BillingAmount__c;
                                update cse;
                            }
                        }
                        
                        if((txn.Recovery__c != oldtxns.get(txn.id).Recovery__c) && txn.Recovery__c == false){
                            fruadlist[0].TotalFraudAmt__c = fruadlist[0].TotalFraudAmt__c + txn.BillingAmount__c;
                            fruadlist[0].TotalRefund__c = fruadlist[0].TotalRefund__c - txn.BillingAmount__c;
                            fruadlist[0].TotalAtRisk__c =  fruadlist[0].TotalFraudAmt__c - fruadlist[0].TotalRefund__c - fruadlist[0].TotalCb__c - fruadlist[0].FraudWO__c - fruadlist[0].NonFraudWO__c;
                            if(cse != null){
                                cse.Gross_Amount__c = cse.Gross_Amount__c + txn.BillingAmount__c;
                                update cse;
                            }
                        }
                        
                        if(fruadlist[0].TotalAtRisk__c < 0){
                            fruadlist[0].TotalAtRisk__c = 0.00;
                        }
                    }
                    
                    update fruadlist[0];
                    
                    system.debug('---fruadlist[0]---'+fruadlist[0]);
                }
            }
        } catch(Exception exp){
            StatusLogHelper.logSalesforceError('txnGridCalculationController', 'E004', 'Error in getFlagAmnts ' , exp, false);
        }
    }
}