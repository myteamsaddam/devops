@isTest
public class TransactionDetailsContrl_Test {
    static testMethod void testmethod1(){
        Account accountObj=new Account(
            Name='Test Account', 
            Institution_Id__c=2, 
            Customer_Serno__c='1123123'
        );
        insert accountObj;
        
        Blob hash = Crypto.generateDigest('MD5', Blob.valueOf('0000000061'));
        string encryptSSN=EncodingUtil.convertToHex(hash);
        Contact contactObj =new Contact(
            LastName='Test Contact', 
            FirstName='First Test Contact',
            SerNo__c='2123123', 
            Institution_Id__c=2, 
            SSN__c='0000000061',
            Encrypted_SSN__c=encryptSSN,
            AccountId=accountObj.Id,
            Phone='123123123', 
            Fax='98789879', 
            MobilePhone='98789879', 
            HomePhone='123123123', 
            Email='testemail@test.com'
        );
        insert contactObj;
        
        Product_Custom__c prodobj=new Product_Custom__c(
            Name='Test Product',
            Pserno__c='56789'
        );
        insert prodobj;
        
        Financial_Account__c finAccObj=new Financial_Account__c(
            Account_Number__c='12w23eeeeedd', 
            Customer__c=accountObj.Id, 
            Account_Serno__c='3123123', 
            Institution_Id__c=2,
            Customer_Serno__c=contactObj.SerNo__c, 
            Product__c=prodobj.Id,
            Product_Serno__c=prodobj.Pserno__c
        );
        insert finAccObj;
        
        Card__c cardObj=new Card__c(
            People__c=contactObj.Id,
            PrimaryCardFlag__c=true,
            Financial_Account__c=finAccObj.Id, 
            Card_Serno__c='90247211', 
            Card_Number_Truncated__c='7136785481583561', 
            Financial_Account_Serno__c=finAccObj.Id, 
            People_Serno__c=contactObj.Id,
            Prod_Serno__c=prodobj.Pserno__c, 
            Product__c=prodobj.Id,
            Institution_Id__c=2
        );
        insert cardObj;
        
        // Test Case Record creation 
        
        Case cs = new Case();
        cs.ContactId=contactObj.Id;
        cs.Status= 'New';
        cs.Category__c='Dispute';
        cs.Origin='Web';
        cs.Department__c='Fraud and Chargeback';
        cs.Dispute_Type__c='0: lost';
        cs.Institutionid__c=1;
        cs.Gross_Amount__c=0;
        cs.Card__c=cardObj.id;
        insert cs;
        
        DCMS_Transaction__c tran = new DCMS_Transaction__c();
        tran.BillingAmount__c = 100;
        tran.DisputeType__c = '7: multiple imprint fraud';
        tran.Institutionid__c = 1;
        tran.MerchName__c = 'Loan';
        tran.MerchTypeMCC__c = 'Test';
        tran.MerchCountry__c = 'DK';
        tran.PCI_Dss_Token__c = 12345;
        tran.TransactionSerno__c = 123;
        tran.PosEntryMode__c='2';
        tran.Txn_CardSerno__c=90247211;
        tran.CaseNumber__c =cs.Id;
        tran.Chargeback__c = true;
        tran.Representment__c = true;
        tran.Arbitration__c = false;
        tran.WriteOff__c = false;
        tran.FraudWriteOff__c = false;
        tran.Recovery__c = false;
        tran.CH_Liable__c = false;
        //txnlist.add(tran);
        insert tran;
        
        List<string> txnlist = new List<string>();
        txnlist.add(tran.id);
        
        /*DCMS_FraudCase__c frdcase = new DCMS_FraudCase__c();
        //frdcase.Case_Number__c = cs.id;
        frdcase.FraudWO__c = 0.00;
        frdcase.TotalAtRisk__c = 0.00;
        frdcase.TotalCb__c = 0.00;
        frdcase.TotalFraudAmt__c = 0.00;
        frdcase.TotalRefund__c = 0.00;
        frdcase.TotalToCh__c = 0.00;
        frdcase.NonFraudWO__c = 0.00;
        
        insert frdcase;
        
        DCMS_FraudCase__c frdcase1 = new DCMS_FraudCase__c();
        frdcase1.Case_Number__c = cs.id;
        frdcase1.FraudWO__c = 1.00;
        frdcase1.TotalAtRisk__c = 2.00;
        frdcase1.TotalCb__c = 3.00;
        frdcase1.TotalFraudAmt__c = 4.00;
        frdcase1.TotalRefund__c = 5.00;
        frdcase1.TotalToCh__c = 6.00;
        frdcase1.NonFraudWO__c = 7.00;
        
        insert frdcase1;*/
        
        /*TransactionDetailsContrl txns = new TransactionDetailsContrl();
txns.getTrans(cs.id);*/
        
        /*UserRole r = new UserRole(DeveloperName = 'NO_Agent', Name = 'NO Agent');
        insert r;
        
        User u = new User(
            ProfileId = [SELECT Id FROM Profile WHERE Name = 'No Operations'].Id,
            LastName = 'last',
            Email = 'puser000@amamama.com',
            Username = 'puser000@amamama.com' + System.currentTimeMillis(),
            CompanyName = 'TEST',
            Title = 'title',
            Alias = 'alias',
            TimeZoneSidKey = 'America/Los_Angeles',
            EmailEncodingKey = 'UTF-8',
            LanguageLocaleKey = 'en_US',
            LocaleSidKey = 'en_US',
            UserRoleId = r.Id
        );
        insert u;*/
        String chekboxval;
        Id CsownerId =  tran.id;
        
        Decimal CHvalue = 200;
        
        TransactionDetailsContrl.getTrans(cs.id);
        TransactionDetailsContrl.updateTxns(tran.id, chekboxval);
        
        TransactionDetailsContrl.getGross(cs.id);
        TransactionDetailsContrl.getCaseOwnerId(cs.id);
        TransactionDetailsContrl.getTotalRisk(cs.id);

        
        chekboxval = 'cb';
        tran.Chargeback__c = false;
        update tran;
        TransactionDetailsContrl.updateTxns(tran.id, chekboxval);
        
        chekboxval = 'cb';
        tran.Chargeback__c = true;
        tran.Representment__c = false;
        update tran;
        TransactionDetailsContrl.updateTxns(tran.id, chekboxval);
        
        chekboxval = 'REP';
        tran.Chargeback__c = true;
        tran.Representment__c = false;
        update tran;
        TransactionDetailsContrl.updateTxns(tran.id, chekboxval);
        
        chekboxval = 'REP';
        tran.Representment__c = true;
        tran.Arbitration__c = false;
        update tran;
        TransactionDetailsContrl.updateTxns(tran.id, chekboxval);
        
        chekboxval = 'CHL';
        tran.Chargeback__c = false;
        update tran;
        TransactionDetailsContrl.updateTxns(tran.id, chekboxval);
        
        chekboxval = 'WO';
        tran.WriteOff__c = false;
        update tran;
        TransactionDetailsContrl.updateTxns(tran.id, chekboxval);
        
        DCMS_FraudCase__c fruadcaselist = [Select id, TotalCb__c,FraudWO__c,NonFraudWO__c,TotalFraudAmt__c,TotalRefund__c,TotalToCh__c,TotalAtRisk__c from DCMS_FraudCase__c where Case_Number__c = :cs.id Limit 1];
        fruadcaselist.NonFraudWO__c = 1.00;
        update fruadcaselist;
       // TransactionDetailsContrl.updateChliable(cs.id, CHvalue);
        
        fruadcaselist.NonFraudWO__c = 0.00;
        fruadcaselist.FraudWO__c = 1.00;
        update fruadcaselist;
        TransactionDetailsContrl.updateChliable(cs.id, CHvalue);
        
        TransactionDetailsContrl.deleteTxns(txnlist);
        
    }
}