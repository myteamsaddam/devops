@isTest 
private class testSOAServiceRetryLogOpsController 
{
    static testMethod void validateSOAServiceRetryLogOpsController() 
    {
        Test.setMock(HttpCalloutMock.class, new EditPeopleControllerCalloutMock());
        //To Cover constructer
        SOAServiceRetryLogOpsController objSOAServiceRetryLogOpsController = new SOAServiceRetryLogOpsController();
        //To cover Correct data assign part
        SOAServiceRetryLogOpsController.assignDataSOAServiceRetryLog(null,2,'test1','Success','UpdateCustomerDAta','Text Area long');
        SOAServiceRetryLogOpsController.assignDataSOAServiceRetryLog(null,2,'test1','Success','UpdateCustomerDAta','Text Area long');
        SOAServiceRetryLogOpsController.assignDataSOAServiceRetryLog(null,2,'test1','Success','UpdateCustomerDAta','Text Area long');
        SOAServiceRetryLogOpsController.assignDataSOAServiceRetryLog(null,2,'test1','Success','UpdateCustomerDAta','Text Area long');
        //To Cover else part
        SOAServiceRetryLogOpsController.assignDataSOAServiceRetryLog(null,null,'','','','');
        //To cover error section
        SOAServiceRetryLogOpsController.assignDataSOAServiceRetryLog(null,2,'test1','Success','Test5','Text Area long');
        //To cover insert method
        SOAServiceRetryLogOpsController.insertDataSOAServiceRetryLog();
    }
}