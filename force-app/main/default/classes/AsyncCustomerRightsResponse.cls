//Generated by wsdl2apex

public class AsyncCustomerRightsResponse {
    public class getRightToAccessDetailsResponseTypeFuture extends System.WebServiceCalloutFuture {
        public CustomerRightsResponse.getRightToAccessDetailsResponseType getValue() {
            CustomerRightsResponse.getRightToAccessDetailsResponseType response = (CustomerRightsResponse.getRightToAccessDetailsResponseType)System.WebServiceCallout.endInvoke(this);
            return response;
        }
    }
}