@isTest
public class TestTriggerToUpdateCampaign
{
    public static Product_Custom__c objProd;
    public static Product_Custom__c objProd2;
    public static Account objAcc;
    public static Financial_Account__c objFA;
    public static Financial_Account__c objFA2;
    public static Contact objCon;
    public static Contact objCon2;
    public static Card__c objCard;
    public static Manage_Campaign__c objCmpgn;
    public static testmethod void testCampaignData() 
    {
        //Product Insert
        objProd = new Product_Custom__c();
        objProd.Name = 'Test Product';
        objProd.Product_Description__c = 'Test Desc';
        objProd.Pserno__c = '85';
        Insert objProd;
        //Product Insert
        objProd2 = new Product_Custom__c();
        objProd2.Name = 'Test Product';
        objProd2.Product_Description__c = 'Test Desc';
        objProd2.Pserno__c = '805';
        Insert objProd2;
        //Account Insert
        objAcc = new Account();
        objAcc.Customer_Serno__c = '123654';
        objAcc.Name = 'Account for Test Class';
        Insert objAcc;
        //Contact Insert
        objCon = new Contact();
        objCon.FirstName='TestContact1';
        objCon.Lastname = 'Test';
        objCon.SerNo__c = '12589';
        objCon.SSN__c = '7895874';
        objCon.Institution_Id__c = 2;
        objCon.Email='xyz@abc.com';
        objCon.MobilePhone='9876543210';
        objCon.Phone='123123123'; 
        objCon.Fax='98789879';
        objCon.MobilePhone='98789879'; 
        objCon.HomePhone='123123123';
        objCon.Email='testemail@test.com';
        Insert objCon;
        //Contact Insert
        objCon2= new Contact();
        objCon2.FirstName='TestContact2';
        objCon2.Lastname = 'Test1';
        objCon2.SerNo__c = '125895';
        objCon2.SSN__c = '78958741';
        objCon2.Institution_Id__c = 2;
        objCon2.Email='xyz@ab1c.com';
        objCon2.MobilePhone='98765432101';
        objCon2.Phone='1231231231'; 
        objCon2.Fax='987898791';
        objCon2.HomePhone='1231231231';
        Insert objCon2;
        //FA Insert
        objFA = new Financial_Account__c();
        objFA.Account_Number__c = '4141144';
        objFA.Institution_Id__c = 2;
        objFA.Account_Serno__c = '11166';
        objFA.Customer__c = objAcc.id;
        objFA.Customer_Serno__c = objAcc.Customer_Serno__c;
        objFA.Product__c = objProd.id;
        objFA.Product_Serno__c = objProd.Pserno__c;
        Insert objFA;
        //FA Insert
        objFA2 = new Financial_Account__c();
        objFA2.Account_Number__c = '414114';
        objFA2.Institution_Id__c = 2;
        objFA2.Account_Serno__c = '1116666';
        objFA2.Customer__c = objAcc.id;
        objFA2.Customer_Serno__c = objAcc.Customer_Serno__c;
        objFA2.Product__c = objProd.id;
        objFA2.Product_Serno__c = objProd.Pserno__c;
        Insert objFA2;
        //Insert Card
        objCard = new Card__c();
        objCard.Card_Number_Truncated__c = '5588********9966';
        objCard.Card_Serno__c = '8855225588';
        objCard.Financial_Account__c = objFA.id;
        objCard.Financial_Account_Serno__c = objFA.Account_Serno__c;
        objCard.People__c = objCon.id;
        objCard.People_Serno__c = objCon.SerNo__c;
        objCard.Product__c = objProd.id;
        objCard.Prod_Serno__c = objProd.Pserno__c;
        objCard.PrimaryCardFlag__c=true;
        objCard.Institution_Id__c = 2;
        Insert objCard;
        
        objCmpgn = new Manage_Campaign__c();
        objCmpgn.Campaign_Code__c = '56161651';
        objCmpgn.People_SerNo__c = '12580';
        objCmpgn.AccountSerno__c = '1116666';
        objCmpgn.ProductSerNo__c = '805';
        objCmpgn.DateTime_Stamp__c = system.now();
        Insert objCmpgn;
        
        objCmpgn.People_SerNo__c = '12589';
        objCmpgn.AccountSerno__c = '11166';
        objCmpgn.ProductSerNo__c = '85';
        Update objCmpgn;
    }
}