public class GetInstallInfoOutput
{
    public String MsgId;
    public String CorrelationId;
    public String RequestorId;
    public String SystemId;
    public String InstitutionId;
    public GetInstallInfoOutput.AccountType[] AccountTypes;
    

    public class AccountType
    {
        public String Numberx;
        public String Name;
        public String Status;
        public GetInstallInfoOutput.InstallmentType[] InstallmentTypes;
    }
    
    public class InstallmentType
    {
        public String Numberx1;
        public String Name;
        public String Status;
        public Decimal OutStandingBal;
        public GetInstallInfoOutput.TransactionInstallResType[] TransactioninstallTypes;
    }

    public class TransactionInstallResType
    {
        public Integer Period;
        public DateTime Trxndate;
        public String Trxnmsgtype;
        public String Feereasoncode;
        public String Trxndescription;
        public Double Trxnamount;
        public String Status;
        public Decimal Outstandingbal;
        public String Stgeneral;
        public String Merchname;
        public String Origcardnumber;
        public DateTime Purchasedate;
    }
    //To Cover Test Class 100%
    public void test()
    {
        
    }
}