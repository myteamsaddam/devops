@istest
public class RetensionResponseCustomerController_Test {
    
    public static testmethod void getslectedContact()
    {
         //Account
        Account account=new Account();
        account.Name='test Account';
        account.Customer_Serno__c='123';
        account.Institution_Id__c=5;
        insert account;
      //Contact
        Contact contact=new Contact();
        contact.FirstName='TestContact1';
        contact.LastName='TestContact';
        contact.AccountId=account.Id;
        contact.SSN__c='11235464';
        contact.SerNo__c='12345';
        contact.Institution_Id__c=account.Institution_Id__c;
        contact.Email='xyz@abc.com';
        contact.MobilePhone='9876543210';
        contact.Phone='123123123'; 
        contact.Fax='98789879';
        contact.MobilePhone='98789879'; 
        contact.HomePhone='123123123';
        contact.Email='testemail@test.com';
        insert contact;
        System.debug(contact);
        
         //EnterCard Product
        Product_Custom__c pd=new Product_Custom__c();
        pd.Product_Description__c='dhhfygygyadyh';
        pd.Pserno__c='123';
        insert pd;
        
        Financial_Account__c fc=new Financial_Account__c();
        fc.Customer__c=account.id;
        fc.Customer_Serno__c=account.Customer_Serno__c;
        fc.Account_Serno__c='1234';
        fc.Account_Number__c='1234567890';
        fc.Institution_Id__c=account.Institution_Id__c;
        fc.Product_Serno__c= pd.Pserno__c;
        fc.Product__c=pd.id;
        insert fc;
        
         Card__c c=new Card__c();
        c.PrimaryCardFlag__c = true;
        c.Institution_Id__c=account.Institution_Id__c;
        c.Card_Number_Truncated__c='12345bhsccccc';
        c.Prod_Serno__c=pd.Pserno__c;
        c.Product__c=pd.id;
        c.People__c=contact.id;
        c.Financial_Account_Serno__c=fc.Account_Serno__c;
        c.Financial_Account__c=fc.id;
        c.People_Serno__c=contact.SerNo__c;
        c.Card_Serno__c='1234';
        insert c;
        
        Retention_Response__c req=new Retention_Response__c();
        if (req == null)
        {               req = new Retention_Response__c ();
         req.OwnerId = UserInfo.getUserId();          
        }
        req.Financial_Account__c = fc.id;
        req.OwnerId = UserInfo.getUserId();          
        req.Person__c = contact.id;
        req.Account_Closed__c='N';
        upsert req;
        
        Contact testcontact=RetensionResponseCustomerController.getslectedContact(contact.ID);
        System.assertEquals(contact.ID,testcontact.id);
        RetensionResponseCustomerController.fetchUser();
        List<Financial_Account__c> fa=RetensionResponseCustomerController.getFinancialAccountvalues(contact.ID);
        List<String> option=RetensionResponseCustomerController.ReasonforAccountClosure();
        List<String> selectedoption=RetensionResponseCustomerController.getselectOptions();
        RetensionResponseCustomerController.saveRetension(contact.Name, 'Other reason','N', fc.Id, UserInfo.getUserId(), UserInfo.getUserId());
        
        
    }

}