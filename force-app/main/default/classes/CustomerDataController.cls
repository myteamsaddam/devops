public with sharing class CustomerDataController{

    public Integer noOfRecords{get; set;} 
    public Id faid{get; set;} 
    public string str{get;set;}
    public StatementHistory[] lstStatement{get;private set;}
    public list<InstallmentInfoRelatedList> InstallmentTypeList{get; set;}
    public list<ViewInsuranceRelatedList> ViewInsuranceList{get; set;}
    public String statementmessage{get; set;}
    public String NoRecordmessage{get; set;}
    public String noInsurancemessage{get; set;}
    public boolean isShowingDetailList{get; set;}
    public boolean clickedInstallmentInfo{get;set;}
    
    public CustomerDataController(ApexPages.StandardController stdController) {
        faid= ApexPages.currentPage().getParameters().get('id');
        isShowingDetailList = false;
        clickedInstallmentInfo=false;
    } 
    public CustomerDataController getCustDataConInstance(){
        return this;
    }
    
    public ApexPages.StandardSetController setCus{
        get{          
           set<id>conId = new set<id>();
           if(setCus==null){
             for(Card__c crd: [select id, name,People__c  from Card__c where Financial_Account__c= :faid and People__c !=null]){
               conId.add(crd.People__c);  
             }
            setCus= new ApexPages.StandardSetController(Database.getQueryLocator(
            [select Name,FirstName,LastName,Email,SSN__c,MobilePhone,HomePhone from Contact where id in:conId]));
            noOfRecords= setCus.getResultSize();                
            }
            if(noOfRecords==0){
               str='No records to display'; 
            }
            return setCus;
        }
        Set;
    }
    
    public List<contact> getcontacts() {
         return (List<contact>) setCus.getRecords();
    }
    
    public List<StatementHistory> getStatements() {
         Integer instId;
         string fin_acc_serNo;
         lstStatement = new List<StatementHistory>();
         try{
         List<Card__c> cardlist= [SELECT Id,People__r.Institution_Id__c,Financial_Account__r.Account_Serno__c FROM Card__c  where Financial_Account__c=:faid and PrimaryCardFlag__c=true];
         if(cardlist.size()>0)
          {
            instId = (Integer)cardlist[0].People__r.Institution_Id__c;
            fin_acc_serNo = cardlist[0].Financial_Account__r.Account_Serno__c;
          } 
          system.debug('instId-->'+instId);
          system.debug('fin_acc_serNo-->'+fin_acc_serNo);
          GetStatementHistoryOutput[] lststmthistory = CSSServiceHelper.getTransactionHistory(instId,fin_acc_serNo); 
          if(lststmthistory==NULL || lststmthistory.size()==0) {
          statementmessage = 'There is no statement to display';
          return null;
          }                   
          for(GetStatementHistoryOutput  sho : lststmthistory)
          {
              StatementHistory sh = new StatementHistory();
              sh.StatementSerno=sho.StatementSerno;
              sh.GenerateDate=sho.GenerateDate;
              sh.ClosingBalance=sho.ClosingBalance;
              sh.MindueAmount=sho.MindueAmount;
              sh.TotalPayments=sho.TotalPayments;
              sh.OverdueAmount=sho.OverdueAmount;
              sh.Overduecycles=sho.Overduecycles;
              sh.Duedate=sho.Duedate;
              sh.OpeningBalance=sho.OpeningBalance;
              sh.Duedate=sho.Duedate;
              sh.TotalDebits=sho.TotalDebits;
              sh.AmtTrxn=sho.AmtTrxn;
              sh.TrxnDate=sho.TrxnDate; 
              sh.TrxnDate2=sho.TrxnDate2;   
              lstStatement.add(sh);           
          }
          }catch(Exception e){
              statementmessage = 'Something went wrong.Please try again later';
          }    
         return lstStatement;
    }
    
    public class StatementHistory{
        public String StatementSerno{get;private set;}
        public String GenerateDate{get;private set;}
        public String ClosingBalance{get;private set;}
        public String MindueAmount{get;private set;}
        public String TotalPayments{get;private set;}
        public String OverdueAmount{get;private set;}
        public String Overduecycles{get;private set;}
        public String Duedate{get;private set;}
        public String OpeningBalance{get;private set;}
        public String TotalDebits{get;private set;}
        public String AmtTrxn{get;private set;}
        public String TrxnDate{get;private set;}
        public String TrxnDate2{get;private set;}

    }
    
    
    public void fetchinstallmentData()
    {
        clickedInstallmentInfo=true;    
    }
    
    //Get InstallInfo added by Ranendu on 02 Feb, 2017
    public List<InstallmentInfoRelatedList> getInstallInfo() 
    {
        system.debug('-------------->faid: '+faid);
        List<Financial_Account__c> listFa = new List<Financial_Account__c>();
        listFa = [SELECT Account_Serno__c,Institution_Id__c from Financial_Account__c where id=: faid limit 1];
        GetInstallInfoOutput output = new GetInstallInfoOutput();
        CSSServiceHelper.getInstallmentInfo(Integer.valueOf(listFa[0].Account_Serno__c), String.ValueOf(listFa[0].Institution_Id__c));
        if(CSSServiceHelper.outputinstallInfo != null)
        {
            output = CSSServiceHelper.outputinstallInfo;
        }
        InstallmentTypeList=new List<InstallmentInfoRelatedList>();
        if(output.AccountTypes != null)
        {
            for(GetInstallInfoOutput.AccountType obj: output.AccountTypes)
            {
                if(obj.InstallmentTypes != null)
                {
                    for(GetInstallInfoOutput.InstallmentType obj1: obj.InstallmentTypes)
                    {
                        if(obj1.TransactioninstallTypes != null)
                        {
                            for(GetInstallInfoOutput.TransactionInstallResType obj2: obj1.TransactioninstallTypes)
                            {
                                InstallmentInfoRelatedList temp = new InstallmentInfoRelatedList();
                                temp.Period = obj2.Period;
                                temp.Trxndate = obj2.Trxndate;
                                temp.Trxnmsgtype = obj2.Trxnmsgtype;
                                temp.Feereasoncode = obj2.Feereasoncode;
                                temp.Trxndescription = obj2.Trxndescription;
                                temp.Trxnamount = obj2.Trxnamount;
                                temp.Status = obj2.Status;
                                temp.Outstandingbal = obj2.Outstandingbal;
                                temp.Stgeneral = obj2.Stgeneral;
                                temp.Merchname = obj2.Merchname;
                                temp.Origcardnumber = obj2.Origcardnumber;
                                temp.Purchasedate = obj2.Purchasedate;
                                InstallmentTypeList.add(temp);
                            }
                        }
                    }
                }
            }
        }
        if(output.AccountTypes == null || output.AccountTypes.size() == 0)
        {
            NoRecordmessage = 'There is no Installment info to display';
            return null;
        }
        system.debug('*/*/*/*/-----> '+InstallmentTypeList);
        
        return InstallmentTypeList;
    }
    public class InstallmentInfoRelatedList
    {
        public Integer Period{get;set;}
        public DateTime Trxndate{get;set;}
        public String Trxnmsgtype{get;set;}
        public String Feereasoncode{get;set;}
        public String Trxndescription{get;set;}
        public Double Trxnamount{get;set;}
        public String Status{get;set;}
        public Decimal Outstandingbal{get;set;}
        public String Stgeneral{get;set;}
        public String Merchname{get;set;}
        public String Origcardnumber{get;set;}
        public DateTime Purchasedate{get;set;}
    }
    
    //Get ViewInsuranceServiceOutput added by Ranendu on 03 Feb, 2017
    public List<ViewInsuranceRelatedList> getViewInsurance() 
    {
        system.debug('-------------->faid: '+faid);
        List<Financial_Account__c> listFa = new List<Financial_Account__c>();
        listFa = [SELECT Account_Serno__c,Account_Number__c,Institution_Id__c from Financial_Account__c where id=: faid limit 1];
        ViewInsuranceServiceInput vinsurinputinstance = new ViewInsuranceServiceInput();
        vinsurinputinstance.InstitutionId=Integer.valueOf(listFa[0].Institution_Id__c);
        vinsurinputinstance.AccountNumber=listFa[0].Account_Number__c;
        ViewInsuranceServiceOutput lstscribed = CSSServiceHelper.ViewInsurance(vinsurinputinstance); 
        if(lstscribed != null)
        {
            if(lstscribed.SubscribedInsuranceList == NULL) 
            {
              noInsurancemessage = 'There is no Insurance to display';
              return null;
            }
            if(lstscribed.SubscribedInsuranceList != NULL) 
            {
                ViewInsuranceList = new list<ViewInsuranceRelatedList>();
                for(ViewInsuranceServiceOutput.ViewInsurancesResponseBody obj:lstscribed.SubscribedInsuranceList.ViewInsurancesResponseBodyList)
                {
                    ViewInsuranceRelatedList temp = new ViewInsuranceRelatedList();
                    temp.InsuranceCode = obj.InsuranceCode;
                    temp.Description = obj.Description;
                    ViewInsuranceList.add(temp);
                } 
            }
        }
        return ViewInsuranceList;                  
    }
    public class ViewInsuranceRelatedList
    {
        public String InsuranceCode{get;set;}
        public String Description{get;set;}
    }
    
    public pagereference showDetailList(){
  
      
      isShowingDetailList = true;
      return null;
  }
}