@IsTest
private class PopulateCaseToAddressSSN_Test{
    
    static testmethod void testPopulateCaseToAddressSSN(){ 
        
        Contact testContact = new Contact();
        testContact.FirstName = 'Foo';
        testContact.LastName = 'Bar';
        testContact.Email ='jdoe_test_test@doe.com';
        testContact.SSN__c='Test';
        testContact.Institution_Id__c=1;
        testContact.SerNo__c='123';
        testContact.MobilePhone='98765432101';
        testContact.Phone='1231231231'; 
        testContact.Fax='987898791';
        testContact.HomePhone='1231231231';
        insert testContact;
        
        Case c = new Case();
        c.Subject = 'Test Case';
        c.Origin='Email';
        c.Status='New';
        c.ContactId = testContact.Id;
        c.Category__c='Account closure';
        insert c;
        
        Emailmessage em = new Emailmessage();
        em.Incoming=true;
        em.fromaddress = 'Test_testemail@test.com';
        em.toaddress = 'test1@test.com';
        em.subject = 'Test Email';
        em.textbody = 'testing';
        em.parentid = c.id;
        
        test.startTest();   
        insert em;
        test.stoptest();  
        
        
        
    }
    
    static testmethod void testPopulateCaseToAddressSSN1(){ 
        
        Account acc = new Account();
        acc.name='test';
        acc.Customer_Serno__c='123';
        acc.Institution_Id__c=2;
        insert acc;
        
        Contact testContact = new Contact();
        testContact.FirstName = 'Foo';
        testContact.LastName = 'Bar';
        testContact.Email ='jdoe_test_test@doe.com';
        testContact.SSN__c='Test1';
        testContact.Institution_Id__c=1;
        testContact.SerNo__c='1231';
        testContact.MobilePhone='98765432101';
        testContact.Phone='1231231231'; 
        testContact.Fax='987898791';
        testContact.HomePhone='1231231231';
        insert testContact;
        
        Product_Custom__c prodobj=new Product_Custom__c(
            Name='Test Product',
            Pserno__c='56789'
        );
        insert prodobj;
        
        Financial_Account__c finAccObj=new Financial_Account__c
            (
                Account_Number__c='Test5700192', 
                //Customer__c=accountObj.Id, 
                Account_Serno__c='3123123', 
                Institution_Id__c=2,
                Customer__c=acc.id,
                Customer_Serno__c=testContact.Id,
                Product__c=prodobj.Id,
                Product_Serno__c=prodobj.Pserno__c
            );
        insert finAccObj;
        
        Card__c cardObj =new Card__c(
            Card_Serno__c='2231236', 
            Card_Number_Truncated__c='5456654565665', 
            People__c=testContact.Id, 
            Institution_Id__c=2, 
            Financial_Account__c=finAccObj.Id,
            Financial_Account_Serno__c=finAccObj.Id, 
            People_Serno__c=testContact.Id,
            Prod_Serno__c=prodobj.Pserno__c, 
            Product__c=prodobj.Id,
            PrimaryCardFlag__c=true
        );
        insert cardObj;
        
        Group gr= new Group();
        gr.Name='queue';
        gr.Type='Queue';
        insert gr;
        
        QueueSobject mappingObject = new QueueSobject(QueueId = gr.Id, SobjectType = 'Case');
        System.runAs(new User(Id = UserInfo.getUserId()))
        {insert mappingObject;}
        
        Case c = new Case();
        c.Subject = 'Test Case';
        c.Origin='Email';
        c.Status='New';
        c.ContactId = testContact.Id;
        c.Category__c='Account closure';
        c.OwnerId=gr.Id;
        c.SuppliedEmail='test1@test.com';
        insert c;
        
        //GlobalSettings__c
        List<GlobalSettings__c>GlobalSettingsList=new List<GlobalSettings__c>();
        GlobalSettings__c objGlobal = new GlobalSettings__c();
        objGlobal.Name = 'ServiceStrategies'; 
        objGlobal.Value__c = 'SOAPService';
        GlobalSettingsList.add(objGlobal);
        
        insert GlobalSettingsList;
        
        //common settings
        Common_Settings__c objcommn = new Common_Settings__c();
        objcommn .Name = 'PrimeActionRetryBatch'; 
        objcommn .Retry_batch_interval_in_hours__c = 4;
        insert(objcommn );
        
        //ServiceHeaders__c
        ServiceHeaders__c objServiceHeaders = new ServiceHeaders__c();
        objServiceHeaders.name = 'SOAPHTTP';
        insert objServiceHeaders;
        
        //ServiceSettings__c
        ServiceSettings__c objServiceSettings = new ServiceSettings__c();
        objServiceSettings.Name = 'StatusLogService';
        objServiceSettings.EndPoint__c = 'https://services-test.entercard.com/gateway/services/CSSService?wsdl';
        objServiceSettings.HeaderName__c = 'SOAPHTTP';
        objServiceSettings.OutputClass__c = 'UpdateCustomerDataOutput';
        objServiceSettings.ProcessingClass__c = 'UpdateCustomerDatacallout'; 
        objServiceSettings.Strategy__c = 'SOAPService';
        objServiceSettings.Input_Class__c = 'UpdateCustomerDataInput'; 
        
        insert objServiceSettings;
        
        
        EmailToCaseSettings__c ec=new EmailToCaseSettings__c();
        ec.Name='Test Email setting';
        ec.To_Email__c='test1@test.com';
        ec.Queue__c='queue';
        ec.Category__c='Account closure';
        ec.Email_Source__c='Mobile Apps';
        ec.Is_Auto_Response_Required__c=true;
        ec.Auto_Response_Email_Template__c='Test Email Template';
        insert ec;
        
        
        
        Emailmessage em = new Emailmessage();
        em.Incoming=true;
        em.fromaddress = 'Test_testemail@test.com';
        em.toaddress = 'test1@test.com';
        em.subject = 'Test Email';
        em.textbody = 'testing';
        em.parentid = c.id;
        em.HtmlBody='<tr><td><b>Test5700192</b></td>\n</tr>';
        
        test.startTest();   
        insert em;
        test.stoptest();  
        
    }
    static testmethod void testPopulateCaseToAddressSSN2(){ 
        
        Contact testContact = new Contact();
        testContact.FirstName = 'Foo';
        testContact.LastName = 'Bar';
        testContact.Email ='jdoe_test_test@doe.com';
        testContact.SSN__c='Test';
        testContact.Institution_Id__c=1;
        testContact.SerNo__c='123';
        testContact.MobilePhone='98765432101';
        testContact.Phone='1231231231'; 
        testContact.Fax='987898791';
        testContact.HomePhone='1231231231';
        insert testContact;
        
        Case c = new Case();
        c.Subject = 'Test Case';
        c.Origin='Email';
        c.Status='Closed';
        c.ContactId = testContact.Id;
        c.Category__c='Account closure';
        c.Subject = 'RE: Test Email: 2020.4.14 20:25:46';
        insert c;
        
        Emailmessage em = new Emailmessage();
        em.Incoming=true;
        em.fromaddress = 'Test_testemail@test.com';
        em.toaddress = 'forsikringer@test.com';
        em.subject = 'RE: Test Email: 2020.4.14 20:25:46';
        em.textbody = 'testing';
        em.Headers = 'Return-Path: Middleware.Oracle@entercard.com';
        em.parentid = c.id;
        
        test.startTest();   
        insert em;
        test.stoptest();  
        
        
        
    }
    static testmethod void testPopulateCaseToAddressSSN3(){ 
        
        Contact testContact = new Contact();
        testContact.FirstName = 'Foo';
        testContact.LastName = 'Bar';
        testContact.Email ='jdoe_test_test@doe.com';
        testContact.SSN__c='Test';
        testContact.Institution_Id__c=1;
        testContact.SerNo__c='123';
        testContact.MobilePhone='98765432101';
        testContact.Phone='1231231231'; 
        testContact.Fax='987898791';
        testContact.HomePhone='1231231231';
        insert testContact;
        
        Case c = new Case();
        c.Subject = 'Test Case';
        c.Origin='Email';
        c.Status='Closed';
        c.ContactId = testContact.Id;
        c.Category__c='Account closure';
        c.Subject = 'RE: Test Email: 2020.4.14 20:25:46';
        insert c;
        
        Emailmessage em = new Emailmessage();
        em.Incoming=true;
        em.fromaddress = 'Test_testemail@test.com';
        em.toaddress = '';
        em.subject = 'RE: Test Email: 2020.4.14 20:25:46';
        em.textbody = 'testing';
        em.Headers = 'Return-Path: Middleware.Oracle@entercard.com';
        em.parentid = c.id;
        
        test.startTest();   
        insert em;
        test.stoptest();  
        
        
        
    }    
    
}