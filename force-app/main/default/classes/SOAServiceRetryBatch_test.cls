@isTest
public class SOAServiceRetryBatch_test {
    
    @isTest
    static void test(){
        
        //GlobalSettings__c
        List<GlobalSettings__c>GlobalSettingsList=new List<GlobalSettings__c>();
        GlobalSettings__c objGlobal = new GlobalSettings__c();
        objGlobal.Name = 'ServiceStrategies'; 
        objGlobal.Value__c = 'SOAPService';
        GlobalSettingsList.add(objGlobal);
        
        insert GlobalSettingsList;
        
        //common settings
        Common_Settings__c objcommn = new Common_Settings__c();
        objcommn .Name = 'SOAServiceRetryBatch'; 
        objcommn .Retry_batch_interval_in_hours__c = 4;
        insert(objcommn );
        
        //ServiceHeaders
        ServiceHeaders__c objServiceHeaders = new ServiceHeaders__c();
        objServiceHeaders.name = 'UpdateAddressHeader';
        objServiceHeaders.ContentType__c='SampleContentType';
        objServiceHeaders.Authorization__c='SampleAuthorization';
        insert objServiceHeaders;
        
        //ServiceSettings__c
        ServiceSettings__c objServiceSettings = new ServiceSettings__c();
        objServiceSettings.Name = 'updateCustomerNameAddress';
        objServiceSettings.HeaderName__c = 'UpdateAddressHeader'; 
        objServiceSettings.EndPoint__c = 'https://services-test.entercard.com/gateway/services/CSSService?wsdl';
        objServiceSettings.OutputClass__c = 'UpdateCustomerDataOutput';
        objServiceSettings.ProcessingClass__c = 'UpdateCustomerDatacallout'; 
        objServiceSettings.Strategy__c = 'SOAPService';
        objServiceSettings.Input_Class__c = 'UpdateCustomerDataInput'; 
        insert objServiceSettings;
        
        ServiceSettings__c objServiceSettings1 = new ServiceSettings__c();
        objServiceSettings1.Name = 'UpdateCustomerData';
        objServiceSettings1.HeaderName__c = 'UpdateAddressHeader'; 
        objServiceSettings1.EndPoint__c = 'https://services-test.entercard.com/gateway/services/CSSService?wsdl';
        objServiceSettings1.OutputClass__c = 'UpdateCustomerDataOutput';
        objServiceSettings1.ProcessingClass__c = 'UpdateCustomerDatacallout'; 
        objServiceSettings1.Strategy__c = 'SOAPService';
        objServiceSettings1.Input_Class__c = 'UpdateCustomerDataInput'; 
        insert objServiceSettings1;
        
        //SOSServiceRetryLog
        SOAServiceRetryLog__c objSOAServiceRetryLog=new SOAServiceRetryLog__c();
        objSOAServiceRetryLog.Number_of_Attempt__c=3;
        objSOAServiceRetryLog.Requested_Agent__c='Agent';
        objSOAServiceRetryLog.Service_name__c='UpdateCustomerNameAddress';
        objSOAServiceRetryLog.Service_request_body__c='{"Address1":null, "Address2":null, "Address3":null, "Address4":null, "Address5":null, "City":null, "Country":null, "CountyCode":null, "CustomerID":null, "Email":null, "Fax":null, "FirstName":null, "InstitutionId":2, "LastName":"Sample", "Location":"Home", "MaritalStatus":null, "MiddleName":null, "Mobile":null, "MotherName":null, "NewIndicator":null, "Phone1":null, "Phone2":null, "Reference":null, "State":null, "Title":null, "ZIP":null, "isOnlyPeopleUpd":null}';
        objSOAServiceRetryLog.Response_Status__c='Failed';
        insert objSOAServiceRetryLog;
        
        SOAServiceRetryLog__c objSOAServiceRetryLog1=new SOAServiceRetryLog__c();
        objSOAServiceRetryLog1.Number_of_Attempt__c=3;
        objSOAServiceRetryLog1.Requested_Agent__c='Agent';
        objSOAServiceRetryLog1.Service_name__c='UpdateCustomerNameAddress';
        objSOAServiceRetryLog1.Service_request_body__c='{"Address1":null, "Address2":null, "Address3":null, "Address4":null, "Address5":null, "City":null, "Country":null, "CountyCode":null, "CustomerID":null, "Email":null, "Fax":null, "FirstName":null, "InstitutionId":2, "LastName":"Sample", "Location":"Home", "MaritalStatus":null, "MiddleName":null, "Mobile":null, "MotherName":null, "NewIndicator":null, "Phone1":null, "Phone2":null, "Reference":null, "State":null, "Title":null, "ZIP":null, "isOnlyPeopleUpd":null}';
        objSOAServiceRetryLog1.Response_Status__c='Success';
        insert objSOAServiceRetryLog1;
        
        SOAServiceRetryLog__c objSOAServiceRetryLog2=new SOAServiceRetryLog__c();
        objSOAServiceRetryLog2.Number_of_Attempt__c=3;
        objSOAServiceRetryLog2.Requested_Agent__c='Agent';
        objSOAServiceRetryLog2.Service_name__c='UpdateCustomerData';
        objSOAServiceRetryLog2.Service_request_body__c='{"Address1":null, "Address2":null, "Address3":null, "Address4":null, "Address5":null, "City":null, "Country":null, "CountyCode":null, "CustomerID":null, "Email":null, "Fax":null, "FirstName":null, "InstitutionId":2, "LastName":"Sample", "Location":"Home", "MaritalStatus":null, "MiddleName":null, "Mobile":null, "MotherName":null, "NewIndicator":null, "Phone1":null, "Phone2":null, "Reference":null, "State":null, "Title":null, "ZIP":null, "isOnlyPeopleUpd":null}';
        objSOAServiceRetryLog2.Response_Status__c='Failed';
        insert objSOAServiceRetryLog2;
        
        
        test.startTest();
        
        SOAServiceRetryBatch objSOA=new SOAServiceRetryBatch();
        Database.executeBatch(objSOA,3);
        
        test.stopTest();
        
    }
    
    @isTest
    static void test1(){
        
        //GlobalSettings__c
        List<GlobalSettings__c>GlobalSettingsList=new List<GlobalSettings__c>();
        GlobalSettings__c objGlobal = new GlobalSettings__c();
        objGlobal.Name = 'ServiceStrategies'; 
        objGlobal.Value__c = 'SOAPService';
        GlobalSettingsList.add(objGlobal);
        insert GlobalSettingsList;
        
        //common settings
        Common_Settings__c objcommn = new Common_Settings__c();
        objcommn .Name = 'SOAServiceRetryBatch'; 
        objcommn .Retry_batch_interval_in_hours__c = 4;
        insert(objcommn );
        
        //ServiceHeaders
        ServiceHeaders__c objServiceHeaders = new ServiceHeaders__c();
        objServiceHeaders.name = 'UpdateConsentHeader';
        objServiceHeaders.ContentType__c='SampleContentType';
        objServiceHeaders.Authorization__c='SampleAuthorization';
        insert objServiceHeaders;
        
        //ServiceSettings__c
        ServiceSettings__c objServiceSettings1 = new ServiceSettings__c();
        objServiceSettings1.Name = 'UpdateConsent';
        objServiceSettings1.HeaderName__c = 'UpdateConsentHeader'; 
        objServiceSettings1.EndPoint__c = 'https://services-test.entercard.com/gateway/services/CSSService?wsdl';
        objServiceSettings1.OutputClass__c = 'UpdateConsentServiceOutput';
        objServiceSettings1.ProcessingClass__c = 'UpdateConsentServiceCallout'; 
        objServiceSettings1.Strategy__c = 'SOAPService';
        objServiceSettings1.Input_Class__c = 'UpdateConsentServiceInput'; 
        insert objServiceSettings1;
        
        //SOAServiceRetryLog
        SOAServiceRetryLog__c objSOAServiceRetryLog=new SOAServiceRetryLog__c();
        objSOAServiceRetryLog.Number_of_Attempt__c=3;
        objSOAServiceRetryLog.Requested_Agent__c='Agent';
        objSOAServiceRetryLog.Service_name__c='UpdateConsentData';
        objSOAServiceRetryLog.Service_request_body__c='{"UpdatedBy":"0053E000000cjOZQAY","Source":"Salesforce","InstitutionId":"2","DateTimeStamp":"2017-04-04T09:52:08.500Z","ConsentEntityIdType":"Account","ConsentEntityId":"121121","ConsentCodeValue":[{"value":"Y","code":"Accept SMS"},{"value":"Y","code":"Accept TM"}]}';
        objSOAServiceRetryLog.Response_Status__c='Success';
        insert objSOAServiceRetryLog;
        
        SOAServiceRetryLog__c objSOAServiceRetryLog2=new SOAServiceRetryLog__c();
        objSOAServiceRetryLog2.Number_of_Attempt__c=3;
        objSOAServiceRetryLog2.Requested_Agent__c='Agent';
        objSOAServiceRetryLog2.Service_name__c='UpdateConsentData';
        objSOAServiceRetryLog2.Service_request_body__c='{"UpdatedBy":"0053E000000cjOZQAY","Source":"Salesforce","InstitutionId":"2","DateTimeStamp":"2017-04-04T09:52:08.500Z","ConsentEntityIdType":"Account","ConsentEntityId":"121121","ConsentCodeValue":[{"value":"Y","code":"Accept SMS"},{"value":"Y","code":"Accept TM"}]}';
        objSOAServiceRetryLog2.Response_Status__c='Failed';
        insert objSOAServiceRetryLog2;
        
        test.startTest();
        
        SOAServiceRetryBatch objSOA=new SOAServiceRetryBatch();
        Database.executeBatch(objSOA,2);
        
        test.stopTest();
        
    }

    
}