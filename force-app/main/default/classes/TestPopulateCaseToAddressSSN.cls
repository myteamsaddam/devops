@isTest
private class TestPopulateCaseToAddressSSN 
{
    static Prime_Action_Log__c objPrimeLog;
    static testmethod void test() 
    {
        //Email template
        /*List<EmailTemplate> folderId = new List<EmailTemplate>();
        folderId = [Select FolderId From EmailTemplate Where IsActive = true order by FolderId limit 1];
        EmailTemplate e = new EmailTemplate (developerName = 'Test_Template',FolderId = folderId[0].FolderId, TemplateType= 'Text', Name = 'Test_Template'); // plus any other fields that you want to set
        insert e;*/
    
    
        //EmailToCaseSettings__c
        EmailToCaseSettings__c objEmailtoCase = new EmailToCaseSettings__c();
        objEmailtoCase.Name = 'LettersEmailToCaseSettingsDK1';
        objEmailtoCase.Country_Code__c = 'DK';
        objEmailtoCase.Email_Source__c = 'Netbank';
        objEmailtoCase.To_Email__c = 'hel_lo@670ocglw7xhomi4oyr5yw2zvf.8kp7yeag.8.case.salesforce.com';
        objEmailtoCase.Queue__c = 'DK Customer Service/CollectionTest';
        objEmailtoCase.Department__c = 'Customer Service';
        objEmailtoCase.SLA__c = '24';
        objEmailtoCase.Subject__c = null;
        objEmailtoCase.Is_Auto_Response_Required__c = true;
        objEmailtoCase.Auto_Response_Email_Template__c = 'Test_Template';
        
        insert objEmailtoCase;
        
        
        //Account Creation for case
        Account accountObj=new Account
        (
            Name='Test Account', 
            Institution_Id__c=2, 
            Customer_Serno__c='2342323'
        );
        insert accountObj;
        
        //Contact creation for case
        Contact contactObj =new Contact
        (
            LastName='Test Contact', 
            FirstName='First Test Contact',
            SerNo__c='2312233', 
            Institution_Id__c=2, 
            SSN__c = '12589',
            AccountId=accountObj.Id,
            Email = '12589_st@abc.org',
            Phone='1123123123', 
            Fax='198789879', 
            MobilePhone='198789879', 
            HomePhone='1123123123'
        );
        insert contactObj;
        
        //Case creation
        Case objCase = new Case
        (
            AccountId = accountObj.Id,
            ContactId = contactObj.id,
            Status = 'New',
            Category__c = 'Payment Free Holiday',
            Subject = 'Test:subject',
            SuppliedEmail = 'abc_234@gfh.com'
            //Financial_Account__c = finAccObj.id
        );
        insert objCase;
        
        Profile p = [SELECT Id FROM Profile WHERE Name='Standard User']; 
        User u = new User(Alias = 'standt', Email='standarduser@testorg.com', 
                        EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
                        LocaleSidKey='en_US', ProfileId = p.Id, 
                        TimeZoneSidKey='America/Los_Angeles', UserName='standarduser2314@testorg.com');
        insert u;
        System.runAs(u) 
        {
            //Email template
            List<EmailTemplate> folderId = new List<EmailTemplate>();
            folderId = [Select FolderId From EmailTemplate Where IsActive = true order by FolderId limit 1];
            EmailTemplate e = new EmailTemplate (developerName = 'Test_Template123',FolderId = folderId[0].FolderId, TemplateType= 'Text', Name = 'Test_Template', isActive = true); // plus any other fields that you want to set
            insert e;
        }
        
        EmailMessage[] newEmail = new EmailMessage[0];
 
        newEmail.add(new EmailMessage(FromAddress = '12589_st@abc.org', Incoming = True, ToAddress= 'hel_lo@670ocglw7xhomi4oyr5yw2zvf.8kp7yeag.8.case.salesforce.com', Subject = 'Test email', TextBody = '23456 ', ParentId = objCase.Id)); 
         
        insert newEmail;
        
    }
}