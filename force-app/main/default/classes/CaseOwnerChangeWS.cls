/************************************************************************************************************************************
Test class Name :CaseOwnerChangeWSTest
Last modified By : Rajesh Mohandoss
Purpose          : Change the owner of the case by Clicking "Accept" Button.
Last Modified Date : 10-Apr-2018
*************************************************************************************************************************************/
global without sharing class CaseOwnerChangeWS {

// Check whether the record is refreshed with latest information
webservice static boolean checkIsprocessing(Id recordId, Id ownerId,String status)
{
   Case cas= [SELECT OwnerId,status FROM Case WHERE Id = :recordId FOR UPDATE];
   if(cas.ownerId == ownerId && cas.status==status){
      return true;   
   }
   else
    return false;
}

// Change the owner of the case and status to "In progress"
webservice static String takeOwnership(Id recordId, Id ownerId,Id currownerId) {
        try {
            Case req = [SELECT OwnerId,status FROM Case WHERE Id = :recordId FOR UPDATE];
            if(req.OwnerId==ownerId) {
                return 'Record already has ownership.';
            }
            else if(req.OwnerId==currownerId){
                req.OwnerId = ownerId;
                req.Status=System.Label.Case_In_Progress_Status;
                update req;
                return null;
            }
            else{
                return 'The case is already assign to other user,Please click on OK to refresh the case.';
            }
        }
        catch(Exception e) {
            return 'Error while trying to operate on records:' + e.getMessage();
        }
        
}
}