global class GetNonCustomerWebServiceMockclass implements WebServiceMock{
	global void doInvoke(
        Object stub,
        Object request,
        Map<String, Object> response,
        String endpoint,
        String soapAction,
        String requestName,
        String responseNS,
        String responseName,
        String responseType) 
        {
            NonCustomerServiceResponse.NonCustomerUpdateConsentResponse_element objEle = new NonCustomerServiceResponse.NonCustomerUpdateConsentResponse_element();
            NonCustomerServiceResponse.NonCustomerUpdateConsentResponseRecordType objRecType = new NonCustomerServiceResponse.NonCustomerUpdateConsentResponseRecordType();
            objRecType.code='Testing';
            objRecType.Description = 'Record save successfully';
            response.put('response_x',objRecType);
        }
}