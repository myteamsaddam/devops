@isTest
public class BatchtoUpdateCaseStatus_Test {
    Public static testMethod void myUnitTest(){
        
        BatchEmailIds__c bemail = new BatchEmailIds__c();
        bemail.Name = 'Test';
        bemail.Email_Id__c = 'test@gmail.com';
        insert bemail;
        Account accountObj=new Account(
            Name='Test Account', 
            Institution_Id__c=2, 
            Customer_Serno__c='1123123'
        );
        insert accountObj;
        
        Blob hash = Crypto.generateDigest('MD5', Blob.valueOf('0000000061'));
        string encryptSSN=EncodingUtil.convertToHex(hash);
        Contact contactObj =new Contact(
            LastName='Test Contact', 
            FirstName='First Test Contact',
            SerNo__c='2123123', 
            Institution_Id__c=2, 
            SSN__c='0000000061',
            Encrypted_SSN__c=encryptSSN,
            AccountId=accountObj.Id,
            Phone='123123123', 
            Fax='98789879', 
            MobilePhone='98789879', 
            HomePhone='123123123', 
            Email='testemail@test.com'
        );
        insert contactObj;
        
        List<Case> cslist = new List<Case>();
        Case cs = new Case();
        cs.Origin = 'Phone';
        cs.Status = 'In progress';
        cs.Category__c = 'Closed by Batch';
        cs.ContactId=contactObj.Id;
        //cs.OwnerId = userinfo.getUserId();
        cslist.add(cs);
        insert cslist;
        
        List<Task> tsk = new List<Task>();
        Task ts = new Task();
        ts.Subject = 'Call';
        ts.WhatId = cslist[0].id;
        ts.Priority = 'Normal';
        ts.Status = 'Open';
        ts.Call_Type__c = 'inbound';
        tsk.add(ts);
        insert tsk;
        
        Test.StartTest();
        BatchtoUpdateCaseStatus dbr = new BatchtoUpdateCaseStatus();
        dbr.errorMap.put(cslist[0].id,'test');
        dbr.caseMap.put(cslist[0].id,cslist[0]);
        Database.executeBatch(dbr,200);
        Test.StopTest();
    }
}