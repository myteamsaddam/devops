public class ReadMembershipDataOutput{
    public String MsgId;
    public String CorrelationId;
    public String RequestorId;
    public String SystemId;
    public String InstitutionId;
    public List<MembershipResponseBody> MembershipResponseBodyList;
    
    public class MembershipResponseBody{
        public Integer Institution_ID;
        public String MemberData_Id;
        public String PartnerName;//{get;set;}
        public String ProductId;
        public String EntityId;
        public String EntityType;
        public String Updated_By;
        public List<memberField> MemberFieldList;//{get;set;}
        public String FromDate;
        public String ToDate;
    }
    
    public class memberField{
        public String MemberFieldName;//{get;set;}
        public String MemberFieldValues;//{get;set;}
    }
}