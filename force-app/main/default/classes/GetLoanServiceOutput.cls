/*
----------------------------------------------------------------------------------------
Author    Date         Description
Shameel   31/03/2017   Output class to implement SOA service using Integration framework.
----------------------------------------------------------------------------------------
*/
public class GetLoanServiceOutput{

 public LoanAccountDetails[] LoanAccountDetails; 
  
 public class LoanAccountDetails {
 
    public String CustomerAccountNumber;
    public String CustomerAccountSerno;
    public String CustomerAccountStatus;
    public String LoanAccountNumber;
    public String LoanAccountSerno;
    public String LoanProductName;
    public String LoanAccountStatus;
    public Decimal InitialLoanAmount;
    public Decimal OutStandingBalance;
    public Decimal NominalInterestRate;
    public Integer RemainingPeriod;
    public String LoanSDORef;
    public PaymentPlanDetails[] PaymentPlanDetails;
    
    
    }
    
   public class PaymentPlanDetails {
   
    public String PaymentNumber;
    public String PaymentDate;
    public String TransactionType;
    public String TransactionDescription;
    public Decimal Amount;
    public Decimal OutstandingBalance;
    public String PaymentStatus;
        
    }
    
    //To Cover Test Class 100%
    public void test()
    {
        
    }
   
}