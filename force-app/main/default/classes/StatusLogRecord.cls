@RestResource(urlMapping='/StatusLogRecord/*')
global without sharing class StatusLogRecord { 
    
    /*  
    HttpPost method is used to capture a HttpPOST request has been sent to our rest apex class.  
    Used to request data on the basis of JSON request sent in the URL  
    */  
    
    @HttpPost
    global static string doPost() {
        String RequestString = '';
        try{
            

            RestRequest req = RestContext.request;
            
            RequestString = req.requestBody.toString();
            system.debug('RequestString'+RequestString);
            StatusLog__c statusLog = (StatusLog__c)JSON.deserialize(RequestString, StatusLog__c.class);
            system.debug('statusLog '+statusLog);
            insert statusLog;
            
            return 'Success';
        }catch(exception ex){
            system.debug('Exception:'+ex.getMessage());
            return 'Error';
        }
    }

}