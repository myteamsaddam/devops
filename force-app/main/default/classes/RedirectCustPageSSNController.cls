public class RedirectCustPageSSNController{
    public string custId{get;private set;}
    public string custName{get;private set;} 
    
    public pagereference custSearch(){
        string fullurl = apexpages.currentpage().geturl();
        String urlssn = apexpages.currentpage().getparameters().get('PV6');
        if(urlssn!=null && urlssn!=''){
            Blob hash = Crypto.generateDigest('MD5', Blob.valueOf(urlssn));
            string encryptSSN=EncodingUtil.convertToHex(hash);
            Contact[] con = [select id,Name from contact where Encrypted_SSN__c = :encryptSSN]; 
            system.debug('------con----'+con);
            if(con.size()>0){
                custId = con[0].Id;  
                custName=con[0].Name;
                return null;
            }  
        }
        ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.INFO,'<b> No customer found </b>');
        ApexPages.Message myMsg1 = new ApexPages.Message(ApexPages.Severity.INFO,'Please search for customer manually');
        ApexPages.addMessage(myMsg);
        ApexPages.addMessage(myMsg1);
        return null;
        
    }
}