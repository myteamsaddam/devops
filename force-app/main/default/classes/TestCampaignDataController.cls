@isTest
private class TestCampaignDataController 
{
    static Prime_Action_Log__c objPrimeLog;
    static testmethod void test() 
    {
        List<Manage_Campaign__c> insercampLst = new List<Manage_Campaign__c>();
        //Account Creation for case
        Account accountObj=new Account
        (
            Name='Test Account', 
            Institution_Id__c=2, 
            Customer_Serno__c='2342323'
        );
        insert accountObj;
        
        Manage_Campaign__c objCamp1 = new Manage_Campaign__c();
        Manage_Campaign__c objCamp2 = new Manage_Campaign__c();
        Manage_Campaign__c objCamp3 = new Manage_Campaign__c();
        
        objCamp1.AccountSerno__c = '2342323';
        objCamp1.Campaign_Code__c = '23423';
        objCamp1.InstitutionId__c = 8;
        objCamp1.Priority__c = 7;
        insercampLst.add(objCamp1);
        objCamp2.AccountSerno__c = '2342323';
        objCamp2.Campaign_Code__c = '234';
        objCamp2.InstitutionId__c = 5;
        objCamp2.Priority__c = 5;
        insercampLst.add(objCamp2);
        objCamp3.AccountSerno__c = '2342323';
        objCamp3.Campaign_Code__c = '2342';
        objCamp3.InstitutionId__c = 4;
        objCamp3.Priority__c = 2;
        insercampLst.add(objCamp3);
        
        insert insercampLst;
        
        Test.startTest();
           CampaignDataController objCampData = new CampaignDataController();
        Test.stopTest();
    }
}