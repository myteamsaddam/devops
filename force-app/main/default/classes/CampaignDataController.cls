public with sharing class CampaignDataController 
{
    public Id contactId{get; set;}
    public List<Manage_Campaign__c> lstCampaignData{get; set;}
    public List<Manage_Campaign__c> lstCampaignDatathree{get; set;}
    public Integer numOfCampaignData{get; set;}
    public CampaignDataController()
    {
        //Id contactId = contdata.id;
        system.debug('contactId-->'+contactId);
        contactId = (Id)ApexPages.currentPage().getParameters().get('Id');
        lstCampaignData = new List<Manage_Campaign__c>();
        lstCampaignDatathree = new List<Manage_Campaign__c>();
        lstCampaignData  = [SELECT AccountSerno__c,Campaign_Type__c, Product_Name__r.name,CLI_Amt__c, End_Date__c , Priority__c from Manage_Campaign__c where People__c=:contactId order by Priority__c ASC];
        numOfCampaignData = lstCampaignData.size();
        Integer i = 0;
        for(Manage_Campaign__c mc : lstCampaignData)
        {
            if(i<3 && i<numOfCampaignData && mc.Priority__c!=null)
            lstCampaignDatathree.add(mc);
            i++;
            
        }
        //lstCampaignData = lstCampaignDatathree;
    }
    
}