public class CustomerMemoExportDataCallout extends SoapIO{
public string InstitutionId;
SOACustomerMemoExportDataCssservice.CustomerMemoExportDataRequestRecordType customerMemoExportData;
public  void convertInputToRequest(){
     
    CustomerMemoExportDataInput ip = (CustomerMemoExportDataInput)serviceInput;
    customerMemoExportData= new SOACustomerMemoExportDataCssservice.CustomerMemoExportDataRequestRecordType();
    customerMemoExportData.Action=ip.Action;
    customerMemoExportData.Reference=ip.Reference;
    customerMemoExportData.Number_x=ip.Number_x;
    customerMemoExportData.Type_x = ip.Type_x;
    customerMemoExportData.Message = ip.Message;    
    InstitutionId = ip.InstitutionId;
    }
    
    public  override object invokeWebserviceCallout(SoapRequest soapRequest){
        SOAPschemasEntercardComTypelib10.headerType HeadInstance=new SOAPschemasEntercardComTypelib10.headerType();
        HeadInstance.MsgId='FinalCAll';
        HeadInstance.CorrelationId=Common_Settings__c.getValues('CorrelationId').Common_Value__c;
        HeadInstance.RequestorId=Common_Settings__c.getValues('RequestorId').Common_Value__c;
        HeadInstance.SystemId=Common_Settings__c.getValues('SystemId').Common_Value__c;
        HeadInstance.InstitutionId=InstitutionId;
        SOACustomerMemoExportDataCssservice.x_xsoap_CSSServiceESB_CSSServicePT invokeInstance= new SOACustomerMemoExportDataCssservice.x_xsoap_CSSServiceESB_CSSServicePT();       
        invokeInstance.timeout_x=30000;
        // Added 12/13/2016
        ServiceSettings__c serviceObj = ServiceSettings__c.getValues('CustomerMemoExportData');
        if(serviceObj!=null && serviceObj.EndPoint__c!='' && serviceObj.EndPoint__c!=null)
        invokeInstance.endpoint_x = serviceObj.EndPoint__c;
        // Added 12/13/2016
        String username = serviceObj.Username__c;//'css_soa';
        String password = serviceObj.Password__c;//'P1%gruzA';
        //String username = 'evry_access';
        //String password = '9oKuwQioQ4';
        Blob headerValue = Blob.valueOf(username + ':' + password);
        String authorizationHeader = 'BASIC ' +EncodingUtil.base64Encode(headerValue);
        
        invokeInstance.inputHttpHeaders_x = new Map<String, String>();
        invokeInstance.inputHttpHeaders_x.put('Authorization', authorizationHeader);
        
        SOAPdocsOasisOpenOrgWss200401Oasis20.UsernameToken creds=new SOAPdocsOasisOpenOrgWss200401Oasis20.UsernameToken();
        creds.Username=serviceObj.Username__c;//'css_soa';
        creds.Password=serviceObj.Password__c;//'P1%gruzA';
        //creds.Username='evry_access';
        //creds.Password='9oKuwQioQ4';
        SOAPdocsOasisOpenOrgWss200401Oasis20.Security_element security_ele=new SOAPdocsOasisOpenOrgWss200401Oasis20.Security_element();
        security_ele.UsernameToken=creds;
        invokeInstance.SecurityPart=security_ele;
        SOACustomerMemoExportDataCssservice.CustomerMemoExportDataResponseRecordType customerMemoExportData_elementinstance = invokeInstance.customerMemoExportData(HeadInstance,customerMemoExportData);
        system.debug('customerMemoExportData_elementinstance-->'+customerMemoExportData_elementinstance);
        return customerMemoExportData_elementinstance;
        }  
        
     public override object convertResponseToOutput(object response, map<string, string> responseHeader, ServiceStatus serviceStatus){
       if(response==null)
       return null;
       SOACustomerMemoExportDataCssservice.CustomerMemoExportDataResponseRecordType respstmt = (SOACustomerMemoExportDataCssservice.CustomerMemoExportDataResponseRecordType)response; 
       SOACustomerMemoExportDataCssservice.Result rr =  respstmt.Result;
       CustomerMemoExportDataOutput op = new CustomerMemoExportDataOutput();       
       op.Code = rr.Code;
       op.Description = rr.Description;
       if(rr.ErrorDetails!=null)
       op.ErrorResult = rr.ErrorDetails.ErrorResult;
       return op;
    } 

}