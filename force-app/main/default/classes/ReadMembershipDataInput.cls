/*
----------------------------------------------------------------------------------------
Author : Subhajit Pal(CG)    Date : 17/01/2017        
Description : Input class to implement SOA service ReadMembershipData using Integration framework.
----------------------------------------------------------------------------------------
*/
public class ReadMembershipDataInput{

    public Integer InstitutionId;
    //public String PartnerName;
    public String ProductId;
    public String EntityId;
    public String FromDate;
    public String ToDate;
    //To Cover Test Class 100%
    public void test()
    {
        
    }
   
}