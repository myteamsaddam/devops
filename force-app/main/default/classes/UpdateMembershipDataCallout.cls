public class UpdateMembershipDataCallout extends SoapIO{
    UpdateMembershipDataRequest.UpdateMembershipRequestBody UpdateMembershipRequestInstance;
    UpdateMembershipDataInput input;
    public  void convertInputToRequest(){
        input = (UpdateMembershipDataInput)serviceInput;
        UpdateMembershipRequestInstance = new UpdateMembershipDataRequest.UpdateMembershipRequestBody();
        
        UpdateMembershipRequestInstance.Institution_ID=input.Institution_ID;
        UpdateMembershipRequestInstance.PartnerName=input.PartnerName;
        UpdateMembershipRequestInstance.ProductId=input.ProductId;
        UpdateMembershipRequestInstance.EntityId=input.EntityId;
        UpdateMembershipRequestInstance.UpdatedBy=input.UpdatedBy;
        UpdateMembershipRequestInstance.DateTimeStamp=input.DateTimeStamp; 
        UpdateMembershipRequestInstance.Source_System=input.Source_System; 
        UpdateMembershipRequestInstance.EntityType=input.EntityType; 
        
        if(input.memberFieldList!=null && input.memberFieldList.size()>0)
        {    
            List<UpdateMembershipDataRequest.memberFieldType>memberFieldList = new List<UpdateMembershipDataRequest.memberFieldType>();
            for(UpdateMembershipDataInput.memberField each: input.memberFieldList)
            {
                UpdateMembershipDataRequest.memberFieldType temp = new UpdateMembershipDataRequest.memberFieldType();
                temp.MemberFieldName=each.MemberFieldName;
                temp.MemberFieldValues=each.MemberFieldValues;
                memberFieldList.add(temp);
            }
            UpdateMembershipRequestInstance.MemberDetails=memberFieldList;
        }
    }
     public  override object invokeWebserviceCallout(SoapRequest soapRequest){
        UpdateMembershipDataTypelib10.headerType HeadInstance=new UpdateMembershipDataTypelib10.headerType();
        HeadInstance.MsgId='SFDC consuming UpdateMembershipData SOA service. Time Stamp=>'+system.now();
        HeadInstance.CorrelationId=Common_Settings__c.getValues('CorrelationId').Common_Value__c;
        HeadInstance.RequestorId=Common_Settings__c.getValues('RequestorId').Common_Value__c;
        HeadInstance.SystemId=Common_Settings__c.getValues('SystemId').Common_Value__c;
        HeadInstance.InstitutionId=string.valueof(input.Institution_ID);
        UpdateMembershipDataInvoke.x_xsoap_CSSServiceESB_CSSServicePT invokeInstance= new UpdateMembershipDataInvoke.x_xsoap_CSSServiceESB_CSSServicePT();
        invokeInstance.timeout_x=120000;

        ServiceSettings__c serviceObj = ServiceSettings__c.getValues('UpdateMembershipData');
        if(serviceObj!=null && serviceObj.EndPoint__c!='' && serviceObj.EndPoint__c!=null)
        invokeInstance.endpoint_x = serviceObj.EndPoint__c;
        
        String username = serviceObj.Username__c; //'css_soa';
        String password = serviceObj.Password__c;//'P1%gruzA';
        
        Blob headerValue = Blob.valueOf(username + ':' + password);
        String authorizationHeader = 'BASIC ' +EncodingUtil.base64Encode(headerValue);
        
        invokeInstance.inputHttpHeaders_x = new Map<String, String>();
        invokeInstance.inputHttpHeaders_x.put('Authorization', authorizationHeader);
        
        UpdateMembershipDataSecurityHeader.UsernameToken creds=new UpdateMembershipDataSecurityHeader.UsernameToken();
        creds.Username=serviceObj.Username__c; //'css_soa';//'evry_access';
        creds.Password=serviceObj.Password__c;//'P1%gruzA';//'9oKuwQioQ4';
        UpdateMembershipDataSecurityHeader.Security_element security_ele=new UpdateMembershipDataSecurityHeader.Security_element();
        security_ele.UsernameToken=creds;
        invokeInstance.SecurityPart=security_ele;
        UpdateMembershipDataResponse.UpdateMembershipResponse_element UpdateMembershipResponse_elementinstance;
        UpdateMembershipResponse_elementinstance =  invokeInstance.updateMembershipData(HeadInstance,UpdateMembershipRequestInstance);
        system.debug('UpdateMembershipResponse_elementinstance -->'+UpdateMembershipResponse_elementinstance);
        return UpdateMembershipResponse_elementinstance;
     }
     
     public override object convertResponseToOutput(object response, map<string, string> responseHeader, ServiceStatus serviceStatus){
         if(response==null)
             return null;
             
         UpdateMembershipDataResponse.UpdateMembershipResponse_element UpdateMembershipResponse_elementinstance = (UpdateMembershipDataResponse.UpdateMembershipResponse_element)response;

         UpdateMembershipDataOutput UpdateMembershipOutput=new UpdateMembershipDataOutput();
         UpdateMembershipOutput.MsgId=UpdateMembershipResponse_elementinstance.Header.MsgId;
         UpdateMembershipOutput.CorrelationId=UpdateMembershipResponse_elementinstance.Header.CorrelationId;
         UpdateMembershipOutput.RequestorId=UpdateMembershipResponse_elementinstance.Header.RequestorId;
         UpdateMembershipOutput.SystemId=UpdateMembershipResponse_elementinstance.Header.SystemId;
         UpdateMembershipOutput.InstitutionId=UpdateMembershipResponse_elementinstance.Header.InstitutionId;
         UpdateMembershipOutput.Code=UpdateMembershipResponse_elementinstance.UpdateMembershipResponseRecord.Code;
         UpdateMembershipOutput.Description=UpdateMembershipResponse_elementinstance.UpdateMembershipResponseRecord.Description;

         
         return UpdateMembershipOutput;
     }
}