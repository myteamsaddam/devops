@isTest
public class DeleteFinanceRelatedData_Test{
    Public static testMethod void myUnitTest(){
        Account ac = new Account();
        ac.Name = 'Test';
        ac.Customer_Serno__c = '1122334455';
        insert ac;
        
        Contact con = new Contact();
        con.LastName = 'Testing';
        con.SSN__c='353535353535';
        con.SerNo__c='4353077';
        insert con;
        
        Product_Custom__c pd = new Product_Custom__c();
        pd.Name = 'Test';
        pd.Pserno__c = '1542';
        insert pd;
        
        List<Financial_Account__c> falist = new List<Financial_Account__c>();
        String clsDat = String.valueof(System.today());
        //Date dt=System.today().addmonths(-120);
        //String clsdDat=String.valueOf(dt);
        Financial_Account__c fa = new Financial_Account__c();
        fa.Product__c = pd.id;
        fa.Account_Serno__c = '1200';
        fa.Account_Number__c = '125646456';
        fa.Customer__c = ac.Id;
        fa.Customer_Serno__c = '126464';
        fa.Product_Serno__c = '454564';
        fa.FA_Status__c = true;
        fa.Fa_ClosedDate__c = clsDat;
        fa.X120_Months__c = clsDat;
        falist.add(fa);
        Insert falist;

        
        List<Memo__c> mlist = new List<Memo__c>();
        Memo__c mem = new Memo__c();
        mem.Financial_Account__c = fa.Id;
        mlist.add(mem);
        insert mlist;
        
        List<Prime_Action_Log__c> primelist = new List<Prime_Action_Log__c>();
        Prime_Action_Log__c prime = new Prime_Action_Log__c();
        prime.Financial_Account__c = fa.Id;
        primelist.add(prime);
        insert primelist;
        
        List<Cross_Sell_Response__c> crosellist = new List<Cross_Sell_Response__c>();
        Cross_Sell_Response__c cros = new Cross_Sell_Response__c();
        cros.Financial_Account__c = fa.id;
        cros.Cross_Sell_Success__c = 'Y';
        cros.Service_Sold__c = 'Test';
        crosellist.add(cros);
        insert crosellist;
        
        List<Retention_Response__c> retentionlist = new List<Retention_Response__c>();
        Retention_Response__c retention = new Retention_Response__c();
        retention.Financial_Account__c = fa.id;
        retention.Account_Closed__c = 'Y';
        retentionlist.add(retention);
        insert retentionlist;
        
        List<Manage_Campaign__c> camplist = new List<Manage_Campaign__c>();
        Manage_Campaign__c camp = new Manage_Campaign__c();
        camp.FinAccount__c = fa.Id;
        camp.AccountSerno__c = '1234566';
        camp.Campaign_Code__c = '12456';
        camplist.add(camp);
        insert camplist;
        
        
        Test.StartTest();
        DeleteFinanceRelatedData dbr = new DeleteFinanceRelatedData();
        dbr.errorMap.put(camplist[0].id,'test');
        Database.executeBatch(dbr,200);
        Test.StopTest(); 
    }
}