/*
Created By: Srinivas D
Created Date: 4th-April-2019
Created for Jira Ticket:SFD-984
Test Class for : 'DisputeTypeCaseCreation' Trigger and 'DisputeTypeCaseHandler' Apex Class
Last Modified By: Srinivas D
Last Modified Date: 
*/

@isTest
public class DisputeTypeCaseHandler_Test {
    public static testmethod void createTransTest(){
     
      DCMS_CaseStatus__c cstatus = new DCMS_CaseStatus__c();
        cstatus.CaseStatusId__c=0;
        cstatus.Name='New';
        cstatus.Description__c='New';
        cstatus.InstitutionId__c=1;
        insert cstatus;
       
       DCMS_Decision__c des = new DCMS_Decision__c();
        des.DecisionId__c=0;
        des.Name='APPROVED';
        des.Description__c='APPROVED';
        des.InstitutionId__c=1;
        insert des;
        
       DCMS_FraudRing__c fring = new DCMS_FraudRing__c();
        fring.Active__c=1;
        fring.Name='3D';
        fring.Name__c='3D';
        fring.Description__c='3D';
        fring.InstitutionId__c=1;
        fring.FraudRingId__c=0;
        insert fring;
        
       DCMS_Queue_Settings__c qsetting = new DCMS_Queue_Settings__c();
        qsetting.Dispute_Type__c='0: lost';
        qsetting.Name='0: lost';
        qsetting.Institutionid__c=1;
        qsetting.Queue_Name__c='NO_FCB_Follow_Up_Stolen_FA_Intercept';
        insert qsetting;
            
      Account accountObj=new Account(
      Name='Test Account', 
      Institution_Id__c=2, 
      Customer_Serno__c='1123123'
      );
      insert accountObj;
        
      Blob hash = Crypto.generateDigest('MD5', Blob.valueOf('0000000061'));
      string encryptSSN=EncodingUtil.convertToHex(hash);
      Contact contactObj =new Contact(
      LastName='Test Contact', 
      FirstName='First Test Contact',
      SerNo__c='2123123', 
      Institution_Id__c=2, 
      SSN__c='0000000061',
      Encrypted_SSN__c=encryptSSN,
      AccountId=accountObj.Id,
      Phone='123123123', 
      Fax='98789879', 
      MobilePhone='98789879', 
      HomePhone='123123123', 
      Email='testemail@test.com'
      );
      insert contactObj;

      Product_Custom__c prodobj=new Product_Custom__c(
      Name='Test Product',
      Pserno__c='56789'
      );
      insert prodobj;
      
      Financial_Account__c finAccObj=new Financial_Account__c(
      Account_Number__c='12w23eeeeedd', 
      Customer__c=accountObj.Id, 
      Account_Serno__c='3123123', 
      Institution_Id__c=2,
      Customer_Serno__c=contactObj.SerNo__c, 
      Product__c=prodobj.Id,
      Product_Serno__c=prodobj.Pserno__c
      );
      insert finAccObj;
        
      Card__c cardObj=new Card__c(
      People__c=contactObj.Id,
      PrimaryCardFlag__c=true,
      Financial_Account__c=finAccObj.Id, 
      Card_Serno__c='90247211', 
      Card_Number_Truncated__c='7136785481583561', 
      Financial_Account_Serno__c=finAccObj.Id, 
      People_Serno__c=contactObj.Id,
      Prod_Serno__c=prodobj.Pserno__c, 
      Product__c=prodobj.Id,
      Institution_Id__c=2
      );
      insert cardObj;
        
      //Test Record for DCMS_Transaction
    DCMS_Transaction__c tran = new DCMS_Transaction__c();
        tran.BillingAmount__c = 100;
        tran.DisputeType__c = '10: credit'; //'7: multiple imprint fraud';
        tran.Institutionid__c = 1;
        tran.MerchName__c = 'Loan';
        tran.MerchTypeMCC__c = 'Test';
        tran.MerchCountry__c='SE';
        tran.PCI_Dss_Token__c = 12345;
        tran.PosEntryMode__c='2';
        tran.Txn_CardSerno__c=90247211;
        //tran.CaseNumber__c =cs.Id;
        insert tran;   
         
    // Test Case Record creation 
    Case cs = new Case();
        cs.ContactId=contactObj.Id;
        cs.Status= 'New';
        cs.Category__c='Dispute';
        cs.Origin='Web';
        cs.Department__c='Fraud and Chargeback';
        cs.Dispute_Type__c='0: lost';
        cs.Institutionid__c=1;
        cs.Gross_Amount__c=0;
        cs.Card__c=cardObj.id;
        insert cs;
        
        system.debug('-----case-creatiion---'+cs.id);
        DCMS_Transaction__c tran2 = new DCMS_Transaction__c();
        tran2.BillingAmount__c = 100;
        tran2.DisputeType__c = '10: credit'; //7: multiple imprint fraud';
        tran2.Institutionid__c = 1;
        tran2.MerchName__c = 'Loan';
        tran2.MerchTypeMCC__c = 'Test';
        tran2.MerchCountry__c='SE';
        tran2.PCI_Dss_Token__c = 12345;
        tran2.PosEntryMode__c='2';
        tran2.Txn_CardSerno__c=90247211;
        //tran2.CaseNumber__c =cs.Id;
       // insert tran2;
             
       DisputeTypeCaseHandler handler = new DisputeTypeCaseHandler();
       Map<Id,Case> newCasesMap = new Map<Id,Case>();
       Map<Id,Case> oldCasesMap = new Map<Id,Case>();
       newCasesMap.put(cs.id,cs);
        cs.Dispute_Type__c='7: multiple imprint fraud';
        //update cs;
        oldCasesMap.put(cs.id,cs);
       handler.updateFraudCases(newCasesMap,oldCasesMap);
        
       
        
        DCMS_Transaction__c tran1 = new DCMS_Transaction__c();
        tran1.BillingAmount__c = 100;
        //tran1.DisputeType__c = '0: lost';
        //tran1.Institutionid__c = 1;
        tran1.MerchName__c = 'Loan';
        tran1.MerchTypeMCC__c = 'Test';
        tran1.PCI_Dss_Token__c = 12345;
        tran1.PosEntryMode__c='2';
        tran1.MerchCountry__c='SE';
        //tran1.Txn_CardSerno__c=90247211;
        tran1.CaseNumber__c =cs.Id;
        insert tran1;
        List<DCMS_Transaction__c> tranList = new List<DCMS_Transaction__c>();
        tranList.add(tran);
        handler.updateGrossAmount(tranList);
        delete tran1;
    }
}