@isTest 
public class LogACallController_test {
    
    static testMethod void testmthd1(){  
        Account accountObj=new Account(
            Name='Test Account', 
            Institution_Id__c=2, 
            Customer_Serno__c='1123123'
        );
        insert accountObj;
        
        Blob hash = Crypto.generateDigest('MD5', Blob.valueOf('0000000061'));
        string encryptSSN=EncodingUtil.convertToHex(hash);
        Contact contactObj =new Contact(
            LastName='Test Contact', 
            FirstName='First Test Contact',
            SerNo__c='2123123', 
            Institution_Id__c=2, 
            SSN__c='0000000061',
            Encrypted_SSN__c=encryptSSN,
            AccountId=accountObj.Id,
            Phone='123123123', 
            Fax='98789879', 
            MobilePhone='98789879', 
            HomePhone='123123123', 
            Email='testemail@test.com'
        );
        insert contactObj;
        
        Product_Custom__c prodobj=new Product_Custom__c(
            Name='Test Product',
            Pserno__c='56789'
        );
        insert prodobj;
        
        Financial_Account__c finAccObj=new Financial_Account__c(
            Account_Number__c='12w23eeeeedd', 
            Customer__c=accountObj.Id, 
            Account_Serno__c='3123123', 
            Institution_Id__c=2,
            Customer_Serno__c=contactObj.SerNo__c, 
            Product__c=prodobj.Id,
            Product_Serno__c=prodobj.Pserno__c
        );
        insert finAccObj;
        
        Card__c cardObj=new Card__c(
            People__c=contactObj.Id,
            PrimaryCardFlag__c=true,
            Financial_Account__c=finAccObj.Id, 
            Card_Serno__c='524858519', 
            Card_Number_Truncated__c='7136785481583561', 
            Financial_Account_Serno__c='3123123', 
            People_Serno__c='09899',
            Prod_Serno__c=prodobj.Pserno__c, 
            Product__c=prodobj.Id,
            Institution_Id__c=2
        );
        insert cardObj;
        
       /* List<String> grpId = new List<String>();
        
        
        Group grp = new Group();
        grp.Name = 'Main group';
        insert grp;
        grpId.add(grp.Id);
            
        
        List<String> strids = new List<String>();
        strids.add('');
        
        task taskobj =new task(whoId=contactObj.Id, 
                               subject='subject', 
                               status='Completed',Category__c='Test', type='Complaint',
                              Comments__c='', description='Test',CreatedById = UserInfo.getUserId(), CreatedDate = system.today(),OwnerId = UserInfo.getUserId());
        insert taskobj;
        system.debug('----taskobj---'+taskobj.Id+'-----ts-CreatedById---'+taskobj.CreatedById+'----CreatedDate--'+taskobj.CreatedDate);
				*/				
        LogACallController.doSave(String.valueOf(finAccObj.Id), String.valueOf(cardObj.Id), contactObj.Id, 'None', 'Testing', 'Complaint', true, true,true, 'Yes','Y','Test','Yearly');
		LogACallController.doSave(String.valueOf('--None--'), String.valueOf('Other'), contactObj.Id, 'Account closure', 'Account closure', 'Complaint', true, true,true,'','--None--','','');
		LogACallController.doSave(String.valueOf(finAccObj.Id), String.valueOf(cardObj.Id), contactObj.Id, 'Account closure', 'Account closure', 'Complaint', true, true,false,'Yes','Y','Test','Yearly');
        LogACallController.doUpdate(contactObj.Id, contactObj.Id);
        LogACallController.getCalltype(contactObj.Id);
        LogACallController.getFinRecs(contactObj.Id);
        LogACallController.getCardRecs(finAccObj.Id, contactObj.Id);
        
        LogACallController.getCardRecs('Other', contactObj.Id);
        LogACallController.getCardRecs('--None--', contactObj.Id);
        //LogACallController.disableCustomer(contactObj.Id);
        LogACallController lac = new LogACallController();
        lac.Ssn = '0000010289';
        lac.CustName = 'Test';
        
        lac.redirctcustomer();
        
        lac.caseobj.Category__c = 'Test';
        lac.updateActivity();
    }
    
    static testMethod void testmthd2(){
        
        LogACallController lac1 = new LogACallController();
        lac1.Ssn = '';
        lac1.redirctcustomer();
        lac1.caseobj.Category__c = 'None';
        lac1.updateActivity();
    }
    
    static testMethod void testmthd3(){
        
        task taskobj =new task(subject='subject', 
                               status='Completed',Category__c='Test', type='Complaint',
                               Comments__c='', description='Test',CreatedById = UserInfo.getUserId(), CreatedDate = system.today(),OwnerId = UserInfo.getUserId());
        insert taskobj;
        LogACallController lac2 = new LogACallController();
        lac2.caseobj.Category__c = 'Test';
        lac2.tsk.Description = 'test';
        lac2.caseobj.type = 'Test';
        lac2.updateActivity();
        
        lac2.caseobj.Category__c = null;
        lac2.updateActivity();
    }
}