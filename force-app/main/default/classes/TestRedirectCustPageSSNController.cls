@isTest
private class TestRedirectCustPageSSNController 
{
    
    private static testmethod void testredirect(){
        Test.setCurrentPageReference(Page.RedirectCustPageSSN);
        System.currentPageReference().getParameters().put('PV6', '0000000065');
        //create data
        Blob hash = Crypto.generateDigest('MD5', Blob.valueOf('0000000065'));
        string encryptSSN=EncodingUtil.convertToHex(hash);
        Account accountObj=new Account
            (
                Name='Test Account', 
                Institution_Id__c=2, 
                Customer_Serno__c='2342323'
            );
        insert accountObj;
        Contact contactObj =new Contact
            (
                LastName='Test Contact', 
                FirstName='First Test Contact',
                SerNo__c='2312233', 
                Institution_Id__c=2, 
                SSN__c = '0000000065',
                Encrypted_SSN__c = encryptSSN,
                AccountId=accountObj.Id,
                Email = '12589_st@abc.org',
                Phone='1123123123', 
                Fax='198789879', 
                MobilePhone='198789879', 
                HomePhone='1123123123'
            );
        insert contactObj;
        
        Test.startTest();
        RedirectCustPageSSNController rcpssncon = new RedirectCustPageSSNController();
        rcpssncon.custSearch(); 
        System.currentPageReference().getParameters().put('PV6', '');       
        rcpssncon.custSearch();
    }
}