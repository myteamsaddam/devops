global class DeleteFinanceRelatedData implements Database.Batchable<SObject>, Database.Stateful, schedulable{ 
    global Map<Id, String> errorMap {get; set;}
    
    global DeleteFinanceRelatedData(){
        errorMap = new Map<Id, String>();
    }
    global Database.QueryLocator start(Database.BatchableContext BC) { 
     // String clsDat = String.valueof(Date.today().addmonths(120).adddays(1).format());//Date.today().adddays(1).format();
        String clsDat=String.valueof(System.today());
       
        system.debug('----clsdat----'+clsDat);
        String query = 'Select id,(Select id from Memos__r),(Select id from Prime_Actions_Log__r),(Select id from Cross_Sell_Responses__r),(Select id from Retention_Responses__r),(Select id from Manage_Campaigns__r) from Financial_Account__c where fa_Status__c = true AND X120_Months__c =: clsDat';
        return Database.getQueryLocator(query);
    }
    
    global void execute(Database.BatchableContext BC, List<Financial_Account__c> scope) { 
        system.debug('-------scope------'+scope);
        List<Financial_Account__c> faList = new List<Financial_Account__c>();
        for(Financial_Account__c f : scope){
            Financial_Account__c fa = (Financial_Account__c)f;
            faList.add(fa);
        }
        
        if(faList.size() > 0) {
            List<Database.DeleteResult> fadele = Database.delete(faList, false);
            Integer index = 0;
            for(Database.DeleteResult dsr : fadele){
                if(!dsr.isSuccess()){
                    String errMsg = dsr.getErrors()[0].getMessage();
                    errorMap.put(faList[index].Id, errMsg);
                }
                index++;
            }
        }
        
        // Deleting related memos data
        List<Memo__c> memlist = new List<Memo__c>();
        for(Financial_Account__c mem : scope){
            if(!mem.Memos__r.isEmpty())
                memlist.addAll(mem.Memos__r);
        }
        
        if(memlist.size() > 0) {
            List<Database.DeleteResult> memdele = Database.delete(memlist, false);
            Integer index = 0;
            for(Database.DeleteResult dsr : memdele){
                if(!dsr.isSuccess()){
                    String errMsg = dsr.getErrors()[0].getMessage();
                    errorMap.put(memlist[index].Id, errMsg);
                }
                index++;
            }
        }
        
        // Deleting related prime action logs data
        List<Prime_Action_Log__c> primelist = new List<Prime_Action_Log__c>();
        for(Financial_Account__c prime : scope){
            if(!prime.Prime_Actions_Log__r.isEmpty())
                primelist.addAll(prime.Prime_Actions_Log__r);
        }
        
        if(primelist.size() > 0) {
            List<Database.DeleteResult> primedele = Database.delete(primelist, false);
            Integer index = 0;
            for(Database.DeleteResult dsr : primedele){
                if(!dsr.isSuccess()){
                    String errMsg = dsr.getErrors()[0].getMessage();
                    errorMap.put(primelist[index].Id, errMsg);
                }
                index++;
            }
        }
        
        // Deleting related cross sell response data
        List<Cross_Sell_Response__c> crosellist = new List<Cross_Sell_Response__c>();
        for(Financial_Account__c crsell : scope){
            if(!crsell.Cross_Sell_Responses__r.isEmpty())
                crosellist.addAll(crsell.Cross_Sell_Responses__r);
        }
        
        if(crosellist.size() > 0) {
            List<Database.DeleteResult> crsdele = Database.delete(crosellist, false);
            Integer index = 0;
            for(Database.DeleteResult dsr : crsdele){
                if(!dsr.isSuccess()){
                    String errMsg = dsr.getErrors()[0].getMessage();
                    errorMap.put(crosellist[index].Id, errMsg);
                }
                index++;
            }
        }
        
        // Deleting related Retention response data
        List<Retention_Response__c> retentionlist = new List<Retention_Response__c>();
        for(Financial_Account__c retention : scope){
            if(!retention.Retention_Responses__r.isEmpty())
                retentionlist.addAll(retention.Retention_Responses__r);
        }
        
        if(retentionlist.size() > 0) {
            List<Database.DeleteResult> retentiondele = Database.delete(retentionlist, false);
            Integer index = 0;
            for(Database.DeleteResult dsr : retentiondele){
                if(!dsr.isSuccess()){
                    String errMsg = dsr.getErrors()[0].getMessage();
                    errorMap.put(retentionlist[index].Id, errMsg);
                }
                index++;
            }
        }
        
        // Deleting related Manage_Campaigns data
        List<Manage_Campaign__c> camplist = new List<Manage_Campaign__c>();
        for(Financial_Account__c camp : scope){
            if(!camp.Manage_Campaigns__r.isEmpty())
                camplist.addAll(camp.Manage_Campaigns__r);
        }
        
        if(camplist.size() > 0) {
            List<Database.DeleteResult> campagdele = Database.delete(camplist, false);
            Integer index = 0;
            for(Database.DeleteResult dsr : campagdele){
                if(!dsr.isSuccess()){
                    String errMsg = dsr.getErrors()[0].getMessage();
                    errorMap.put(camplist[index].Id, errMsg);
                }
                index++;
            }
        }
        
    } 
    
     global void execute(SchedulableContext sc)
    {
        DeleteFinanceRelatedData d=new DeleteFinanceRelatedData();
        Database.executeBatch(d, 200);
    }
    
    global void finish(Database.BatchableContext BC){
        if(!errorMap.isEmpty()){
            string myid = 'ecsfdc.in@capgemini.com';
            AsyncApexJob a = [SELECT id, ApexClassId,JobItemsProcessed, TotalJobItems,NumberOfErrors, CreatedBy.Email FROM AsyncApexJob WHERE id = :BC.getJobId()];
            String body = 'Hi,\n \n' + 'The batch job ' + 'DeleteFinanceRelatedData																								 ' + 'has finished. \n' + 'There were ' + errorMap.size() + ' errors. Please find the error list attached. \n \n Thanks,';
            
            // Creating the CSV file
            String finalstr = 'ObjectName,Id, Error \n';
            String subject = 'Financial_Account - Apex Batch Error List';
            String attName = 'Delete Financial_Account Errors.csv';
            for(Id id  : errorMap.keySet()){
                string err = errorMap.get(id);
                String sob= id.getSObjectType().getDescribe().getName();
                string recordString = '"'+sob+'","'+id+'","'+err+'"\n';
                finalstr = finalstr +recordString;
            } 
            
            // Define the email
            Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage(); 
            
            // Create the email attachment    
            Messaging.EmailFileAttachment efa = new Messaging.EmailFileAttachment();
            efa.setFileName(attName);
            efa.setBody(Blob.valueOf(finalstr));
            
            // Sets the paramaters of the email
            email.setSubject( subject );
            email.setToAddresses( new String[] {myid} );
            email.setPlainTextBody( body );
            email.setFileAttachments(new Messaging.EmailFileAttachment[] {efa});
            
            // Sends the email
            Messaging.SendEmailResult [] r = Messaging.sendEmail(new Messaging.SingleEmailMessage[] {email});   
        }
    } 
}