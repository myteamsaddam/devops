/*******************************************************************************************************************************
Purpose of the calss : To increase the code coverage of "SaveDeskController" Trigger
           Created by: Rajesh Mohandoss
           Created on:04-OCT-2018
********************************************************************************************************************************/
@isTest(SeeAllData=false)
public class SaveDeskControllerTest
{
   /* @testSetup static void setup() {
 
          // Create common test accounts
          Account accountObj=new Account(
          Name='Test Account', 
          Institution_Id__c=2, 
          Customer_Serno__c='1123123'
          );
          insert accountObj;
          
          Blob hash = Crypto.generateDigest('MD5', Blob.valueOf('0000000061'));
          string encryptSSN=EncodingUtil.convertToHex(hash);
          Contact contactObj =new Contact(
          LastName='Test Contact', 
          FirstName='First Test Contact',
          SerNo__c='2123123', 
          Institution_Id__c=2, 
          SSN__c='0000000061',
          Encrypted_SSN__c=encryptSSN,
          AccountId=accountObj.Id,
          Phone='123123123', 
          Fax='98789879', 
          MobilePhone='98789879', 
          HomePhone='123123123', 
          Email='testemail@test.com'
          );
          insert contactObj;
          
          Blob hash2 = Crypto.generateDigest('MD5', Blob.valueOf('0000000062'));
          string encryptSSN2=EncodingUtil.convertToHex(hash2);
          Contact contactObj1 =new Contact(
          LastName='1Test Contact', 
          FirstName='1First Test Contact',
          SerNo__c='12123123', 
          Institution_Id__c=2, 
          SSN__c='0000000062',
          Encrypted_SSN__c=encryptSSN2,
          AccountId=accountObj.Id,
          Phone='1123123123', 
          Fax='198789879', 
          MobilePhone='198789879', 
          HomePhone='1123123123', 
          Email='1testemail@test.com'
          );
          insert contactObj1;
            
          Product_Custom__c prodobj=new Product_Custom__c(
          Name='Test Product',
          Pserno__c='56789'
          );
          insert prodobj;
          
          Financial_Account__c finAccObj=new Financial_Account__c(
          Account_Number__c='12w23eeeeedd', 
          Customer__c=accountObj.Id, 
          Account_Serno__c='3123123', 
          Institution_Id__c=2,
          Customer_Serno__c=contactObj.SerNo__c, 
          Product__c=prodobj.Id,
          Product_Serno__c=prodobj.Pserno__c
          );
          insert finAccObj;
        
     }

     public static testMethod void testUpdateSaveDeskPhoneNo() {
         Contact c=[Select id,SerNo__c,MobilePhone from Contact limit 1];
         Financial_Account__c facc=[Select id,Account_Serno__c from Financial_Account__c limit 1];
          
          SaveDeskQueue__c sdq1=new SaveDeskQueue__c(
          Campaign_Type__c='BWO',
          Case_Number__c='0000001',
          Comments__c='Comments',
          People_Serno__c=c.SerNo__c,
           Institution_Id__c='1',
          Customer_Name__c=c.id,
          Financial_Account__c=facc.id,
          Financial_Account_Serno__c=facc.Account_Serno__c
          );
          insert sdq1;
           
          SaveDeskQueue__c sdq2=new SaveDeskQueue__c(
          Campaign_Type__c='BWO',
          Case_Number__c='0000001',
          Comments__c='Comments',
          People_Serno__c=c.SerNo__c,
           Institution_Id__c='2',
          Customer_Name__c=c.id,
          Financial_Account__c=facc.id,
          Financial_Account_Serno__c=facc.Account_Serno__c
          );
          insert sdq2;
          
          SaveDeskQueue__c sdq3=new SaveDeskQueue__c(
          Campaign_Type__c='BWO',
          Case_Number__c='0000001',
          Comments__c='Comments',
          People_Serno__c=c.SerNo__c,
           Institution_Id__c='3',
          Customer_Name__c=c.id,
          Financial_Account__c=facc.id,
          Financial_Account_Serno__c=facc.Account_Serno__c
          );
          insert sdq3;
          
          List<SaveDeskQueue__c> sdqList = [SELECT Campaign_Type__c,Case_Number__c,Comments__c,Financial_Account__c,Customer_Name__c,Institution_Id__c FROM SaveDeskQueue__c LIMIT 20];
          SaveDeskQueue__c sdq = [SELECT Campaign_Type__c,Case_Number__c,Comments__c,Financial_Account__c,Customer_Name__c,Institution_Id__c FROM SaveDeskQueue__c LIMIT 1];
          ApexPages.StandardSetController ssc = new ApexPages.StandardSetController(sdqList );
          PageReference newpage= Page.SaveDeskQueuePage;  
          Test.setCurrentPage(newpage);
          ApexPages.StandardController sc=new ApexPages.StandardController(sdq);
          SaveDeskController sdeskcon=new SaveDeskController(sc);
          sdeskcon.size=5;
          sdeskcon.selectedList=sdqList;
          sdeskcon.displaySave=True;
          sdeskcon.NumberOfPages=10;
          sdeskcon.countryCode='1';
          sdeskcon.CampgnType='1BWO';
          sdeskcon.statusCode='All';
          sdeskcon.getCampaignTypes();
          sdeskcon.getstatusCodes();
          sdeskcon.getcountryCodes();
          sdeskcon.displaySave=false;
          sdeskcon.SetSaveDeskQueue=ssc;
          sdeskcon.wrapperlist=sdeskcon.getSaveDeskQueue();
          sdeskcon.SetSaveDeskQueue=ssc;
          sdeskcon.ShowDisplay();
          sdeskcon.SetSaveDeskQueue=ssc;
          sdeskcon.getSelectedSaveDeskQueue();
          sdeskcon.fetchData();
          sdeskcon.SaveRec1();
          sdeskcon.CancelRec();
          sdeskcon.SaveRec();
          sdeskcon.clickMe();
          sdeskcon.getSelectedSaveDeskQueue();
          sdeskcon.setCallAttachedData();
          sdeskcon.setCallEndData();
          sdeskcon.save();
          sdeskcon.saveaction();
          //List <SaveDeskWrapperClass> wrapperlist = new List <SaveDeskWrapperClass>();
          
           //SaveDeskController sdeskcon1=new SaveDeskController(ssc);

    }*/
}