@isTest
private class NoDeleteonTask_test{
    static testMethod void testNoDeleteonTask() {
    Boolean errorThrown = false;
    Profile Prof = [select id from profile where name='SE - Operations']; 
    User testuser = new User(alias = 'stests', email='testuser@test.com',emailencodingkey='UTF-8',FirstName='sittest',
                       lastname='user',languagelocalekey='en_US',localesidkey='en_US',profileid = Prof.Id,
                       timezonesidkey='America/Los_Angeles',username='testuser@test.com.sit');
    System.runAs(testuser ) {                    
    Task tasks = new Task(Subject='Test',Status='New',Priority='Normal',CallType='Outbound');
    test.startTest();
        insert tasks;
    test.stopTest();
    
    // Verify that  error is thrown when the task is deleted.
    try {
            delete tasks;
            errorThrown = true;
    } catch (Exception e) {
         
    }
        //System.assert(errorThrown);
    
    }
    }
    }