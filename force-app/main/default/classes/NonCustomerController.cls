Public class NonCustomerController {
    Public String SelectedInstId {set;get;}
    Public string Firstname {set;get;}
    Public String Lastname {set;get;}
    Public String Address1 {set;get;}
    Public String City {set;get;}
    Public String Zip {set;get;}
    Public String SelectPartner {set;get;}
    public String selectIdentityType {set;get;}
    public String IdentifierValue {set;get;}
    public String UnionName {set;get;}
    public String DepartmentNumber {set;get;}
    Public List<Selectoption> IdentityType{set;get;}
    Public List<Selectoption> InstIds {set;get;}
    Public List<SelectOption> option {set;get;}
    Public string code {set;get;}
    Public String description {set;get;}
    
    
    public NonCustomerController(){
        option = new List<selectOption>();
        option.add(new SelectOption('--None--', '--None--'));
    }
    
    public List<SelectOption> getInstitutionIds(){
        InstIds = new List<selectOption>();
        InstIds.add(new SelectOption('--None--', '--None--'));
        InstIds.add(new SelectOption('1', 'Norway'));
        InstIds.add(new SelectOption('2', 'Sweden'));
        InstIds.add(new SelectOption('3', 'Denmark'));
        
        return InstIds;
    }
    
    public List<SelectOption> getIdentifierType(){
        IdentityType = new List<selectOption>();
        IdentityType.add(new SelectOption('--None--', '--None--'));
        IdentityType.add(new SelectOption('SSN', 'SSN'));
        IdentityType.add(new SelectOption('Membership Number', 'Membership Number'));
        IdentityType.add(new SelectOption('Email', 'Email'));
        IdentityType.add(new SelectOption('Mobile', 'Mobile'));
        
        return IdentityType;
    }
    
    public PageReference getPartnerNames(){
        System.debug('---SelectedInstId--'+SelectedInstId);
        GetPartnerInputs getPartnerServiceInput = new GetPartnerInputs();
        system.debug('----getPartnerServiceInput-----'+getPartnerServiceInput);
        
        try{
            if(SelectedInstId != '--None--')
            {
                option.clear();
                option.add(new SelectOption('--None--', '--None--'));
                System.debug('---SelectedInstId--'+SelectedInstId);
                getPartnerServiceInput.InstitutionId = SelectedInstId;
                
                GetPartnerOutputs getPartnerServiceOutput = CSSServiceHelper.getPartnerDetails(getPartnerServiceInput);
                system.debug('--------getPartnerServiceOutput----->'+getPartnerServiceOutput);
                
                if(getPartnerServiceOutput !=null && getPartnerServiceOutput.GetPartnerResponseRecord != null) //modify later
                {
                    for(GetPartnerOutputs.GetPartnerResponseRecordType partns : getPartnerServiceOutput.GetPartnerResponseRecord){
                        option.add(new SelectOption(partns.PartnerID, partns.PartnerName)); //partns.PartnerID
                    }
                }
            } else{
                option.clear();
                option.add(new SelectOption('--None--', '--None--'));
            }
            
        } catch(dmlException e)   {
            system.debug('--------Getpartner details-41----->'+'getPartnerDetails: ' + SelectedInstId);
            system.debug('------Get exception-42----->'+e);
            StatusLogHelper.logSalesforceError('GetPartnerDetails', 'E005', 'GetPartnerDetails: ' + SelectedInstId,e, true);
        }
        catch(calloutException e)   {
            system.debug('--------Getpartner details-47----->'+'getPartnerDetails: ' + SelectedInstId);
            system.debug('------Get exception-48----->'+e);
            StatusLogHelper.logSalesforceError('GetPartnerDetails', 'E001', 'GetPartnerDetails: ' + SelectedInstId,e, true);
        }
        catch(Exception e)   {
            system.debug('--------Getpartner details-52----->'+'GetPartnerDetails: ' + SelectedInstId);
            system.debug('------Get exception-53----->'+e);
            StatusLogHelper.logSalesforceError('GetPartnerDetails', 'E004', 'GetPartnerDetails: ' + SelectedInstId,e, true);
        }
        
        return null;
    }
    
    Public PageReference insertNonCustomer(){
        System.debug('---SelectedInstId--'+SelectedInstId);
        System.debug('---SelectPartner--'+SelectPartner);
        System.debug('---Address--'+Address1);
        System.debug('---DepartmentNumber--'+DepartmentNumber);
        System.debug('---selectIdentityType--'+selectIdentityType);
        System.debug('---UnionName--'+UnionName);
        
        GetNonCustomerInputs getNonCustomerServiceInput = new GetNonCustomerInputs();
        try{
            
            if(Firstname == '' || Lastname ==''|| City =='' || Zip ==''|| SelectedInstId == '--None--' || SelectPartner == '--None--' || selectIdentityType == '--None--' || IdentifierValue == ''){
                if(Firstname =='')
                    ApexPages.addmessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Please enter the FirstName'));
                if(Lastname =='')
                    ApexPages.addmessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Please enter the Lastname'));
                if(selectIdentityType == '--None--')
                    ApexPages.addmessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Please Select IdentifierType'));
                if(IdentifierValue=='')
                    ApexPages.addmessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Please enter the IdentifierValue'));
                if(City =='')
                    ApexPages.addmessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Please enter the City'));
                if(Zip =='')
                    ApexPages.addmessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Please enter the ZipCode'));
                if(SelectedInstId == '--None--')
                    ApexPages.addmessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Please Select InstitutionId'));
                if(SelectPartner == '--None--')
                    ApexPages.addmessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Please Select SelectPartner'));
                if(selectIdentityType == 'SSN' || selectIdentityType == 'Mobile' || selectIdentityType == 'Membership Number'){
                    if(!IdentifierValue.isNumeric())
                        ApexPages.addmessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Please Enter the valid number'));
                }
                if(selectIdentityType == 'Email'){
                    if(!Pattern.matches('[a-zA-Z0-9._-]+@[a-zA-Z]+.[a-zA-Z]{2,4}[.]{0,1}[a-zA-Z]{0,2}', IdentifierValue))
                        ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error, 'Please Enter the valid Email Address')); 
                }
                return null;
            }
            
            getNonCustomerServiceInput.InstitutionId = SelectedInstId;    
            getNonCustomerServiceInput.FirstName = Firstname;
            getNonCustomerServiceInput.LastName = Lastname;
            getNonCustomerServiceInput.City = City;
            getNonCustomerServiceInput.Zip = Zip;
            getNonCustomerServiceInput.Address1 = Address1;
            getNonCustomerServiceInput.PartnerId = Integer.valueOf(SelectPartner);
            getNonCustomerServiceInput.IdentifierType = selectIdentityType;
            getNonCustomerServiceInput.IdentifierValue = IdentifierValue;
            getNonCustomerServiceInput.UnionName = UnionName;  
            getNonCustomerServiceInput.DepartmentNumber = DepartmentNumber;
            system.debug('---After enter getNonCustomerServiceInput'+getNonCustomerServiceInput.DepartmentNumber);
            
            
            
            GetNonCustomerOutputs getNonCustomerserviceOutputs = CSSServiceHelper.getNonCustomer(getNonCustomerServiceInput);
            system.debug('-------getNonCustomerserviceOutputs-->'+getNonCustomerserviceOutputs);
            
            if(getNonCustomerserviceOutputs != null){
                
                code = getNonCustomerserviceOutputs.Code;
                Description = getNonCustomerserviceOutputs.Description;
            }
            
            System.debug('-------code-------'+code);
            system.debug('-----Description----'+Description);
            /*PageReference pageRefresh = new PageReference(ApexPages.currentPage().getUrl());
pageRefresh.setRedirect(false);
return null;*/
            
            if(Description != ''){
                
                ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.CONFIRM,'Record created '+ Description + 'fully');
                ApexPages.addMessage(myMsg);
            }
            
            /*PageReference pageRefresh = new PageReference(ApexPages.currentPage().getUrl());
pageRefresh.setRedirect(true);
return pageRefresh;*/
            
        }catch(dmlException e)   {
            system.debug('--------GetNonCustomerDetails-41----->'+'GetNonCustomerDetails: ' + getNonCustomerServiceInput);
            system.debug('------Get exception-42----->'+e);
            StatusLogHelper.logSalesforceError('GetNonCustomerDetails', 'E005', 'GetNonCustomerDetails: ' + getNonCustomerServiceInput,e, true);
        }
        catch(calloutException e)   {
            system.debug('--------GetNonCustomerDetails -47----->'+'GetNonCustomerDetails: ' + getNonCustomerServiceInput);
            system.debug('------Get exception-48----->'+e);
            StatusLogHelper.logSalesforceError('GetNonCustomerDetails', 'E001', 'GetNonCustomerDetails: ' + getNonCustomerServiceInput,e, true);
        }
        catch(Exception e)   {
            system.debug('--------GetNonCustomerDetails-52----->'+'GetNonCustomerDetails: ' + getNonCustomerServiceInput);
            system.debug('------Get exception-53----->'+e);
            StatusLogHelper.logSalesforceError('GetNonCustomerDetails', 'E004', 'GetNonCustomerDetails: ' + getNonCustomerServiceInput,e, true);
        }
        
        return null;
    } 
}