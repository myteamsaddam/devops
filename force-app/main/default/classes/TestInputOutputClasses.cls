@isTest
public class TestInputOutputClasses
{
    public static testmethod void testCLIApplicationServiceInput() 
    {
        test.startTest();
            GetInstallInfoInput obj = new GetInstallInfoInput();
            obj.test();
        
            GetInstallInfoOutput obj1 = new GetInstallInfoOutput();
            obj1.test();
            
            ViewTransactionsHistoryOutput obj2 = new ViewTransactionsHistoryOutput();
            obj2.test();
            
            GetLimitChangesServiceInput obj3 = new GetLimitChangesServiceInput();
            obj3.test();
            
            GetLimitChangesServiceOutput obj4 = new GetLimitChangesServiceOutput();
            obj4.test();
            
            ViewInsuranceServiceInput obj5 = new ViewInsuranceServiceInput();
            obj5.test();
            
            ViewInsuranceServiceOutput obj6 = new ViewInsuranceServiceOutput();
            obj6.test();
            
            GetConsentServiceInput obj7 = new GetConsentServiceInput();
            obj7.test();
            
            UpdateConsentServiceInput obj8 = new UpdateConsentServiceInput();
            obj8.test();
            
            UpdateConsentServiceOutput obj9 = new UpdateConsentServiceOutput();
            obj9.test();
            
            GetConsentServiceOutput obj10 = new GetConsentServiceOutput();
            obj10.test();
            
            CLIApplicationServiceOutput obj11 = new CLIApplicationServiceOutput();
            obj11.test();
            
            UpdateCustomerDataOutput obj12 = new UpdateCustomerDataOutput();
            obj12.test();
            
            PayBackCreditBalanceOutput obj27 = new PayBackCreditBalanceOutput ();
            obj27.test();
            
            ViewTransactionsHistoryInput obj26 = new ViewTransactionsHistoryInput ();
            obj26.test();
            
            GetCustomerOutput obj25 = new GetCustomerOutput ();
            obj25.test();
            
            GetCustomerInput obj24 = new GetCustomerInput ();
            obj24.test();
            
            UpdateCustomerDataInput obj23 = new UpdateCustomerDataInput ();
            obj23.test();
            
            PayBackCreditBalanceInput obj22 = new PayBackCreditBalanceInput ();
            obj22.test();
            
            GetStatementHistoryOutput obj18 = new GetStatementHistoryOutput ();
            obj18.test();
            
            UpdateCustomerAddressOutput obj17 = new UpdateCustomerAddressOutput ();
            obj17.test();
            
            UpdateCustomerAddressInput obj16 = new UpdateCustomerAddressInput ();
            obj16.test();
            
            //SetMTPRequestOutput obj15= new SetMTPRequestOutput ();
            //obj15.test();
            
            //SetMTPRequestInput obj14 = new SetMTPRequestInput ();
           // obj14.test();
            
            GetStatementHistoryInput obj13 = new GetStatementHistoryInput ();
            obj13.test();
            
            GetLoanServiceOutput objOLoans=new GetLoanServiceOutput();
            objOLoans.test();
            
            GetLoanServiceInput objILoans=new GetLoanServiceInput();
            objILoans.test();
        test.StopTest();
              
    }
}