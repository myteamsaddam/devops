public class TransactionDetailsContrl {
    
    @AuraEnabled
    Public Static List<DCMS_Transaction__c> getTrans(Id CasId){
        List<DCMS_Transaction__c> TransList = [Select id,AuthDateTime__c, Name, Truncated_Card_Number__c,TransactionAmount__c,BillingAmount__c,TransactionCurrency__c,Account_Serno__c,MerchTypeMCC__c,MerchName__c,MerchID__c,MerchCountry__c,MerchCity__c,PosEntryMode__c,Chargeback__c,Representment__c,Arbitration__c,WriteOff__c,FraudWriteOff__c,CH_Liable__c,Recovery__c from DCMS_Transaction__c where CaseNumber__c = :CasId];
        return TransList;
    }
    
    @AuraEnabled
    Public Static String deleteTxns(List<String> lstRecordId){
        Id CsownerId;
        List <String> oErrorMsg = new List <String>();
        system.debug('----Txns-Id----'+lstRecordId);
        List<DCMS_Transaction__c> lstDeletRec = [Select id,Chargeback__c, Representment__c,Arbitration__c, WriteOff__c, FraudWriteOff__c, CH_Liable__c, Recovery__c, CaseNumber__r.OwnerId from DCMS_Transaction__c where Id IN: lstRecordId];
        if(lstDeletRec.size() > 0){
            for(DCMS_Transaction__c txns : lstDeletRec){
                CsownerId = txns.CaseNumber__r.OwnerId;
                if(txns.Chargeback__c == true || txns.Representment__c == true || txns.Arbitration__c == true || txns.WriteOff__c == true || txns.FraudWriteOff__c == true || txns.Recovery__c == true || txns.CH_Liable__c == true){
                    txns.Chargeback__c = false;
                    txns.Representment__c = false;
                    txns.Arbitration__c = false;
                    txns.WriteOff__c = false;
                    txns.FraudWriteOff__c = false;
                    txns.CH_Liable__c = false;
                    txns.Recovery__c = false;
                }
            }
        }else{
            oErrorMsg.add('No Transaction got selected');
        }
        if(CsownerId == userInfo.getUserId()){
            update lstDeletRec;
            system.debug('------txns-update--'+lstDeletRec);
            Database.DeleteResult[] DleRec = Database.delete(lstDeletRec, false);
            
            for(Database.DeleteResult dr : DleRec){
                if(dr.isSuccess()){
                    oErrorMsg.add('Del');
                    system.debug('successful delete Txns');
                } else {
                    oErrorMsg.add('');
                    for (Database.Error err: dr.getErrors()) {
                        
                        oErrorMsg.add(err.getStatusCode() + ': ' + err.getMessage());
                    }
                }
            }
        } else {
            oErrorMsg.add('Your not owner of the Case');
        }
        return oErrorMsg[0];
    }
    
    @AuraEnabled
    Public Static List<String> updateTxns(Id TxnId, String chekboxval){
        Boolean flag =true;
        Id CsownerId;
        List <String> oErrorMsg = new List <String>();
        system.debug('--TxnId---'+TxnId);
        system.debug('--chekboxval---'+chekboxval);
        
        List<DCMS_Transaction__c> txnlist = [Select id,Chargeback__c, Representment__c,Arbitration__c, WriteOff__c, FraudWriteOff__c, CH_Liable__c, Recovery__c, CaseNumber__r.OwnerId from DCMS_Transaction__c where Id = :TxnId];
        system.debug('---txnlist--'+txnlist);
        
        for(DCMS_Transaction__c txn : txnlist){
            CsownerId = txn.CaseNumber__r.OwnerId;
            if(CsownerId != userInfo.getUserId()){
                oErrorMsg.add('Sorry, you are not the owner of Case. Please accept the case and try again.');
                break;
            }
            
            system.debug('--Chargeback__c---'+txn.Chargeback__c + '---chekboxval---'+chekboxval);
            if(chekboxval == 'cb' && txn.Chargeback__c == false){
                txn.Chargeback__c = true; 
            } else if(chekboxval == 'cb' && txn.Chargeback__c == true && txn.Representment__c == false){
                txn.Chargeback__c = false;
            }
            system.debug('--Representment__c---'+txn.Representment__c + + '---chekboxval---'+chekboxval);
            if(chekboxval == 'REP' &&  txn.Representment__c == false && txn.Chargeback__c == true){
                txn.Representment__c = true;  
            } else if(chekboxval == 'REP' &&  txn.Representment__c == true && txn.Arbitration__c == false){
                txn.Representment__c = false; 
            } else if(chekboxval == 'REP' &&  txn.Representment__c == true && txn.Arbitration__c == true && txn.Chargeback__c == true){
                flag = false;
                oErrorMsg.add('Sorry, Not Possible to unselect REP if ARB is selected');
            }else if((chekboxval == 'REP' && txn.Representment__c == false && txn.Chargeback__c == false)){
                flag = false;
                oErrorMsg.add('Sorry, you can not select REP without CB');
            } else if(chekboxval == 'cb' && txn.Chargeback__c == true && (txn.Representment__c == true || txn.Arbitration__c == true)){
                flag = false;
                oErrorMsg.add('Sorry, Not Possiable to uncheck if REP/ARB is checked');
            }
            if(chekboxval == 'ARB' && txn.Arbitration__c == false && txn.Representment__c == true && txn.Chargeback__c == true){
                txn.Arbitration__c = true; 
            } else if(chekboxval == 'ARB' && txn.Arbitration__c == true && txn.Representment__c == true && txn.Chargeback__c == true) {
                txn.Arbitration__c = false;
            }
            else if(chekboxval == 'ARB' && txn.Arbitration__c == false && (txn.Representment__c == false || txn.Chargeback__c == false)){
                flag = false;
                oErrorMsg.add('Sorry, You can not select ARB without CB and REP');
            }
            
            System.debug('--chekboxval--'+chekboxval+'--FraudWriteOff__c---'+txn.FraudWriteOff__c+'----Chargeback__c----'+txn.Chargeback__c+'----Representment__c----'+txn.Representment__c);
            system.debug('----Arbitration__c-----'+txn.Arbitration__c);
            
            if(chekboxval == 'FWO' && txn.FraudWriteOff__c == false && ((txn.Chargeback__c == true && txn.Representment__c == true && txn.Arbitration__c == false) || (txn.Chargeback__c == false) || (txn.Chargeback__c == true && txn.Representment__c == true && txn.Arbitration__c == true))){
                txn.FraudWriteOff__c = true; 
            } else{
                txn.FraudWriteOff__c = false;
            }
            
            if(chekboxval == 'CHL' && txn.CH_Liable__c == false){
                txn.CH_Liable__c = true;
            } else {
                txn.CH_Liable__c = false;
            }
            
            if(chekboxval == 'WO' && txn.WriteOff__c == false){
                txn.WriteOff__c = true;
            } else {
                txn.WriteOff__c = false;
            }
            
            
            if(chekboxval == 'REC' && (txn.Recovery__c == false && txn.Chargeback__c == false && txn.Representment__c == false && txn.Arbitration__c == false && txn.FraudWriteOff__c == false && txn.WriteOff__c == false)){
                txn.Recovery__c = true; 
            } else {
                txn.Recovery__c = false;
            }
        }
        
        if((chekboxval == 'cb' || chekboxval == 'REP' || chekboxval == 'ARB' || chekboxval == 'FWO' || chekboxval == 'WO' || chekboxval == 'REC' || chekboxval == 'CHL') && flag && CsownerId == userInfo.getUserId()){
            Database.SaveResult[] UpdtRec = Database.update(txnlist, false);
            for(Database.SaveResult dr : UpdtRec){
                if(dr.isSuccess()){
                    system.debug('successful update txns');
                } else {
                    oErrorMsg.add('');
                    for (Database.Error err: dr.getErrors()) {
                        
                        oErrorMsg.add(err.getStatusCode() + ': ' + err.getMessage());
                    }
                }
            }
        } else {
            // oErrorMsg.add('Your not owner of the Case');
        }
        system.debug('-----txnlist-after---'+txnlist);
        return oErrorMsg;
    }
    @AuraEnabled
    public Static Decimal getGross(ID CasId)
    {
        DCMS_FraudCase__c fruadlist = [Select id, TotalFraudAmt__c from DCMS_FraudCase__c where Case_Number__c = :CasId];
        return fruadlist.TotalFraudAmt__c;
    }
    
    @AuraEnabled
    public Static List<Decimal> getTotalRisk(ID CasId)
    {
        List<Decimal> amnt = new List<Decimal>();
        DCMS_FraudCase__c res=[Select id, TotalCb__c,FraudWO__c,NonFraudWO__c,TotalFraudAmt__c,TotalRefund__c,TotalToCh__c,TotalAtRisk__c from DCMS_FraudCase__c where Case_Number__c = :CasId];
        system.debug('--res--'+res);
        amnt.add(res.TotalAtRisk__c);
        amnt.add(res.TotalCb__c);
        amnt.add(res.TotalRefund__c);
        amnt.add(res.TotalToCh__c);
        amnt.add(res.NonFraudWO__c);
        amnt.add(res.FraudWO__c);
        system.debug('-----amnt-----'+amnt);
        return amnt;        
    } 
    
    @AuraEnabled
    Public Static String updateChliable(Id caseId, Decimal chLiableValue){
        system.debug('----Case-Id----'+caseId);
        try{
        List<DCMS_FraudCase__c> dcmsFraudCaseList = [Select Id,TotalToCh__c,
                                                     Case_Number__r.ownerId 
                                                     from DCMS_FraudCase__c WHERE Case_Number__c = :caseId];
        system.debug('--dcmsFraudCaseList---'+dcmsFraudCaseList[0].TotalToCh__c);
        List<DCMS_FraudCase__c> updateFraudCaseChList = new List<DCMS_FraudCase__c>();
        if(dcmsFraudCaseList.size() > 0
           && chLiableValue != null){
               if(dcmsFraudCaseList[0].Case_Number__r.ownerId != userInfo.getUserId()){
                   return 'Please accept the case before enter the C/H Liable amount';
               }else{
                   dcmsFraudCaseList[0].TotalToCh__c = chLiableValue;
                   updateFraudCaseChList.add(dcmsFraudCaseList[0]);
                   update updateFraudCaseChList;
               }
           } 
        }catch(Exception e){
            StatusLogHelper.logSalesforceError('updateChLiable', 'E004', 'Error in ChLiableAmount', e, false);
        }
        return null;
    }
    
    @AuraEnabled
    Public Static String getCaseOwnerId(Id CasId){
        String caseOwnerId = [Select id, OwnerId from Case where id = :CasId].OwnerId;
        return caseOwnerId;
    }
}