/**********************************************************************
Name: EC_CTI_LogACallController_Test
=======================================================================
Purpose: This is the test class for EC_CTI_LogACallController SFD-1254

======================================================================= 
History
-----------------------------------------------------------------------         
Version     Author                   Date              Detail
1.0         Saddaam Hussain         9-Jul-19       Initial Version

**********************************************************************/
@isTest 
public class EC_CTI_LogACallController_Test {
    
    static testMethod void testmthd1() {  
        System.runAs(EC_UnitTestDataGenerator.adminUser) {
            Account accountObj = EC_UnitTestDataGenerator.TestAccount.buildInsert(new Map<String, Object>{
            });
            
            Blob hash = Crypto.generateDigest('MD5', Blob.valueOf('0000000061'));
            string encryptSSN=EncodingUtil.convertToHex(hash);
            Contact contactObj = EC_UnitTestDataGenerator.TestContact.buildInsert(new Map<String, Object>{
                'AccountId'=>accountObj.Id, 
                    'Encrypted_SSN__c'=>encryptSSN
                    });
            
            Product_Custom__c prodobj = EC_UnitTestDataGenerator.TestProductCustom.buildInsert();
            
            Financial_Account__c finAccObj = EC_UnitTestDataGenerator.TestFinancialAccount.buildInsert(new Map<String, Object>{
                'Customer__c'=>accountObj.Id,
                    'Customer_Serno__c'=>contactObj.SerNo__c, 
                    'Product__c'=>prodobj.Id,
                    'Product_Serno__c'=>prodobj.Pserno__c
                    });
            task taskObj =new task(subject='subject', 
                                   status='Completed',Category__c='Test', type='Complaint',
                                   Comments__c='', CallType = 'Inbound', WhoId = contactObj.Id,
                                   CreatedById = UserInfo.getUserId()
                                  ); 
            List<Task> taskList = new List<Task>();
            taskList.add(taskObj);
            insert taskList;
            
            EC_CTI_LogACallController.updateCtiTask('', '', contactObj.Id, 
                                                    '', 'Testing', 'Complaint', false, true, '','','','');
            EC_CTI_LogACallController.updateCtiTask('', '', contactObj.Id, 
                                                    'Account closure', 'Testing', 'Complaint', false, true, '','','','');
        }
    }
    
    static testMethod void testmthd2() {  
        System.runAs(EC_UnitTestDataGenerator.adminUser) {
            Account accountObj = EC_UnitTestDataGenerator.TestAccount.buildInsert(new Map<String, Object>{
            });
            
            Blob hash = Crypto.generateDigest('MD5', Blob.valueOf('0000000061'));
            string encryptSSN=EncodingUtil.convertToHex(hash);
            Contact contactObj = EC_UnitTestDataGenerator.TestContact.buildInsert(new Map<String, Object>{
                'AccountId'=>accountObj.Id, 
                    'Encrypted_SSN__c'=>encryptSSN
                    });
            Product_Custom__c prodobj = EC_UnitTestDataGenerator.TestProductCustom.buildInsert();
            
            Financial_Account__c finAccObj = EC_UnitTestDataGenerator.TestFinancialAccount.buildInsert(new Map<String, Object>{
                'Customer__c'=>accountObj.Id,
                    'Customer_Serno__c'=>contactObj.SerNo__c, 
                    'Product__c'=>prodobj.Id,
                    'Product_Serno__c'=>prodobj.Pserno__c
                    });
            
            Card__c cardObj = EC_UnitTestDataGenerator.TestCard.buildInsert(new Map<String, Object>{
                'People__c'=>contactObj.Id,
                    'Financial_Account__c'=>finAccObj.Id, 
                    'Product__c'=>prodobj.Id,
                    'Prod_Serno__c'=>prodobj.Pserno__c
                    });
            List<Card__c> cardList = new List<Card__c>();
            cardList.add(cardObj);
            List<String>  cardAttr = new List<String> {'People__c','Financial_Account__c','Product__c','Prod_Serno__c'};
                List<Card__c> cardReload = EC_UnitTestDataGenerator.TestCard.reloadList(cardList,cardAttr);
            
            task taskObj =new task(subject='subject', 
                                   status='Completed',Category__c='Test', type='Complaint',
                                   Comments__c='', CallType = 'Inbound', WhoId = contactObj.Id,
                                   CreatedById = UserInfo.getUserId()
                                  ); 
            List<Task> taskList = new List<Task>();
            taskList.add(taskObj);
            insert taskList;
            
            //GlobalSettings__c
            List<GlobalSettings__c>GlobalSettingsList=new List<GlobalSettings__c>();
            GlobalSettings__c objGlobal = new GlobalSettings__c();
            objGlobal.Name = 'ServiceStrategies'; 
            objGlobal.Value__c = 'SOAPService';
            GlobalSettingsList.add(objGlobal);
            
            insert GlobalSettingsList;
            
            List<ServiceHeaders__c> serviceHeadersList =new List<ServiceHeaders__c>();
            //ServiceHeaders
            ServiceHeaders__c objServiceHeaders = new ServiceHeaders__c();
            objServiceHeaders.name = 'CustomerMemo';
            serviceHeadersList.add(objServiceHeaders);
            
            insert serviceHeadersList;
            
            //ServiceSettings
            List<ServiceSettings__c> serviceSettingList = new List<ServiceSettings__c>();
            
            ServiceSettings__c objService = new ServiceSettings__c();
            objService.name = 'CustomerMemoExportData';
            objService.HeaderName__c = 'CustomerMemoHeader'; 
            objService.EndPoint__c = 'https://midtlb4d2pub.entercard.com/gateway/services/CSSService?wsdl';
            objService.OutputClass__c = 'CustomerMemoExportDataOutput';
            objService.ProcessingClass__c = 'CustomerMemoExportDataCallout';
            objService.Strategy__c = 'SOAPService';
            objService.LogRequest__c = false;
            objService.LogResponse__c = false;
            objService.LogWithCallout__c = false;
            objService.Input_Class__c = 'CustomerMemoExportDataInput';
            objService.Username__c='Username';
            objService.Password__c='Password';
            serviceSettingList.add(objService);
            
            ServiceSettings__c objService2 = new ServiceSettings__c();
            objService2.name = 'StatusLogService';
            objService2.HeaderName__c = 'RESTJSONOAuth'; 
            objService2.EndPoint__c = 'https://cs82.salesforce.com/services/apexrest/StatusLogRecord/';
            objService2.OutputClass__c = '';
            objService2.Operation__c = 'POST';
            objService2.ProcessingClass__c = 'StatusLogServiceIO';
            objService2.Strategy__c = 'RESTService';
            objService2.LogRequest__c = false;
            objService2.LogResponse__c = false;
            objService2.LogWithCallout__c = false;
            objService2.Input_Class__c = '';
            objService2.Username__c='';
            objService2.Password__c='';
            serviceSettingList.add(objService2);
            
            insert serviceSettingList;
            
            //Common_Settings__c
            List<Common_Settings__c> commonSettingList =new List<Common_Settings__c>();
            Common_Settings__c commonSetting =new Common_Settings__c(
                Name='CorrelationId',
                Common_Value__c='1'
            );
            commonSettingList.add(commonSetting);
            Common_Settings__c commonSetting1 =new Common_Settings__c(
                Name='RequestorId',
                Common_Value__c='Salesforce'
            );
            commonSettingList.add(commonSetting1);
            Common_Settings__c commonSetting2 =new Common_Settings__c(
                Name='SystemId',
                Common_Value__c='1'
            );
            commonSettingList.add(commonSetting2);
            insert commonSettingList;
            
            ErrorCodes__c errorCodes =new ErrorCodes__c();
            errorCodes.Name='0';
            errorCodes.ErrorDescription__c='ErrorDescription';
            errorCodes.UIDisplayMessage__c='UIDisplayMessage';
            insert errorCodes;
            
            ErrorCodes__c errorCodes1 =new ErrorCodes__c();
            errorCodes1.Name='defaultError';
            errorCodes1.ErrorDescription__c='ErrorDescription';
            errorCodes1.UIDisplayMessage__c='UIDisplayMessage';
            insert errorCodes1;
            
            EC_CTI_LogACallController.updateCtiTask(String.valueOf(finAccObj.Id), String.valueOf(cardObj.Id), contactObj.Id, 
                                                    'Account closure', 'Testing', 'Complaint', true, true, 'test','test','test','test');
            
            EC_CTI_LogACallController.getCategory();
            EC_CTI_LogACallController.getType();
            EC_CTI_LogACallController.updateCustomerName(contactObj.Id, contactObj.Id);
            EC_CTI_LogACallController.getCallType(contactObj.Id);
            EC_CTI_LogACallController.getCallType(null);
            EC_CTI_LogACallController.getFinancialDetails(contactObj.Id);
            EC_CTI_LogACallController.getCardDetails(String.valueOf(finAccObj.Id), contactObj.Id);
            EC_CTI_LogACallController.getCardDetails('Other',contactObj.Id);
            EC_CTI_LogACallController.getCardDetails('--None--',contactObj.Id);
            
            EC_CTI_LogACallController ecCtiCall =  new EC_CTI_LogACallController();
            ecCtiCall.ssn = '8112289874';

            ecCtiCall.redirctcustomer();
            ecCtiCall.ssn = '';
            ecCtiCall.redirctcustomer();
        }
    }
    
    static testMethod void testmthd3() {  
        System.runAs(EC_UnitTestDataGenerator.adminUser) {
            Case caseObj = new Case(Status = 'Working',
                                    Origin = 'Phone',Category__c = '--None--',Type = 'Incident',Description =  'test ec');
            insert caseObj;
            
            task taskObj =new task(subject='subject', 
                                   status='Completed',Category__c='Test', type='Complaint',
                                   Comments__c='', CreatedById = UserInfo.getUserId()
                                  ); 
            List<Task> taskList = new List<Task>();
            taskList.add(taskObj);
            insert taskList;
            EC_CTI_LogACallController ecCtiCall =  new EC_CTI_LogACallController();
            ecCtiCall.updateActivity(); 
        }
    }
}