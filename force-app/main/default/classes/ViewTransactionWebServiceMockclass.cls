@isTest
global class ViewTransactionWebServiceMockclass implements WebServiceMock {
   global void doInvoke(
           Object stub,
           Object request,
           Map<String, Object> response,
           String endpoint,
           String soapAction,
           String requestName,
           String responseNS,
           String responseName,
           String responseType) {
       SOAPCSSServiceResponse.viewTransactionResponseRecordType respElement= new SOAPCSSServiceResponse.viewTransactionResponseRecordType ();
       SOAPCSSServiceResponse.ViewTransactionResponse_element  transactionResponse_elementinstance  = new SOAPCSSServiceResponse.ViewTransactionResponse_element ();
       SOAPCSSServiceResponse.viewTransactionType transtype = new  SOAPCSSServiceResponse.viewTransactionType();
       SOAPCSSServiceResponse.viewTransactionType[] transtypelist = new  List<SOAPCSSServiceResponse.viewTransactionType>();
       transtype.TxSerno=123;
       transtype.Sequence=2;
       transtype.PostDate=system.today().adddays(-5);
       transtype.ReasonCode='ReasonCode';
       transtype.TxAmount=100.11;
       transtype.TxDate=system.today().adddays(-5);
       transtype.TxCurrency='TxCurrency';
       transtype.BillingAmount=100.234;
       transtype.BillingCurrency='BillingCurrency';
       transtype.Text='Text';
       transtype.OrigMsgType='OrigMsgType';
       transtype.TruncatedCardNo='TruncatedCardNo';
       transtype.CanBeSplitted=true;
       transtypelist.add(transtype);
       respElement.Number_x  = '12w23eeeeedd'; 
       respElement.TotalNoOfTx  = 15;
       respElement.Transaction_x=new List<SOAPCSSServiceResponse.viewTransactionType>();
       respElement.Transaction_x.addall(transtypelist);
       transactionResponse_elementinstance.ViewTransactionResponseRecord=respElement;
       response.put('response_x', transactionResponse_elementinstance );
    }
    
}