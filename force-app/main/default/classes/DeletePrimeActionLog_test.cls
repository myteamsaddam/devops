@isTest
public class DeletePrimeActionLog_test {
    
    @isTest
    static void test1(){
        
        //Case creation
        Case objCase = new Case
        (
            Status = 'New',
            Category__c = 'Payment Free Holiday'
        );
        insert objCase;
        
        //Prime Action Log
        Prime_Action_Log__c objPrimeLog1 = new Prime_Action_Log__c();
        objPrimeLog1.Number_of_Attempt__c = 3;
        objPrimeLog1.Case__c = objCase.id;
        objPrimeLog1.Prime_Action_Type__c = 'Payback Credit Balance';
        objPrimeLog1.Response_Status__c = 'Failure';
        objPrimeLog1.SOA_service_Inbound_Message__c = '{"ServiceType" : "mapa","ServiceIndicator" : "AccountServices ", "Reference" : "Account","PaybackAmount" : "90", "LogAction" : "manual","InstitutionID" : null,"ActionDate" : "2017-01-09","Action" : "Add", "AccountNumber" : "5438730000000614 " }';
        insert objPrimeLog1;
        
        Prime_Action_Log__c objPrimeLog = new Prime_Action_Log__c();
        objPrimeLog.Number_of_Attempt__c = 3;
        objPrimeLog.Case__c = objCase.id;
        //objPrimeLog.Financial_Account__c = finAccObj.id;
        objPrimeLog.Prime_Action_Type__c = 'Close Account';
        objPrimeLog.Response_Status__c = 'Failure';
        objPrimeLog.SOA_service_Inbound_Message__c = '{"setPPIReq" : null,"replaceCardReq" : null,"PaymentHolidaysReq" : null,"MobileReq" : null,"InstitutionId" : 2,"InstalmentReq" : null,"EmailReq" : null,"CloseAccReq" : {"ActionDate" : "2016-11-24","AccountNo" : "5435600034776114"},"cardReissueReq" : null,"addExtraCardReq" : null}';
        insert objPrimeLog;
        
        Prime_Action_Log__c objPrimeLog2 = new Prime_Action_Log__c();
        objPrimeLog2.Number_of_Attempt__c = 3;
        objPrimeLog2.Case__c = objCase.id;
        objPrimeLog2.Prime_Action_Type__c = 'Credit Limit Increase';
        objPrimeLog2.Response_Status__c = 'Failure';
        objPrimeLog2.SOA_service_Inbound_Message__c = '{"setPPIReq" : null,"replaceCardReq" : null,"PaymentHolidaysReq" : null,"MobileReq" : null,"InstitutionId" : 2,"InstalmentReq" : null,"EmailReq" : null,"CloseAccReq" : {"ActionDate" : "2016-11-24","AccountNo" : "5435600034776114"},"cardReissueReq" : null,"addExtraCardReq" : null}';
        insert objPrimeLog2;
        
        test.setCreatedDate(objPrimeLog.Id,date.today()-100);
        test.setCreatedDate(objPrimeLog1.Id,date.today()-95);
        test.setCreatedDate(objPrimeLog2.Id,date.today()-91);
        System.assertEquals(3, [Select Id from Prime_Action_Log__c where CreatedDate < N_DAYS_AGO:90].size());
        
        test.startTest();
        
        DeletePrimeActionLogData d=new DeletePrimeActionLogData();
        Database.executeBatch(d, 3);
        
        test.stopTest();
        
        System.assertEquals(0, [Select Id from Prime_Action_Log__c where CreatedDate < N_DAYS_AGO:90].size());
        
        
    }

    
    @isTest
    static void test2(){
        
        //Case creation
        Case objCase = new Case
        (
            Status = 'New',
            Category__c = 'Payment Free Holiday'
        );
        insert objCase;
        
        //Prine Action Log
        Prime_Action_Log__c objPrimeLog1 = new Prime_Action_Log__c();
        objPrimeLog1.Number_of_Attempt__c = 3;
        objPrimeLog1.Case__c = objCase.id;
        objPrimeLog1.Prime_Action_Type__c = 'Payback Credit Balance';
        objPrimeLog1.Response_Status__c = 'Failure';
        objPrimeLog1.SOA_service_Inbound_Message__c = '{"ServiceType" : "mapa","ServiceIndicator" : "AccountServices ", "Reference" : "Account","PaybackAmount" : "90", "LogAction" : "manual","InstitutionID" : null,"ActionDate" : "2017-01-09","Action" : "Add", "AccountNumber" : "5438730000000614 " }';
        insert objPrimeLog1;
        
        Prime_Action_Log__c objPrimeLog = new Prime_Action_Log__c();
        objPrimeLog.Number_of_Attempt__c = 3;
        objPrimeLog.Case__c = objCase.id;
        //objPrimeLog.Financial_Account__c = finAccObj.id;
        objPrimeLog.Prime_Action_Type__c = 'Close Account';
        objPrimeLog.Response_Status__c = 'Failure';
        objPrimeLog.SOA_service_Inbound_Message__c = '{"setPPIReq" : null,"replaceCardReq" : null,"PaymentHolidaysReq" : null,"MobileReq" : null,"InstitutionId" : 2,"InstalmentReq" : null,"EmailReq" : null,"CloseAccReq" : {"ActionDate" : "2016-11-24","AccountNo" : "5435600034776114"},"cardReissueReq" : null,"addExtraCardReq" : null}';
        insert objPrimeLog;
        
        Prime_Action_Log__c objPrimeLog2 = new Prime_Action_Log__c();
        objPrimeLog2.Number_of_Attempt__c = 3;
        objPrimeLog2.Case__c = objCase.id;
        objPrimeLog2.Prime_Action_Type__c = 'Credit Limit Increase';
        objPrimeLog2.Response_Status__c = 'Failure';
        objPrimeLog2.SOA_service_Inbound_Message__c = '{"setPPIReq" : null,"replaceCardReq" : null,"PaymentHolidaysReq" : null,"MobileReq" : null,"InstitutionId" : 2,"InstalmentReq" : null,"EmailReq" : null,"CloseAccReq" : {"ActionDate" : "2016-11-24","AccountNo" : "5435600034776114"},"cardReissueReq" : null,"addExtraCardReq" : null}';
        insert objPrimeLog2;
        
        test.setCreatedDate(objPrimeLog.Id,date.today()-100);
        test.setCreatedDate(objPrimeLog1.Id,date.today()-90);
        test.setCreatedDate(objPrimeLog2.Id,date.today()-90);
        //System.assertEquals(2, [Select Id from Prime_Action_Log__c where CreatedDate < N_DAYS_AGO:90].size());
        
        test.startTest();
        Delete objPrimeLog;
        Delete objPrimeLog1; 
        Delete objPrimeLog2;
        DeletePrimeActionLogData d=new DeletePrimeActionLogData();
        String sch = '0 0 * * * ?';
        System.schedule('test job', sch, d);
        
        test.stopTest();
    }
    /*@isTest
    static void test3()
    {
        test.startTest();
        
        DeletePrimeActionLogData d=new DeletePrimeActionLogData();
            Database.executeBatch(d, 0);
        test.stopTest();
    }*/


}