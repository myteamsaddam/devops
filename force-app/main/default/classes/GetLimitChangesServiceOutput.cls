/*
----------------------------------------------------------------------------------------
Author    Date         Description
Shameel   24/11/2016   Output class to implement SOA service using Integration framework.
----------------------------------------------------------------------------------------
*/
public class GetLimitChangesServiceOutput{

    public HeaderType HT;
    public GetLimitChangesResponse GLCR;
    
    public class HeaderType {
        public String MsgId;
        public String CorrelationId;
        public String RequestorId;
        public String SystemId;
        public String InstitutionId;
    }
    
    public class GetLimitChangesResponse{
        public String AccountNo;
        public Date From_x;
        public LimitChange[] limitChange;
    }
    
    public class LimitChange {
        public Date Date_x;
        public String OldLimit;
        public String NewLimit;
    }
    //To Cover Test Class 100%
    public void test()
    {
        
    }
}