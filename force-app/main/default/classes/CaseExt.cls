public class CaseExt {
    public List<Case> caseList {get;set;}
    List<Case> caseToUpdate = new List<Case>();
    public CaseExt(ApexPages.StandardSetController controller) {
       caseList= (List<Case>)controller.getSelected();
    }
    public PageReference assignCases(){
      if(caseList.size() > 0){
          for(Case cs : caseList){
            cs.OwnerId = UserInfo.getUserId();
              caseToUpdate.add(cs);
           }
      }
      if(caseToUpdate.size() > 0){
          update caseToUpdate;
      }
      else{
      ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,'Please Select atleast one case to change owner'));
      }
      
      Schema.DescribeSObjectResult result = Case.SObjectType.getDescribe();
      PageReference pageRef = new PageReference('/' + result.getKeyPrefix());
      return pageRef;
      
    }
}