public class PayBackCreditBalancecallout extends SoapIO{
    SOAPayBackCreditBalanceRequest.PayBackCreditRequestRecordType PayBackCreditRequestRecordTypeInstance;

    PayBackCreditBalanceInput input;
    public  void convertInputToRequest(){
    
        input = (PayBackCreditBalanceInput)serviceInput;
                   
        if(input!=null)
        {    
            if(input.InstitutionID!=null && input.Action!=null && input.Reference!=null && input.AccountNumber!=null && input.ServiceIndicator!=null && input.ServiceType!=null && input.LogAction!=null && input.PaybackAmount!=null)
            {
                PayBackCreditRequestRecordTypeInstance=new SOAPayBackCreditBalanceRequest.PayBackCreditRequestRecordType();
                PayBackCreditRequestRecordTypeInstance.InstitutionID=string.valueof(input.InstitutionID);
                PayBackCreditRequestRecordTypeInstance.Action=input.Action;
                PayBackCreditRequestRecordTypeInstance.Reference=input.Reference;
                PayBackCreditRequestRecordTypeInstance.AccountNumber=input.AccountNumber;
                PayBackCreditRequestRecordTypeInstance.ServiceIndicator=input.ServiceIndicator;
                PayBackCreditRequestRecordTypeInstance.ServiceType=input.ServiceType;
                PayBackCreditRequestRecordTypeInstance.LogAction=input.LogAction;
                PayBackCreditRequestRecordTypeInstance.PaybackAmount=input.PaybackAmount;
            }    
        }
    }
    
    public  override object invokeWebserviceCallout(SoapRequest soapRequest){
        SOAPayBackCreditBalanceTypelib.headerType HeadInstance=new SOAPayBackCreditBalanceTypelib.headerType();
        HeadInstance.MsgId='SFDC consuming Payback credit balance SOA service. Time Stamp=>'+system.now();
        HeadInstance.CorrelationId=Common_Settings__c.getValues('CorrelationId').Common_Value__c;
        HeadInstance.RequestorId=Common_Settings__c.getValues('RequestorId').Common_Value__c;
        HeadInstance.SystemId=Common_Settings__c.getValues('SystemId').Common_Value__c;
        HeadInstance.InstitutionId=string.valueof(input.InstitutionId);//'2';

        SOAPayBackCreditBalanceInvoke.x_xsoap_CSSServiceESB_CSSServicePT invokeInstance= new SOAPayBackCreditBalanceInvoke.x_xsoap_CSSServiceESB_CSSServicePT();
        invokeInstance.timeout_x=30000;

        ServiceSettings__c serviceObj = ServiceSettings__c.getValues('PayBackCreditBalance');
        if(serviceObj!=null && serviceObj.EndPoint__c!='' && serviceObj.EndPoint__c!=null)
        invokeInstance.endpoint_x = serviceObj.EndPoint__c;

        //String username = 'evry_access';
        //String password = '9oKuwQioQ4';

        // new cred
        String username = serviceObj.Username__c; //'css_soa';
        String password = serviceObj.Password__c;//'P1%gruzA';
        
        Blob headerValue = Blob.valueOf(username + ':' + password);
        String authorizationHeader = 'BASIC ' +EncodingUtil.base64Encode(headerValue);
        
        invokeInstance.inputHttpHeaders_x = new Map<String, String>();
        invokeInstance.inputHttpHeaders_x.put('Authorization', authorizationHeader);
        
        SOAPayBackCreditBalanceSecurityHeader.UsernameToken creds=new SOAPayBackCreditBalanceSecurityHeader.UsernameToken();
        //new cred
        creds.Username=serviceObj.Username__c; //'css_soa';
        creds.Password=serviceObj.Password__c;//'P1%gruzA';
          
        //creds.Username='evry_access';
        //creds.Password='9oKuwQioQ4';
        
        SOAPayBackCreditBalanceSecurityHeader.Security_element security_ele=new SOAPayBackCreditBalanceSecurityHeader.Security_element();
        security_ele.UsernameToken=creds;
        invokeInstance.SecurityPart=security_ele;
        system.debug('==Just Before callout');
        SOAPayBackCreditBalanceResponse.PayBackCreditResponseRecordType PayBackCreditResponse_elementinstance;
        //try{
            PayBackCreditResponse_elementinstance =  invokeInstance.payBackCredit(HeadInstance,PayBackCreditRequestRecordTypeInstance);
        //}catch(calloutexception callouterror){system.debug('~~~~'+callouterror.getmessage());}
        system.debug('SOAPayBackCreditBalanceRequest.PayBackCreditRequestRecordType -->'+PayBackCreditResponse_elementinstance);
        return PayBackCreditResponse_elementinstance;
     }  
        
     public override object convertResponseToOutput(object response, map<string, string> responseHeader, ServiceStatus serviceStatus){
       system.debug('[[===<<((:))>>===]]'+response);
       if(response==null || !(response instanceof SOAPayBackCreditBalanceResponse.PayBackCreditResponseRecordType))
           return null;
       SOAPayBackCreditBalanceResponse.PayBackCreditResponseRecordType PayBackCreditResponse_elementInstance = (SOAPayBackCreditBalanceResponse.PayBackCreditResponseRecordType)response;
     
       PayBackCreditBalanceOutput PayBackCreditBalanceOutputinstance=new PayBackCreditBalanceOutput();

       PayBackCreditBalanceOutputinstance.MsgId=PayBackCreditResponse_elementInstance.Header.MsgId;
       PayBackCreditBalanceOutputinstance.CorrelationId=PayBackCreditResponse_elementInstance.Header.CorrelationId;
       PayBackCreditBalanceOutputinstance.RequestorId=PayBackCreditResponse_elementInstance.Header.RequestorId;
       PayBackCreditBalanceOutputinstance.SystemId=PayBackCreditResponse_elementInstance.Header.SystemId;
       PayBackCreditBalanceOutputinstance.InstitutionId=PayBackCreditResponse_elementInstance.Header.InstitutionId;
       PayBackCreditBalanceOutputinstance.Code=PayBackCreditResponse_elementInstance.Result.Code;
       PayBackCreditBalanceOutputinstance.Description=PayBackCreditResponse_elementInstance.Result.Description;
       list<PayBackCreditBalanceOutput.ErrorResult> ErrorResultList=new list<PayBackCreditBalanceOutput.ErrorResult>();  
       system.debug('~~~~'+PayBackCreditResponse_elementInstance.Result.ErrorDetails);
       if(PayBackCreditResponse_elementInstance.Result.ErrorDetails!=null && PayBackCreditResponse_elementInstance.Result.ErrorDetails.ErrorResult!=null){
           for(SOAPayBackCreditBalanceResponse.ErrorResult eachError: PayBackCreditResponse_elementInstance.Result.ErrorDetails.ErrorResult)
           {
               PayBackCreditBalanceOutput.ErrorResult temp=new PayBackCreditBalanceOutput.ErrorResult();
               temp.Code=eachError.Code;
               temp.Description=eachError.Description;
               ErrorResultList.add(temp);
           }
       }
       PayBackCreditBalanceOutputinstance.ErrorResultList=ErrorResultList;
       return PayBackCreditBalanceOutputinstance;
    }        
}