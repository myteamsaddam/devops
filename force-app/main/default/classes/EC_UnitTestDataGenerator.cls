/**************************************************************************************************
* @Author:      Priyanka Singh  
* @Date:        
* @PageCode:    UT
* @Description: Class to create test data centrally during unit test and user perspective testmethod. 
*               Will be called from UnitTestGenerator.
*               N.B. - For each of entity a static gateway must be created. Through that static class
*                       caller can create / insert data for testing
* @Revision(s): [Date] - [Change Reference] - [Changed By] - [Description]   
***************************************************************************************************/
@isTest
public class EC_UnitTestDataGenerator {   
    
    public static final TestAccount testAccount = new TestAccount();
    public static final TestContact testContact = new TestContact(); 
    public static final TestProductCustom testProductCustom = new TestProductCustom(); // this should be class name
    public static final TestFinancialAccount testFinancialAccount = new TestFinancialAccount(); 
    public static final TestCard testCard = new TestCard(); 
    public static final TestCase testCase = new TestCase();
    public static final TestEmailMessage testEmailMessage = new TestEmailMessage();
    public static final TestTask testTask = new TestTask();
    public static final TestSaveDesk testSaveDesk = new TestSaveDesk();
    public static final TestSalesforceTeam testSalesforceTeam = new TestSalesforceTeam();
    public static final TestStatusLog testStatus= new TestStatusLog();
    public static User standardUser;
    public static User adminUser;
    
    static {
        standardUser = EC_TestUserGenerator.createStandardUser_Default();
        insert standardUser; 
    }
    static {
        adminUser = EC_TestUserGenerator.createAdminUser_Default();
        insert adminUser; 
    }
    
    /**********************************************************************************************
* @Author:        
* @Date:        
* @Description: Responsible class to handle caller request to create instance/insert/reload 
*               related to Account 
* @Revision(s): [Date] - [Change Reference] - [Changed By] - [Description]   
***********************************************************************************************/    
    public class TestAccount {
        
        public TestAccount(){}
        public Account default(){
            Account record = (Account)buildSObject(Account.sObjectType, new Map<String,Object> {
                'Institution_Id__c'=>2 ,
                    'Customer_Serno__c'=>'1123124',
                    'Name'=>'Test  Account'
                   
                    });
            return record;
        }       
        public Account build(Map<String,Object> attrs){
            Account record = default();
            record = (Account)buildSObject(record, attrs);
            return record;
        }
        public Account buildInsert(Map<String,Object> attrs){
            Account record = build(attrs);
            insert record;
            return record;
        }
        public Account buildInsert(){
            Account record = default();
            Map<String,Object> attrs = new Map<String,Object>{
            };
                record = (Account)buildSObject(record, attrs);
            insert record;
            return record;
        }
        public Account reload(Account record, String[] attrs) {
            return (Account)reloadSObject(record, attrs);
        }
        public List<Account> reloadList(List<Account> recordList, String[] attrs) {
            return (List<Account>)reloadSObjects(recordList, attrs);
        }
    } 
    /**********************************************************************************************
* @Author:        
* @Date:        
* @Description: Responsible class to handle caller request to create instance/insert/reload 
*               related to Contact 
* @Revision(s): [Date] - [Change Reference] - [Changed By] - [Description]   
***********************************************************************************************/    
    public class TestContact{
        
        public TestContact(){}
        public Contact default(){
            Contact record = (Contact)buildSObject(Contact.sObjectType, new Map<String,Object> {
                'LastName'=>'Test Contact', 
                    'FirstName'=>'First Test Contact',
                    'SerNo__c'=>'09899', 
                    'Institution_Id__c'=>2, 
                    'SSN__c'=>'0000000061',
                    //'Encrypted_SSN__c'=>'encryptSSN',
                    //AccountId=accountObj.Id',
                    'Phone'=>'123123123', 
                    'Fax'=>'98789879', 
                    'MobilePhone'=>'98789879', 
                    'HomePhone'=>'123123123', 
                    'Email'=>'testemail@test.com',
                    'customer_email__C'=>'test@gmail.com'
                    });
            return record;
        }       
        public Contact build(Map<String,Object> attrs){
            Contact record = default();
            record = (Contact)buildSObject(record, attrs);
            return record;
        }
        public Contact buildInsert(Map<String,Object> attrs){
            Contact record = build(attrs);
            insert record;
            return record;
        }
        public Contact buildInsert(){
            Contact record = default();
            Map<String,Object> attrs = new Map<String,Object>{
            };
                record = (Contact)buildSObject(record, attrs);
            insert record;
            return record;
        }
        public Contact reload(Contact record, String[] attrs) {
            return (Contact)reloadSObject(record, attrs);
        }
        public List<Contact> reloadList(List<Contact> recordList, String[] attrs) {
            return (List<Contact>)reloadSObjects(recordList, attrs);
        }
    } 
    
    /**********************************************************************************************
* @Author:        
* @Date:        
* @Description: Responsible class to handle caller request to create instance/insert/reload  Product_Custom__c
*               related to Custom Product 
* @Revision(s): [Date] - [Change Reference] - [Changed By] - [Description]   
***********************************************************************************************/  
    public class TestProductCustom {
        
        public TestProductCustom(){}
        public Product_Custom__c default(){
            Product_Custom__c record = (Product_Custom__c)buildSObject(Product_Custom__c.sObjectType, new Map<String,Object> {
                'Name'=>'Test Product',
                 'Pserno__c'=>'56789'
            });
            return record;
        }       
        public Product_Custom__c build(Map<String,Object> attrs){
            Product_Custom__c record = default();
            record = (Product_Custom__c)buildSObject(record, attrs);
            return record;
        }
       
        public Product_Custom__c buildInsert(Map<String,Object> attrs){
            Product_Custom__c record = build(attrs);
            insert record;
            return record;
        }
        public Product_Custom__c buildInsert(){
            Product_Custom__c record = default();
            Map<String,Object> attrs = new Map<String,Object>{
            };
            record = (Product_Custom__c)buildSObject(record, attrs);
            insert record;
            return record;
            }
        public Product_Custom__c reload(Product_Custom__c record, String[] attrs) {
            return (Product_Custom__c)reloadSObject(record, attrs);
        }
        public List<Product_Custom__c> reloadList(List<Product_Custom__c> recordList, String[] attrs) {
            return (List<Product_Custom__c>)reloadSObjects(recordList, attrs);
        }
    }
    
    
    /**********************************************************************************************
* @Author:        
* @Date:        
* @Description: Responsible class to handle caller request to create instance/insert/reload 
*               related to Financial Account 
* @Revision(s): [Date] - [Change Reference] - [Changed By] - [Description]   
***********************************************************************************************/    
    public class TestFinancialAccount {
       
        public TestFinancialAccount(){}
       
        public Financial_Account__c default(){
            Financial_Account__c record = (Financial_Account__c)buildSObject(Financial_Account__c.sObjectType, new Map<String,Object> {
                    'Account_Serno__c' => '3123123', 
                    'Product_Serno__c'=>'56789',
                    'Institution_Id__c' => 2,
                    'Account_Number__c'=>'12w23eeeeedd'
                    });
            return record;
        }
        
      
        
        public Financial_Account__c build(Map<String,Object> attrs){
            Financial_Account__c record = default();
            record = (Financial_Account__c)buildSObject(record, attrs);
            return record;
        }
        public Financial_Account__c buildInsert(Map<String,Object> attrs){
            Financial_Account__c record = build(attrs);
            insert record;
            return record;
        }
       
        public Financial_Account__c buildInsert(){
            Financial_Account__c record = default();
            Map<String,Object> attrs = new Map<String,Object>{
            };
                record = (Financial_Account__c)buildSObject(record, attrs);
            insert record;
            return record;
        }
       
        public Financial_Account__c reload(Financial_Account__c record, String[] attrs) {
            return (Financial_Account__c)reloadSObject(record, attrs);
        }
       
        public List<Financial_Account__c> reloadList(List<Financial_Account__c> recordList, String[] attrs) {
            return (List<Financial_Account__c>)reloadSObjects(recordList, attrs);
        }
    }
    /**********************************************************************************************
* @Author:        
* @Date:        
* @Description: Responsible class to handle caller request to create instance/insert/reload 
*               related to Card 
* @Revision(s): [Date] - [Change Reference] - [Changed By] - [Description]   
***********************************************************************************************/    
    public class TestCard {        
        public TestCard(){}
        public Card__c default(){
            Card__c record = (Card__c)buildSObject(Card__c.sObjectType, new Map<String,Object> {
                'PrimaryCardFlag__c'=>true, 
                    'Card_Serno__c'=>'524858519', 
                    'Card_Number_Truncated__c'=>'7136785481583561', 
                    'Financial_Account_Serno__c'=>'3123123', 
                    'People_Serno__c'=>'09899',
                    'Institution_Id__c'=>2
                    });
            return record;  
        }       
        public Card__c build(Map<String,Object> attrs){
            Card__c record = default();
            record = (Card__c)buildSObject(record, attrs);
            return record;
        }
        public Card__c buildInsert(Map<String,Object> attrs){
            Card__c record = build(attrs);  
           
            insert record;
            return record;
        }
        public Card__c buildInsert(){
            Card__c record = default();
            Map<String,Object> attrs = new Map<String,Object>{
            };
                record = (Card__c)buildSObject(record, attrs);
            insert record;
            return record;
        }
        public Card__c reload(Card__c record, String[] attrs) {
            return (Card__c)reloadSObject(record, attrs);
        }
        public List<Card__c> reloadList(List<Card__c> recordList, String[] attrs) {
            return (List<Card__c>)reloadSObjects(recordList, attrs);
        }
    }
/**********************************************************************************************
* @Author:        
* @Date: 06/03/2020       
* @Description: Responsible class to handle caller request to create instance/insert/reload 
*               related to Case 
* @Revision(s): [Date] - [Change Reference] - [Changed By] - [Description]   
***********************************************************************************************/    
    public class TestCase {        
        public TestCase(){}
        public Case default(){
            //Id queueId = [SELECT Id FROM Group WHERE Type = 'Queue' AND Name='SE Customer Service'].Id;
            Case record = (Case)buildSObject(Case.sObjectType, new Map<String,Object> {
                'EC_Subject__c'=>'Ny överföring', 
                    'Status'=>'New', 
                    'To_Address__c'=>'tekwkorts@entercard.com', 
                   'Description'=>'kundens personnummer: 0000147861',
                    'Case_View__c'=>'SE Fund Transfer & Bill Payments',
                    'Category__c'=>'FT / Bill payment'
					
                    });
            return record;
        }       
        public Case build(Map<String,Object> attrs){
            Case record = default();
            record = (Case)buildSObject(record, attrs);
            return record;
        }
        public Case buildInsert(Map<String,Object> attrs){
            Case record = build(attrs);
            insert record;
            return record;
        }
        public Case buildInsert(){
            Case record = default();
            Map<String,Object> attrs = new Map<String,Object>{
            };
                record = (Case)buildSObject(record, attrs);
            insert record;
            return record;
        }
        public Case reload(Case record, String[] attrs) {
            return (Case)reloadSObject(record, attrs);
        }
        public List<Case> reloadList(List<Case> recordList, String[] attrs) {
            return (List<Case>)reloadSObjects(recordList, attrs);
        }
    } //End TestCase
    /**********************************************************************************************
* @Author:        
* @Date: 06/03/2020       
* @Description: Responsible class to handle caller request to create instance/insert/reload 
*               related to EmailMessage 
* @Revision(s): [Date] - [Change Reference] - [Changed By] - [Description]   
***********************************************************************************************/    
    public class TestEmailMessage {        
        public TestEmailMessage(){}
        public EmailMessage default(){
            EmailMessage record = (EmailMessage)buildSObject(EmailMessage.sObjectType, new Map<String,Object> {
                'Subject'=>'Ny överföring', 
                    'FromAddress'=>'test.001@test.com', 
                    'ToAddress'=>'tekwkorts@entercard.com', 
                    'TextBody'=>'kundens personnummer: 0000147861'
                    });
            return record;
        }       
        public EmailMessage build(Map<String,Object> attrs){
            EmailMessage record = default();
            record = (EmailMessage)buildSObject(record, attrs);
            return record;
        }
        public EmailMessage buildInsert(Map<String,Object> attrs){
            EmailMessage record = build(attrs);
            insert record;
            return record;
        }
        public EmailMessage buildInsert(){
            EmailMessage record = default();
            Map<String,Object> attrs = new Map<String,Object>{
            };
                record = (EmailMessage)buildSObject(record, attrs);
            insert record;
            return record;
        }
        public EmailMessage reload(EmailMessage record, String[] attrs) {
            return (EmailMessage)reloadSObject(record, attrs);
        }
        public List<EmailMessage> reloadList(List<EmailMessage> recordList, String[] attrs) {
            return (List<EmailMessage>)reloadSObjects(recordList, attrs);
        }
    } //End TestEmailMessage
    /**********************************************************************************************
* @Author:        
* @Date: 06/03/2020       
* @Description: Responsible class to handle caller request to create instance/insert/reload 
*               related to Task 
* @Revision(s): [Date] - [Change Reference] - [Changed By] - [Description]   
***********************************************************************************************/ 
    public class TestTask {        
        public TestTask(){}
        public Task default(){
            Task record = (Task)buildSObject(Task.sObjectType, new Map<String,Object> {
                      'subject'=>'subject', 
                      'status'=>'Completed',
                      'Category__c'=>'Test', 
                      'type'=>'Complaint',
                      'Comments__c'=>'',
                      'CallType' => 'Inbound'
                    });
            return record;
        }       
        public Task build(Map<String,Object> attrs){
            Task record = default();
            record = (Task)buildSObject(record, attrs);
            return record;
        }
        public Task buildInsert(Map<String,Object> attrs){
            Task record = build(attrs);
            insert record;
            return record;
        }
        public Task buildInsert(){
            Task record = default();
            Map<String,Object> attrs = new Map<String,Object>{
            };
                record = (Task)buildSObject(record, attrs);
            insert record;
            return record;
        }
        public Task reload(Task record, String[] attrs) {
            return (Task)reloadSObject(record, attrs);
        }
        public List<Task> reloadList(List<Task> recordList, String[] attrs) {
            return (List<Task>)reloadSObjects(recordList, attrs);
        }
    }
    
        /**********************************************************************************************
* @Author:        
* @Date:        
* @Description: Responsible class to handle caller request to create instance/insert/reload 
*               related to SaveDeskQueue__c 
* @Revision(s): [Date] - [Change Reference] - [Changed By] - [Description]   
***********************************************************************************************/    
    public class TestSaveDesk{
        
        public TestSaveDesk(){}
        public SaveDeskQueue__c default(){
            SaveDeskQueue__c record = (SaveDeskQueue__c)buildSObject(SaveDeskQueue__c.sObjectType, new Map<String,Object> {
                'Campaign_Type__c'=>'Test',
                'Institution_Id__c'=>'2',
                'People_Serno__c'=>'09899',
                'Financial_Account_Serno__c'=>'3123123',
                'Offer_Description__c'=>'Offer',
                'Offer_Start_Date__c'=>Date.valueOf(system.now()),
                'Offer_End_Date__c' => Date.valueOf(system.now()),
                'Deadline_Date_Campaign__c' => Date.valueOf(system.now())
                    });
            return record;
        }       
        public SaveDeskQueue__c build(Map<String,Object> attrs){
            SaveDeskQueue__c record = default();
            record = (SaveDeskQueue__c)buildSObject(record, attrs);
            return record;
        }
        public SaveDeskQueue__c buildInsert(Map<String,Object> attrs){
            SaveDeskQueue__c record = build(attrs);
            insert record;
            return record;
        }
        public SaveDeskQueue__c buildInsert(){
            SaveDeskQueue__c record = default();
            Map<String,Object> attrs = new Map<String,Object>{
            };
                record = (SaveDeskQueue__c)buildSObject(record, attrs);
            insert record;
            return record;
        }
        public SaveDeskQueue__c reload(SaveDeskQueue__c record, String[] attrs) {
            return (SaveDeskQueue__c)reloadSObject(record, attrs);
        }
        public List<SaveDeskQueue__c> reloadList(List<SaveDeskQueue__c> recordList, String[] attrs) {
            return (List<SaveDeskQueue__c>)reloadSObjects(recordList, attrs);
        }
    } 

    /*************************
     * 
    * @Author:        Kakasaheb Ekshinge 
	* @Date:          18-06-2020
	* @Description: Responsible class to cover Status log helper methods 
	* @Revision(s): [Date] - [Change Reference] - [Changed By] - [Description] 
	*
	*/
       
    public class TestStatusLog{
        public TestStatusLog(){}
        public StatusLog__c default(){
            StatusLog__c record = (StatusLog__c)buildSObject(StatusLog__c.sObjectType, new Map<String,Object> {
                'Name'=>'Status log',
                'Log_Type__c'=>'2',
                'ErrorCode__c'=>'E',
                'EndTime__c'=>System.now(),
                'StartTime__c'=>System.now(),
                'Status__c' => 'E',
                'RequestBody__c' => 'RequestBody'  
                    });
            return record;
        } 
    	 public StatusLog__c build(Map<String,Object> attrs){
            StatusLog__c record = default();
            record = (StatusLog__c)buildSObject(record, attrs);
            return record;
        }
    	
        public StatusLog__c buildInsert(){
            StatusLog__c record = default();
            Map<String,Object> attrs = new Map<String,Object>{
            };
                record = (StatusLog__c)buildSObject(record, attrs);
            insert record;
            return record;
        }
    
    }
    /**********************************************************************************************
* @Author:        
* @Date:        
* @Description: Responsible class to handle caller request to create instance/insert/reload 
*               related to EC_SalesforceTeam__c 
* @Revision(s): [Date] - [Change Reference] - [Changed By] - [Description]   
***********************************************************************************************/    
    public class TestSalesforceTeam {        
        public TestSalesforceTeam(){}
        public EC_SalesforceTeam__c default(){
            EC_SalesforceTeam__c record = (EC_SalesforceTeam__c)buildSObject(EC_SalesforceTeam__c.sObjectType, new Map<String,Object> {
                'Name'=>'Test User', 
                 'EC_Email__c'=>'test.test@test.com'                    
            });
            return record;
        }       
        public EC_SalesforceTeam__c build(Map<String,Object> attrs){
            EC_SalesforceTeam__c record = default();
            record = (EC_SalesforceTeam__c)buildSObject(record, attrs);
            return record;
        }
        public EC_SalesforceTeam__c buildInsert(Map<String,Object> attrs){
            EC_SalesforceTeam__c record = build(attrs);
            insert record;
            return record;
        }
        public EC_SalesforceTeam__c buildInsert(){
            EC_SalesforceTeam__c record = default();
            Map<String,Object> attrs = new Map<String,Object>{
            };
                record = (EC_SalesforceTeam__c)buildSObject(record, attrs);
            insert record;
            return record;
        }
        public EC_SalesforceTeam__c reload(EC_SalesforceTeam__c record, String[] attrs) {
            return (EC_SalesforceTeam__c)reloadSObject(record, attrs);
        }
        public List<EC_SalesforceTeam__c> reloadList(List<EC_SalesforceTeam__c> recordList, String[] attrs) {
            return (List<EC_SalesforceTeam__c>)reloadSObjects(recordList, attrs);
        }
    }
    private static SObject buildSObject(SObject sobjectObj, Map<String, Object> attributes){
        
        if (attributes != null){
            for (String fieldName : attributes.keySet())
                sobjectObj.put(fieldName, attributes.get(fieldName));
        }       
        return sobjectObj;
    }
    
    private static SObject buildSObject(Schema.SObjectType sType, Map<String, Object> attributes){
        SObject sobj = sType.newSObject();
        if (attributes != null){
            for (String fieldName : attributes.keySet())
                sobj.put(fieldName, attributes.get(fieldName));
        }
        
        return sobj;
    }    
    private static SObject reloadSObject(SObject sobj, String[] fieldNames) {
        Schema.SObjectType sType = sobj.getSObjectType();
        String soql = 'SELECT Id' + (fieldNames.size() > 0 ? ', ' + toStringList(fieldNames) : '') +
            ' FROM ' + sType.getDescribe().getName() + ' WHERE Id = \'' + sobj.Id + '\' LIMIT 1';
        return Database.query(soql);
    }
    
    private static List<SObject> reloadSObjects(List<SObject> objects, String[] fieldNames) {
        if (objects == null || objects.size() == 0)
            return new List<SObject>();
        String objectApiName = objects[0].getSObjectType().getDescribe().getName();
        String[] ids = new String[] {};
            for (SObject obj : objects){
                ids.add('\'' + obj.Id + '\'');
            }            
        String soql = 'SELECT Id' + (fieldNames.size() > 0 ? ', ' + toStringList(fieldNames) : '') +
            ' FROM ' + objectApiName +
            ' WHERE Id IN (' + toStringList(ids) + ')';
        return Database.query(soql);
    }
    
    private static String toStringList(String[] arr) {
        String stringList = '';
        for (Integer i = 0; i < arr.size(); i++){
            stringList += arr[i];
            if (i < (arr.size() - 1))
                stringList += ', ';
        }
        return stringList;
    }
}