//Generated by wsdl2apex

public class SOAUpdateCustomerInvoke {
    public class x_xsoap_CSSServiceESB_CSSServicePT {
        //public String endpoint_x = 'http://eclvmidapp04t.ectest.local:8010/CSSService/ProxyServices/CSSServicePS';
        public String endpoint_x = 'https://services-test.entercard.com/gateway/services/CSSService?wsdl';
        public Map<String,String> inputHttpHeaders_x;
        public Map<String,String> outputHttpHeaders_x;
        public String clientCertName_x;
        public String clientCert_x;
        public String clientCertPasswd_x;
        public Integer timeout_x;
        public SOAUpdateCustomerSecurityHeader.Security_element SecurityPart;
        private String SecurityPart_hns = 'Security=http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd';
        private String[] ns_map_type_info = new String[]{'http://schemas.entercard.com/service/CSSService/1.0', 'SOAUpdateCustomerInvoke', 'http://schemas.entercard.com/service/CSSServiceResponse/1.0', 'SOAUpdateCustomerResponse', 'http://schemas.entercard.com/header/1.0', 'SOAUpdateCustomerHeader', 'http://schemas.entercard.com/TYPELIB/1.0', 'SOAUpdateCustomerTypelib', 'http://schemas.entercard.com/service/CSSServiceRequest/1.0', 'SOAUpdateCustomerRequest', 'http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd', 'SOAUpdateCustomerSecurityHeader'};
        public SOAUpdateCustomerResponse.UpdateCustomerDataResponse_element updateCustomerData(SOAUpdateCustomerTypelib.headerType Header,SOAUpdateCustomerRequest.setEmailRequestRecordType setEmailRequest,SOAUpdateCustomerRequest.setMobileRequestRecordType setMobileRequest,SOAUpdateCustomerRequest.closeAccountRequestRecordType closeAccountRequest,SOAUpdateCustomerRequest.setInstalmentRequestRecordType setInstalmentRequest,SOAUpdateCustomerRequest.addExtraCardRequestRecordType addExtraCardRequest,SOAUpdateCustomerRequest.replaceCardRequestRecordType replaceCardRequest,SOAUpdateCustomerRequest.setPaymentHolidaysRequestRecordType setPaymentHolidaysRequest,SOAUpdateCustomerRequest.setPPIRequestRecordType setPPIRequest,SOAUpdateCustomerRequest.setInsuranceRequestRecordType setInsuranceRequest,SOAUpdateCustomerRequest.cardReissueReprintRequestRecordType cardReissueReprintRequest,SOAUpdateCustomerRequest.setPickupCodeRequestRecordType SetPickupCodeRequest,SOAUpdateCustomerRequest.setMTPRequestRecordType SetMTPRequest,SOAUpdateCustomerRequest.setPhone1RequestRecordType SetPhone1Request,SOAUpdateCustomerRequest.SeteStatementRequestRecordType SeteStatementRequest) {
            SOAUpdateCustomerRequest.UpdateCustomerDataRequest_element request_x = new SOAUpdateCustomerRequest.UpdateCustomerDataRequest_element();
            request_x.Header = Header;
            request_x.setEmailRequest = setEmailRequest;
            request_x.setMobileRequest = setMobileRequest;
            request_x.closeAccountRequest = closeAccountRequest;
            request_x.setInstalmentRequest = setInstalmentRequest;
            request_x.addExtraCardRequest = addExtraCardRequest;
            request_x.replaceCardRequest = replaceCardRequest;
            request_x.setPaymentHolidaysRequest = setPaymentHolidaysRequest;
            request_x.setPPIRequest = setPPIRequest;
            request_x.setInsuranceRequest = setInsuranceRequest;
            request_x.cardReissueReprintRequest = cardReissueReprintRequest;
            request_x.SetPickupCodeRequest = SetPickupCodeRequest;
            request_x.SetMTPRequest = SetMTPRequest;
            request_x.SetPhone1Request = SetPhone1Request;
            request_x.SeteStatementRequest = SeteStatementRequest;
            SOAUpdateCustomerResponse.UpdateCustomerDataResponse_element response_x;
            Map<String, SOAUpdateCustomerResponse.UpdateCustomerDataResponse_element> response_map_x = new Map<String, SOAUpdateCustomerResponse.UpdateCustomerDataResponse_element>();
            response_map_x.put('response_x', response_x);
            WebServiceCallout.invoke(
              this,
              request_x,
              response_map_x,
              new String[]{endpoint_x,
              'updateCustomerData',
              'http://schemas.entercard.com/service/CSSServiceRequest/1.0',
              'UpdateCustomerDataRequest',
              'http://schemas.entercard.com/service/CSSServiceResponse/1.0',
              'UpdateCustomerDataResponse',
              'SOAUpdateCustomerResponse.UpdateCustomerDataResponse_element'}
            );
            response_x = response_map_x.get('response_x');
            return response_x;
        }
    }
}