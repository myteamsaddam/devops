global class GetCaseforPeopleSerno {

    webService static case[] getcasesForpeopleserno(String Customer_SerNo) 
    {
        case[] cases = [SELECT CaseNumber, Subject, Description, CreatedDate FROM case
                        WHERE case.Customer_SerNo__c =:Customer_SerNo and case.origin='Phone'];
        return cases;
    }
}