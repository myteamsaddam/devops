@isTest
public class DeleteSOAServiceRetryLogData_test {
    
    @isTest
    static void test1(){
        
        
        //SOAServiceRetry Log
        SOAServiceRetryLog__c objSOAServiceRetryLog1 = new SOAServiceRetryLog__c();
        //objSOAServiceRetryLog1.Name='Test SOAServiceRetry Log1';
        insert objSOAServiceRetryLog1;
        
        SOAServiceRetryLog__c objSOAServiceRetryLog2 = new SOAServiceRetryLog__c();
        //objSOAServiceRetryLog2.Name='Test SOAServiceRetry Log2';
        insert objSOAServiceRetryLog2;
        
        SOAServiceRetryLog__c objSOAServiceRetryLog3 = new SOAServiceRetryLog__c();
        //objSOAServiceRetryLog3.Name='Test SOAServiceRetry Log3';
        insert objSOAServiceRetryLog3;
        
        test.setCreatedDate(objSOAServiceRetryLog1.Id,date.today()-100);
        test.setCreatedDate(objSOAServiceRetryLog2.Id,date.today()-95);
        test.setCreatedDate(objSOAServiceRetryLog3.Id,date.today()-91);
        System.assertEquals(3, [Select Id from SOAServiceRetryLog__c where CreatedDate < N_DAYS_AGO:90].size());
        
        test.startTest();
        
        DeleteSOAServiceRetryLogData d=new DeleteSOAServiceRetryLogData();
        Database.executeBatch(d, 3);
        
        test.stopTest();
        
        System.assertEquals(0, [Select Id from SOAServiceRetryLog__c where CreatedDate < N_DAYS_AGO:90].size());
        
        
    }

    
    @isTest
    static void test2(){
        
        //SOAServiceRetry Log
        SOAServiceRetryLog__c objSOAServiceRetryLog1 = new SOAServiceRetryLog__c();
        //objSOAServiceRetryLog1.Name='Test SOAServiceRetry Log1';
        insert objSOAServiceRetryLog1;
        
        SOAServiceRetryLog__c objSOAServiceRetryLog2 = new SOAServiceRetryLog__c();
        //objSOAServiceRetryLog2.Name='Test SOAServiceRetry Log2';
        insert objSOAServiceRetryLog2;
        
        SOAServiceRetryLog__c objSOAServiceRetryLog3 = new SOAServiceRetryLog__c();
        //objSOAServiceRetryLog3.Name='Test SOAServiceRetry Log3';
        insert objSOAServiceRetryLog3;
        
        test.setCreatedDate(objSOAServiceRetryLog1.Id,date.today()-100);
        test.setCreatedDate(objSOAServiceRetryLog2.Id,date.today()-95);
        test.setCreatedDate(objSOAServiceRetryLog3.Id,date.today()-91);
        System.assertEquals(3, [Select Id from SOAServiceRetryLog__c where CreatedDate < N_DAYS_AGO:90].size());
        
        test.startTest();
        
        DeleteSOAServiceRetryLogData d=new DeleteSOAServiceRetryLogData();
        String sch = '0 0 * * * ?';
        System.schedule('test job', sch, d);
        
        test.stopTest();
        
        
    }


}