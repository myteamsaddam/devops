@isTest
public class DeleteCaseData_Test {
    
    Public static testMethod void myUnitTest() {
        
        Account ac = new Account();
        ac.Name = 'Test';
        ac.Customer_Serno__c = '123';
        insert ac;
        
        Contact con = new Contact();
        con.LastName = 'Testing';
        con.SSN__c='353535353535';
        con.SerNo__c='4353077';
        insert con;
        
        Product_Custom__c pd = new Product_Custom__c();
        pd.Name = 'Test';
        pd.Pserno__c = '1542';
        insert pd;
        
        Financial_Account__c fa = new Financial_Account__c();
        fa.Product__c = pd.id;
        fa.Account_Serno__c = '1200';
        fa.Account_Number__c = '125646456';
        fa.Customer__c = ac.Id;
        fa.Customer_Serno__c = '126464';
        fa.Product_Serno__c = '454564';
        insert fa;
        
        Card__c cd = new Card__c();
        cd.Card_Number_Truncated__c = '353453******5345';
        cd.Product__c = pd.Id;
        cd.People_Serno__c = '12456465';
        cd.Card_Serno__c = '45785415';
        cd.Prod_Serno__c='4746565';
        cd.People__c = con.Id;
        cd.Financial_Account__c =fa.Id;
        cd.Financial_Account_Serno__c = '784966';
        insert cd;
        
        String clsDat = Date.today().format();
        String errMsg = 'Error';
        List<Case> cslist = new List<Case>();
        Case cs1 = new Case();//(Status='New',Origin = 'Email', Category__c='None');
        cs1.AccountId = ac.Id;
        cs1.Financial_Account__c = fa.Id;
        cs1.ContactId = con.Id;
        cs1.Origin = 'email';
        cs1.Status='Closed';
        cs1.Card__c = cd.Id;
        cs1.X120_Months__c = clsDat;
        cs1.X18_Months__c = clsDat;
        cs1.Category__c = 'None';
        cslist.add(cs1);
        Insert cslist;

        Test.StartTest();
        DeleteCaseData deletecase1=new DeleteCaseData();
        deletecase1.errorMap.put(cslist[0].id,'test');
        deletecase1.IdToSObjectMap.put(cslist[0].id,cslist[0]); 
        Database.executeBatch(deletecase1,200);
        Test.StopTest(); 

    }
}