public class RetentionResponseController 
{
    public List<Financial_Account__c> result { get;set; }
    public contact con{set;get;}
    public Retention_Response__c req 
    {
        get {
            if (req == null)
                req = new Retention_Response__c ();
            req.OwnerId = UserInfo.getUserId();          
            return req;
        }
        set;
    }
    public Id contactId {get;set;}
    public List<Financial_Account__c> financialAccountList {get;set;}
    Public string selectedfinacc{get;set;}   
    
    
    public retentionresponsecontroller(ApexPages.StandardController controller)
    {
        req = new Retention_Response__c();
    }
    public retentionresponsecontroller()
    {
        contactId = getParameterValue('conId');   
        system.debug('----contact--id------'+contactId); 
        con =[select  name from contact where id=:contactid limit 1];
        financialAccountList =new List<Financial_Account__c>();  
        if(con!=null) 
        {
            set<Id> financialAccIdSet = new set<Id>();
            List<Card__c> cardList = [SELECT Id, Name, People__c, Financial_Account__c FROM Card__c
                                      where People__c=:con.Id and Financial_Account__c!=null 
                                      Limit 50000];
            for(Card__c card:cardList)
            {
                financialAccIdSet.add(card.Financial_Account__c);
            }
            financialAccountList =[select Id,Name, Account_Number__c, Account_Serno__c, Partner_Description__c from Financial_Account__c where
                                   Id IN :financialAccIdSet order by Name desc];
        }
    }
    Public List<Selectoption> getselectedaccnamefields()
    {
        List<SelectOption> FinAccList = new List<SelectOption>();
        for (Financial_Account__c f: financialAccountList)
        {
            FinAccList.add(new SelectOption(f.Id ,'Financial Account: '  + f.Name + 
                                            '   Account Number: '+ f.Account_Number__c+ '   Partner Description: '
                                            +  f.Partner_Description__c  + '   Account Serno: '+ f.Account_Serno__c));
        }
        return FinAccList;
    }
    public Id getParameterValue(string key)
    {      
        return apexpages.currentpage().getparameters().get(key);
    }
    public PageReference save() 
    {
                    if(String.isnotBlank(selectedfinacc)  ) 
        {
            result=[select id from Financial_Account__c where id= :selectedfinacc limit 1]; 
            req.Person__c = con.id;
            if (result == null  || result.size()== 0 )
            {
                req.Financial_Account__c = null ;
                insert req;
            }
            else if (result != null  || result.size() > 0 || !result.isEmpty())
            {
                req.Financial_Account__c = result[0].id;
            	upsert req;
            }
            system.debug('FA insert');
           }
        else
        {
            system.debug('FA not insert');
        }
        return null;
    }
}