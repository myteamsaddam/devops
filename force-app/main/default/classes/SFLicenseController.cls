Public class SFLicenseController {
    Public Integer available {set;get;}
    Public List<UserLicense> ulist {set;get;}
    Public List<SFlicensedata> sflist {set;get;}
    
    Public List<SFlicensedata> getSFlic(){
        ulist = new List<UserLicense>();
        sflist = new List<SFlicensedata>();
        String query = 'SELECT TotalLicenses, UsedLicenses, Name FROM UserLicense where Name=\'Salesforce\'';
        ulist = Database.query(query);
        system.debug('-----ulist----'+ulist);
        for(UserLicense ul : ulist)
        {
            available = ul.TotalLicenses - ul.UsedLicenses;
            system.debug('-----available----'+available);
            sflist.add(new SFlicensedata('Total',ul.TotalLicenses));
            sflist.add(new SFlicensedata('Used',ul.UsedLicenses));
            sflist.add(new SFlicensedata('Avaiable',available));
        }
        return sflist;
    }
    
    public class SFlicensedata
    {  
        public String name { get; set; }  
        public Integer sfcount { get; set; }  
        
        public SFlicensedata(String name, Integer sfcount) 
        {  
            this.name = name;  
            this.sfcount = sfcount;  
        }  
    }   
}