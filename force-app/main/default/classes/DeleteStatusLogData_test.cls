@isTest
public class DeleteStatusLogData_test {
    
    @isTest
    static void test1(){
        
        
        //Status Log
        StatusLog__c objStatusLog1 = new StatusLog__c();
        objStatusLog1.Name='Test Status Log1';
        insert objStatusLog1;
        
        StatusLog__c objStatusLog2 = new StatusLog__c();
        objStatusLog2.Name='Test Status Log2';
        insert objStatusLog2;
        
        StatusLog__c objStatusLog3 = new StatusLog__c();
        objStatusLog3.Name='Test Status Log3';
        insert objStatusLog3;
        
        test.setCreatedDate(objStatusLog1.Id,date.today()-100);
        test.setCreatedDate(objStatusLog2.Id,date.today()-95);
        test.setCreatedDate(objStatusLog3.Id,date.today()-91);
        System.assertEquals(3, [Select Id from StatusLog__c where CreatedDate < N_DAYS_AGO:90].size());
        
        test.startTest();
        
        DeleteStatusLogData d=new DeleteStatusLogData();
        Database.executeBatch(d, 3);
        
        test.stopTest();
        
        System.assertEquals(0, [Select Id from StatusLog__c where CreatedDate < N_DAYS_AGO:90].size());
        
        
    }

    
    @isTest
    static void test2(){
        
        //Status Log
        StatusLog__c objStatusLog1 = new StatusLog__c();
        objStatusLog1.Name='Test Status Log1';
        insert objStatusLog1;
        
        StatusLog__c objStatusLog2 = new StatusLog__c();
        objStatusLog2.Name='Test Status Log2';
        insert objStatusLog2;
        
        StatusLog__c objStatusLog3 = new StatusLog__c();
        objStatusLog3.Name='Test Status Log3';
        insert objStatusLog3;
        
        test.setCreatedDate(objStatusLog1.Id,date.today()-100);
        test.setCreatedDate(objStatusLog2.Id,date.today()-95);
        test.setCreatedDate(objStatusLog3.Id,date.today()-91);
        System.assertEquals(3, [Select Id from StatusLog__c where CreatedDate < N_DAYS_AGO:90].size());
        
        test.startTest();
        
        DeleteStatusLogData d=new DeleteStatusLogData();
        String sch = '0 0 * * * ?';
        System.schedule('test job', sch, d);
        
        test.stopTest();
        
        
    }

}