/*
----------------------------------------------------------------------------------------
Author    Date         Description
Shameel   31/03/2017   Input class to implement SOA service using Integration framework.
----------------------------------------------------------------------------------------
*/
public class GetLoanServiceInput{

    public integer InstitutionId;
    public String SSN;
    public String AccountNumber;
    
    //To Cover Test Class 100%
    public void test()
    {
        
    }
   
}