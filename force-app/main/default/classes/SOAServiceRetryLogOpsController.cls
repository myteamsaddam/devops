public with sharing class SOAServiceRetryLogOpsController 
{
    //To insert SOAServiceRetryLog__c records.
    public static List<SOAServiceRetryLog__c> insertSOAServiceRetryLog = new List<SOAServiceRetryLog__c>();
    //To assign field values of SOAServiceRetryLog__c object.
    Public static SOAServiceRetryLog__c objSOAServiceRetryLog = new SOAServiceRetryLog__c();
    //Constructer
    /*public SOAServiceRetryLogOpsController()
    {
         insertSOAServiceRetryLog = new List<SOAServiceRetryLog__c>();
    }*/
    //To be called from outside with parameters to assign field values of the record.
    public static void assignDataSOAServiceRetryLog(Id id,Decimal NumOfAttmpt, String ReqAgnt, String RespnseStats, String ServceName, String ServceReqBdy)
    {
        objSOAServiceRetryLog = new SOAServiceRetryLog__c();
        if(id != null)
        {
            objSOAServiceRetryLog.Id = id;
        }
        if(NumOfAttmpt != null)
        {
            objSOAServiceRetryLog.Number_of_Attempt__c = NumOfAttmpt;
        }
        else
        {
            objSOAServiceRetryLog.Number_of_Attempt__c = 0;
        }
        if(ReqAgnt != null && ReqAgnt != '')
        {
            objSOAServiceRetryLog.Requested_Agent__c = ReqAgnt;
        }
        else
        {
            objSOAServiceRetryLog.Requested_Agent__c = '';
        }
        if(RespnseStats != null && RespnseStats != '')
        {
            objSOAServiceRetryLog.Response_Status__c = RespnseStats;
        }
        else
        {
            objSOAServiceRetryLog.Response_Status__c = '';
        }
        if(ServceName != null && ServceName != '')
        {
            objSOAServiceRetryLog.Service_name__c = ServceName;
        }
        else
        {
            objSOAServiceRetryLog.Service_name__c = '';
        }
        if(ServceReqBdy != null && ServceReqBdy != '')
        {
            objSOAServiceRetryLog.Service_request_body__c = ServceReqBdy;
        }
        else
        {
            objSOAServiceRetryLog.Service_request_body__c = '';
        }
        if(objSOAServiceRetryLog != null)
        {
            insertSOAServiceRetryLog.add(objSOAServiceRetryLog);
        }
    }
    //To be called from outside to insert records.
    public static void insertDataSOAServiceRetryLog()
    {
        if(insertSOAServiceRetryLog.size()>0){
        Database.UpsertResult[] srList = Database.Upsert(insertSOAServiceRetryLog, false);
        
        // Iterate through each returned result
        for (Database.UpsertResult sr : srList) 
        {
            if (sr.isSuccess()) 
            {
                // Operation was successful, so get the ID of the record that was processed
                System.debug('Successfully inserted SOAServiceRetryLog. SOAServiceRetryLog ID: ' + sr.getId());
            }
            else 
            {
                // Operation failed, so get all errors                
                for(Database.Error err : sr.getErrors()) 
                {
                    System.debug('The following error has occurred.');                    
                    System.debug(err.getStatusCode() + ': ' + err.getMessage());
                    System.debug('SOAServiceRetryLog fields that affected this error: ' + err.getFields());
                }
            }
        }
        }
    }
}