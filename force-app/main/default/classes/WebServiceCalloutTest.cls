@isTest
private class WebServiceCalloutTest {
   Static{
   GlobalSettings__c globalSet = new GlobalSettings__c();
       globalSet.name='ServiceStrategies';
       globalSet.Value__c='SOAPService';
   insert globalSet ;
   
   ServiceHeaders__c servHeader = new ServiceHeaders__c();
       servHeader.name='SOAPHTTP';
       servHeader.AcceptEncoding__c=null;
       servHeader.Authorization__c=null;
       servHeader.ContentEncoding__c=null;
       servHeader.Content_Length__c=null;
       servHeader.ContentType__c=null;
   insert servHeader ;
   
    List<ServiceSettings__c> servsettinglist = new List<ServiceSettings__c>();
    
   ServiceSettings__c servsetting = new ServiceSettings__c();
       servsetting.name='GetStatementHistory';
       servsetting.Certificate__c=null;
       servsetting.CertificateName__c=null;
       servsetting.CertificatePassword__c=null;
       servsetting.Compressed__c=false;
       servsetting.EndPoint__c='https://services-test.entercard.com/gateway/services/CSSService';
       servsetting.EndPointParameters__c=false;   
       servsetting.HeaderName__c='SOAPHTTP';
       servsetting.Input_Class__c='GetStatementHistoryInput';
       servsetting.LogAtFuture__c=false;
       servsetting.LogRequest__c=true;
       servsetting.LogResponse__c=true; 
       servsetting.LogStatus__c=true;
       servsetting.LogWithCallout__c=false;
       servsetting.Operation__c='test';
       servsetting.OutputClass__c='GetStatementHistoryOutput';
       servsetting.ProcessingClass__c='GetStatementCallout';
       servsetting.Strategy__c='SOAPService';
       servsetting.Timeout__c=null;
       servsettinglist.add(servsetting) ;
  
   ServiceSettings__c servsettingtrans = new ServiceSettings__c();
       servsettingtrans.name='viewTransactionDetails';
       servsettingtrans.Certificate__c=null;
       servsettingtrans.CertificateName__c=null;
       servsettingtrans.CertificatePassword__c=null;
       servsettingtrans.Compressed__c=false;
       servsettingtrans.EndPoint__c='https://services-test.entercard.com/gateway/services/CSSService';
       servsettingtrans.EndPointParameters__c=false;   
       servsettingtrans.HeaderName__c='SOAPHTTP';
       servsettingtrans.Input_Class__c='ViewTransactionsHistoryInput';
       servsettingtrans.LogAtFuture__c=false;
       servsettingtrans.LogRequest__c=true;
       servsettingtrans.LogResponse__c=true; 
       servsettingtrans.LogStatus__c=true;
       servsettingtrans.LogWithCallout__c=false;
       servsettingtrans.Operation__c='test';
       servsettingtrans.OutputClass__c='ViewTransactionsHistoryOutput';
       servsettingtrans.ProcessingClass__c='ViewTransCallout';
       servsettingtrans.Strategy__c='SOAPService';
       servsettingtrans.Timeout__c=null;
       servsettinglist.add(servsettingtrans);
       
       
        ServiceSettings__c servsettingGetCust = new ServiceSettings__c();
       servsettingGetCust.name='GetCustomerDetails';
       servsettingGetCust.Certificate__c=null;
       servsettingGetCust.CertificateName__c=null;
       servsettingGetCust.CertificatePassword__c=null;
       servsettingGetCust.Compressed__c=false;
       servsettingGetCust.EndPoint__c='https://services-test.entercard.com/gateway/services/CSSService';
       servsettingGetCust.EndPointParameters__c=false;   
       servsettingGetCust.HeaderName__c='SOAPHTTP';
       servsettingGetCust.Input_Class__c='GetCustomerInput';
       servsettingGetCust.LogAtFuture__c=false;
       servsettingGetCust.LogRequest__c=true;
       servsettingGetCust.LogResponse__c=true; 
       servsettingGetCust.LogStatus__c=true;
       servsettingGetCust.LogWithCallout__c=false;
       servsettingGetCust.Operation__c='test';
       servsettingGetCust.OutputClass__c='GetCustomerOutput';
       servsettingGetCust.ProcessingClass__c='GetCustomerCallout';
       servsettingGetCust.Strategy__c='SOAPService';
       servsettingGetCust.Timeout__c=null;
       servsettinglist.add(servsettingGetCust);
       
       ServiceSettings__c servicesettingLimitChanges = new ServiceSettings__c(
       Name='GetLimitChanges', 
       CertificateName__c=null, 
       CertificatePassword__c=null, 
       Certificate__c=null, 
       Compressed__c=false, 
       EndPoint__c='https://services-test.entercard.com/gateway/services/CSSService', 
       HeaderName__c='SOAPHTTP', 
       LogStatus__c=true, 
       Operation__c='Get', 
       OutputClass__c='GetLimitChangesServiceOutput', 
       ProcessingClass__c='GetLimitChangesServiceCallout', 
       Strategy__c='SOAPService', 
       Timeout__c=null, 
       LogRequest__c=true, 
       LogResponse__c=true, 
       LogAtFuture__c=false, 
       LogWithCallout__c=false, 
       Input_Class__c='GetLimitChangesServiceInput', 
       EndPointParameters__c=false
       );
       servsettinglist.add(servicesettingLimitChanges);
       insert servsettinglist ;
       
   }
    @isTest static void testGetStatementCallout() {              
        
        GetStatementHistoryInput input = new GetStatementHistoryInput();
        input.serNo ='35916';
         CSSServiceHelper.getTransactionHistory(2,'35916');

        Test.startTest();
 // This causes a fake response to be generated 
        Test.setMock(WebServiceMock.class, new GetStatementsHistoryWebServiceMockclass());
        
       
        // Call the method that invokes a callout 
     CSSServiceHelper.getTransactionHistory(2,input.serNo);
             Test.stopTest();
     

    }
     @isTest static void testviewTransactionCallout() {              
        String a ='1990-01-01';
        Date fromdate   = Date.valueOf(a);
        ViewTransactionsHistoryInput input = new ViewTransactionsHistoryInput();
        input.InstitutionId=2;
        input.Reference='ACCOUNT';
        input.PageSize=5;
        input.SkipIndex=1;
        input.From_x=fromdate;
        input.To=SYSTEM.TODAY();
        input.Number_x='5299360000083134';
        input.TransactionType='ALL';
        CSSServiceHelper.viewTransactionDetails(input.InstitutionId, null,input.PageSize, input.SkipIndex,input.From_x,input.To,'',false,input.Number_x,input.TransactionType);

        Test.startTest();
 // This causes a fake response to be generated 
        Test.setMock(WebServiceMock.class, new ViewTransactionWebServiceMockclass());
        
       
        // Call the method that invokes a callout 
     CSSServiceHelper.viewTransactionDetails(input.InstitutionId, input.Reference,input.PageSize, input.SkipIndex,input.From_x,input.To,'',false,input.Number_x,input.TransactionType);
      Test.stopTest();

    }
    
     @isTest static void testGetCustomerCallout() { 
    
        GetCustomerInput InputObj = new GetCustomerInput();
        InputObj.InstitutionId=2;
        InputObj.SSN='00016';
        InputObj.RetrieveCards='Y';
        InputObj.RetrieveAccounts='N';
        CSSServiceHelper.getCustomerDetails(InputObj.InstitutionId,null,InputObj.RetrieveCards,InputObj.RetrieveAccounts);

        Test.startTest();
        // This causes a fake response to be generated 
        Test.setMock(WebServiceMock.class, new GetCustomerWebServiceMockclass());
        
        // Call the method that invokes a callout 
        CSSServiceHelper.getCustomerDetails(InputObj.InstitutionId, InputObj.SSN,InputObj.RetrieveCards,InputObj.RetrieveAccounts);
         Test.stopTest();
    }
    
    
    @isTest static void testGetLimitChangesCallout() { 
    
        GetLimitChangesServiceInput InputObj = new GetLimitChangesServiceInput();
        date fromDate = date.parse('2016-09-17');
        InputObj.AccountNo='5438730000000614';//4581990000005919, 5299360000024774
        InputObj.From_x=fromDate;
        InputObj.InstitutionId=2;
         CSSServiceHelper.GetLimitChanges(null);
        Test.startTest();
        // This causes a fake response to be generated 
        Test.setMock(WebServiceMock.class, new GetLimitChangesServiceMockclass());
        
        // Call the method that invokes a callout 
        CSSServiceHelper.GetLimitChanges(InputObj);
        Test.stopTest();
    }
}