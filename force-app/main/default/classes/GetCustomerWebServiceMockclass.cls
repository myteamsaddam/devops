@isTest
global class GetCustomerWebServiceMockclass implements WebServiceMock {
   global void doInvoke(
           Object stub,
           Object request,
           Map<String, Object> response,
           String endpoint,
           String soapAction,
           String requestName,
           String responseNS,
           String responseName,
           String responseType) {
           
           
           system.debug('***stub***'+stub);
           system.debug('***request***'+request);
           system.debug('***requestName***'+requestName);
           system.debug('***responseNS***'+responseNS);
           system.debug('***responseName***'+responseName);
           system.debug('***responseType***'+responseType);
           system.debug('***response***'+response);
           
       SOAPCSSServiceResponse.GetCustomerResponse_element  respElement= new SOAPCSSServiceResponse.GetCustomerResponse_element ();
       SOAPCSSServiceResponse.getCustomerResponseRecordType  stshis = new SOAPCSSServiceResponse.getCustomerResponseRecordType ();
       SOAPCSSServiceResponse.customerType customerTypeinstance = new SOAPCSSServiceResponse.customerType();
       SOAPCSSServiceResponse.insurenceListType insuranceListTypeinstance = new SOAPCSSServiceResponse.insurenceListType();
       SOAPCSSServiceResponse.accountCustResType accountCustResTypeinstance = new SOAPCSSServiceResponse.accountCustResType();
       SOAPCSSServiceResponse.customerAddressType[] CusAddresslist = new List<SOAPCSSServiceResponse.customerAddressType>();
       SOAPCSSServiceResponse.accountCustResType[] CusResTypelist = new List<SOAPCSSServiceResponse.accountCustResType>();
       SOAPCSSServiceResponse.cardType[] CardTypelist = new List<SOAPCSSServiceResponse.cardType>();
       SOAPCSSServiceResponse.customerAddressType[] CusAddrTypelist = new List<SOAPCSSServiceResponse.customerAddressType>();
       SOAPCSSServiceResponse.rememberInsurenceListType[] rememberInsurencelist = new List<SOAPCSSServiceResponse.rememberInsurenceListType>();
       SOAPCSSServiceResponse.freeInsurenceListType[] freeInsurencelist = new List<SOAPCSSServiceResponse.freeInsurenceListType>();
       
       SOAPCSSServiceResponse.cardStatusType cardStatusTypeinstance = new SOAPCSSServiceResponse.cardStatusType();
       SOAPCSSServiceResponse.productType[] productTypelist = new List<SOAPCSSServiceResponse.productType>();
       
       SOAPCSSServiceResponse.rememberInsurenceListType rememberInsuranceinstance = new SOAPCSSServiceResponse.rememberInsurenceListType();
       SOAPCSSServiceResponse.freeInsurenceListType freeInsurenceinstance = new SOAPCSSServiceResponse.freeInsurenceListType();
       SOAPCSSServiceResponse.cardType cardTypeinstance = new SOAPCSSServiceResponse.cardType();
       SOAPCSSServiceResponse.productType productTypeinstance = new SOAPCSSServiceResponse.productType();
       SOAPschemasEntercardComTypelib10.directDebitType directDebitTypeinstance = new SOAPschemasEntercardComTypelib10.directDebitType();
       SOAPCSSServiceResponse.accountStatusType accountStatusTypeInstance = new SOAPCSSServiceResponse.accountStatusType();
       SOAPCSSServiceResponse.customerAddressType cusAddrTypeinstance = new SOAPCSSServiceResponse.customerAddressType();
       
       //freeInsurenceinstance.FreeInsurence[0] = 'FreeInsurence';
       freeInsurenceinstance.FreeInsurence =new List<string>();
       freeInsurenceinstance.FreeInsurence.add('FreeInsurence');
       //rememberInsuranceinstance.RememberInsurence[0] = 'RememberInsurence';
       rememberInsuranceinstance.RememberInsurence =new List<string>();
       rememberInsuranceinstance.RememberInsurence .add('RememberInsurence');
       rememberInsurencelist.add(rememberInsuranceinstance);
       freeInsurencelist.add(freeInsurenceinstance);
       insuranceListTypeinstance.RememberInsurenceList = rememberInsurencelist;       
       insuranceListTypeinstance.FreeInsurenceList = freeInsurencelist;
       
       productTypeinstance.PSerno = 1258963;
       productTypeinstance.InstitutionId = 2;
       productTypeinstance.Name = 'Name';
       productTypeinstance.Description = 'Description';
       productTypelist.add(productTypeinstance);
       
       cardStatusTypeinstance.StGen = 'Description';
       cardStatusTypeinstance.StGenDesc = 'Description';
       cardStatusTypeinstance.StFin = 'Description';
       cardStatusTypeinstance.StFinDesc = 'Description';
       cardStatusTypeinstance.StAuth = 'Description';
       cardStatusTypeinstance.StAuthDesc = 'Description';
       cardStatusTypeinstance.StEmb = 'Description';
       cardStatusTypeinstance.StEmbDesc = 'Description';
       
       cardTypeinstance.CardSerno = 524858519;
       cardTypeinstance.TruncatedCardNo = '7136785481583561';
       cardTypeinstance.WrongPinAttempts = 2;
       cardTypeinstance.LastName = 'LastName';
       cardTypeinstance.FirstName = 'LastName';
       cardTypeinstance.EmbName = 'LastName';
       cardTypeinstance.EmbDate = date.today();
       cardTypeinstance.PrimaryCard = TRUE;
       cardTypeinstance.ExpiryDate = date.today() + 100;
       cardTypeinstance.SecureCodeInfo = '8574147';
       cardTypeinstance.InsurenceList = insuranceListTypeinstance;
       cardTypeinstance.Product = productTypelist;
       cardTypeinstance.CardStatus = cardStatusTypeinstance;
       CardTypelist.add(cardTypeinstance);
       
       directDebitTypeinstance.Enabled = 'fdsfsf';
       directDebitTypeinstance.Amount = 10.25;
       directDebitTypeinstance.Percentage = 85.26;
       directDebitTypeinstance.Combination = 88552255;
       directDebitTypeinstance.ClearingNo = '6556';
       directDebitTypeinstance.AccountNo = '5165165';
       
       accountStatusTypeInstance.StGen = 'Status';
       accountStatusTypeInstance.StGenDesc = 'Status';
       accountStatusTypeInstance.StFin = 'Status';
       accountStatusTypeInstance.StFinDesc = 'Status';
       accountStatusTypeInstance.StAuth = 'Status';
       accountStatusTypeInstance.StAuthDesc = 'Status';
               
       accountCustResTypeinstance = new SOAPCSSServiceResponse.accountCustResType();
       accountCustResTypeinstance.Serno = 3123123;
       accountCustResTypeinstance.AccountNo = '12w23eeeeedd';
       accountCustResTypeinstance.AccountStatus = accountStatusTypeInstance;
       accountCustResTypeinstance.AccountRating = 'AccountRating';
       accountCustResTypeinstance.CreateDate = date.today();
       accountCustResTypeinstance.RiskIndicator = 'Y';
       accountCustResTypeinstance.Product = productTypelist;
       accountCustResTypeinstance.OTB = 555.25;
       accountCustResTypeinstance.ProductChanged = 'ProductChanged';
       accountCustResTypeinstance.Balance = 52;
       accountCustResTypeinstance.Limit_x = 556;
       accountCustResTypeinstance.PossibleLimitIncrease = 1122;
       accountCustResTypeinstance.Einvoice = 'EInvoice';
       accountCustResTypeinstance.DirectDebit = directDebitTypeinstance;
       //accountCustResTypeinstance.Membership = 
       accountCustResTypeinstance.PPI = 'PPI';
       accountCustResTypeinstance.PickupCode = 'PickupCode';
       accountCustResTypeinstance.MTP = 616;
       accountCustResTypeinstance.Card = CardTypelist;
       CusResTypelist.add(accountCustResTypeinstance);
       
       cusAddrTypeinstance.AddressType = 'Home';
       cusAddrTypeinstance.Address1 = 'Test address';
       cusAddrTypeinstance.Zip = '711105';
       cusAddrTypeinstance.City = 'Test City';
       cusAddrTypeinstance.Country = 'Test Country';
       cusAddrTypeinstance.Telephone = '16165156161';
       CusAddrTypelist.add(cusAddrTypeinstance);
       
       customerTypeinstance.Serno = 2123123; 
       customerTypeinstance.SSN = '0000000061';
       customerTypeinstance.FirstName = 'FirstName';
       customerTypeinstance.LastName = 'LastName';
       customerTypeinstance.Mobile = 'Mobile';
       customerTypeinstance.Email = 'Testemail22@test.com';
       customerTypeinstance.TOTB = 100.000;
       customerTypeinstance.TotalLimit = 10.00;
       customerTypeinstance.TotalPossibleLimitIncrease = 100.00;
       customerTypeinstance.CustomerAddress = CusAddrTypelist;
       customerTypeinstance.Account = CusResTypelist;
       respElement.GetCustomerResponseRecord=stshis;
       respElement.GetCustomerResponseRecord.Customer=customerTypeinstance;
       response.put('response_x', respElement);
    }
    }