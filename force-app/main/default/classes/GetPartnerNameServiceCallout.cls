Public class GetPartnerNameServiceCallout extends SoapIO {
    NonCustomerServiceRequest.GetPartnersNameRequestBodyType getPartnersNameRequestBodyInstance;
    GetPartnerInputs inputs;
    
    Public  void convertInputToRequest(){
        inputs = (GetPartnerInputs)serviceInput;
        getPartnersNameRequestBodyInstance = new NonCustomerServiceRequest.GetPartnersNameRequestBodyType();
        getPartnersNameRequestBodyInstance.Institution_ID = inputs.InstitutionId;
        system.debug('------getPartnersNameRequestBodyInstance-----'+getPartnersNameRequestBodyInstance.Institution_ID);
    }
    
    Public  override object invokeWebserviceCallout(SoapRequest soapRequest){
        NonCustomerTypelib10.headerType HeadInstance = new NonCustomerTypelib10.headerType();
        HeadInstance.MsgId = 'SFDC consuming Get Consent SOA service. Time Stamp=>'+system.now();
        HeadInstance.CorrelationId=Common_Settings__c.getValues('CorrelationId').Common_Value__c;
        HeadInstance.RequestorId=Common_Settings__c.getValues('RequestorId').Common_Value__c;
        HeadInstance.SystemId=Common_Settings__c.getValues('SystemId').Common_Value__c;
        HeadInstance.InstitutionId=inputs.InstitutionId;
        
        NonCustomerCssserviceInvoke.x_xsoap_CSSServiceESB_CSSServicePT invokeInstance = new NonCustomerCssserviceInvoke.x_xsoap_CSSServiceESB_CSSServicePT();
        invokeInstance.timeout_x = 30000;
        
        ServiceSettings__c serviceObj = ServiceSettings__c.getValues('GetPartnerDetails');
        if(serviceObj!=null && serviceObj.EndPoint__c!='' && serviceObj.EndPoint__c!=null)
            invokeInstance.endpoint_x = serviceObj.EndPoint__c;
        String username = serviceObj.Username__c;
        String password = serviceObj.Password__c;
        
        Blob headerValue = Blob.valueOf(username + ':' + password);
        String authorizationHeader = 'BASIC ' +EncodingUtil.base64Encode(headerValue);
        
        invokeInstance.inputHttpHeaders_x = new Map<String, String>();
        invokeInstance.inputHttpHeaders_x.put('Authorization', authorizationHeader);
        
        NonCustomerOasis20.UsernameToken creds=new NonCustomerOasis20.UsernameToken();
        creds.Username=serviceObj.Username__c;
        creds.Password=serviceObj.Password__c;
        
        NonCustomerOasis20.Security_element security_ele = new NonCustomerOasis20.Security_element();
        security_ele.UsernameToken = creds;
        invokeInstance.SecurityPart = security_ele;
        
        system.debug('----security_ele--'+security_ele);
        
        NonCustomerServiceResponse.GetPartnersNameResponse_element getPartnersNameResponse_element;
        getPartnersNameResponse_element = invokeInstance.getPartnersName(HeadInstance, getPartnersNameRequestBodyInstance);
        System.debug('-----getPartnersNameResponse_element -->'+getPartnersNameResponse_element);
        return getPartnersNameResponse_element;
        //return null;
        
    }
    
    Public override object convertResponseToOutput(object response, map<string, string> responseHeader, ServiceStatus serviceStatus){
        system.debug('---response---'+response);
        if(response==null)
            return null;
        
        NonCustomerServiceResponse.GetPartnersNameResponse_element getPartnersNameResponse_elementInstance = (NonCustomerServiceResponse.GetPartnersNameResponse_element)response;
        NonCustomerServiceResponse.GetPartnersNameResponseRecordType[] getPartnersNameResponseRecordInstance = getPartnersNameResponse_elementInstance.GetPartnersNameResponseRecord;
        
        system.debug('***getPartnersNameResponseRecordInstance***'+getPartnersNameResponseRecordInstance);
        
        GetPartnerOutputs getPartnerServiceOutput = new GetPartnerOutputs();
        
        if(getPartnersNameResponseRecordInstance!=null)
        {
            getPartnerServiceOutput.GetPartnerResponseRecord = new List<GetPartnerOutputs.GetPartnerResponseRecordType>();
            for(NonCustomerServiceResponse.GetPartnersNameResponseRecordType partnerRes : getPartnersNameResponseRecordInstance)
            {
                GetPartnerOutputs.GetPartnerResponseRecordType  Partner = new GetPartnerOutputs.GetPartnerResponseRecordType();
                Partner.InstitutionId = partnerRes.Institution_ID;
                Partner.PartnerId = partnerRes.PartnerID;
                Partner.PartnerName = partnerRes.PartnerName;
                
                getPartnerServiceOutput.GetPartnerResponseRecord.add(Partner);
            }
        }	
        
        system.debug('***getPartnerServiceOutput***'+getPartnerServiceOutput);
        
        return getPartnerServiceOutput;
        //return null;
        
    }
}