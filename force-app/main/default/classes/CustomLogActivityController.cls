public with sharing class CustomLogActivityController {
    
    @AuraEnabled(cacheable=true)
    Public Static Map<String,String> getFinRecs(Id CustId){
        System.debug('-----conId---'+CustId);
        Map<String, String> option =  new Map<String, String>();
        Set<Id> financialAccIdSet = new Set<Id>();
        List<Card__c> cardList = [SELECT Id, Name, People__c, Financial_Account__c FROM Card__c where People__c=:CustId and Financial_Account__c!=null ORDER BY CreatedDate DESC Limit 50000];
        for(Card__c card:cardList)
        {
            financialAccIdSet.add(card.Financial_Account__c);
        }
        
        List<Financial_Account__c> financialAccList = [SELECT Id, Name, Customer__c, Account_Number__c,Account_Serno__c,Product__c,Product__r.name FROM Financial_Account__c where Id IN:financialAccIdSet ORDER BY CreatedDate DESC Limit 50000];
        
        if(financialAccList.size() == 1){
            option.put(financialAccList[0].Id,financialAccList[0].Account_Number__c+' : '+(financialAccList[0].Product__r.name!=null?financialAccList[0].Product__r.name:'N/A'));
        }else {
            for(Financial_Account__c fa : financialAccList){
                option.put('--None--','--None--');
                option.put(fa.Id,fa.Account_Number__c+' : '+(fa.Product__r.name!=null?fa.Product__r.name:'N/A'));
            }
        }
        option.put('Other','Other');
        return option;
    }
        
    @AuraEnabled(cacheable=true)
    Public Static Map<String, String> getCardRecs(String financialVal, Id CustId){
        Map<String, String> cardMap = new Map<String, String>();
        List<Card__c> cardList = [SELECT Id, Name, People__c, Financial_Account__c,Card_Number_Truncated__c,Product__r.name,GeneralStatus__c FROM Card__c where People__c=:CustId and Financial_Account__c=:financialVal ORDER BY ExpiryDate__C DESC Limit 50000];
        system.debug('------card----'+cardList.size());        
        system.debug('---financialVal-'+financialVal);
        
        if(cardList.size() == 1){
            cardMap.put(cardList[0].Id,cardList[0].Card_Number_Truncated__c+' : '+(cardList[0].GeneralStatus__c!=null?cardList[0].GeneralStatus__c:'N/A'));
        } else {
            for(Card__c card:cardList)
            {
                cardMap.put(card.id , card.Card_Number_Truncated__c+' : '+(card.GeneralStatus__c!=null?card.GeneralStatus__c:'N/A'));
            }
        }
        if(financialVal == 'Other'){
            cardMap.put('Other','Other'); 
        }
        if(financialVal == '--None--'){
            cardMap.put('--None--','--None--');
        }
        return cardMap;
    }
    
    @AuraEnabled
    public static string doCaseUpdate(String CasId, String custId, String finId, String CardId, String commnt, String Catgry,String SaveAtmp, String Saved, String Reason, String Offer, String type){
        
        String msg = '';
        try
        {
            system.debug('---'+SaveAtmp+'--'+Saved+'--'+Reason+'--'+Offer);
            if((FinId == '--None--' || FinId == '' || FinId == 'Other' || Catgry == null)
               || (Catgry == 'Account closure' && (SaveAtmp == '' || Saved == '' || Reason == '' || Offer == '' || FinId == 'Other')))
            {
                if(FinId == '--None--' || FinId == '' || FinId == 'Other'){
                    msg = System.Label.Financial_Value;
                }
                If(Catgry == null){
                    msg = System.Label.Category_not_select_msg;
                }
                system.debug('---'+SaveAtmp+'--'+Saved+'--'+Reason+'--'+Offer);
                if(Catgry == 'Account closure')
                {
                    if(SaveAtmp == '')
                        msg ='Please Select the Save Attempt';
                    if(Saved == '')
                        msg ='Please Select the Saved';
                    if(Reason == '')
                        msg='Please Select the Reason';
                    if(Offer == '')
                        msg = 'Please Select the Offer';
                    if(FinId == 'Other')
                        msg = System.Label.Financial_Value;
                }	
                return msg;
            }
            
            system.debug('--FinId-'+FinId+'--Catgry-'+Catgry);
            
            if(FinId != '--None--' && FinId != 'Other' && Catgry != null){
                Card__c[] crdlst = [Select Id, Card_Serno__c,People_Serno__c,Financial_Account_Serno__c,GeneralStatus__c, People__r.Account.Customer_Serno__c, People__r.SSN__c, People__r.Institution_Id__c from Card__c where People__c =: CustId AND Id =: CardId]; 
                case cselist = [Select Id, ContactId,Category__c, Financial_Account__c from Case where Id =: CasId];
                
                System.debug('-ContactId-'+cselist.ContactId+'=='+custId+'--finan--'+cselist.Financial_Account__c+'=='+finId);
                if(cselist.ContactId == null || cselist.ContactId != custId || cselist.Financial_Account__c == null || cselist.Financial_Account__c != finId || cselist.Category__c != Catgry){
                    Case cse = new Case();
                    cse.Id = CasId;
                    cse.Category__c = Catgry;
                    cse.ContactId = custId;
                    cse.Financial_Account__c = finId;
                    cse.Card__c = CardId;
                    
                    update cse;
                }
                //msg = 'Case updated successfully.';
                if(Catgry != null){
                    Task ts = new Task();
                    if(commnt != ''){
                        ts.Subject = 'Memo';
                    }else{
                        ts.Subject = 'Case'; 
                    }
                    ts.Category__c = Catgry;
                    ts.Type = type;
                    ts.Status = 'Completed';
                    ts.Description = commnt;
                    ts.WhatId = CasId;
                    if(commnt != null && commnt.length()>255){
                        ts.Comments__c = commnt.substring(0,254);  
                    } else {
                        ts.Comments__c =  commnt;
                    }
                    ts.WhoId = CustId;
                    if(FinId !='Other'){	
                        ts.Account_Serno__c = crdlst[0].Financial_Account_Serno__c;
                        ts.Customer_Serno__c = crdlst[0].People_Serno__c;
                        ts.Card_Serno__c = crdlst[0].Card_Serno__c;
                        ts.MAH_Serno__c = crdlst[0].People__r.Account.Customer_Serno__c;
                    }
                    insert ts;
                }
                msg = System.Label.Case_update_message; //'Case/Task is created/updated Successfully.';
                system.debug('-SaveAtmp-'+SaveAtmp+'-'+Saved+'-'+Reason+'-'+Offer);
                If(Catgry == 'Account closure' && (SaveAtmp != '' || Saved != '' || Reason != '' || Offer != '')){
                    Retention_Response__c rresp = new Retention_Response__c();
                    rresp.Financial_Account__c = FinId;
                    rresp.Save_Attempt__c = SaveAtmp;
                    rresp.Account_Closed__c = Saved;
                    rresp.Reason_for_Account_Closure__c = Reason;
                    rresp.Offer__c = Offer;
                    
                    insert rresp;
                }
                
                Contact[] conlst = [select id,Institution_Id__c,SSN__c from Contact where id=:CustId];
                system.debug('-conlst--'+conlst);
                if(conlst!= null && conlst.size()>0 && conlst[0].Institution_Id__c!=null)
                {
                    CustomerMemoExportDataOutput outputmemoexportdata = CSSServiceHelper.updateCustomerMemo('NewMemo','PersonSSN',conlst[0].SSN__c,'css',commnt,String.valueOf(conlst[0].Institution_Id__c));
                    system.debug('--outputmemoexportdata---'+outputmemoexportdata);
                }
            }
            
        }catch(Exception exp){
            StatusLogHelper.logSalesforceError('LogACall', 'E005', 'Error in Log Activity', exp, false);
        }
        return msg;
    }
}