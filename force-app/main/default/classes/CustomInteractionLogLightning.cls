public without sharing class CustomInteractionLogLightning {
    public id financialVal{get;set;}
    public id selectedcard{get;set;}
    public boolean tocreateCase{get;set;}
    public Datetime callStartTime;
    public string caseId{get; set;}
    public string statusMessageCTI{get; set;}
    public boolean isCaseTransfered{get; set;}
    public string queuename{get; set;}
    public string caseIdChosen {set;get;}
    Public string ContactId {set;get;}
    
    // To get current recordId.
    public String recordId { 
        get {
            if(recordId == null) {
                recordId = ApexPages.currentPage().getParameters().get('id');
            }
            System.debug('@@recordId : ' +recordId);
            return recordId;
        }
        set; 
    }
    
    //Creation of subject with system time stamp.
    private String Subject { 
        get {
            Subject = 'Call at ' + System.now().format('yyyy-MM-dd HH:mm:ss','Europe/Paris');
            return Subject;
        }
        set; 
    }
    public String ANI { get; set;}
    public String CallObject { get; set;} 
    public Integer CallDurationInSeconds { get; set;} 
    public String CallType { get; set;} 
    Public String PV6 {set;get;}
    public String CallDisposition { get; set;} 
    
    public String statusMessage { get; set; }
    
    //To create task
    public Task task { 
        get {
            if(task == null) {
                task = new Task();
                task.Status='Open';
            }
            return task;
        }
        set; 
    }    
    
    //To create Case.
    public Case objCase { 
        get {
            if(objCase == null) {
                objCase = new Case();
                objCase.Category__c='None';
                objCase.Status='In Progress';
                objCase.Origin = 'Phone';
                objCase.OwnerId = UserInfo.getUserId();
            }
            return objCase;
        }
        set; 
    }
    
    // Constructor.
    Public CustomInteractionLogLightning(RedirectCustPageSSNController controller){
		System.Debug('@@Constructor-for redirect');
    }
    public CustomInteractionLogLightning(){
        System.Debug('@@Constructor-for sample');
    }
    public CustomInteractionLogLightning(ApexPages.StandardController controller) 
    {
        System.Debug('@@Constructor');
        Id taskId;
        isCaseTransfered = false;
        if (Cache.Session.contains('local.CTI.caseId')) 
        {
            caseId = (string)Cache.Session.get('local.CTI.caseId');
            System.debug('@@caseId Session: ' +caseId);
        }
        //for outbound
        if (Cache.Session.contains('local.CTI.taskId')) 
        {
            taskId = (string)Cache.Session.get('local.CTI.taskId');
            System.debug('@@taskId Session: ' +taskId);
        }
        
        If(caseId !=null & caseId != '')
        {
            List<case> lstCase = [Select Id, Type, Description, CaseNumber, Origin, Category__c, Status, ContactId, Country__c, Department__c, Financial_Account__c, Card__c, Subject,OwnerId from Case where Id = :caseId];           
            System.debug('@@lstCase: ' +lstCase);
            
            If(lstCase != null && lstCase.size() > 0)
            {
                financialVal = lstCase[0].Financial_Account__c;
                selectedcard = lstCase[0].Card__c;
                If(lstCase[0].ContactId == recordId)
                {
                    objCase = lstCase[0];
                    
                    List<task> lstTask = [Select Id, whoId, whatId, subject, CallObject, cnx__UniqueId__c, ANI__c, Call_Type__c, status, Description, CallDurationInSeconds,createddate from Task where whatId = :caseId order by CreatedDate desc];
                    System.debug('@@lstTask: ' +lstTask);
                    If(lstTask != null && lstTask.size() > 0)
                    {
                        task = lstTask[0];
                        CallType = lstTask[0].Call_Type__c;
                        ANI = lstTask[0].ANI__c;
                        callStartTime = lstTask[0].createddate;
                    }
                    
                    financialVal = objCase.Financial_Account__c;
                    selectedcard = objCase.Card__c;
                }
            }
        }
        if(taskId!=null)
        {
            List<task> lstTask = [Select Id, whoId, whatId, subject, CallObject, cnx__UniqueId__c, ANI__c, Call_Type__c, status, Description, CallDurationInSeconds,createddate from Task where Id = :taskId order by CreatedDate desc];
            If(lstTask != null && lstTask.size() > 0 && lstTask[0].whoId==recordId)
            {
                task = lstTask[0];
                CallType = lstTask[0].Call_Type__c;
                ANI = lstTask[0].ANI__c;
                callStartTime = lstTask[0].createddate;
            }
        }
        
        If(objCase.Id == null)
        {
            SetDefaultValues();
        }
    }
    // To update the customer name in Task and case object.
    public void updateWhoWhatId() {
        system.debug('----number-updateWhoWhatId-3------');
        if(recordId!=null && recordId.substring(0,3) == '003')
        {
            List<contact> lstContact = [Select id from Contact where id =:recordId];
            If(lstContact != null && lstContact.size() > 0)
            {
                task.whoId = lstContact[0].Id;
                objCase.ContactId = lstContact[0].Id;
            }          
            System.debug('@@lstContact : ' +lstContact);
            System.debug('@@lstContact objCase : ' +objCase);
        }
        else
        {
            statusMessage = 'This log is not associated with a Case or Contact! Please save the case.';
            return;
        }
    }
    
    private void initializeTask() {
        updateWhoWhatId();
        //task.subject = Subject;
    }
    // To auto populate the country and department value in Log a call section.
    private void SetDefaultValues()
    {
        System.debug('@@Case null');
        UserRole[] ur = [select id,developername from UserRole where id=:UserInfo.getUserRoleId()];
        if(ur.size()>0)
        {   
            RDPC_Mapping__c rdpc = RDPC_Mapping__c.getValues(ur[0].developername);
            if(rdpc!=Null){
                objCase.Country__c=rdpc.Country__c;
                objCase.Department__c=rdpc.Department__c;
            }
        }
        tocreateCase = false;
        initializeTask();
    }
    
    public void setCallAttachedData() 
    {
        Cache.Session.remove('local.CTI.caseId');
        Cache.Session.remove('local.CTI.taskId');
        Cache.Session.remove('local.CTI.PV6');
        system.debug('---objCase.Id--'+objCase.Id);
        system.debug('---task.Id--'+task.Id);
        If(objCase.Id != null || task.Id != null)
        {
            objCase = null;
            task = null;
            SetDefaultValues();
        }  
        
        updateWhoWhatId();          
        System.Debug('@@CallObject: ' + CallObject);
        System.Debug('@@ANI: ' + ANI);
        task.CallObject = CallObject;
        task.cnx__UniqueId__c = CallObject;
        task.ANI__c = ANI;
        task.Call_Type__c = CallType;
        task.subject = Subject;
        callStartTime = System.now();
        system.debug('----Pv6----'+PV6);
        if(PV6 != null){
           Cache.Session.put('local.CTI.PV6', PV6); 
        }
        
        if(CallType=='inbound')
        {
            insert objCase;
            system.debug('----objCase.Id--'+objCase.Id);
            task.whatId = objCase.Id;
            insert task;//change by swarnava
            caseId = objCase.Id;
            Cache.Session.put('local.CTI.caseId', caseId);
        }
        
        else if(CallType=='outbound')
        {
            insert task;
            Cache.Session.put('local.CTI.taskId', task.Id);
        }
    }  
    
    Public Long connectedTime{set;get;}
    Public Long EndupTime{set;get;}
    
    public void setCallEndData() 
    {
        System.Debug('@@connectedTime: ' + connectedTime);
        System.Debug('@@EndupTime: ' + EndupTime);

        DateTime startime = DateTime.newInstance(connectedTime);
        system.debug('---strt--'+startime.second());

        CallDurationInSeconds = integer.valueOf(startime.second()); //endtime.second() - startime.second();
        system.debug('----CallDurationInSeconds--'+CallDurationInSeconds);
        task.CallDurationInSeconds = CallDurationInSeconds;
        task.CallDisposition = CallDisposition;  
        try{
            if(callType=='inbound')
            {
                If((callType=='inbound' && objCase.Category__c != null && objCase.Category__c != 'None')||(callType=='outbound'))
                {
                    Contact[] conlst = [select id,Institution_Id__c,SSN__c from Contact where id=:recordId];
                    
                    task.CallDurationInSeconds+=(Integer)Common_Settings__c.getValues('CTI').CTI_Wrap_Time__c;//add 10 seconds wrap to previous call duration
                    system.debug('----task.CallDurationInSeconds-'+task.CallDurationInSeconds);
                    task.Description = objCase.description;
                    
                    save();
                    
                    //update not closed case for total call handling time
                    if(callType=='inbound' && objCase.status!='Closed')
                    {
                        List<Case> casetotaltime = [select id ,Total_Call_Handling_Time__c,(select CallDurationInSeconds from tasks) from Case where id=:objCase.id];
                        Integer totalcallhandlingtime = 0;
                        
                        If(casetotaltime != null && casetotaltime.size() > 0)
                        {
                            Case case1 = casetotaltime[0];
                            for(Task t : case1.tasks)
                            {
                                if(t.CallDurationInSeconds!=null)
                                    totalcallhandlingtime+=t.CallDurationInSeconds;      
                            }
                        }
                        objCase.Total_Call_Handling_Time__c = totalcallhandlingtime;
                        update objCase;
                    }        
                }
                else
                {
                    ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error,System.Label.CTI_Category_Select));
                    
                }
            }
        }catch(Exception exp){
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error,exp.getMessage()));//for 1847
            StatusLogHelper.logSalesforceError('CustomInteractionLog', 'E005', 'Error in CTI', exp, false);//for 1847
        }
    }
    
    public void save() {
        System.Debug('@@Save Task: ' + task);
        System.debug('@@lstContact objCase : ' +objCase);
        try{
            If(recordId.substring(0,3) == '003') 
            {
                objCase.Origin = 'Phone';
                objCase.Priority = 'Medium';
                objCase.Subject = task.subject; 
                if(CallType=='inbound')
                {                  
                    System.Debug('@@Save objCase: ' + objCase);       
                    task.whatId = objCase.Id;
                }
                
            }
            List<Task> lt = [select status from Task where id=:task.id];
            If(callType=='outbound' || (lt != null && lt.size()>0 && lt[0].status!='Completed')){
                task.Status='Open';//task will not be closed upon call end
                upsert task;
            }    
            statusMessage = 'Last save at ' + DateTime.now();
        }catch(Exception exp){
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error,exp.getMessage()));//for 1847
            StatusLogHelper.logSalesforceError('CustomInteractionLog', 'E005', 'Error in CTI', exp, false);//for 1847
        }
    }
    
    public void saveaction() 
    {
        System.Debug('@@Save Task: ' + task);
        
        System.debug('@@lstContact objCase : ' +objCase);
        try{
            If(recordId.substring(0,3) == '003') 
            {
                //objCase.Origin = 'Phone';
                //objCase.Priority = 'Medium';
                //objCase.Status = 'Closed';
                objCase.Subject = task.subject; 
                objCase.Financial_Account__c = financialVal;
                objCase.Card__c=selectedcard;
                //if(objCase.status=='Closed')
                if(objCase.status!='Closed' || objCase.OwnerId==UserInfo.getUserId()){
                    if((callType=='inbound' && (objCase.status=='Closed' || objCase.OwnerId!=UserInfo.getUserId()) && objCase.Category__c != null && objCase.Category__c != 'None')||(callType=='outbound' && task.status=='Completed'))
                    { 
                        Contact[] conlst = [select id,Institution_Id__c,SSN__c from Contact where id=:recordId];   
                        task.status='Completed';
                        task.CallDurationInSeconds = CallDurationInSeconds;
                        task.Description = objCase.description;
                        Long cst = callStartTime.getTime();
                        Long cet = System.now().getTime();
                        task.CallDurationInSeconds = (Integer)(cet - cst)/1000;
                        
                        //Create memo in salesforce
                        Memo__c memoinsert = new Memo__c();
                        if(conlst != null && conlst.size()>0)
                            memoinsert.People__c = conlst[0].Id;
                        
                        memoinsert.Card__c = selectedcard;
                        memoinsert.Financial_Account__c = financialVal;
                        memoinsert.CTI_Category__c = objCase.Category__c;
                        if(objCase.id!=null)
                            memoinsert.Case__c = objCase.id;
                        if(task.Description!=null && task.Description.length()>255){//for 1847
                            memoinsert.Comments__c=task.Description.substring(0,254);
                            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Info,System.label.Memo_255_Charecter));
                        }
                        else    
                            memoinsert.Comments__c=task.Description;
                        
                        objCase.Financial_Account__c = financialVal;
                        objCase.Card__c=selectedcard;
                        
                        //create memo in prime
                        if(conlst!= null && conlst.size()>0 && conlst[0].Institution_Id__c!=null)
                            CSSServiceHelper.updateCustomerMemo('NewMemo','PersonSSN',conlst[0].SSN__c,'css',task.Description,String.valueOf(conlst[0].Institution_Id__c));
                        insert memoinsert;  
                        
                        //If(callType=='outbound' || task.status != 'Completed')
                        List<Task> lt = [select status from Task where id=:task.id];
                        If(callType=='outbound' || (lt.size()>0 && lt[0].status!='Completed'))             
                            upsert task;
                        
                        if(callType=='outbound')
                            objCase.status='Closed'; 
                        
                        Cache.Session.remove('local.CTI.caseId'); 
                        Cache.Session.remove('local.CTI.taskId');
                        statusMessage = 'Last save at ' + DateTime.now();
                    }    
                    //if(objCase.status=='Closed' && CallType=='inbound')
                    if(CallType=='inbound')
                    {
                        If(objCase.Category__c != null && objCase.Category__c != 'None')
                        {          
                            //insert objCase;
                            if(objCase.status=='Closed')
                                objCase.Total_Call_Handling_Time__c = task.CallDurationInSeconds;
                            update objCase;  
                            if(objCase.OwnerId!=UserInfo.getUserId()){//ownership changed
                                //objCase.Status='Closed';
                                isCaseTransfered = true;
                                statusMessageCTI = System.Label.CTI_Case_Transfered; 
                            }
                            else if(objCase.Status=='Closed'){
                                statusMessageCTI = System.Label.Case_Close_Message;
                            }                     
                            System.Debug('@@Save objCase: ' + objCase);       
                            //task.whatId = objCase.Id;
                            statusMessage = 'Last save at ' + DateTime.now();
                        }
                        else
                        {
                            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error,System.Label.CTI_Category_Select));
                        }
                    }
                }
                else
                {
                    objCase.status='In Progress';
                    ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error,System.Label.CTI_Case_Transfered_Cannot_close));
                }
                
                
            }
            //else if((recordId.substring(0,3) == '003') && objCase.id!=null)//on contact page and case is already saved so update the case
            //update objCase;
        }catch(Exception exp){
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error,exp.getMessage()));//for 1847
            StatusLogHelper.logSalesforceError('CustomInteractionLog', 'E005', 'Error in CTI', exp, false);//for 1847
        }
    }
    
    //get Fin account based on customer
    public List<SelectOption> getfinancialAccounts(){
        List<SelectOption> optns = new List<Selectoption>();
        optns.add(new selectOption('', '--none--'));
        map<string,string> financialAccNoToFinancialAccIdMap=new map<string,string>();
        List<Financial_Account__c> financialAccList =new List<Financial_Account__c>();
        String prodname;
        String queuenm = queuename;
        
        CTI_Queue_Product_Mapping__c cqpm = CTI_Queue_Product_Mapping__c.getValues(queuenm);
        if(cqpm!=null)
            prodname=cqpm.Product_Name__c;
        if(recordId!=null)
        {           
            set<Id> financialAccIdSet = new set<Id>();
            Id recId;
            if(recordId.substring(0,3) == '003')
                recId = recordId;
            else if(recordId.substring(0,3) == '500')
                recId = task.whoId;
            system.debug('recId-->'+recId);
            List<Card__c> cardList = [SELECT Id, Name, People__c, Financial_Account__c FROM Card__c where People__c=:recId and Financial_Account__c!=null Limit 50000];
            
            for(Card__c card:cardList)
            {
                financialAccIdSet.add(card.Financial_Account__c);
            }
            
            financialAccList = [SELECT Id, Name, Customer__c, Account_Number__c,Account_Serno__c,Product__c,Product__r.name FROM Financial_Account__c where Id IN:financialAccIdSet];
            if(prodname!=null && prodname!='')//defect : 1694
                for(Financial_Account__c fa : financialAccList)
            {
                if(fa.Product__r.name==prodname)
                    financialVal = fa.Id;
            }
        }
        
        for(Financial_Account__c fa : financialAccList){
            optns.add(new selectOption(fa.id , fa.Account_Number__c+' : '+(fa.Product__r.name!=null?fa.Product__r.name:'N/A')));
            financialAccNoToFinancialAccIdMap.put(fa.Account_Number__c,fa.Id);
        } 
        //if only one FA then show it by default
        if(recordId!=null && financialAccList.size()==1)
            financialVal=financialAccList[0].Id;           
        
        return optns;
    }
    //get card based on selected FA 
    public List<SelectOption> getCardfinancialAccounts(){
        List<SelectOption> optns = new List<Selectoption>();
        optns.add(new selectOption('', '--none--'));
        
        List<Financial_Account__c> financialAccList =new List<Financial_Account__c>();
        system.debug('financialVal-->'+financialVal);
        String prodname;
        String queuenm = queuename;
        
        CTI_Queue_Product_Mapping__c cqpm = CTI_Queue_Product_Mapping__c.getValues(queuenm);
        if(cqpm!=null)
            prodname=cqpm.Product_Name__c;
        if(financialVal!=null)
        {           
            set<Id> financialAccIdSet = new set<Id>();
            Id recId;
            if(recordId.substring(0,3) == '003')
                recId = recordId;
            else if(recordId.substring(0,3) == '500')
                recId = task.whoId;
            system.debug('recId-->'+recId);
            List<Card__c> cardList;
            
            cardList = [SELECT Id, Name, People__c, Financial_Account__c,Card_Number_Truncated__c,Product__r.name FROM Card__c where People__c=:recId and Financial_Account__c=:financialVal Limit 50000];
            if(prodname!=null && prodname!='')//defect : 1694
                for(Card__c card : cardList)
            {
                if(card.Product__r.name==prodname)
                    selectedcard = card.Id;
            }
            for(Card__c card:cardList)
            {
                optns.add(new selectOption(card.id , card.Card_Number_Truncated__c));
            }  
            //if only one card then show it by default
            if(cardList.size()==1)
                selectedcard = cardList[0].Id;         
            
        }          
        
        return optns;
    }
    
    //Add by saddam on April-20-2018 [SFD-136]
    Public PageReference loadContact(){
        string cseid = task.whatId;
        system.debug('--contactId--loading------'+cseid);
        if(cseid != null){
            CallType = 'inbound';
            task=[Select id,whoId, whatId,CallObject,cnx__UniqueId__c,ANI__c,Call_Type__c,subject,status from Task where whatId =:cseid AND status='Open' limit 1];
            system.debug('-----task------'+task);
        }
        return null;
    }
    
    Public PageReference updateCase(){
        system.debug('--CaseNo--'+caseIdChosen);
        system.debug('--before--task.whoId--'+task.whoId);
        contactId = task.whoId;
        if(caseIdChosen != ''){
            objCase = [Select id, caseNumber,status,Category__c,Department__c,AccountId,Country__c,ContactId,Type,Description,OwnerId from case where id=:caseIdChosen];
            task = [Select id,whoid, status, whatId, subject from Task where whatId =:caseIdChosen AND CreatedById = :UserInfo.getUserId() limit 1];//CreatedById
            Contact cons = [Select id,accountId from Contact where id =:contactId];
            system.debug('-----cons------'+cons);
            system.debug('----------task-----'+task);
            system.debug('-----case------'+objcase);
            objCase.ContactId = contactId;
            objCase.AccountId = cons.AccountId;
            task.whoId = contactId;
            upsert task;
            upsert objcase;
        }
        return null;
    }//[END SFD-136]
    
    // [START SFD-421]   Modified by saddam on May-20-2018 
    Public string SSN {set;get;}
    public string custsName{set;get;}
    Public List<Contact> conlist {set;get;}
    public string custsId{set;get;}
    Public Id passcaseId {set;get;}

    
    Public Pagereference redirectSSN(){
        system.debug('-----SSNo---'+SSN);
        system.debug('--CaseNo--'+task.whatId);
        passcaseId = task.whatId;
        if(SSN != null && SSN.isNumeric() == true){
            blob hash = Crypto.generateDigest('MD5', Blob.valueOf(SSN));
            string encryptSSN=EncodingUtil.convertToHex(hash);
            system.debug('----encryptSSN-'+encryptSSN);
            if(encryptSSN != '' || encryptSSN!=null){
                conlist = [select id,Name,SSN__c from contact where Encrypted_SSN__c = :encryptSSN];
                System.debug('-----------'+conlist);
                if(conlist.size() > 0){
                    custsId = conlist[0].id;
                    custsName = conlist[0].name;
                    if(passcaseId != null){
                        objcase = [Select id,ContactId from Case where id =: passcaseId];
                        system.debug('----objcase after---'+objcase);
                        task = [Select id, WhoId, WhatId from Task where WhatId =: passcaseId AND status='Open' limit 1];
                        system.debug('------task--after----'+task);
                        objcase.ContactId = conlist[0].id;
                        task.WhoId = conlist[0].id;
                        upsert objcase;
                        upsert task;
                    }
                    return null;
                } 
                if(conlist.isEmpty()){
                    Apexpages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'No Customer found with this SSN : ' + '' + SSN));
                }
            }
        } else if(SSN != '' && SSN.isNumeric() == false){
            Apexpages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'SSN should be Numaric value'));
        }else{
            Apexpages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Please enter SSN'));
        }
        return null;
    }
    
    Public Pagereference redirectName(){
        
        contactId = ApexPages.currentPage().getParameters().get('recordId');
        passcaseId = task.whatId;
        system.debug('-----recordId----'+contactId);
        system.debug('--CaseNo--'+passcaseId);
        conlist = [select id,Name,SSN__c from contact where Id = :contactId];
        system.debug('-----con----'+conlist);
        if(conlist.size() > 0){
            custsId = conlist[0].id;
            custsName = conlist[0].name;
            if(passcaseId != null){
                objcase = [Select id,ContactId from Case where id =: passcaseId];
                system.debug('----objcase after---'+objcase);
                task = [Select id, WhoId, WhatId from Task where WhatId =: passcaseId AND status='Open' limit 1];
                system.debug('------task--after----'+task);
                objcase.ContactId = conlist[0].id;
                task.WhoId = conlist[0].id;
                upsert objcase;
                upsert task;
            }
            return null;
        }
        
        if(conlist.size() == 0){
            Apexpages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO,'No Customer found with this Name : ' + '' + conlist[0].name));
        }
        
        return null;
    }
    Public PageReference redirectCase(){
        Pagereference casepage = new Pagereference('/apex/CreateCaseforNonCustLightning');
        return casepage;
    }
    
    Public PageReference displayWhatId(){
        passcaseId = task.whatId;
        system.debug('-------casenumber-----'+passcaseId);
        if(passcaseId != null){
            objCase = [Select id,Category__c,Description, CaseNumber from Case where id =: passcaseId];
            system.debug('------objcase on update----'+objcase);
        }
        return null;    
    }
    
    Public PageReference createCase(){
        objCase.Status = 'Closed';
        upsert objCase;
        ApexPages.Message myMsg1 = new ApexPages.Message(ApexPages.Severity.CONFIRM,'Case updated Successfully');
        ApexPages.addMessage(myMsg1);
        return null;
    }//[END-421]
}