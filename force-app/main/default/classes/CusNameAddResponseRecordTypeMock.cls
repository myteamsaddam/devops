@isTest
global class CusNameAddResponseRecordTypeMock implements WebServiceMock {
     global void doInvoke(
           Object stub,
           Object request,
           Map<String, Object> response,
           String endpoint,
           String soapAction,
           String requestName,
           String responseNS,
           String responseName,
           String responseType) {
               
               SOAPCSSServiceResponse.UpdateCustomerNameAddressResponse_element respElement = new SOAPCSSServiceResponse.UpdateCustomerNameAddressResponse_element();
               SOAPCSSServiceResponse.UpdateCustomerNameAddressResponseRecordType upCusDatResRecType = new SOAPCSSServiceResponse.UpdateCustomerNameAddressResponseRecordType();
               SOAPCSSServiceResponse.Result result                         = new SOAPCSSServiceResponse.Result();
               SOAPCSSServiceResponse.AddressSerno addressSerNo             = new SOAPCSSServiceResponse.AddressSerno();
               SOAPCSSServiceResponse.ErrorResult  errorResult              = new SOAPCSSServiceResponse.ErrorResult();
               SOAPCSSServiceResponse.ArrayOfErrorResult arrayOfErrorResult = new SOAPCSSServiceResponse.ArrayOfErrorResult();
               
               
               errorResult.Code        = 401;
               errorResult.Description = 'Error Test';
               
               
               
               addressSerNo.AddressSerno = '10';
               
               result.Address     = addressSerNo;
               result.Code        = '1';
               result.Description = 'Test'; 
               
               
               upCusDatResRecType.Result = result;
               respElement.UpdateCustomerNameAddressResponseRecord = upCusDatResRecType;
               response.put('response_x', respElement);
           }
        

}