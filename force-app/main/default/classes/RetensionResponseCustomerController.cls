public class RetensionResponseCustomerController {
    
@AuraEnabled
public static Contact getslectedContact(Id recordId){
        System.debug('ContactId'+recordId);
        Contact Con=[Select Id,Name From Contact Where ID=:recordId];
        return Con;
   }  
    
@AuraEnabled 
public static user fetchUser(){
         // query current user information  
         User oUser = [select id,Name FROM User Where id =: userInfo.getUserId()];
         return oUser;
}
   
@AuraEnabled
public static List<Financial_Account__c> getFinancialAccountvalues(Id recordId){
    System.debug('ContactIdFinancialAccount'+recordId);
     List<Financial_Account__c> financialAccountList=new List<Financial_Account__c>();
        if(recordId!=null){
            set<Id> financialAccIdSet = new set<Id>();
            List<Card__c> cardList = [SELECT Id, Name, People__c, Financial_Account__c FROM Card__c
                                      where People__c=:recordId and Financial_Account__c!=null 
                                      Limit 50000];
            for(Card__c card:cardList)
            {
                financialAccIdSet.add(card.Financial_Account__c);
            }
            financialAccountList =[select Id,Name, Account_Number__c, Account_Serno__c, Partner_Description__c from Financial_Account__c where
                                   Id IN :financialAccIdSet order by Name desc];
        }
        return financialAccountList;
   }  
  
@AuraEnabled
public static List < String > ReasonforAccountClosure() {
      String field='Reason_for_Account_Closure__c';
      system.debug('field'+field);
      List < String > allOpts = new list < String > ();
      Schema.sObjectType objType = Retention_Response__c.getSObjectType();
      Schema.DescribeSObjectResult objDescribe = objType.getDescribe();
      map < String, Schema.SObjectField > fieldMap = objDescribe.fields.getMap();
      list < Schema.PicklistEntry > values =fieldMap.get(field).getDescribe().getPickListValues();


        for (Schema.PicklistEntry a: values) {
           allOpts.add(a.getValue());
           }
           allOpts.sort();
           return allOpts;
 }
    
@AuraEnabled
public static List < String > getselectOptions() {
         String field='Account_Closed__c';
         List < String > allOpts = new list < String > ();
        // Get the object type of the SObject.
         Schema.sObjectType objType = Retention_Response__c.getSObjectType();
        // Describe the SObject using its object type.
        Schema.DescribeSObjectResult objDescribe = objType.getDescribe();
       // Get a map of fields for the SObject
        map < String, Schema.SObjectField > fieldMap = objDescribe.fields.getMap();
      // Get the list of picklist values for this field.
        list < Schema.PicklistEntry > values =
         fieldMap.get(field).getDescribe().getPickListValues();
     // Add these values to the selectoption list.
        for (Schema.PicklistEntry a: values) {
         allOpts.add(a.getValue());
        }
        allOpts.sort();
        return allOpts;
}
    
@AuraEnabled
public static void saveRetension(String contactname,String reasonforacoountclousre,String Accountclosed,String FinancialAccount,String AgentID,String Ownerid){
         System.debug('contactname'+contactname);
         System.debug('reasonforacoountclousre'+reasonforacoountclousre);
         System.debug('Accountclosed'+Accountclosed);
         System.debug('FinancialAccount'+FinancialAccount);
         System.debug('AgentID'+AgentID);
         System.debug('Owner'+Ownerid);
        if(FinancialAccount.equals(''))
        {
            system.debug('Financial Account ID Null');
          
        }
            Retention_Response__c r=new Retention_Response__c(Person__c=contactname,
            Reason_for_Account_Closure__c=reasonforacoountclousre,
            Account_Closed__c=Accountclosed,
            Financial_Account__c=FinancialAccount,
            Agent_Id__c=AgentID,
            OwnerId=Ownerid) ;   
            insert r;
}
}