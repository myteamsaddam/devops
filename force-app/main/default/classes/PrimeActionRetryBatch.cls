global class PrimeActionRetryBatch implements Database.Batchable<sObject>, Schedulable,Database.AllowsCallouts{

   global final String Query;

   global PrimeActionRetryBatch(){        
      Query='SELECT id,name,Number_of_Attempt__c,Response_Status__c,SOA_service_Inbound_Message__c,Prime_Action_Type__c from Prime_Action_Log__c where Response_Status__c=\'Failure\' and Number_of_Attempt__c<6';
   }
    
   global Database.QueryLocator start(Database.BatchableContext BC){
      return Database.getQueryLocator(query);
   }

   global void execute(Database.BatchableContext BC, List<sObject> scope){
     List<Prime_Action_Log__c>PrimeLogListtoUpdate=new List<Prime_Action_Log__c>();
     for(sobject s : scope){       
         Prime_Action_Log__c pLog=(Prime_Action_Log__c)s;
         if(pLog.SOA_service_Inbound_Message__c!=null && pLog.SOA_service_Inbound_Message__c!=''){
             if(pLog.Prime_Action_Type__c=='Remember Insurance' || pLog.Prime_Action_Type__c=='Manage Replace Card requests' || pLog.Prime_Action_Type__c=='Payment Holiday' || pLog.Prime_Action_Type__c=='Extra Card' ||pLog.Prime_Action_Type__c=='Registration of PPI'||pLog.Prime_Action_Type__c=='Create Installment'||pLog.Prime_Action_Type__c=='Close Account'||pLog.Prime_Action_Type__c=='Reprint PIN' ||pLog.Prime_Action_Type__c=='Remember More Insurance')    
             {
                 UpdateCustomerDataInput UpdateCustInput=(UpdateCustomerDataInput)JSON.deserializeStrict(pLog.SOA_service_Inbound_Message__c,UpdateCustomerDataInput.class);
                 if(UpdateCustInput!=null){
                     
                     UpdateCustomerDataOutput UpdateCustOutput = CSSServiceHelper.UpdateCustomerData(UpdateCustInput);
                     if(UpdateCustOutput!=null && UpdateCustOutput.UCDR!=null && UpdateCustOutput.UCDR.Code=='0' && UpdateCustOutput.UCDR.Description.equalsignorecase('SUCCESS'))
                     {
                         pLog.Response_Status__c='Success';
                         
                     }
                 }
                 
                 pLog.Number_of_Attempt__c=(pLog.Number_of_Attempt__c!=null?pLog.Number_of_Attempt__c+1:1);
                 PrimeLogListtoUpdate.add(pLog);
             }
             else if(pLog.Prime_Action_Type__c=='Payback Credit Balance')
             {
                 PayBackCreditBalanceInput PayBackInput=(PayBackCreditBalanceInput)JSON.deserializeStrict(pLog.SOA_service_Inbound_Message__c,PayBackCreditBalanceInput.class);
                 if(PayBackInput!=null){
                 PayBackCreditBalanceOutput PayBackOutput=CSSServiceHelper.updatePayBackCreditBalance(PayBackInput.InstitutionID,PayBackInput.Action,PayBackInput.Reference,PayBackInput.AccountNumber,PayBackInput.ServiceIndicator,PayBackInput.ServiceType,PayBackInput.LogAction,PayBackInput.ActionDate,PayBackInput.PaybackAmount);
                     if( PayBackOutput!=null && PayBackOutput.Code=='0' && PayBackOutput.Description.equalsignorecase('Success')) // << Modified by shameel
                     {
                         pLog.Response_Status__c='Success';
                     }
                 }
                 
                 pLog.Number_of_Attempt__c=(pLog.Number_of_Attempt__c!=null?pLog.Number_of_Attempt__c+1:1);
                 PrimeLogListtoUpdate.add(pLog);
             }
             else if(pLog.Prime_Action_Type__c=='Credit Limit Increase')
             {
             
             }
             
         }
     }
     if(PrimeLogListtoUpdate!=null & PrimeLogListtoUpdate.size()>0)
         update PrimeLogListtoUpdate;
    }

   global void finish(Database.BatchableContext BC){
       //System.scheduleBatch(new PrimeActionRetryBatch(),'Prime Failure Retry job @ '+system.now(),240,9); // schedule after every 4 hr
   }
   
   global void execute(SchedulableContext sc) {
      PrimeActionRetryBatch b = new PrimeActionRetryBatch(); 
      database.executebatch(b,9);
   }
   
   public void cronschedule()     
   {
       PrimeActionRetryBatch m = new PrimeActionRetryBatch();
       integer interval=integer.valueof(Common_Settings__c.getValues('PrimeActionRetryBatch').Retry_batch_interval_in_hours__c);
       String sch = '0 0 0';
       integer hr=0;
       for(integer i=1;i<=24/interval;i++){
           hr+=interval;
           if(hr<24){
               sch+=','+hr;
           }
           else
               break;
       }    
       sch+=' * * ?';
       String jobID = system.schedule('PrimeRetryJob', sch, m);
   }
}