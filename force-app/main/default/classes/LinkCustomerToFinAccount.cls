global class LinkCustomerToFinAccount implements Database.Batchable<sObject>{

global Database.QueryLocator start(Database.BatchableContext BC){
      return Database.getQueryLocator('select id,AccountId from Contact where AccountId=null');
   }
   
global void execute(Database.BatchableContext BC, List<Contact> scope){
     List<Id> lstconid = new List<Id>();
     system.debug('scope-->'+scope);
     for(Contact c: scope)
     {
         lstconid.add(c.id);
     }
     
     Card__c[] lstcard = [select id,People__c,Financial_Account__r.Customer__c from Card__c where People__c In :lstconid];
     system.debug('card list size-->'+lstcard.size());
     
     Map<Id,Card__c> mCardContact = new Map<Id,Card__c>();
     for(Card__c c : lstcard)
     {
         mCardContact.put(c.People__c,c);
     }
     system.debug('mCardContact-->'+mCardContact);     
     
     for(Contact con : scope)
     {
         if(mCardContact.get(con.id)!=null)
             con.AccountId = mCardContact.get(con.id).Financial_Account__r.Customer__c;
     }
     update scope;
    }

   global void finish(Database.BatchableContext BC){
   }   

}