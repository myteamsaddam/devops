public class GetConsentServiceCallout extends SoapIO{
   
    SOAConsentRequest.GetConsentDataRequestBodyType getConsentDataRequestBodyInstance;
    GetConsentServiceInput input;
    
    public  void convertInputToRequest(){
    
        input = (GetConsentServiceInput)serviceInput;
        getConsentDataRequestBodyInstance = new SOAConsentRequest.GetConsentDataRequestBodyType();
        getConsentDataRequestBodyInstance.Institution_ID=input.InstitutionId;
        getConsentDataRequestBodyInstance.ConsentEntityID=input.ConsentEntityId;
        getConsentDataRequestBodyInstance.ConsentEntityID_Type=input.ConsentEntityIdType;
        if(input.FromDate!=null)
        getConsentDataRequestBodyInstance.FromDate=input.FromDate;
        if(input.ToDate!=null)
        getConsentDataRequestBodyInstance.ToDate=input.ToDate;

    }
    
    public  override object invokeWebserviceCallout(SoapRequest soapRequest){
        
        SOAConsentTypelib.headerType HeadInstance=new SOAConsentTypelib.headerType();
        HeadInstance.MsgId='SFDC consuming Get Consent SOA service. Time Stamp=>'+system.now();
        HeadInstance.CorrelationId=Common_Settings__c.getValues('CorrelationId').Common_Value__c;
        HeadInstance.RequestorId=Common_Settings__c.getValues('RequestorId').Common_Value__c;
        HeadInstance.SystemId=Common_Settings__c.getValues('SystemId').Common_Value__c;
        HeadInstance.InstitutionId=string.valueof(input.InstitutionId);
        SOAConsentInvoke.x_xsoap_CSSServiceESB_CSSServicePT   invokeInstance= new SOAConsentInvoke.x_xsoap_CSSServiceESB_CSSServicePT ();
        invokeInstance.timeout_x=30000;
         // Added 12/13/2016
        ServiceSettings__c serviceObj = ServiceSettings__c.getValues('GetConsent');
        if(serviceObj!=null && serviceObj.EndPoint__c!='' && serviceObj.EndPoint__c!=null)
        invokeInstance.endpoint_x = serviceObj.EndPoint__c;
        // Added 12/13/2016
        
        //String username = 'evry_access';
        //String password = '9oKuwQioQ4';
        
        String username = serviceObj.Username__c; //'css_soa';
        String password = serviceObj.Password__c;//'P1%gruzA';
        
        Blob headerValue = Blob.valueOf(username + ':' + password);
        String authorizationHeader = 'BASIC ' +EncodingUtil.base64Encode(headerValue);
        
        invokeInstance.inputHttpHeaders_x = new Map<String, String>();
        invokeInstance.inputHttpHeaders_x.put('Authorization', authorizationHeader);
        
        SOAConsentSecurityHeader.UsernameToken creds=new SOAConsentSecurityHeader.UsernameToken();
        //creds.Username='evry_access';
        //creds.Password='9oKuwQioQ4';
        
        creds.Username=serviceObj.Username__c;//'css_soa';
        creds.Password=serviceObj.Password__c;//'P1%gruzA';
        
        SOAConsentSecurityHeader.Security_element security_ele=new SOAConsentSecurityHeader.Security_element();
        security_ele.UsernameToken=creds;
        invokeInstance.SecurityPart=security_ele;
        SOAConsentResponse.GetConsentDataResponse_element  getConsentDataResponse_element ;
        getConsentDataResponse_element =  invokeInstance.GetConsentData(HeadInstance,getConsentDataRequestBodyInstance);
        system.debug('getConsentDataResponse_element -->'+getConsentDataResponse_element);
        return getConsentDataResponse_element;
     }  
        
     public override object convertResponseToOutput(object response, map<string, string> responseHeader, ServiceStatus serviceStatus){
       if(response==null)
           return null;
       
       SOAConsentResponse.GetConsentDataResponse_element  getConsentDataResponse_elementInstance = (SOAConsentResponse.GetConsentDataResponse_element)response;
       SOAConsentResponse.GetConsentDataResponseRecordType[]  getConsentDataResponseRecordInstance =getConsentDataResponse_elementInstance.GetConsentDataResponseRecord;
      
       system.debug('***getConsentDataResponseRecordInstance***'+getConsentDataResponseRecordInstance);
       
       GetConsentServiceOutput getConsentServiceOutput = new GetConsentServiceOutput();
       
       if(getConsentDataResponseRecordInstance!=null)
       {
           getConsentServiceOutput.GetConsentDataResponseRecord= new List<GetConsentServiceOutput.GetConsentDataResponseRecordType>();
           for(SOAConsentResponse.GetConsentDataResponseRecordType consentRes:getConsentDataResponseRecordInstance)
           {
               GetConsentServiceOutput.GetConsentDataResponseRecordType consent = new GetConsentServiceOutput.GetConsentDataResponseRecordType();
               
               consent.InstitutionId=consentRes.Institution_ID;
               consent.ConsentEntityId=consentRes.ConsentEntityID;
               consent.ConsentEntityIdType=consentRes.ConsentEntityID_Type;
               consent.ConsentCode=consentRes.Consent_Code;
               consent.Value=consentRes.Value;
               /*consent.ConsentCodeValue = new List<GetConsentServiceOutput.ConsentCodeType>();
               if(consentRes.Consent_Code!=null)
               {
                   
                   for(SOAConsentResponse.Consent_Code_type cc:consentRes.Consent_Code)
                   {
                       GetConsentServiceOutput.ConsentCodeType cCtype = new GetConsentServiceOutput.ConsentCodeType();
                       cCtype.code=cc.code;
                       cCtype.value=cc.value;
                       consent.ConsentCodeValue.add(cCtype);
                   }
                   
               }*/
               
               consent.DateTimeStamp=consentRes.DateTimeStamp;
               consent.UpdatedBy=consentRes.UpdatedBy;
               consent.Source=consentRes.Source;
               
               getConsentServiceOutput.GetConsentDataResponseRecord.add(consent); 
           }
       }
       system.debug('***getConsentServiceOutput***'+getConsentServiceOutput);
       
      return getConsentServiceOutput;
    }        
}