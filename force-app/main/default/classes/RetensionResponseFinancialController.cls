public class RetensionResponseFinancialController {
    
    @AuraEnabled
public static Financial_Account__c GetFinancialAccount(Id recordId){
        System.debug('ContactId'+recordId);
        Financial_Account__c financialaccount=[Select Id,Name From Financial_Account__c Where ID=:recordId];
        return financialaccount;
   }  
    
@AuraEnabled 
public static user fetchUser(){
         // query current user information  
         User oUser = [select id,Name FROM User Where id =: userInfo.getUserId()];
         return oUser;
}
     
@AuraEnabled
public static List < String > ReasonforAccountClosure() {
      String field='Reason_for_Account_Closure__c';
      system.debug('field'+field);
      List < String > allOpts = new list < String > ();
      Schema.sObjectType objType = Retention_Response__c.getSObjectType();
      Schema.DescribeSObjectResult objDescribe = objType.getDescribe();
      map < String, Schema.SObjectField > fieldMap = objDescribe.fields.getMap();
      list < Schema.PicklistEntry > values =fieldMap.get(field).getDescribe().getPickListValues();


        for (Schema.PicklistEntry a: values) {
           allOpts.add(a.getValue());
           }
           allOpts.sort();
           return allOpts;
 }
    
@AuraEnabled
public static List < String > getselectOptions() {
         String field='Account_Closed__c';
         List < String > allOpts = new list < String > ();
        // Get the object type of the SObject.
         Schema.sObjectType objType = Retention_Response__c.getSObjectType();
        // Describe the SObject using its object type.
        Schema.DescribeSObjectResult objDescribe = objType.getDescribe();
       // Get a map of fields for the SObject
        map < String, Schema.SObjectField > fieldMap = objDescribe.fields.getMap();
      // Get the list of picklist values for this field.
        list < Schema.PicklistEntry > values =
         fieldMap.get(field).getDescribe().getPickListValues();
     // Add these values to the selectoption list.
        for (Schema.PicklistEntry a: values) {
         allOpts.add(a.getValue());
        }
        allOpts.sort();
        return allOpts;
}
    @AuraEnabled
    public static Contact GetContact(Id recordId){
          system.debug('----financial account --id------'+recordId); 
        Contact returncontact=new Contact();
          List<Financial_Account__c> fin =[select id, name from Financial_Account__c  where id=:recordId limit 1];
        if(fin!=null) 
        {
            set<Id> ContactIdSet = new set<Id>();
            List<Card__c> cardList = [SELECT Id, Name, People__c, Financial_Account__c FROM Card__c where People__c!=null and Financial_Account__c=:recordId Limit 1];
            system.debug('cardlist[0].People__c'+cardlist[0].People__c);
            returncontact= [select ID,Name From Contact Where id=:cardlist[0].People__c];
            system.debug('returncontact'+returncontact);
            }
        return returncontact;
    }
    @AuraEnabled
public static void saveRetension(String contactname,String reasonforacoountclousre,String Accountclosed,String FinancialAccount,String AgentID,String Ownerid){
         System.debug('contactname'+contactname);
         System.debug('reasonforacoountclousre'+reasonforacoountclousre);
         System.debug('Accountclosed'+Accountclosed);
         System.debug('FinancialAccount'+FinancialAccount);
         System.debug('AgentID'+AgentID);
         System.debug('Owner'+Ownerid);
          Retention_Response__c r=new Retention_Response__c(Person__c=contactname,
            Reason_for_Account_Closure__c=reasonforacoountclousre,
            Account_Closed__c=Accountclosed,
            Financial_Account__c=FinancialAccount,
            Agent_Id__c=AgentID,
            OwnerId=Ownerid) ;   
            upsert r;
}
    
    

}