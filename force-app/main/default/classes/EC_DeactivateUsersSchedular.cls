/********************************************************************************************
Name: EC_DeactivateUsers
=============================================================================================
Purpose: This Class is used for deactivating the users who are last login is today - 60 days
User Story - SFD-1422
============================================================================================= 
History
---------------------------------------------------------------------------------------------        
Version     Author                          Date              Detail
1.0        Harisivaramakrishna          02/04/2020     Initial Version

*********************************************************************************************/

global class EC_DeactivateUsersSchedular implements Schedulable {
    global static DateTime lastLoginDate = date.today().addDays(-60); 
    /*********************************************************************************************************
*   Author: Harisivaramakrishna
*   Date:     08/04/2020
*   User Story : SFD-1422
*   Param: None
*   Return: None    
*   Description: In this method we are trying to deactivate the users who doesn't logged in since 60 days. 

**********************************************************************************************************/
    global void execute(SchedulableContext SC) {
        try { 
            Id profileId = [SELECT Id FROM Profile WHERE name=: EC_StaticConstant.PROFILE_NAME].Id;
            //DateTime lastLoginDate = date.today().addDays(-60); 
            List<User> userList = [SELECT ID, Name, LastLoginDate, IsActive, Profile.Name 
                                   FROM User 
                                   WHERE IsActive = TRUE 
                                   AND LastLoginDate <: lastLoginDate
                                   AND Name !=: EC_StaticConstant.USER_SALESFORCE_SUPPORT
                                   AND ProfileId !=:profileId ];
            System.debug('*****userList***** '+userList.size());
            List<User> deactivateUserList = new List<User>();
            if(userList.size() > 0 ){ 
                for (User usr : userList){
                        usr.IsActive= FALSE;  // This is used to deactivating user
                        deactivateUserList.add(usr);
                }
            } 
            System.debug('*****Deactivated users***** '+deactivateUserList);
            Set<Id> userIdSet = new Set<Id>();
            if(deactivateUserList.size() > 0) {
                update deactivateUserList;
                for(User usrObj : deactivateUserList) {
                    userIdSet.add(usrObj.Id);
                }
                if(userIdSet.size() > 0) {
                    sendDeactivateUsers(userIdSet);
                }
            }
            
            
        } catch(Exception exp){
            StatusLogHelper.logSalesforceError('DeactivateUsers', 'defaultError', 
                                               'Error in Deactivate user', exp, false);
        }
        
    }
    // Method Comments 
    @future
    public static void sendDeactivateUsers(Set<Id> deactivateUserSet){
        List<String> adminEmailIds = new List<String>(); // use custom setting
         //adminEmailIds.add('haathmak@capgemini.com');
        //adminEmailIds.add('saddam.hussain@capgemini.com');
        for(EC_SalesforceTeam__c emailAdd : EC_SalesforceTeam__c.getAll().values()){
            adminEmailIds.add(emailAdd.EC_Email__c);
        }
        system.debug('---EmailList-'+adminEmailIds);
        String emailSubject = Label.EC_Deactivate_User_Email_Subject;
        String emailBody = Label.EC_Deactivated_Users_Email_Body;
        String emailSenderName = Label.EC_Deactivated_Users_Email_Sender_Name;
        String fileBody = EC_StaticConstant.ATTACHMENT_USER_COLUMNS;// Include file columns name and Profile name
        String fileName = Label.EC_Deactivated_User_File_Name;
        String fileColumnValues;
        system.debug('---EmailSubject-'+emailSubject);
        DateTime lastLoginDate = date.today().addDays(-60);
        List<User> modifiedUserList = new List<User>();
        if(!Test.isRunningTest()){
            modifiedUserList = [SELECT Id, Name, Profile.Name
                                FROM User
                                WHERE Id IN : deactivateUserSet]; // Put comments here
            system.debug('-if-inside--modifiedUserList-'+modifiedUserList.size());
        }else{
            modifiedUserList = [SELECT Id, Name, Profile.Name
                                FROM User];
            //WHERE IsActive = FALSE 
            //AND LastModifiedDate = Today ];
            // Put comments here
            system.debug('-else--modifiedUserList-'+modifiedUserList.size());
        }
        
        system.debug('---modifiedUserList-'+modifiedUserList.size());
        
        if(modifiedUserList.size() > 0) {
            for(User usrObj : modifiedUserList) {
                fileColumnValues = '\n"'+usrObj.name+'","'+usrObj.Profile.Name+'"';
                fileBody = fileBody + fileColumnValues; 
            }
            List<Messaging.EmailFileAttachment> eMailAttachment = new List<Messaging.EmailFileAttachment>();
            Messaging.EmailFileAttachment eAttachment = new Messaging.EmailFileAttachment();
            
            eAttachment.setFileName(fileName);
            eAttachment.setBody(Blob.valueOf(fileBody));
            eMailAttachment.add(eAttachment);
            
            EC_UtilitySendEmail.sendEmailWithAttachment(adminEmailIds, emailSubject, 
                                                        emailBody, emailSenderName, eMailAttachment);
        }
    }
}