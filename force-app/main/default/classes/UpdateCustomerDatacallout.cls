public class UpdateCustomerDatacallout extends SoapIO{
    SOAUpdateCustomerRequest.setEmailRequestRecordType setEmailRequestRecordTypeInstance;
    SOAUpdateCustomerRequest.setMobileRequestRecordType setMobileRequestRecordTypeInstance;
    SOAUpdateCustomerRequest.closeAccountRequestRecordType closeAccountRequestRecordTypeInstance;
    SOAUpdateCustomerRequest.setInstalmentRequestRecordType setInstalmentRequestRecordTypeInstance;
    SOAUpdateCustomerRequest.addExtraCardRequestRecordType addExtraCardRequestRecordTypeInstance;
    SOAUpdateCustomerRequest.replaceCardRequestRecordType replaceCardRequestRecordTypeInstance;
    SOAUpdateCustomerRequest.setPaymentHolidaysRequestRecordType setPaymentHolidaysRequestRecordTypeInstance;
    SOAUpdateCustomerRequest.setPPIRequestRecordType setPPIRequestRecordTypeInstance;
    SOAUpdateCustomerRequest.setInsuranceRequestRecordType setInsuranceRequestRecordTypeInstance;
    SOAUpdateCustomerRequest.cardReissueReprintRequestRecordType cardReissueReprintRequestRecordTypeInstance;
    SOAUpdateCustomerRequest.setPickupCodeRequestRecordType PickupCodeRequestRecordTypeInstance;
    SOAUpdateCustomerRequest.setMTPRequestRecordType MTPRequestRecordTypeInstance;
    SOAUpdateCustomerRequest.setPhone1RequestRecordType Phone1RequestRecordTypeInstance;
    SOAUpdateCustomerRequest.SeteStatementRequestRecordType StatementRequestRecordTypeInstance;
    
    UpdateCustomerDataInput input;
    public  void convertInputToRequest(){
    
        input = (UpdateCustomerDataInput)serviceInput;
                   
        if(input!=null)
        {    
            //==== Email==========
            if(input.EmailReq!=null)
            {
                UpdateCustomerDataInput.setEmailRequest Emailrq=input.EmailReq;
                setEmailRequestRecordTypeInstance = new SOAUpdateCustomerRequest.setEmailRequestRecordType();
                setEmailRequestRecordTypeInstance.SSN=Emailrq.SSN;
                setEmailRequestRecordTypeInstance.Email=Emailrq.Email;
            }
            //==== Mobile==========
            if(input.MobileReq!=null)
            {
                UpdateCustomerDataInput.setMobileRequest Mobilerq=input.MobileReq;
                setMobileRequestRecordTypeInstance = new SOAUpdateCustomerRequest.setMobileRequestRecordType();
                setMobileRequestRecordTypeInstance.SSN=Mobilerq.SSN;
                setMobileRequestRecordTypeInstance.Mobile=Mobilerq.mobile;
            }
            
            //==== Close Account==========
            if(input.CloseAccReq!=null)
            {
                UpdateCustomerDataInput.closeAccountRequest closeAccountrq=input.CloseAccReq;
                closeAccountRequestRecordTypeInstance = new SOAUpdateCustomerRequest.closeAccountRequestRecordType();
                closeAccountRequestRecordTypeInstance.AccountNo=closeAccountrq.AccountNo;
                closeAccountRequestRecordTypeInstance.ActionDate=closeAccountrq.ActionDate;
            }
            //==== SetInsatalment==========
            if(input.InstalmentReq!=null)
            {
                UpdateCustomerDataInput.setInstalmentRequest Instalmentrq=input.InstalmentReq;
                setInstalmentRequestRecordTypeInstance = new SOAUpdateCustomerRequest.setInstalmentRequestRecordType();
                setInstalmentRequestRecordTypeInstance.Serno=Instalmentrq.Serno;
                setInstalmentRequestRecordTypeInstance.Number_x=Instalmentrq.Number_x;
                setInstalmentRequestRecordTypeInstance.TransactionAmount=Instalmentrq.TransactionAmount;
                setInstalmentRequestRecordTypeInstance.TransactionType=Instalmentrq.TransactionType;
                setInstalmentRequestRecordTypeInstance.MessageText=Instalmentrq.MessageText;
                setInstalmentRequestRecordTypeInstance.StartDate=Instalmentrq.StartDate;
                setInstalmentRequestRecordTypeInstance.InstalmentCode=Instalmentrq.InstalmentCode;
                setInstalmentRequestRecordTypeInstance.MerchType=Instalmentrq.MerchType;
                setInstalmentRequestRecordTypeInstance.MerchId=Instalmentrq.MerchId;
                setInstalmentRequestRecordTypeInstance.MerchName=Instalmentrq.MerchName;
                setInstalmentRequestRecordTypeInstance.MerchCity=Instalmentrq.MerchCity;
                setInstalmentRequestRecordTypeInstance.MerchCnt=Instalmentrq.MerchCnt;
                setInstalmentRequestRecordTypeInstance.AuthId=Instalmentrq.AuthId;
                setInstalmentRequestRecordTypeInstance.TransactionFee=Instalmentrq.TransactionFee;
            }
            //==== addExtraCard==========
            if(input.addExtraCardReq!=null)
            {
                UpdateCustomerDataInput.addExtraCardRequest addExtraCardrq=input.addExtraCardReq;
                addExtraCardRequestRecordTypeInstance = new SOAUpdateCustomerRequest.addExtraCardRequestRecordType();
                addExtraCardRequestRecordTypeInstance.AccountNo=addExtraCardrq.AccountNo;
                addExtraCardRequestRecordTypeInstance.ProductSerno=addExtraCardrq.ProductSerno;
                SOAUpdateCustomerRequest.extraCardType excrdType=new SOAUpdateCustomerRequest.extraCardType();
                excrdType.SSN=addExtraCardrq.SSN;
                excrdType.FirstName=addExtraCardrq.FirstName;
                excrdType.LastName=addExtraCardrq.LastName;
                excrdType.Gender=addExtraCardrq.Gender;
                excrdType.EmbossingName=addExtraCardrq.EmbossingName;
                excrdType.Phone=addExtraCardrq.Phone;
                excrdType.Email=addExtraCardrq.Email;
                SOAUpdateCustomerRequest.CardServices_element cardele=new SOAUpdateCustomerRequest.CardServices_element();
                List<SOAUpdateCustomerRequest.cardServiceType>cardServiceTypeList=new List<SOAUpdateCustomerRequest.cardServiceType>();
                if(addExtraCardrq.cardServiceTypeList!=null){
                    for(UpdateCustomerDataInput.cardServiceType eachcardtype: addExtraCardrq.cardServiceTypeList)
                    {
                        SOAUpdateCustomerRequest.cardServiceType temp =new SOAUpdateCustomerRequest.cardServiceType();
                        temp.Id=eachcardtype.Id;
                        temp.ActionDelay=eachcardtype.ActionDelay;
                        temp.LogAction=eachcardtype.LogAction;
                        temp.Argument=eachcardtype.Argument;
                        cardServiceTypeList.add(temp);
                    }
                }
                cardele.Service=cardServiceTypeList;
                excrdType.CardServices=cardele; 
                addExtraCardRequestRecordTypeInstance.ExtraCard=excrdType;
            }
            //==== replaceCard ==========
            if(input.replaceCardReq!=null)
            {
                UpdateCustomerDataInput.replaceCardRequest replaceCardrq=input.replaceCardReq;
                replaceCardRequestRecordTypeInstance = new SOAUpdateCustomerRequest.replaceCardRequestRecordType();
                replaceCardRequestRecordTypeInstance.OldCardSerno=replaceCardrq.OldCardSerno;
                //replaceCardRequestRecordTypeInstance.NewCardNumber=replaceCardrq.NewCardNumber;
                replaceCardRequestRecordTypeInstance.OldStatus=replaceCardrq.OldStatus;
                replaceCardRequestRecordTypeInstance.NewStatus=replaceCardrq.NewStatus;
                replaceCardRequestRecordTypeInstance.BlockDate=replaceCardrq.BlockDate;
                replaceCardRequestRecordTypeInstance.RenewDate=replaceCardrq.RenewDate;
                replaceCardRequestRecordTypeInstance.LostCardFee=replaceCardrq.LostCardFee;
            }
            //==== setPaymentHolidays  ==========
            if(input.PaymentHolidaysReq!=null)
            {
                UpdateCustomerDataInput.setPaymentHolidaysRequest PaymentHolidaysrq=input.PaymentHolidaysReq;
                setPaymentHolidaysRequestRecordTypeInstance = new SOAUpdateCustomerRequest.setPaymentHolidaysRequestRecordType();
                setPaymentHolidaysRequestRecordTypeInstance.AccountNo=PaymentHolidaysrq.AccountNo;
            }
            //==== setPPIRequest ===========
            if(input.setPPIReq!=null)
            {
                UpdateCustomerDataInput.setPPIRequest setPPIrq=input.setPPIReq;
                setPPIRequestRecordTypeInstance = new SOAUpdateCustomerRequest.setPPIRequestRecordType();
                setPPIRequestRecordTypeInstance.AccountNo=setPPIrq.AccountNo;
                setPPIRequestRecordTypeInstance.Action=setPPIrq.Action;
                setPPIRequestRecordTypeInstance.PPI=setPPIrq.PPI;
                setPPIRequestRecordTypeInstance.InceptionDate=setPPIrq.InceptionDate;
            }
            //==== setInsuranceRequest ========
            if(input.InsuranceReq!=null)
            {
                UpdateCustomerDataInput.setInsuranceRequest setInsurancerq=input.InsuranceReq;
                setInsuranceRequestRecordTypeInstance = new SOAUpdateCustomerRequest.setInsuranceRequestRecordType();
                setInsuranceRequestRecordTypeInstance.CardSerno=setInsurancerq.CardSerno;
                SOAUpdateCustomerRequest.insurenceListType inslistInstance = new SOAUpdateCustomerRequest.insurenceListType();
                SOAUpdateCustomerRequest.rememberInsurenceListType remInsListTypeinstance = new SOAUpdateCustomerRequest.rememberInsurenceListType();
                List<SOAUpdateCustomerRequest.rememberInsurenceType> remInsurenceTypeList=new List<SOAUpdateCustomerRequest.rememberInsurenceType>();
                for(UpdateCustomerDataInput.rememberInsurenceType eachrememberInsurenceType :setInsurancerq.rememberInsurenceTypelist)
                {
                    list<string> RememberInsurenceNamelist = eachrememberInsurenceType.RememberInsurenceName;
                    list<string> RememberInsurenceActionList = eachrememberInsurenceType.RememberInsurenceAction;
                    SOAUpdateCustomerRequest.rememberInsurenceType temp=new SOAUpdateCustomerRequest.rememberInsurenceType();
                    temp.RememberInsurenceName=RememberInsurenceNamelist ;
                    temp.RememberInsurenceAction=RememberInsurenceActionList;
                    remInsurenceTypeList.add(temp);
                }
                remInsListTypeinstance.RememberInsurence=remInsurenceTypeList;
                SOAUpdateCustomerRequest.freeInsurenceListType freeinsListTypeinstance =new SOAUpdateCustomerRequest.freeInsurenceListType();
                List<string>freeinsListtypeList=new list<string>();
                for(string s : setInsurancerq.FreeInsurence)
                {
                    freeinsListtypeList.add(s);
                }
                freeinsListTypeinstance.FreeInsurence=freeinsListtypeList;
                inslistInstance.RememberInsurenceList=remInsListTypeinstance;
                inslistInstance.FreeInsurenceList=freeinsListTypeinstance;
                setInsuranceRequestRecordTypeInstance.InsurenceList=inslistInstance;
            }
            //==== cardReissueReprint =======
            if(input.cardReissueReq!=null)
            {
                UpdateCustomerDataInput.cardReissueReprintRequest cardReissueReprintrq=input.cardReissueReq;
                cardReissueReprintRequestRecordTypeInstance = new SOAUpdateCustomerRequest.cardReissueReprintRequestRecordType();
                cardReissueReprintRequestRecordTypeInstance.Action=cardReissueReprintrq.Action;
                cardReissueReprintRequestRecordTypeInstance.Reference=cardReissueReprintrq.Reference;
                cardReissueReprintRequestRecordTypeInstance.Serno=cardReissueReprintrq.Serno;
                cardReissueReprintRequestRecordTypeInstance.ServiceIndicator=cardReissueReprintrq.ServiceIndicator;
                cardReissueReprintRequestRecordTypeInstance.ServiceType=cardReissueReprintrq.ServiceType;
                cardReissueReprintRequestRecordTypeInstance.ActionDate=cardReissueReprintrq.ActionDate;
                cardReissueReprintRequestRecordTypeInstance.LogAction=cardReissueReprintrq.LogAction;
                cardReissueReprintRequestRecordTypeInstance.Argument=cardReissueReprintrq.Argument;
            }
            //==== PickupCode ======
            if(input.pickupCodeReq!=null)
            {
                UpdateCustomerDataInput.PickupCodeRequest PickupCoderq=input.pickupCodeReq;  
                PickupCodeRequestRecordTypeInstance = new SOAUpdateCustomerRequest.setPickupCodeRequestRecordType();
                PickupCodeRequestRecordTypeInstance.AccountNo=PickupCoderq.AccountNo;
                PickupCodeRequestRecordTypeInstance.PickupCode=PickupCoderq.PickupCode;
            }
            //==== MTPRequest ====
            if(input.MTRReq!=null)
            {
                UpdateCustomerDataInput.MTPRequest MTPrq=input.MTRReq;
                MTPRequestRecordTypeInstance = new SOAUpdateCustomerRequest.setMTPRequestRecordType();
                MTPRequestRecordTypeInstance.Serno=MTPrq.Serno;
                MTPRequestRecordTypeInstance.AccountNo=MTPrq.AccountNo;
                MTPRequestRecordTypeInstance.MTP=MTPrq.MTP;
            }
            //=== Phone1Request ===
            if(input.Phone1Req!=null)
            {
                UpdateCustomerDataInput.Phone1Request Phone1rq=input.Phone1Req;
                Phone1RequestRecordTypeInstance = new SOAUpdateCustomerRequest.setPhone1RequestRecordType();
                Phone1RequestRecordTypeInstance.NewIndicator=Phone1rq.NewIndicator;
                Phone1RequestRecordTypeInstance.Sex=Phone1rq.Sex;
                Phone1RequestRecordTypeInstance.DOB=Phone1rq.DOB;
                Phone1RequestRecordTypeInstance.MaritalStatus=Phone1rq.MaritalStatus;
                Phone1RequestRecordTypeInstance.MotherName=Phone1rq.MotherName;
                SOAUpdateCustomerTypelib.personType personTyp=new SOAUpdateCustomerTypelib.personType();
                personTyp.Title =Phone1rq.Person.Title;
                personTyp.FirstName=Phone1rq.Person.FirstName;
                personTyp.MiddleName=Phone1rq.Person.MiddleName;
                personTyp.LastName=Phone1rq.Person.LastName;
                personTyp.SSN=Phone1rq.Person.SSN;
                personTyp.CustomerId=Phone1rq.Person.CustomerId;
                personTyp.LanguageCode=Phone1rq.Person.LanguageCode;
                personTyp.VIP=Phone1rq.Person.VIP;
                Phone1RequestRecordTypeInstance.Person=personTyp;
                SOAUpdateCustomerTypelib.personType personAlternateTyp=new SOAUpdateCustomerTypelib.personType();
                personAlternateTyp.Title =Phone1rq.PersonAlternateDetails.Title;
                personAlternateTyp.FirstName=Phone1rq.PersonAlternateDetails.FirstName;
                personAlternateTyp.MiddleName=Phone1rq.PersonAlternateDetails.MiddleName;
                personAlternateTyp.LastName=Phone1rq.PersonAlternateDetails.LastName;
                personAlternateTyp.SSN=Phone1rq.PersonAlternateDetails.SSN;
                personAlternateTyp.CustomerId=Phone1rq.PersonAlternateDetails.CustomerId;
                personAlternateTyp.LanguageCode=Phone1rq.PersonAlternateDetails.LanguageCode;
                personAlternateTyp.VIP=Phone1rq.PersonAlternateDetails.VIP;
                Phone1RequestRecordTypeInstance.PersonAlternateDetails=personAlternateTyp;
                SOAUpdateCustomerTypelib.addressType addrs=new SOAUpdateCustomerTypelib.addressType();
                addrs.Id=Phone1rq.Id;
                addrs.Location=Phone1rq.Location;
                addrs.Address1=Phone1rq.Address1;
                addrs.Address2=Phone1rq.Address2;
                addrs.Address3=Phone1rq.Address3;
                addrs.Address4=Phone1rq.Address4;
                addrs.Address5=Phone1rq.Address5;
                addrs.City=Phone1rq.City;
                addrs.Country=Phone1rq.Country;
                addrs.Zip=Phone1rq.Zip;
                addrs.Phone1=Phone1rq.Phone1;
                addrs.Phone2=Phone1rq.Phone2;
                addrs.Fax=Phone1rq.Fax;
                addrs.Mobile=Phone1rq.Mobile;
                addrs.Email=Phone1rq.Email;
                addrs.Text=Phone1rq.Text;
                addrs.Contact=Phone1rq.Contact;
                Phone1RequestRecordTypeInstance.Address=addrs;
            }
            //=== StatementRequest ===
            if(input.StatementReq!=null)
            {
                UpdateCustomerDataInput.StatementRequest Statementrq=input.StatementReq;
                StatementRequestRecordTypeInstance =new SOAUpdateCustomerRequest.SeteStatementRequestRecordType();   
                StatementRequestRecordTypeInstance.Account=Statementrq.Account;
                StatementRequestRecordTypeInstance.PhoneNumber=Statementrq.PhoneNumber;
                StatementRequestRecordTypeInstance.E_statement=Statementrq.E_statement;
                StatementRequestRecordTypeInstance.Email=Statementrq.Email;
                StatementRequestRecordTypeInstance.Bank=Statementrq.Bank;
                StatementRequestRecordTypeInstance.MTP=Statementrq.MTP;
            }
        }
    }
    
    public  override object invokeWebserviceCallout(SoapRequest soapRequest){
        SOAUpdateCustomerTypelib.headerType HeadInstance=new SOAUpdateCustomerTypelib.headerType();
        HeadInstance.MsgId='SFDC consuming Update customer SOA service. Time Stamp=>'+system.now();
        HeadInstance.CorrelationId=Common_Settings__c.getValues('CorrelationId').Common_Value__c;
        HeadInstance.RequestorId=Common_Settings__c.getValues('RequestorId').Common_Value__c;
        HeadInstance.SystemId=Common_Settings__c.getValues('SystemId').Common_Value__c;
        HeadInstance.InstitutionId=string.valueof(input.InstitutionId);//'2';
        SOAUpdateCustomerInvoke.x_xsoap_CSSServiceESB_CSSServicePT invokeInstance= new SOAUpdateCustomerInvoke.x_xsoap_CSSServiceESB_CSSServicePT();
        invokeInstance.timeout_x=30000;
         // Added 12/13/2016
        ServiceSettings__c serviceObj = ServiceSettings__c.getValues('UpdateCustomerData');
        if(serviceObj!=null && serviceObj.EndPoint__c!='' && serviceObj.EndPoint__c!=null)
        invokeInstance.endpoint_x = serviceObj.EndPoint__c;
        // Added 12/13/2016
        //String username = 'evry_access';
        //String password = '9oKuwQioQ4';
        
        String username = serviceObj.Username__c; //'css_soa';
        String password = serviceObj.Password__c;//'P1%gruzA';
        
        Blob headerValue = Blob.valueOf(username + ':' + password);
        String authorizationHeader = 'BASIC ' +EncodingUtil.base64Encode(headerValue);
        
        invokeInstance.inputHttpHeaders_x = new Map<String, String>();
        invokeInstance.inputHttpHeaders_x.put('Authorization', authorizationHeader);
        
        SOAUpdateCustomerSecurityHeader.UsernameToken creds=new SOAUpdateCustomerSecurityHeader.UsernameToken();
        creds.Username=serviceObj.Username__c;//'css_soa';//'evry_access';
        creds.Password=serviceObj.Password__c;//'P1%gruzA';//'9oKuwQioQ4';
        SOAUpdateCustomerSecurityHeader.Security_element security_ele=new SOAUpdateCustomerSecurityHeader.Security_element();
        security_ele.UsernameToken=creds;
        invokeInstance.SecurityPart=security_ele;
        system.debug('==Just Before callout');
        SOAUpdateCustomerResponse.UpdateCustomerDataResponse_element UpdateCustomerResponse_elementinstance;
        //try{
            UpdateCustomerResponse_elementinstance =  invokeInstance.updateCustomerData(HeadInstance,setEmailRequestRecordTypeInstance,setMobileRequestRecordTypeInstance,closeAccountRequestRecordTypeInstance,setInstalmentRequestRecordTypeInstance,addExtraCardRequestRecordTypeInstance,replaceCardRequestRecordTypeInstance,setPaymentHolidaysRequestRecordTypeInstance,setPPIRequestRecordTypeInstance,setInsuranceRequestRecordTypeInstance,cardReissueReprintRequestRecordTypeInstance,PickupCodeRequestRecordTypeInstance,MTPRequestRecordTypeInstance,Phone1RequestRecordTypeInstance,StatementRequestRecordTypeInstance);
        //}catch(calloutexception callouterror){system.debug('~~~~'+callouterror.getmessage());}
        system.debug('SOAUpdateCustomerResponse.UpdateCustomerDataResponse_element -->'+UpdateCustomerResponse_elementinstance);
        return UpdateCustomerResponse_elementinstance;
     }  
        
     public override object convertResponseToOutput(object response, map<string, string> responseHeader, ServiceStatus serviceStatus){
       system.debug('===<>'+response);
       if(response==null || !(response instanceof SOAUpdateCustomerResponse.UpdateCustomerDataResponse_element))
           return null;
       SOAUpdateCustomerResponse.UpdateCustomerDataResponse_element UpdateCustomerDataResponse_elementInstance = (SOAUpdateCustomerResponse.UpdateCustomerDataResponse_element)response;
       SOAUpdateCustomerResponse.UpdateCustomerDataResponseRecordType UpdateCustomerDataResponseRecordTypeInstance=UpdateCustomerDataResponse_elementInstance.UpdateCustomerDataResponseRecord; 
       SOAUpdateCustomerTypelib.headerType hType=UpdateCustomerDataResponse_elementInstance.Header;
       UpdateCustomerDataOutput.HeaderType htinstance =new UpdateCustomerDataOutput.HeaderType();
       htinstance.MsgId=hType.MsgId;
       htinstance.CorrelationId=hType.CorrelationId;
       htinstance.RequestorId=hType.RequestorId;
       htinstance.SystemId=hType.SystemId;
       htinstance.InstitutionId=hType.InstitutionId;
       UpdateCustomerDataOutput.UpdateCustomerDataResponse UpdateCustomerDataResponseInstance =new UpdateCustomerDataOutput.UpdateCustomerDataResponse(); 
       UpdateCustomerDataResponseInstance.code = UpdateCustomerDataResponseRecordTypeInstance.code;
       UpdateCustomerDataResponseInstance.Description = UpdateCustomerDataResponseRecordTypeInstance.Description;
       UpdateCustomerDataOutput UpdateCustomerOutput=new UpdateCustomerDataOutput();
       UpdateCustomerOutput.HT =htinstance;
       UpdateCustomerOutput.UCDR=UpdateCustomerDataResponseInstance;
       return UpdateCustomerOutput;
    }        
}