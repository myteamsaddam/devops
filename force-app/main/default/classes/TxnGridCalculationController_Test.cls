@isTest
public class TxnGridCalculationController_Test {
    static testMethod void testmethod1(){
        Account accountObj=new Account(
            Name='Test Account', 
            Institution_Id__c=2, 
            Customer_Serno__c='1123123'
        );
        insert accountObj;
        
        Blob hash = Crypto.generateDigest('MD5', Blob.valueOf('0000000061'));
        string encryptSSN=EncodingUtil.convertToHex(hash);
        Contact contactObj =new Contact(
            LastName='Test Contact', 
            FirstName='First Test Contact',
            SerNo__c='2123123', 
            Institution_Id__c=2, 
            SSN__c='0000000061',
            Encrypted_SSN__c=encryptSSN,
            AccountId=accountObj.Id,
            Phone='123123123', 
            Fax='98789879', 
            MobilePhone='98789879', 
            HomePhone='123123123', 
            Email='testemail@test.com'
        );
        insert contactObj;
        
        Product_Custom__c prodobj=new Product_Custom__c(
            Name='Test Product',
            Pserno__c='56789'
        );
        insert prodobj;
        
        Financial_Account__c finAccObj=new Financial_Account__c(
            Account_Number__c='12w23eeeeedd', 
            Customer__c=accountObj.Id, 
            Account_Serno__c='3123123', 
            Institution_Id__c=2,
            Customer_Serno__c=contactObj.SerNo__c, 
            Product__c=prodobj.Id,
            Product_Serno__c=prodobj.Pserno__c
        );
        insert finAccObj;
        
        Card__c cardObj=new Card__c(
            People__c=contactObj.Id,
            PrimaryCardFlag__c=true,
            Financial_Account__c=finAccObj.Id, 
            Card_Serno__c='90247211', 
            Card_Number_Truncated__c='7136785481583561', 
            Financial_Account_Serno__c=finAccObj.Id, 
            People_Serno__c=contactObj.Id,
            Prod_Serno__c=prodobj.Pserno__c, 
            Product__c=prodobj.Id,
            Institution_Id__c=2
        );
        insert cardObj;
        
        // Test Case Record creation 
        
        Case cs = new Case();
        cs.ContactId=contactObj.Id;
        cs.Status= 'New';
        cs.Category__c='Dispute';
        cs.Origin='Web';
        cs.Department__c='Fraud and Chargeback';
        cs.Dispute_Type__c='0: lost';
        cs.Institutionid__c=1;
        cs.Gross_Amount__c=0;
        cs.Card__c=cardObj.id;
        insert cs;
        
        DCMS_Transaction__c tran = new DCMS_Transaction__c();
        tran.BillingAmount__c = 0.00;
        tran.DisputeType__c = '7: multiple imprint fraud';
        tran.Institutionid__c = 1;
        tran.MerchName__c = 'Loan';
        tran.TransactionSerno__c = 123;
        tran.MerchTypeMCC__c = 'Test';
        tran.MerchCountry__c = 'DK';
        tran.PCI_Dss_Token__c = 12345;
        tran.PosEntryMode__c='2';
        tran.Txn_CardSerno__c=90247211;
        tran.CaseNumber__c =cs.Id;
        tran.Chargeback__c = false;
        tran.Representment__c = false;
        tran.Arbitration__c = false; 
        tran.WriteOff__c = false;
        tran.FraudWriteOff__c = false;
        tran.Recovery__c = false;
        tran.CH_Liable__c =false;
        insert tran;
        
        DCMS_FraudCase__c fruadcase = [Select id, TotalCb__c,FraudWO__c,NonFraudWO__c,TotalFraudAmt__c,TotalRefund__c,TotalToCh__c,TotalAtRisk__c from DCMS_FraudCase__c where Case_Number__c = :cs.id Limit 1];
        fruadcase.TotalAtRisk__c = 0.00;
        fruadcase.NonFraudWO__c = 1.00;
        fruadcase.FraudWO__c = 1.00;
        update fruadcase;
        
        system.debug('---fruadcase--'+fruadcase);
        
        checkRecursive.run = true;
        tran.Chargeback__c = true;
        tran.Representment__c = true;
        tran.Arbitration__c = true; 
        tran.WriteOff__c = true;
        tran.FraudWriteOff__c = true;
        tran.Recovery__c = true;
        tran.CH_Liable__c = true;
        update tran;
        
        checkRecursive.run = true;
        tran.FraudWriteOff__c = false;
        tran.WriteOff__c =false;
        tran.Chargeback__c = true;
        tran.Representment__c = true;
        tran.Arbitration__c = true; 
        update tran;
        
        checkRecursive.run = true;
        tran.Chargeback__c = false;
        tran.Representment__c = false;
        tran.Arbitration__c = false; 
        tran.WriteOff__c = false;
        tran.FraudWriteOff__c = false;
        tran.Recovery__c = false;
        tran.CH_Liable__c = false;
        update tran;
        
        system.debug('-------tran----'+tran);

    } 
}