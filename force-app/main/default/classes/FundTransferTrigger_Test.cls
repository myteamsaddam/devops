@isTest 
private class FundTransferTrigger_Test{

  public static testmethod void testMethod1() {
      Account accountObj=new Account(
      Name='Test Account', 
      Institution_Id__c=2, 
      Customer_Serno__c='1123123'
      );
      insert accountObj;
      
      Blob hash = Crypto.generateDigest('MD5', Blob.valueOf('0000000061'));
      string encryptSSN=EncodingUtil.convertToHex(hash);
      Contact contactObj =new Contact(
      LastName='Test Contact', 
      FirstName='First Test Contact',
      SerNo__c='2123123', 
      Institution_Id__c=2, 
      SSN__c='0000000061',
      Encrypted_SSN__c=encryptSSN,
      AccountId=accountObj.Id,
      Phone='123123123', 
      Fax='98789879', 
      MobilePhone='98789879', 
      HomePhone='123123123', 
      Email='testemail@test.com'
      );
      insert contactObj;
      
      Blob hash2 = Crypto.generateDigest('MD5', Blob.valueOf('0000000062'));
      string encryptSSN2=EncodingUtil.convertToHex(hash2);
      Contact contactObj1 =new Contact(
      LastName='1Test Contact', 
      FirstName='1First Test Contact',
      SerNo__c='12123123', 
      Institution_Id__c=2, 
      SSN__c='0000000062',
      Encrypted_SSN__c=encryptSSN2,
      AccountId=accountObj.Id,
      Phone='1123123123', 
      Fax='198789879', 
      MobilePhone='198789879', 
      HomePhone='1123123123', 
      Email='1testemail@test.com'
      );
      insert contactObj1;
        
      Product_Custom__c prodobj=new Product_Custom__c(
      Name='Test Product',
      Pserno__c='56789'
      );
      insert prodobj;
      
      Financial_Account__c finAccObj=new Financial_Account__c(
      Account_Number__c='12w23eeeeedd', 
      Customer__c=accountObj.Id, 
      Account_Serno__c='3123123', 
      Institution_Id__c=2,
      Customer_Serno__c=contactObj.SerNo__c, 
      Product__c=prodobj.Id,
      Product_Serno__c=prodobj.Pserno__c
      );
      insert finAccObj;
        
      Card__c cardObj=new Card__c(
      People__c=contactObj.Id,
      PrimaryCardFlag__c=true,
      Financial_Account__c=finAccObj.Id, 
      Card_Serno__c='524858519', 
      Card_Number_Truncated__c='7136785481583561', 
      Financial_Account_Serno__c=finAccObj.Id, 
      People_Serno__c=contactObj.Id,
      Prod_Serno__c=prodobj.Pserno__c, 
      Product__c=prodobj.Id,
      Institution_Id__c=2
      );
      insert cardObj;
      
      Case caseObj=new Case(
      ContactId=contactObj.Id, 
      AccountId=accountObj.Id, 
      Card__c=cardObj.Id,
      Financial_Account__c=finAccObj.Id, 
      Category__c='Account closure', 
      Origin='Web'
      );
      insert caseObj;
      
      FundsTransfer__c fundsTransfer =new FundsTransfer__c(
      SSN__c=contactObj.SSN__c, 
      RegistrationDate__c=system.now(), 
      TransactionType__c='Credit Balance', 
      Amount__c=5555, 
      Case__c=caseObj.Id, 
      Card__c=cardObj.Id, 
      Customer__c=contactObj.Id, 
      SASProcessed__c=false, 
      SASRecieved__c=false, 
      AccountSerno__c='123123', 
      RequestedOn__c=system.now()
      );
      insert fundsTransfer;
      
      fundsTransfer.AccountSerno__c='54576';
      update fundsTransfer;
      delete fundsTransfer;
  }

}