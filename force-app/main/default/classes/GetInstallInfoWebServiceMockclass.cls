@isTest
global class GetInstallInfoWebServiceMockclass implements WebServiceMock 
{
   global void doInvoke(
       Object stub,
       Object request,
       Map<String, Object> response,
       String endpoint,
       String soapAction,
       String requestName,
       String responseNS,
       String responseName,
       String responseType) 
       {
           SOAPCSSServiceResponse.GetInstallInfoResponse_element respElement= new SOAPCSSServiceResponse.GetInstallInfoResponse_element();
           SOAPCSSServiceResponse.AccountsType respAccountstype = new SOAPCSSServiceResponse.AccountsType();
           SOAPCSSServiceResponse.AccountType respAccounttype = new SOAPCSSServiceResponse.AccountType();
           SOAPCSSServiceResponse.InstallmentsType respInstallmenttype = new SOAPCSSServiceResponse.InstallmentsType();
           SOAPCSSServiceResponse.InstallmentType respInstallmentstype = new SOAPCSSServiceResponse.InstallmentType();
           SOAPCSSServiceResponse.TransactionsType respTransactionsType = new SOAPCSSServiceResponse.TransactionsType();
           SOAPCSSServiceResponse.TransactionInstallResType respTransactionInstallType = new SOAPCSSServiceResponse.TransactionInstallResType();
           SOAPCSSServiceResponse.Accounttype[] respElementlist = new List<SOAPCSSServiceResponse.Accounttype>();
           SOAPCSSServiceResponse.InstallmentType[] respInstallmentTypeElementlist = new List<SOAPCSSServiceResponse.InstallmentType>();
           SOAPCSSServiceResponse.TransactionInstallResType[] respTransactionInstall = new List<SOAPCSSServiceResponse.TransactionInstallResType>();
           SOAPCSSServiceResponse.GetInstallInfoResponse_element objInfoResEle = new SOAPCSSServiceResponse.GetInstallInfoResponse_element();
           
           respTransactionInstallType.Period = 1;
           respTransactionInstallType.Trxnmsgtype = 'Trxnmsgtype';
           respTransactionInstallType.Feereasoncode = 'Trxnmsgtype';
           respTransactionInstallType.Trxndescription = 'Trxnmsgtype';
           respTransactionInstallType.Status = 'Trxnmsgtype';
           respTransactionInstallType.Stgeneral = 'Trxnmsgtype';
           respTransactionInstallType.Merchname = 'Trxnmsgtype';
           respTransactionInstallType.Origcardnumber = 'Trxnmsgtype';
           respTransactionInstall.add(respTransactionInstallType);
           respTransactionsType.Transaction_x = respTransactionInstall;
           
           respInstallmentstype.Numberx1 = 'Numberx1';
           respInstallmentstype.Name = 'Name';
           respInstallmentstype.Status = 'Status';
           respInstallmentstype.OutStandingBal = 123456;
           respInstallmentstype.Transactions = respTransactionsType;
           respInstallmentTypeElementlist.add(respInstallmentstype);
           
           respInstallmenttype.Installment = respInstallmentTypeElementlist;
           
           respAccounttype.Numberx = 'Numberx';
           respAccounttype.Name = 'Name';
           respAccounttype.Status = 'Status';
           respAccounttype.Installments = respInstallmenttype;
           respElementlist.add(respAccounttype);
           
           respAccountstype.Account = respElementlist;
           respElement.Accounts = respAccountstype;
           response.put('response_x', respElement);
        }
}