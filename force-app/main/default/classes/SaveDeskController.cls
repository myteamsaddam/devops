/***********************************************************************************************************
Created By : RAJESH
Date       :31/05/2018
VF Page    : SaveDeskQueueTablePage
Purpose    : To make the outbound call to customers from Savedesk
*******************************************************************************************************************/
Public class SaveDeskController

{
    
    public Integer size { get; set; }
    public boolean displaySave { get; set; }
    public Integer NumberOfPages {get; set;}
    
    public string countryCode{get;set;}   
    public string CampgnType{get;set;}
    public string statusCode{get;set;}
    public Date Dat30 = date.today().adddays(-30);
    
    Public SaveDeskController(ApexPages.StandardController controller){
        flag =false;
        task = new Task();getstatusCodes();
    }
    
    // Read the Insurances from custom settings
   public List<SelectOption> getCampaignTypes()
    {
        Set<String> CampaignSets = new Set<String>();
        List<SelectOption> optns = new List<Selectoption>();
        optns.add(new selectOption('', '--None--'));
        if(countryCode!=null){
            for(AggregateResult l : [SELECT Campaign_Type__c FROM SaveDeskQueue__c where Institution_Id__c=:countryCode GROUP BY Campaign_Type__c]){ 
               optns.add(new SelectOption(countryCode+String.valueOf(l.get('Campaign_Type__c')),String.valueOf(l.get('Campaign_Type__c'))));
            }
        }
                
       /* for (CampaignTypes__c camp : CampaignTypes__c.getAll().values())
        {
            boolean noCampStr = camp.Applicable_for_NO__c;
            boolean dkCampStr = camp.Applicable_for_DK__c;
            boolean seCampStr = camp.Applicable_for_SE__c;
            
            if(countryCode=='1' && camp.Applicable_for_NO__c==True){
                optns.add(new selectOption(countryCode+camp.Name,camp.Name));
            }
            
            if(countryCode=='3' && camp.Applicable_for_DK__c== true){
                optns.add(new selectOption(countryCode+camp.Name,camp.Name));
            }
            
            if(countryCode=='2'&& camp.Applicable_for_SE__c==True){
                optns.add(new selectOption(countryCode+camp.Name,camp.Name));
            }
        }*/
        
        return optns; 
    }
    
    public List<SelectOption> getstatusCodes(){
        
        List<SelectOption> optns = new List<Selectoption>();
        //optns.add(new selectOption('', '--None--'));
        string[] actionsArray = new string[]{};
            
            // Need to fetch from custom setting
            actionsArray = new string[]{'Open','All','Call Back','Closed'};
                for(string ac : actionsArray){
                    
                    // if(countryCode!=null&&CampgnType!=null){
                    //optns.add(new selectOption(countryCode+CampgnType+ac, ac));
                    optns.add(new selectOption(ac, ac));
                    //}
                }          
        
        return optns;
    }
    
    public List<SelectOption> getcountryCodes(){
        
        List<SelectOption> optns = new List<Selectoption>();
        optns.add(new selectOption('', '--None--'));
        optns.add(new selectOption('1', 'NO'));
        optns.add(new selectOption('2', 'SE'));
        optns.add(new selectOption('3', 'DK'));
        return optns;
    }
    
    //This is Our collection of the class/wrapper objects SaveDeskWrapperClass
    
    Public List<SaveDeskWrapperClass> wrapperlist;
    
    Public Integer noOfRecords{get; set;}
    
    // Create a new Map to verify whether the SaveDeskrecord is already added in the Map
    
    Map <id,SaveDeskQueue__c> SelectedSaveDeskQueuetMap = new Map <id,SaveDeskQueue__c>();
    
    public boolean display{get;set;}
    
    public list<SaveDeskQueue__c> selectedList {get;set;}
    
    // instantiate the StandardSetController from a query locator
    
    public ApexPages.StandardSetController SetSaveDeskQueue
        
    {
        
        get
            
        {
            
            if(SetSaveDeskQueue== Null)
                
            {
                displaySave=true;
                SetSaveDeskQueue = new ApexPages.StandardSetController(Database.getQueryLocator([SELECT Id,Financial_Account__c,Financial_Account__r.id,Financial_Account__r.Name,Customer_Name__c,Customer_Name__r.id,Customer_Name__r.Name,Calling_Attempts__c,Case_Number__c,End_Date__c,Customer_Reaction_In_call__c,Interested_In_Offer__c,Name,Offer__c,Phone_No__c,Status__c,Comments__c,Time_of_Call__c,Agent_ID__c,Agent_ID__r.Name FROM SaveDeskQueue__c where End_Date__c >=: Dat30]));
                
                // sets the number of records in each page set
                size=10;
                SetSaveDeskQueue.setpagesize(size);
                
                noOfRecords = SetSaveDeskQueue.getResultSize();
                
                NumberOfPages =  (noOfRecords/(Decimal)size).round(System.RoundingMode.CEILING).intValue();
                
            }
            
            return SetSaveDeskQueue;
            
        }
        
        set;
        
    }
    
    //Returns a list of wrapper objects for the sObjects in the current page set
    
    Public List<SaveDeskWrapperClass> getSaveDeskQueue()
        
    {
        
        getSelectedSaveDeskQueue();
        
        // Initilaize the list to add the selected SaveDeskrecord
        
        wrapperlist = new List <SaveDeskWrapperClass>();
        
        for(SaveDeskQueue__c cc : (List<SaveDeskQueue__c>)SetSaveDeskQueue.getRecords())
            
        {
            if( SelectedSaveDeskQueuetMap.ContainsKey(cc.id))
            {
                wrapperlist.add (new SaveDeskWrapperClass(cc,true));
            }
            
            else
            {
                
                wrapperlist.add(new SaveDeskWrapperClass(cc,false));
            }
            
        }
        
        return wrapperlist;
    }
    
    public void getSelectedSaveDeskQueue(){
        
        if(wrapperlist!=null)
        {
            for(SaveDeskWrapperClass wr:wrapperlist)
            {
                if(wr.checked == true)
                {
                    SelectedSaveDeskQueuetMap .put(wr.savedeskobj.id,wr.savedeskobj); // Add the selected SaveDeskrecord id in to the SelectedSaveDeskQueuetMap.
                }
                else
                {
                    SelectedSaveDeskQueuetMap .remove(wr.savedeskobj.id); // If you uncheck the SaveDeskrecord, remove it from the SelectedSaveDeskQueuetMap
                }
            }
        }
        
    }
    
    Public Boolean flag {set;get;}
    
    public void fetchData(){
        System.debug('----countryCode---'+countryCode);
        if(CampgnType!= null&& countryCode != null && statusCode!=null)
        {
            String CampgnTypeRev=CampgnType.substringAfter(countryCode);
            //String statusCodeRev=statusCode.substringAfter(countryCode+CampgnType);
            String statusCodeRev=statusCode;
            flag = true;
            if(statusCodeRev=='All'){
                SetSaveDeskQueue = new ApexPages.StandardSetController(Database.getQueryLocator([SELECT Id,Financial_Account__c,Financial_Account__r.id,Financial_Account__r.Name,Customer_Name__c,Customer_Name__r.id,Customer_Name__r.Name,Calling_Attempts__c,Case_Number__c,End_Date__c,Customer_Reaction_In_call__c,Interested_In_Offer__c,Name,Offer__c,Phone_No__c,Status__c,Comments__c,Time_of_Call__c,Agent_ID__c,Agent_ID__r.Name FROM SaveDeskQueue__c where (Campaign_Type__c=:CampgnTypeRev and Institution_Id__c=:countryCode and End_Date__c >=: Dat30)]));
            }
            else{
                SetSaveDeskQueue = new ApexPages.StandardSetController(Database.getQueryLocator([SELECT Id,Financial_Account__c,Financial_Account__r.id,Financial_Account__r.Name,Customer_Name__c,Customer_Name__r.id,Customer_Name__r.Name,Calling_Attempts__c,Case_Number__c,End_Date__c,Customer_Reaction_In_call__c,Interested_In_Offer__c,Name,Offer__c,Phone_No__c,Status__c,Comments__c,Time_of_Call__c,Agent_ID__c,Agent_ID__r.Name FROM SaveDeskQueue__c where (Campaign_Type__c=:CampgnTypeRev and Institution_Id__c=:countryCode and Status__c=:statusCodeRev and End_Date__c >=: Dat30)]));
            }
            
            ShowDisplay();  
            size=10;
            SetSaveDeskQueue.setpagesize(size);
            
            noOfRecords = SetSaveDeskQueue.getResultSize();
            NumberOfPages = (noOfRecords/(Decimal)size).round(System.RoundingMode.CEILING).intValue();
            
        }
        
    }
    
    public PageReference CancelRec() {
        PageReference newpage = new PageReference(System.currentPageReference().getURL());    
        newpage.getParameters().clear();
        newpage.setRedirect(true);
        return newpage;
    }
    
    public PageReference SaveRec() {
        if(selectedList.size()>0){
            update selectedList;
            fetchData();
        }
        
        PageReference newpage = new PageReference(System.currentPageReference().getURL());    
        newpage.getParameters().clear();
        newpage.setRedirect(true);
        return newpage;
    }
    
    public void clickMe()
        
    {
        display = true;
        getSelectedSaveDeskQueue();
        selectedList = SelectedSaveDeskQueuetMap.values();
    }
    
    public integer pageNumber
        
    { get
        
    {
        return SetSaveDeskQueue.getPageNumber();
        
    }
     
     set;
    }
    
    public PageReference SaveRec1() {
        
        List<SaveDeskQueue__c> updateList =new List<SaveDeskQueue__c>();
        for(SaveDeskWrapperClass wrap : wrapperlist)
        {
            if(wrap.checked ==true)
            {
                //wrap.savedeskobj.CaseNumber__c = Decimal.valueOf(task.What.name);
                updateList.add(wrap.savedeskobj);
                wrap.checked = false;
            }
        }
        update updateList ;
        
        Calltype = '';
        if(CallType == ''){
            task.WhoId = null;
            
        }
        fetchData();
        /*PageReference newpage = new PageReference(System.currentPageReference().getURL());    
          newpage.getParameters().clear();
          newpage.setRedirect(true);
          return newpage;*/
        return null;
    }
    
    public void ShowDisplay() {
        displaySave=true;
        List<SaveDeskQueue__c> updateList =new List<SaveDeskQueue__c>();
        if(wrapperlist.size()>0){
            for(SaveDeskWrapperClass wrap : wrapperlist)
            {
                if(wrap.checked ==true)
                {
                   displaySave=false;
                }
            }
        }
    }
    
    /*======To create the case and task for save desk team.*/
    
    public String ANI { get; set;}
    public String CallObject { get; set;} 
    public Integer CallDurationInSeconds { get; set;} 
    public String CallType { get; set;}  
    public String CallDisposition { get; set;} 
    
    public Datetime callStartTime;
    public string statusMessageCTI{get; set;}
    
    public String statusMessage { get; set; }
    public Task task { 
        get {
            if(task == null) {
                task = new Task();
                task.Status='Open';
            }
            return task;
        }
        set; 
    }    
    
    public Case objCase { 
        get {
            if(objCase == null) {
                objCase = new Case();
                objCase.Category__c='SaveDesk';
                objCase.Status='In Progress';
                objCase.Origin = 'Phone';
                objCase.Call_Type__c = 'outbound';
                objCase.OwnerId = UserInfo.getUserId();
            }
            return objCase;
        }
        set; 
    }
    
    private String Subject { 
        get {
            Subject = 'SaveDesk Call at ' + System.now().format('yyyy-MM-dd HH:mm:ss','Europe/Paris');
            return Subject;
        }
        set; 
    }
    
    Public String whoId {set;get;}
    Public Id finId {set;get;}
    Public Id saveDeskId {set;get;}
    Public Case cse {set;get;}
    
    /*Public PageReference getRecoredId(){
      system.debug('------record----'+whoId);
      task.WhoId = whoId;
      system.debug('------task.whoid-----'+task.WhoId);
      return null;
      }*/
    
    public void setCallAttachedData() 
    {
        //Cache.Session.remove('local.CTI.caseId');
        //Cache.Session.remove('local.CTI.taskId');
        If(objCase.Id != null || task.Id != null)
        {
            objCase = null;
            task = null;
        }  
        
        System.Debug('@@CallObject: ' + CallObject);
        task.WhoId = whoId;
        task.CallObject = CallObject;
        task.cnx__UniqueId__c = CallObject;
        task.ANI__c = ANI;
        task.Call_Type__c = CallType;
        task.subject = Subject;
        objCase.ContactId = whoId;
        objCase.Financial_Account__c = finId;
        callStartTime = System.now();
        
        system.debug('-----calltype----'+CallType);
        
        if(CallType=='outbound')
        {
            system.debug('----countryCode--'+countryCode);
            if(countryCode == '3'){
                    objCase.Country__c = 'DK';
                }
                if(countryCode == '2'){
                    objCase.Country__c = 'SE';
                }
                if(countryCode == '1'){
                    objCase.Country__c = 'NO';
                }
            insert objCase;
            system.debug('----objCase.Id--'+objCase.Id);
            SaveDeskQueue__c sdq = new SaveDeskQueue__c();
            sdq.id = saveDeskId;
            system.debug('---saveDeskId--'+saveDeskId);
            cse = [Select id, CaseNumber,description from case where id =: objCase.id];
            system.debug('---objCase.CaseNumber--'+cse.CaseNumber);
            system.debug('---objCase.description--'+cse.description);
            sdq.Case_Number__c = cse.CaseNumber;
            sdq.Time_of_Call__c=System.now();
            sdq.Agent_ID__c=UserInfo.getUserId();
            update sdq;
            fetchData();
            system.debug('-----afterupdate-----'+sdq.Id);
            task.whatId = objCase.Id;
            insert task;
            
            /*Case cse = [Select id, CaseNumber from case where id =: objCase.id];
                SaveDeskQueue__c sdq=[Select id, Case_Number__c, Response_from_the_logger_window__c from SaveDeskQueue__c where Customer_Name__c =:whoId];
                system.debug('-------sdq-----'+sdq+'======'+sdq.Case_Number__c+'=========='+cse.CaseNumber);
                //sdq.id=objCase.id;
                  sdq.Case_Number__c=cse.CaseNumber;
                  sdq.Response_from_the_logger_window__c=UserInfo.getName()+' '+System.now().format('yyyy-MM-dd HH:mm:ss','Europe/Paris');
                  update sdq;
                  system.debug('----afterupdate-------'+sdq);*/
            //Cache.Session.put('local.CTI.taskId', task.Id);
        }
    }
    
    public void setCallEndData() 
    {
        System.Debug('@@CallEndData: ' + CallDurationInSeconds);
        System.Debug('@@CallDisposition: ' + CallDisposition);
        task.CallDurationInSeconds = CallDurationInSeconds;
        task.CallDisposition = CallDisposition; 
        try{
            If(callType=='outbound'){
                task.CallDurationInSeconds+=(Integer)Common_Settings__c.getValues('CTI').CTI_Wrap_Time__c;//add 10 seconds wrap to previous call duration
                //task.status='Completed';
                task.Description = objCase.description;
                save();
            }else {
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error,System.Label.CTI_Category_Select));
            }
        }catch(Exception exp){
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error,exp.getMessage()));//for 1847
            StatusLogHelper.logSalesforceError('SaveDeskController', 'E005', 'Error in CTI', exp, false);//for 1847
        }
        
    }
    public void save() {
        System.Debug('@@Save Task: ' + task);
        
        System.debug('@@lstContact objCase : ' +objCase);
        try{
            List<Task> lt = [select status from Task where id=:task.id];
            If(callType=='outbound' || (lt != null && lt.size()>0 && lt[0].status!='Completed')){
                //task.Status='Open';//task will not be closed upon call end
                //upsert task;
            }    
            statusMessage = 'Last save at ' + DateTime.now();
        }catch(Exception exp){
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error,exp.getMessage()));//for 1847
            StatusLogHelper.logSalesforceError('SaveDeskController', 'E005', 'Error in CTI', exp, false);//for 1847
        }
    }
    
    public void saveaction() 
    {
        System.Debug('@@Save Task: ' + task);
        
        System.debug('@@lstContact objCase : ' +objCase);
        
        try{
            If(whoId.substring(0,3) == '003') 
            {
                objCase.Subject = task.subject; 
                
                if(objCase.status!='Closed' || objCase.OwnerId==UserInfo.getUserId()){
                    if((callType=='outbound' && task.status=='Completed'))
                    { 
                        task.status='Completed';
                        task.Description = objCase.description;
                        task.CallDurationInSeconds = 200;// CallDurationInSeconds;
                        
                        Long cst = callStartTime.getTime();
                        Long cet = System.now().getTime();
                        task.CallDurationInSeconds = (Integer)(cet - cst)/1000;
                        System.debug('----task.status--'+task.status);
                        System.debug('----task.CallDurationInSeconds--'+task.CallDurationInSeconds);
                        System.debug('----cst--'+cst);
                        System.debug('----cet--'+cet);
                        System.debug('----task.CallDurationInSeconds--'+task.CallDurationInSeconds);
                        
                        
                        
                        List<Task> lt = [select status from Task where id=:task.id];
                        If(callType=='outbound' || (lt.size()>0 && lt[0].status!='Completed'))             
                            upsert task;
                        
                        if(callType=='outbound')
                            objCase.status='Closed'; 
                        
                        Cache.Session.remove('local.CTI.caseId'); 
                        Cache.Session.remove('local.CTI.taskId');
                        statusMessage = 'Last save at ' + DateTime.now();
                    }    
                    
                    if(CallType=='outbound')
                    {
                        If(objCase.Category__c != null && objCase.Category__c != 'None')
                        {          
                            //insert objCase;
                            if(objCase.status=='Closed')
                                objCase.Total_Call_Handling_Time__c = task.CallDurationInSeconds;
                            system.debug('----objCase.Total_Call_Handling_Time__c--'+task.CallDurationInSeconds);
                            SaveDeskQueue__c sdq = new SaveDeskQueue__c();
                            sdq.id = saveDeskId;
                           // sdq.Comments__c=objCase.description;
                            update objCase;
                            update sdq;
                            fetchData();
                            if(objCase.OwnerId!=UserInfo.getUserId()){//ownership changed
                                //objCase.Status='Closed';
                                //isCaseTransfered = true;
                                statusMessageCTI = System.Label.CTI_Case_Transfered; 
                            }
                            else if(objCase.Status=='Closed'){
                                statusMessageCTI = System.Label.Case_Close_Message;
                                /* callType = '';
                                if(Calltype == ''){
                                    task.WhoId = null;
                                }*/
                            }                     
                            System.Debug('@@Save objCase: ' + objCase);       
                            //task.whatId = objCase.Id;
                            statusMessage = 'Last save at ' + DateTime.now();
                        }
                        else
                        {
                            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error,System.Label.CTI_Category_Select));
                        }
                    }
                }
                else
                {
                    objCase.status='In Progress';
                    ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error,System.Label.CTI_Case_Transfered_Cannot_close));
                }
                
                
            }
            //else if((recordId.substring(0,3) == '003') && objCase.id!=null)//on contact page and case is already saved so update the case
            //update objCase;
        }catch(Exception exp){
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error,exp.getMessage()));//for 1847
            StatusLogHelper.logSalesforceError('SaveDeskController', 'E005', 'Error in CTI', exp, false);//for 1847
        }
        
    }
    
}