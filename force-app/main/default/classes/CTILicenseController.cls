public with sharing class CTILicenseController {  
    Public Integer Avaiable {set;get;}
    Public List<CTIlicensedata> Ctilist {set;get;}
    Public List<PackageLicense> Pkglist {Set;get;}
    
    public List<CTIlicensedata> getLicenseData() 
    {  
        Ctilist = new List<CTIlicensedata>();
        Pkglist = new List<PackageLicense>();  
        
        String sql = 'SELECT AllowedLicenses,NamespacePrefix,UsedLicenses FROM PackageLicense where NamespacePrefix = \'cnx\'';
        Pkglist = Database.Query(sql);
        
        for(PackageLicense pl:Pkglist)
        {           
            Avaiable = pl.AllowedLicenses - pl.UsedLicenses;
            Ctilist.add(new CTIlicensedata('Total',pl.AllowedLicenses));
            Ctilist.add(new CTIlicensedata('Used',pl.UsedLicenses));
            Ctilist.add(new CTIlicensedata('Avaiable',Avaiable));
        }
        return Ctilist;  
    } 
    
    
    
    // Wrapper class  
    public class CTIlicensedata
    {  
        public String name { get; set; }  
        public Integer data { get; set; }  
        
        public CTIlicensedata(String name, Integer data) 
        {  
            this.name = name;  
            this.data = data;  
        }  
    }  
}