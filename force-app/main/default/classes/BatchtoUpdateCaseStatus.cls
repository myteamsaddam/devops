global class BatchtoUpdateCaseStatus implements Database.Batchable<sObject>, Database.Stateful, schedulable {
    
    global Map<Id, String> errorMap {get; set;}
    global Map<Id, SObject> taskMap {set;get;}
    global Map<Id, SObject> caseMap {get; set;}
    global String body='';
    global String casestr='';
    global String taskstr='';
    global String subject='';
    global String finalstr='';
    global String caseattName='';
    global String taskattName='';
    global String errorattName='';
    global string recordString='';
    
    
    global BatchtoUpdateCaseStatus(){
        errorMap = new Map<Id, String>();
        taskMap = new Map<Id, SObject>();
        caseMap = new Map<Id, SObject>();
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC){
        String query = 'Select id, casenumber,Category__c, status,Owner.name, Department__c, owner.profile.name, owner.UserRole.name from Case where status = \'In Progress\' And Call_type__c = \'inbound\' ';
        return Database.getQueryLocator(query);
    }
    
    global void execute(Database.BatchableContext BC, List<Case> scope) {
        System.debug('-------scope-----'+scope.size());
        List<Case> cslist = new List<Case>();
        Set<Id> cseId = new Set<Id>();
        for(Case cs : scope){
            cseId.add(cs.id);
            /*if(cs.OwnerId != null){
cs.OwnerId = cs.OwnerId;
}*/
            if(cs.Category__c == 'None'){
                cs.Category__c = 'Closed by Batch';
            }
            if(cs.status == 'In Progress'){
                cs.status = 'Closed';
            }
            cslist.add(cs);
        }
        
        If(cslist.size() > 0){
            List<Database.SaveResult> caseupdate = Database.update(cslist, false);
            Integer index = 0;
            for(Database.SaveResult drs : caseupdate){
                if(!drs.isSuccess()){
                    String msg = drs.getErrors()[0].getMessage();
                    system.debug('-----msg---'+msg);
                    errorMap.put(cslist[index].Id, msg);
                    //  caseMap.put(cslist[index].Id, cslist[index]);
                }
                if(drs.isSuccess()){
                    caseMap.put(cslist[index].Id, cslist[index]);
                }
                index++;
            }
        }
        
        List<Task> taskquery = [Select id, Subject,Status,owner.name,owner.profile.name, owner.UserRole.name from Task where whatId IN : cseId AND (status = 'Open' AND Call_type__c = 'Inbound')];
        system.debug('-----task----'+taskquery);
        List<Task> tsk = new List<Task>();
        if(taskquery.size() > 0){
            for(Task ts : taskquery){
                ts.Status = 'Completed';
                tsk.add(ts);
            }
        }
        if(tsk.size()>0){
            List<Database.SaveResult> taskupdate = Database.update(tsk, false);
            Integer index = 0;
            for(Database.SaveResult drs : taskupdate){
                if(!drs.isSuccess()){
                    String msg = drs.getErrors()[0].getMessage();
                    errorMap.put(tsk[index].Id, msg);
                    // taskMap.put(tsk[index].Id, tsk[index]);
                }
                if(drs.isSuccess()){
                    taskMap.put(tsk[index].Id, tsk[index]);
                }
                index++;
            }
        }   
    }
    
    global void execute(SchedulableContext sc)
    {
        BatchtoUpdateCaseStatus d=new BatchtoUpdateCaseStatus();
        Database.executeBatch(d, 200);
    }
    
    global void finish(Database.BatchableContext BC) 
    {
        
        User ur = [Select id,Email,name from user where id =: userinfo.getUserId()];
        system.debug('---errorMap.isEmpty()--'+errorMap.isEmpty());
        
        system.debug('-----caseemapty----'+caseMap.isEmpty());
        system.debug('-----Taskemapty----'+taskMap.isEmpty());
        if(!errorMap.isEmpty()){
            
            AsyncApexJob a = [SELECT id, ApexClassId,JobItemsProcessed, TotalJobItems,NumberOfErrors, CreatedBy.Email FROM AsyncApexJob WHERE id = :BC.getJobId()];
            if(!errorMap.isEmpty() && (taskMap.isEmpty() && caseMap.isEmpty())){
                body = 'Hi,\n \n' + 'Daily Case closure Job has completed. \n' + errorMap.size() + '  Error/s reported. Details in the attached file. \n \n Thanks, \n ' + ur.name + ' ';
                subject = 'Daily Case Closure Job - Reject List';
            } else {
                body = 'Hi,\n \n' + 'Daily Case closure Job has completed. \n' + errorMap.size() + '  Error/s reported, ' + caseMap.size() + ' Case record/s and ' + taskMap.size() + ' Task record/s updated. Details in the attached file. \n \n Thanks, \n ' + ur.name + ' ';
                subject = 'Daily Case Closure Job - Success and Reject List';
            }
            // Creating the CSV file
            
            finalstr = 'ObjectName,Id, Error \n';
            
            errorattName = 'ClosedCases Errors.csv';
            for(Id id  : errorMap.keySet())
            {
                string err = errorMap.get(id);
                //Case Cse = (Case) IdToSObjectMap.get(id);
                String sob= id.getSObjectType().getDescribe().getName();
                string recordString = '"'+sob+'","'+id+'","'+err+'"\n';
                finalstr = finalstr +recordString;
            } 
        }
        
        system.debug('-----caseemapty11----'+caseMap.size());
        system.debug('-----Taskemapty11----'+taskMap.size());
        
        if(caseMap.size() > 0)
        {        
            if(caseMap.size() > 0 && !errorMap.isEmpty())
            {
                body = 'Hi,\n \n' + 'Daily Case closure Job has completed. \n' + caseMap.size() + ' Case record/s, ' + taskMap.size() + ' Task record/s updated and ' + errorMap.size() + '  Error/s reported. Details in the attached file. \n \n Thanks, \n ' + ur.name + ' ';
                subject = 'Daily Case Closure Job - Success and Reject List';
            } else {
                body = 'Hi,\n \n' + 'Daily Case closure Job has completed. \n' + caseMap.size() + ' Case record/s and ' + taskMap.size() + ' Task record/s updated. Details in the attached file. \n \n Thanks, \n ' + ur.name + ' ';
                subject = 'Daily Case Closure Job - Success List';
            } 
            casestr = 'CaseNumber, Owner Name, Department, Agent Role, Agent Profile, Status, Category, Id \n';
            
            caseattName = 'ClosedCases success.csv';
            for(Id id  : caseMap.keySet())
            {
                //String sob= id.getSObjectType().getDescribe().getName();
                Case cse = (Case) caseMap.get(id);
                system.debug('-----cse------'+cse);
                if(cse != null)
                {
                    recordString = '"'+cse.CaseNumber+'","'+cse.owner.name+'","'+cse.Department__c+'","'+cse.owner.UserRole.name+'","'+cse.owner.profile.name+'","'+cse.Status+'","'+cse.Category__c+'","'+id+'"\n';
                }
                casestr = casestr +recordString;
            }
            
            system.debug('casestr =====>'+casestr);
        }
        
        if(taskMap.size()>0)
        {
            taskstr = 'Subject, Owner Name, Agent Role, Agent Profile, Status, Id \n';
            taskattName = 'ClosedTasks success.csv';
            for(Id id  : taskMap.keySet())
            {
                Task t = (Task) taskMap.get(id);
                system.debug('-----Task------'+t);
                if(t != null)
                {
                    recordString = '"'+t.subject+'","'+t.owner.name+'","'+t.owner.UserRole.name+'","'+t.owner.profile.name+'","'+t.status+'","'+id+'"\n';
                }
                taskstr = taskstr +recordString;
            }
            
            system.debug('taskstr =====>'+taskstr);
        }
        
        if(caseMap.isEmpty() && taskMap.isEmpty() && errorMap.isEmpty()){
            body = 'Hi,\n \n' + 'Daily Case closure Job has completed. \nThere is no Case and Task record/s to update. \n \n Thanks, \n ' + ur.name + ' ';
            subject = 'Daily Case Closure Job'; 
        }
        
        // Define the email
        Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage(); 
        
        // Create the email attachment  
        
        Messaging.EmailFileAttachment efa = new Messaging.EmailFileAttachment();
        if(caseMap.size() > 0 ){
            efa.setFileName(caseattName); 
            efa.setBody(Blob.valueOf(casestr));
        }
        
        Messaging.EmailFileAttachment efa1 = new Messaging.EmailFileAttachment();
        if(taskMap.size() > 0 ){
            efa1.setFileName(taskattName);
            system.debug('taskstr----> '+taskstr);
            efa1.setBody(Blob.valueOf(taskstr));
        }
        
        Messaging.EmailFileAttachment efa2 = new Messaging.EmailFileAttachment();
        if(errorMap.size() > 0){
            efa2.setFileName(errorattName);
            system.debug('finalstr----> '+finalstr);
            efa2.setBody(Blob.valueOf(finalstr)); 
        }
        
        // Sets the paramaters of the email
        List<String> emailIds = new List<String>();
        for(BatchEmailIds__c emailAdd : BatchEmailIds__c.getAll().values()){
            emailIds.add(emailAdd.Email_Id__c);
        }
        system.debug('--emailId--'+emailIds);
        
        email.setSubject( subject );
        email.setToAddresses(emailIds);//{'ecsfdc.in@capgemini.com','Joakim.Holtz@entercard.com'});
        system.debug('-----body------'+body);
        email.setPlainTextBody( body );
        
        List<Messaging.EmailFileAttachment> alist = new List<Messaging.EmailFileAttachment>();
        
        if(caseMap.size() > 0 )
            alist.add(efa); 
        //email.setFileAttachments(new Messaging.EmailFileAttachment[] {efa});
        
        if(taskMap.size() > 0 )
            alist.add(efa1); 
        //email.setFileAttachments(new Messaging.EmailFileAttachment[] {efa1});
        
        if(errorMap.size() > 0)
            alist.add(efa2); 
        
        email.setFileAttachments(alist);
        
        // Sends the email
        Messaging.SendEmailResult [] batchmail = Messaging.sendEmail(new Messaging.SingleEmailMessage[] {email});   
    } 
}