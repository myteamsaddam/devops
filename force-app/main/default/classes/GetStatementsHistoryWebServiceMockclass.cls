@isTest
global class GetStatementsHistoryWebServiceMockclass implements WebServiceMock {
   global void doInvoke(
           Object stub,
           Object request,
           Map<String, Object> response,
           String endpoint,
           String soapAction,
           String requestName,
           String responseNS,
           String responseName,
           String responseType) {
       SOAPCSSServiceResponse.getStatementsHistoryResponseTypeCollection respElement= new SOAPCSSServiceResponse.getStatementsHistoryResponseTypeCollection();
       SOAPCSSServiceResponse.getStatementsHistoryResponseType stshis = new SOAPCSSServiceResponse.getStatementsHistoryResponseType();
       SOAPCSSServiceResponse.getStatementsHistoryResponseTypeCollection[] respElementlist = new List<SOAPCSSServiceResponse.getStatementsHistoryResponseTypeCollection>();
       respElement.StatementSerno = 'StatementSerno'; 
       respElement.GenerateDate = 'GenerateDate';
       respElement.ClosingBalance = 'ClosingBalance';
       respElement.MindueAmount = 'MindueAmount';
       respElement.TotalPayments = 'TotalPayments';
       respElement.OverdueAmount = 'OverdueAmount';
       respElement.Overduecycles = 'Overduecycles';
       respElement.Duedate = 'Duedate';
       respElement.TotalDebits = 'TotalDebits';
       respElement.AmtTrxn = 'AmtTrxn';
       respElement.TrxnDate = 'TrxnDate';
       respElement.TrxnDate2 = 'TrxnDate2';
       respElementlist.add(respElement);
       stshis.getStatementsHistoryResponseTypeCollection=respElementlist;
       response.put('response_x', stshis);
    }
    }