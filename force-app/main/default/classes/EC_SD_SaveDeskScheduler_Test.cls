/**********************************************************************
Name: EC_SD_SaveDeskScheduler_Test
=======================================================================
Purpose: This test class is used for cover the code of the EC_SD_SaveDeskScheduler class -User Story -SFD-1430

======================================================================= 
History
-----------------------------------------------------------------------         
Version     Author                   Date              Detail
1.0         Saddaam Hussain         20-May-2020       Initial Version

**********************************************************************/
@isTest(SeeAllData = false)
public class EC_SD_SaveDeskScheduler_Test {
     /**********************************************************************
*   Author: Saddam Hussain
*   Date:     20-May-2020
*   User Story : SFD-1430
*   Param: None
*   Return: None    
*   Description: In this method we are created save desk record and schedule the class.
* @Revision(s): [Date] - [Change Reference] - [Changed By] - [Description]   

**********************************************************************/
    static testmethod void saveDeskSchedulerTest() {
        String CRON_EXP = '0 0 0 15 3 ? *';
        System.runAs(EC_UnitTestDataGenerator.adminUser) {
            SaveDeskQueue__c sdObj = EC_UnitTestDataGenerator.TestSaveDesk.buildInsert(new Map<String, Object>{
                'Status__c'=>'Open',
                    'Response__c'=>'Callback Later',
                    'Calling_Attempts__c'=>'Inbound Call'
                    });
            List<SaveDeskQueue__c> saveDeskList = new List<SaveDeskQueue__c>();
            saveDeskList.add(sdObj);
            List<String>  sDeskAttr = new List<String> {'Status__c','Response__c','Calling_Attempts__c'};
                List<SaveDeskQueue__c> saveDeskReload = EC_UnitTestDataGenerator.TestSaveDesk.reloadList(saveDeskList, sDeskAttr);
        }
        Test.startTest();
        String jobId = System.schedule('ScheduleApexClassTest',  CRON_EXP, new EC_SD_SaveDeskScheduler());
        CronTrigger ct = [SELECT Id, CronExpression, TimesTriggered, NextFireTime FROM CronTrigger WHERE id = :jobId];
        System.assertEquals(CRON_EXP, ct.CronExpression);
        System.assertEquals(0, ct.TimesTriggered);
        Test.stopTest();
    }
}