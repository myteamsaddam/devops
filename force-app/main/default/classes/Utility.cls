global class Utility {
    
    public String futureDate;
    
    //global static list<ServiceErrorLogs__c> lstServiceErrorlogs;
    //user is authorized to Web Service Calls
    public Boolean isAuthorized{get;set;}
    
    // This method checks if a string is empty or not
    public static Boolean isEmptyString(String str) {
        if(str==null || str.trim().length()==0 || str.equalsIgnoreCase('null')) {
            return true;    
        }
        return false;
    }
    
    global class ServiceStatus {
        public String serviceName;
        public String quoteNumber;
        public String status;
        public String statusCode;
        public String statusDesc;
        public String errorCode;
        public String errorDesc;
        public String requestID;
        public String userID;
    }
    
    global class Servicelogging {
        public String BillingAccount;
        public String UniqueNumber;
        public String errorCode;
        public String errorDescription;
        public String userID;
        public String UniqueNumberType;
        public string interfaceName;
        public string methodName;
        public string request;
    }
    
    global class Globalsettings {
        public String attributeName;
        public String attributeValue;
        public String otherValue;
    }
    
    global class performancelog{
        public String interfaceName;
        public String methodName;
        public String guidPageName;
        public Decimal estimatedTime;
        public Datetime endTime;
        public Datetime startTime;
    }
    }