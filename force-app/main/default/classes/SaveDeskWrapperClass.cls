public class SaveDeskWrapperClass {
        public Boolean checked {get;set;}
        public SaveDeskQueue__c savedeskobj{get;set;}
        public SaveDeskWrapperClass (SaveDeskQueue__c savedeskobj,Boolean checked) {
            this.savedeskobj= savedeskobj;
             this.checked = checked;
        }
    }