@isTest
public class TestupdateFAbyCard
{
    public static Product_Custom__c objProd;
    public static Account objAcc;
    public static Financial_Account__c objFA;
    public static Contact objCon;
    public static Card__c objCard;
    public static Card__c objCard1;
    public static testmethod void testupdateFAbyCard() 
    {
        //Product Insert
        objProd = new Product_Custom__c();
        objProd.Name = 'Test Product';
        objProd.Product_Description__c = 'Test Desc';
        objProd.Pserno__c = '85';
        Insert objProd;
        //Account Insert
        objAcc = new Account();
        objAcc.Customer_Serno__c = '123654';
        objAcc.Name = 'Account for Test Class';
        Insert objAcc;
        //Contact Insert
        objCon = new Contact();
        objCon.FirstName='TestContact1';
        objCon.Lastname = 'Test';
        objCon.SerNo__c = '12589';
        objCon.SSN__c = '7895874';
        objCon.Institution_Id__c = 2;
        objCon.Email='xyz@abc.com';
        objCon.MobilePhone='9876543210';
        objCon.Phone='123123123'; 
        objCon.Fax='98789879';
        objCon.MobilePhone='98789879'; 
        objCon.HomePhone='123123123';
        objCon.Email='testemail@test.com';
        Insert objCon;
        //FA Insert
        objFA = new Financial_Account__c();
        objFA.Account_Number__c = '4141144';
        objFA.Institution_Id__c = 2;
        objFA.Account_Serno__c = '11166';
        objFA.Customer__c = objAcc.id;
        objFA.Customer_Serno__c = objAcc.Customer_Serno__c;
        objFA.Product__c = objProd.id;
        objFA.Product_Serno__c = objProd.Pserno__c;
        objFA.vip__c = false;
        Insert objFA;
        //Insert Card
        objCard = new Card__c();
        objCard.Card_Number_Truncated__c = '5588********9966';
        objCard.Card_Serno__c = '8855225588';
        objCard.Financial_Account__c = objFA.id;
        objCard.Financial_Account_Serno__c = objFA.Account_Serno__c;
        objCard.People__c = objCon.id;
        objCard.People_Serno__c = objCon.SerNo__c;
        objCard.Product__c = objProd.id;
        objCard.Prod_Serno__c = objProd.Pserno__c;
        objCard.PrimaryCardFlag__c=true;
        objCard.Institution_Id__c = 2;
        Insert objCard;
        
        objCard1 = new Card__c();
        objCard1.Card_Number_Truncated__c = '5588********9566';
        objCard1.Card_Serno__c = '8855225588558';
        objCard1.Financial_Account__c = objFA.id;
        objCard1.Financial_Account_Serno__c = objFA.Account_Serno__c;
        objCard1.People__c = objCon.id;
        objCard1.People_Serno__c = objCon.SerNo__c;
        objCard1.Product__c = objProd.id;
        objCard1.Prod_Serno__c = objProd.Pserno__c;
        objCard1.PrimaryCardFlag__c=true;
        objCard1.Institution_Id__c = 2;
        Insert objCard1;
        
Test.startTest();
            objCon.vip__c = true;
            update objCon;
Test.stopTest();
    }
}