/**********************************************************************
Name: EC_TaskTriggerHandler
=======================================================================
Purpose: This trigger handler class is created on Task object to handle all the Task related logics.User Story -SFD-1363

======================================================================= 
History
-----------------------------------------------------------------------         
Version     Author                   Date              Detail
1.0         Saddaam Hussain         03-June-2020       Initial Version

**********************************************************************/
public with sharing class EC_TaskTriggerHandler {
    /**********************************************************************
*   Author: Saddam Hussain
*   Date:     03-June-2020
*   User Story : SFD-1363
*   Param: None
*   Return: None    
*   Description: In this method we are trying to update task count and Assign to field in SaveDesk object.
* @Revision(s): [Date] - [Change Reference] - [Changed By] - [Description]   

**********************************************************************/
    
    public static void updateTaskCount(List<Task> taskList) { 
        try {
            Set<Id> saveDeskIdSet = new Set<Id>();
            for(Task ts : taskList) {
                if(ts.SaveDesk__c != null) {
                    saveDeskIdSet.add(ts.saveDesk__C);  
                }
            }
            
            List<SaveDeskQueue__c> saveDeskList = new List<SaveDeskQueue__c>();
            system.debug('---saveDeskIdSet---'+saveDeskIdSet);
            if(saveDeskIdSet.size() > 0) {
                for(AggregateResult taskRec : [SELECT COUNT(Id), saveDesk__C, OwnerId FROM Task WHERE  
                                               saveDesk__C IN : saveDeskIdSet 
                                               GROUP BY saveDesk__C,OwnerId]
                   ) {
                       saveDeskList.add(new SaveDeskQueue__c(Id = (Id)taskRec.get('saveDesk__C'), 
                                                             Task_Count__c =(INTEGER)taskRec.get('expr0'),
                                                             Agent_ID__c = (Id)taskRec.get('OwnerId'))
                                       );
                   }
            }
            if(saveDeskList.size() > 0){
                EC_StaticConstant.SAVEDESK_ISRECURSIVE = true;
                Update saveDeskList;
            }
        } catch(Exception exp) {
            StatusLogHelper.logSalesforceError('EC_TaskTriggerHandler', 'E005', 'updateTaskCount', exp, false); // Class,method
        }
    }
    
    /**********************************************************************
*   Author: Saddam Hussain
*   Date:     16-June-2020
*   User Story : SFD-1567
*   Param: None
*   Return: None    
*   Description: In this method we are trying to update whoId and MAH,Customer,FA,Card Serno's in Task table when memo created in Prime.
* @Revision(s): [Date] - [Change Reference] - [Changed By] - [Description]   

**********************************************************************/
   /*
    public static void updateSernos(List<Task> taskList) { 
        Set<String> mahSernoSet = new Set<String>();
        Set<String> contactSernoSet = new Set<String>();
        Set<String> finAccSernoSet = new Set<String>();
        Set<String> cardSernoSet = new Set<String>();
        Map<String, TaskWrapper> sernoMap = new Map<String, TaskWrapper>();
        for(Task taskObj : taskList) {
            if( taskObj.Entity__c !=null 
               && taskObj.Entity_Serno__c!=null 
               && taskObj.Entity__c !='' 
               && taskObj.Entity_Serno__c!=''
              ) {
                  if(taskObj.Entity__c == 'ccustomer'){
                      mahSernoSet.add(taskObj.MAH_Serno__c);
                  }else if(taskObj.Entity__c == 'people') {
                      contactSernoSet.add(taskObj.Customer_Serno__c);
                  } else if(taskObj.Entity__c == 'caccounts') {
                      finAccSernoSet.add(taskObj.Account_Serno__c);
                  } else if(taskObj.Entity__c == 'cardx') {
                      cardSernoSet.add(taskObj.Card_Serno__c);
                  }
              }
        }
        
        if(mahSernoSet.size() > 0) {
            for(AccountContactRelation accConObj : [SELECT Id,ContactId, Contact.SerNo__c, Account.Customer_Serno__c
                                                    FROM AccountContactRelation 
                                                    WHERE IsDirect = true 
                                                    AND Account.Customer_Serno__c IN : mahSernoSet
                                                    ORDER BY
                                                    CreatedDate ASC Limit 1]
               ) {
                   for(Card__c cardObj : [SELECT Id, Card_Serno__c,Financial_Account__c,
                                          People_Serno__c,Financial_Account_Serno__c
                                          FROM Card__c 
                                          WHERE People__c=:accConObj.contactId 
                                          ORDER BY ExpiryDate__C DESC Limit 1]
                      ) {
                          TaskWrapper objAccount = new TaskWrapper();
                          objAccount.contactId = accConObj.contactId;
                          objAccount.contactSerno = accConObj.contact.SerNo__c;
                          objAccount.finAccountSerno = cardObj.Financial_Account_Serno__c;
                          objAccount.cardSerno = cardObj.Card_Serno__c;
                          objAccount.financialId = cardObj.Financial_Account__c;
                          sernoMap.put(accConObj.Account.Customer_Serno__c, objAccount);
                      }
               } 
        } else if(contactSernoSet.size() > 0) {
            for(Contact conObj : [SELECT Id, Serno__c, Account.Customer_Serno__c
                                  FROM Contact 
                                  WHERE Serno__c IN:contactSernoSet]
               ) {
                   for(Card__c cardObj : [SELECT Id, Card_Serno__c,Financial_Account__c,
                                          People_Serno__c,Financial_Account_Serno__c
                                          FROM Card__c 
                                          WHERE People__c=:conObj.Id 
                                          ORDER BY ExpiryDate__C DESC Limit 1]
                      ) {
                          TaskWrapper objContact = new TaskWrapper();
                          objContact.mahSerno = conObj.Account.Customer_Serno__c;
                          objContact.finAccountSerno = cardObj.Financial_Account_Serno__c;
                          objContact.cardSerno = cardObj.Card_Serno__c;
                          objContact.financialId = cardObj.Financial_Account__c;
                          sernoMap.put(conObj.SerNo__c, objContact);
                      }
               }
        } else if(finAccSernoSet.size() > 0) {
            for(Card__c crdObj : [SELECT Id,People__c, People_Serno__c,Financial_Account_Serno__c,
                                  People__r.Account.Customer_Serno__c,Financial_Account__c
                                  FROM Card__c 
                                  WHERE Financial_Account_Serno__c IN : finAccSernoSet]
               ) {
                   TaskWrapper objFinancial = new TaskWrapper();
                   objFinancial.mahSerno = crdObj.People__r.Account.Customer_Serno__c;
                   objFinancial.contactId = crdObj.People__c;
                   objFinancial.financialId = crdObj.Financial_Account__c;
                   objFinancial.contactSerno = crdObj.People_Serno__c;
                   objFinancial.cardSerno = crdObj.Card_Serno__c;
                   sernoMap.put(crdObj.Financial_Account_Serno__c, objFinancial);
               }
        } else if(cardSernoSet.size() > 0) {
            for(Card__c crdObj : [SELECT Id,People__c, People_Serno__c,Financial_Account_Serno__c,
                                  People__r.Account.Customer_Serno__c,Financial_Account__c,
                                  Card_Serno__c
                                  FROM Card__c 
                                  WHERE Card_Serno__c IN : cardSernoSet]
               ) {
                   TaskWrapper objCard = new TaskWrapper();
                   objCard.mahSerno = crdObj.People__r.Account.Customer_Serno__c;
                   objCard.contactId = crdObj.People__c;
                   objCard.contactSerno = crdObj.People_Serno__c;
                   objCard.finAccountSerno = crdObj.Financial_Account_Serno__c;
                   objCard.financialId = crdObj.Financial_Account__c;
                   sernoMap.put(crdObj.Card_Serno__c, objCard);
               }
        }

        for(Task taskObj : taskList) {
            if( taskObj.Entity__c != null 
               && taskObj.Entity_Serno__c != null 
               && taskObj.Entity__c != '' 
               && taskObj.Entity_Serno__c != ''
              ) {
                  taskObj.Status = EC_StaticConstant.TASK_COMPLETE_STATUS;
                  taskObj.Subject = EC_StaticConstant.TASK_MEMO_SUBJECT;
                  if(taskObj.Entity__c == 'ccustomer') {
                      TaskWrapper objSernos = sernoMap.get(taskObj.Entity_Serno__c);
                      if(objSernos != null) {
                          taskObj.WhoId = objSernos.contactId;
                          taskObj.Customer_Serno__c = objSernos.contactSerno;
                          taskObj.Account_Serno__c = objSernos.finAccountSerno;
                          taskObj.Card_Serno__c = objSernos.cardSerno;
                          taskObj.WhatId = objSernos.financialId;
                      }
                  } else if(taskObj.Entity__c == 'people') {
                      TaskWrapper objSernos = sernoMap.get(taskObj.Entity_Serno__c);
                      if(objSernos != null) {
                          taskObj.MAH_Serno__c = objSernos.mahSerno;
                          taskObj.Account_Serno__c = objSernos.finAccountSerno;
                          taskObj.Card_Serno__c = objSernos.cardSerno;
                          taskObj.WhatId = objSernos.financialId;
                      }
                  } else if(taskObj.Entity__c == 'caccounts') {
                      TaskWrapper objSernos = sernoMap.get(taskObj.Entity_Serno__c);
                      if(objSernos != null) {
                          taskObj.MAH_Serno__c = objSernos.mahSerno;
                          taskObj.WhoId = objSernos.contactId;
                          taskObj.Customer_Serno__c = objSernos.contactSerno;
                          taskObj.Card_Serno__c = objSernos.cardSerno;
                          taskObj.WhatId = objSernos.financialId;
                      }
                  } else if(taskObj.Entity__c == 'caccounts') {
                      TaskWrapper objSernos = sernoMap.get(taskObj.Entity_Serno__c);
                      if(objSernos != null) {
                          taskObj.MAH_Serno__c = objSernos.mahSerno;
                          taskObj.WhoId = objSernos.contactId;
                          taskObj.Customer_Serno__c = objSernos.contactSerno;
                          taskObj.Account_Serno__c = objSernos.finAccountSerno;
                          taskObj.WhatId = objSernos.financialId; 
                      }
                  }
              }
        }
    }*/

/**********************************************************************
Name: EC_TaskTriggerHandler
=======================================================================
Purpose: This below wrapper class is used in updateSernos method.  User Story -SFD-1567

======================================================================= 
History
-----------------------------------------------------------------------         
Version     Author                   Date              Detail
1.0         Saddaam Hussain         16-June-2020       Initial Version

**********************************************************************/

 /*   
    public class TaskWrapper {
        Id contactId;
        Id financialId;
        String mahSerno;
        String contactSerno;
        String finAccountSerno;
        String cardSerno;
    }*/
}