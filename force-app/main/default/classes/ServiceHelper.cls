public without sharing class ServiceHelper {
    
    //Added for Statlus Log
    public final static string SERVICE_LOG = 'Service Log';
    
    //Integration Framework settings
    public final static string serviceStrategies = 'ServiceStrategies';
    public final static string statusLogService = 'StatusLogService';
    public final static string testSoapService = 'TestSoapService';
    public final static string testRestService = 'TestRestService';
    
    
    //Added for Callout Services
    //public final static string WeatherService = 'WeatherService';// Not required, commented by Subhajit Pal 16 NOV 2016 : This is used as part of demo of the framework , commented by Swarnava Bhattacharyya
    public final static string GetStatementHistory = 'GetStatementHistory';//added by Swarnava Bhattacharyya 20 OCT 2016 for get statement history
    public final static string GetCustomerDetails= 'GetCustomerDetails';//Added By Subhajit Pal, 24 OCT 2016 for get Customer 
    
    public final static string viewTransactionDetails= 'viewTransactionDetails';//Added By Shanthi, 17 NOV 2016
    public final static string UpdateCustomerData= 'UpdateCustomerData';//Added By Subhajit Pal, 16 NOV 2016 for updateCustomerData
    public final static string updateCustomerNameAddress = 'updateCustomerNameAddress';//added by Swarnava Bhattacharyya 21 NOV 2016 for updating customer name and address
    Public final static string updateMAHName = 'UpdateMAH'; // added by saddam
    
    public final static string GetLimitChanges= 'GetLimitChanges';//Added By Shameel, 28 NOV 2016 for GetLimitChanges
    
    public final static string ViewInsurance= 'ViewInsurance';//Added By Shameel, 16 DEC 2016 for ViewInsurance
    public final static string GetConsent= 'GetConsent';//Added By Shameel, 17 Jan 2017 for GetConsent
    public final static string UpdateConsent= 'UpdateConsent';//Added By Shameel, 17 Jan 2017 for UpdateConsent
    public final static string CLIApplication= 'CLIApplication';//Added By Shameel, 30 Jan 2017 for CLIApplication
    
    public final static string PayBackCreditBalance= 'PayBackCreditBalance';//Added By Subhajit, 02 DEC 2017 for PayBackCreditBalance
    public final static string CustomerMemoExportData = 'CustomerMemoExportData';//added by Swarnava Bhattacharyya 13 FEB 2017 for new memo
    
    public final static string GetInstallment= 'GetInstallment';//Added By Subhajit, 27 JAN 2017 for GetInstallment
    public final static string GetLoans= 'GetLoans';//Added By Shameel, 31 March 2017 for GetLoans
    
    public final static string ReadMembershipData= 'ReadMembershipData';//Added By Subhajit Pal, 12 Sept 2017 for ReadMembershipData
    
    public final static string UpdateMembershipData= 'UpdateMembershipData';//Added By Subhajit Pal, 21 Sept 2017 for UpdateMembershipData
    
    public final static string GetPartnerDetails = 'GetPartnerDetails';// Added By Saddam, 08 FEB 2018 for get partner names
    public final static string GetNonCustomerDetails ='GetNonCustomerDetails';// Added By Saddam, 12 FEB 2018 for get Push noncustomer  details
    
    public final static string CustomerRights ='CustomerRights';// Added By Shauryanaditya, 03 MAY 2018 for Customer rights service  details
    
    private final static string googleCertificate = 'dsdasdujoifdfoiodffdjdfjjdjfd';
    private final static string facebookCertificate = 'dhfhdhfshuhfiuhiudfhf'; 
    
    
    public static map<String, String> getServiceHeaders(string headerType){
        system.debug('ServiceHelper - getServiceHeaders');
        map<string, string> headers = new map<string, string>();
        
        if(headerType != null){
            ServiceHeaders__c serviceHeaders = ServiceHeaders__c.getInstance(headerType);
            if(serviceHeaders.ContentType__c != null) headers.put('Content-Type',serviceHeaders.ContentType__c);
            if(serviceHeaders.Authorization__c != null){
                headers.put('Authorization',serviceHeaders.Authorization__c);
            }
            if(serviceHeaders.ContentEncoding__c != null) headers.put('Content-Encoding',serviceHeaders.ContentEncoding__c);
            if(serviceHeaders.AcceptEncoding__c != null) headers.put('Accept-Encoding',serviceHeaders.AcceptEncoding__c);
            if(serviceHeaders.ContentType__c != null) headers.put('Content-Type',serviceHeaders.ContentType__c);
        }
        System.debug(headers);
        return headers;
    }
    
    public static string getCertificate(string name){
        system.debug('ServiceHelper - getCertificate');
        string certificate;
        
        if(name != null){
            if(name == 'google')
                certificate = googleCertificate;
            else if(name == 'facebook')
                certificate = facebookCertificate;
        }
        
        return certificate;
    }
    
}