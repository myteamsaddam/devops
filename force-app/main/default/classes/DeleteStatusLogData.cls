global class DeleteStatusLogData implements Database.Batchable<sObject>, schedulable{

    global String query;
    
    global Database.QueryLocator start(Database.BatchableContext BC)
    {
        query = 'SELECT Id,name FROM StatusLog__c Where CreatedDate < N_DAYS_AGO:90' ;  
      
        return Database.getQueryLocator(query);
        
    }  
    
    global void execute(Database.BatchableContext BC, List<StatusLog__c> scope)
    {
        try{
                
            delete scope;
        }
            
        catch(DmlException e)
            {
                System.debug('Unexpected error occurred: ' + e.getMessage());
                StatusLogHelper.logSalesforceError('DeletePrimeActionLogData', 'E005', 'scope: ' + scope, e, true);
            }
            catch(calloutException e)
            {
                System.debug('Unexpected error occurred: ' + e.getMessage());
                StatusLogHelper.logSalesforceError('DeletePrimeActionLogData', 'E001', 'scope: ' + scope, e, true);
            }
            catch(Exception e)
            {
                System.debug('Unexpected error occurred: ' + e.getMessage());
                StatusLogHelper.logSalesforceError('DeletePrimeActionLogData', 'E004', 'scope: ' + scope, e, true);
            }
       

    }
    
    global void execute(SchedulableContext sc)
    {
        
        DeleteStatusLogData d=new DeleteStatusLogData();
        Database.executeBatch(d, 200);
        
    }
    
    global void finish(Database.BatchableContext BC)
    {
       /* 
        String adminEmail=String.valueof(Common_Settings__c.getValues('DeleteStatusLogData').Admin_Email__c);
        Messaging.SingleEmailMessage message = new Messaging.SingleEmailMessage();
        message.toAddresses = new String[] { adminEmail };
        message.optOutPolicy = 'FILTER';
        message.subject = 'Subject Test Message';
        message.plainTextBody = 'This is the message body.';
        Messaging.SingleEmailMessage[] messages =   new List<Messaging.SingleEmailMessage> {message};
        Messaging.SendEmailResult[] results = Messaging.sendEmail(messages);
        
        if (results[0].success) {
            System.debug('The email was sent successfully.');
        } else {
            System.debug('The email failed to send: ' + results[0].errors[0].message);
        }
        */
    }
    
}