@IsTest(seeallData=false)
public class EncryptAccountNumber_Test{
 static testmethod void testMethod1(){
        test.startTest();  
        
        Account acc = new Account();
        acc.name='test';
        acc.Customer_Serno__c='123';
        acc.Institution_Id__c=2;
        insert acc;
        
        Contact testContact = new Contact();
            testContact.FirstName = 'Foo';
            testContact.LastName = 'Bar';
            testContact.Email ='jdoe_test_test@doe.com';
            testContact.SSN__c='Test1';
            testContact.Institution_Id__c=1;
            testContact.SerNo__c='1231';
            testContact.MobilePhone='98765432101';
            testContact.Phone='1231231231'; 
            testContact.Fax='987898791';
            testContact.HomePhone='1231231231';
            testContact.SerNo__c = 'test123';
        insert testContact;
        
        Product_Custom__c prodobj=new Product_Custom__c(
        Name='Test Product',
        Pserno__c='56789'
        );
        insert prodobj;
        
        Financial_Account__c finAccObj=new Financial_Account__c
        (
            Account_Number__c='Test5700192', 
            //Customer__c=accountObj.Id, 
            Account_Serno__c='3123123', 
            Institution_Id__c=2,
            Customer__c=acc.id,
            Customer_Serno__c=testContact.Id,
            Product__c=prodobj.Id,
            Product_Serno__c=prodobj.Pserno__c,
            FA_Status__c = true,
            Fa_ClosedDate__c = '12-12-9999'
        );
        insert finAccObj;
        update finAccObj;
        test.stopTest();
        
       }
    }