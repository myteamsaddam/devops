//Generated by wsdl2apex

public class CustomerRightsResponse {
     public class getRightToAccessDetailsResponseBodyType {
        public String RightToAccessDetails;
        private String[] RightToAccessDetails_type_info = new String[]{'RightToAccessDetails','http://schemas.entercard.com/service/CustomerRightsServiceResponsePS/1.0',null,'1','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://schemas.entercard.com/service/CustomerRightsServiceResponsePS/1.0','true','false'};
        private String[] field_order_type_info = new String[]{'RightToAccessDetails'};
    }
    public class getRightToAccessDetailsResponseType {
        public CustomerRightsType.headerType Header;
        public CustomerRightsResponse.getRightToAccessDetailsResponseBodyType GetRightToAccessDetailsResponseBody;
        private String[] Header_type_info = new String[]{'Header','http://schemas.entercard.com/service/CustomerRightsServiceResponsePS/1.0',null,'1','1','false'};
        private String[] GetRightToAccessDetailsResponseBody_type_info = new String[]{'GetRightToAccessDetailsResponseBody','http://schemas.entercard.com/service/CustomerRightsServiceResponsePS/1.0',null,'1','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://schemas.entercard.com/service/CustomerRightsServiceResponsePS/1.0','true','false'};
        private String[] field_order_type_info = new String[]{'Header','GetRightToAccessDetailsResponseBody'};
    }
    public class CustomerRightsServiceException_element {
        public String Msg;
        public String Detail;
        private String[] Msg_type_info = new String[]{'Msg','http://schemas.entercard.com/service/CustomerRightsServiceResponsePS/1.0',null,'1','1','false'};
        private String[] Detail_type_info = new String[]{'Detail','http://schemas.entercard.com/service/CustomerRightsServiceResponsePS/1.0',null,'1','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://schemas.entercard.com/service/CustomerRightsServiceResponsePS/1.0','true','false'};
        private String[] field_order_type_info = new String[]{'Msg','Detail'};
    }
}