@isTest
global class MockResponseClass implements WebServiceMock {
   global string statusTest;
   public MockResponseClass(){}
   public MockResponseClass(string statusTest)
   {
    this.statusTest=statusTest;    
   }
   global void doInvoke(
           Object stub,
           Object request,
           Map<String, Object> response,
           String endpoint,
           String soapAction,
           String requestName,
           String responseNS,
           String responseName,
           String responseType) {
         
         system.debug('***requestName****'+requestName);
         system.debug('***request****'+request);
               
        if(requestName=='GetConsentDataRequest')
        {
            SOAConsentResponse.GetConsentDataResponse_element responseElement = new SOAConsentResponse.GetConsentDataResponse_element();
            responseElement.GetConsentDataResponseRecord = new List<SOAConsentResponse.GetConsentDataResponseRecordType>();
            SOAConsentResponse.GetConsentDataResponseRecordType recordtype =new SOAConsentResponse.GetConsentDataResponseRecordType();
            recordtype.Institution_ID='2';
            recordtype.ConsentEntityID='123123';
            recordtype.ConsentEntityID_Type='Account';
            recordtype.Consent_Code='Accept Nemkonto';
            recordtype.Value='Y';
            recordtype.DateTimeStamp=system.now();
            recordtype.UpdatedBy='test updatedBy';
            recordtype.Source='Salesforce';
            responseElement.GetConsentDataResponseRecord.add(recordtype);
            SOAConsentResponse.GetConsentDataResponseRecordType recordtype1 =new SOAConsentResponse.GetConsentDataResponseRecordType();
            recordtype1.Institution_ID='2';
            recordtype1.ConsentEntityID='123123';
            recordtype1.ConsentEntityID_Type='Account';
            recordtype1.Consent_Code='Accept Nemkonto';
            recordtype1.Value='N';
            recordtype1.DateTimeStamp=system.now();
            recordtype1.UpdatedBy='test updatedBy';
            recordtype1.Source='Salesforce';
            responseElement.GetConsentDataResponseRecord.add(recordtype1);
            response.put('response_x', responseElement);            
        }
        else if(requestName=='UpdateCustomerDataRequest')
        {
            SOAUpdateCustomerResponse.UpdateCustomerDataResponse_element  respElement =new SOAUpdateCustomerResponse.UpdateCustomerDataResponse_element();
            respElement.UpdateCustomerDataResponseRecord =new SOAUpdateCustomerResponse.UpdateCustomerDataResponseRecordType();
            respElement.Header = new SOAUpdateCustomerTypelib.headerType();
            
            respElement.Header.MsgId='MsgId';
            respElement.Header.CorrelationId='CorrelationId';
            respElement.Header.RequestorId='RequestorId';
            respElement.Header.SystemId='SystemId';
            respElement.Header.InstitutionId='2';
            
            SOAUpdateCustomerResponse.UpdateCustomerDataResponseRecordType respType =new SOAUpdateCustomerResponse.UpdateCustomerDataResponseRecordType();
            respType.Code='0';
            respType.Description='Success';
            respElement.UpdateCustomerDataResponseRecord =respType;
            response.put('response_x', respElement);    
        }
        else if(requestName=='PayBackCreditRequest')
        {
            SOAPayBackCreditBalanceResponse.PayBackCreditResponse_element  respEle = new SOAPayBackCreditBalanceResponse.PayBackCreditResponse_element();
            
            respEle.PayBackCreditResponseRecord=new SOAPayBackCreditBalanceResponse.PayBackCreditResponseRecordType();
            respEle.PayBackCreditResponseRecord.Result=new SOAPayBackCreditBalanceResponse.Result();
            respEle.PayBackCreditResponseRecord.Header = new SOAPayBackCreditBalanceTypelib.headerType();
            respEle.PayBackCreditResponseRecord.Header.MsgId='MsgId';
            respEle.PayBackCreditResponseRecord.Header.CorrelationId='CorrelationId';
            respEle.PayBackCreditResponseRecord.Header.RequestorId='RequestorId';
            respEle.PayBackCreditResponseRecord.Header.SystemId='SystemId';
            respEle.PayBackCreditResponseRecord.Header.InstitutionId='2';
            respEle.PayBackCreditResponseRecord.Result.Code ='0';
            respEle.PayBackCreditResponseRecord.Result.Description='Success';
            
            respEle.PayBackCreditResponseRecord.Result.ErrorDetails = new SOAPayBackCreditBalanceResponse.ArrayOfErrorResult();
            respEle.PayBackCreditResponseRecord.Result.ErrorDetails.ErrorResult =new List<SOAPayBackCreditBalanceResponse.ErrorResult>();
            
            SOAPayBackCreditBalanceResponse.ErrorResult errorRes = new SOAPayBackCreditBalanceResponse.ErrorResult();
            errorRes.Code=0;
            errorRes.Description='Sucess';
            respEle.PayBackCreditResponseRecord.Result.ErrorDetails.ErrorResult.add(errorRes);
            response.put('response_x', respEle);    
        }
        else if(requestName=='SubmitCLIApplicationRequest')
        {
            SOACLI.submitCLIApplicationResponseType respEl = new SOACLI.submitCLIApplicationResponseType();
            respEl.SubmitCLIApplicationResponseBody = new SOACLI.submitCLIApplicationResponseBodyType();
            respEl.SubmitCLIApplicationResponseBody.ApplicationStatus=statusTest;
            response.put('response_x', respEl);  
        }
        else if(requestName=='GetLoanAccountsInfoRequest')
        {
            SOALoanResponse.GetLoanAccountsInfoResponse_element respEle = new SOALoanResponse.GetLoanAccountsInfoResponse_element();
            respEle.GetLoanAccountsInfoResponseBody = new SOALoanResponse.GetLoanAccountsInfoResponseBodyType();
            respEle.GetLoanAccountsInfoResponseBody.GetLoanAccountsInfoResponseRecord =new List<SOALoanResponse.GetLoanAccountsInfoResponseRecordType>();
            //respEle.GetLoanAccountsInfoResponseBody.GetLoanAccountsInfoResponseRecord.LoanAccountDetails=new SOALoanResponse.LoanAccountDetailsType ();
            list<SOALoanResponse.GetLoanAccountsInfoResponseRecordType> respType =new  List<SOALoanResponse.GetLoanAccountsInfoResponseRecordType>();
            respType[0].LoanAccountDetails=new List<SOALoanResponse.LoanAccountDetailsType>();
            list<SOALoanResponse.LoanAccountDetailsType> loanAcclist =new List<SOALoanResponse.LoanAccountDetailsType>();
            SOALoanResponse.LoanAccountDetailsType loanAcc =new SOALoanResponse.LoanAccountDetailsType();
            
            loanAcc.CustomerAccountNumber='123123123';
            loanAcc.CustomerAccountSerno='123';
            loanAcc.CustomerAccountStatus='C AccountStatus';
            loanAcc.LoanAccountNumber='987987987';
            loanAcc.LoanAccountSerno='987';
            loanAcc.LoanProductName='Silver card';
            loanAcc.LoanAccountStatus='L AccountStatus';
            loanAcc.InitialLoanAmount=1000;
            loanAcc.OutStandingBalance=1000;
            loanAcc.NominalInterestRate=1000;
            loanAcc.RemainingPeriod=2;
            loanAcc.LoanSDORef='DoRef';
            loanAcc.PaymentPlanDetails =new List<SOALoanResponse.PaymentPlanDetailsType>();
            SOALoanResponse.PaymentPlanDetailsType paymentDetl = new SOALoanResponse.PaymentPlanDetailsType();
            paymentDetl.PaymentNumber='54565478';
            paymentDetl.PaymentDate='2017-04-04';
            paymentDetl.TransactionType='CRDT';
            paymentDetl.TransactionDescription='Description';
            paymentDetl.Amount=1000;
            paymentDetl.OutstandingBalance=100;
            paymentDetl.PaymentStatus='PaymentStatus';
            loanAcc.PaymentPlanDetails.add(paymentDetl);
            loanAcclist.add(loanAcc);
            respType[0].LoanAccountDetails=loanAcclist;
            respEle.GetLoanAccountsInfoResponseBody.GetLoanAccountsInfoResponseRecord.addAll(respType);
            response.put('response_x', respEle);  
        }
        else if(requestName=='CustomerMemoExportDataRequest')
        {
            SOACustomerMemoExportDataCssservice.CustomerMemoExportDataResponse_element respX =new SOACustomerMemoExportDataCssservice.CustomerMemoExportDataResponse_element();
            respX.CustomerMemoExportDataResponseRecord =new SOACustomerMemoExportDataCssservice.CustomerMemoExportDataResponseRecordType();
            respX.CustomerMemoExportDataResponseRecord.Result = new SOACustomerMemoExportDataCssservice.Result();
            respX.CustomerMemoExportDataResponseRecord.Result.code ='0';
            respX.CustomerMemoExportDataResponseRecord.Result.Description='success';
            respX.CustomerMemoExportDataResponseRecord.Result.ErrorDetails = new SOACustomerMemoExportDataCssservice.ArrayOfErrorResult();
            response.put('response_x', respX);  
        }
        else if(requestName=='UpdateCustomerNameAddressRequest')
        {
            SOAPCSSServiceResponse.UpdateCustomerNameAddressResponse_element respon =new SOAPCSSServiceResponse.UpdateCustomerNameAddressResponse_element();
            respon.UpdateCustomerNameAddressResponseRecord =new SOAPCSSServiceResponse.UpdateCustomerNameAddressResponseRecordType();
            respon.UpdateCustomerNameAddressResponseRecord.Result=new SOAPCSSServiceResponse.Result();
            respon.UpdateCustomerNameAddressResponseRecord.Result.Address =new SOAPCSSServiceResponse.AddressSerno();
            respon.UpdateCustomerNameAddressResponseRecord.Result.Code='0';
            respon.UpdateCustomerNameAddressResponseRecord.Result.Description='success';
            respon.UpdateCustomerNameAddressResponseRecord.Result.ErrorDetails=new SOAPCSSServiceResponse.ArrayOfErrorResult();
            response.put('response_x', respon);     
        }
    }
    
}