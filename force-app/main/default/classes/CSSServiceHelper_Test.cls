@isTest 
private class CSSServiceHelper_Test {
    static testMethod void Cover_getCustomerDetails() {
       ServiceSettings__c serveset = new ServiceSettings__c();
       serveset.Name='GetCustomerDetails';
       serveset.Certificate__c='';
       serveset.CertificateName__c='';
       serveset.CertificatePassword__c='';
       serveset.Compressed__c=false;
       serveset.EndPoint__c='https://services-test.entercard.com/gateway/services/CSSService?wsdl';
       serveset.EndPointParameters__c=false;
       serveset.HeaderName__c='SOAPHTTP';
       serveset.Input_Class__c='GetCustomerInput';
       serveset.LogAtFuture__c=false;
       serveset.LogRequest__c=true;
       serveset.LogResponse__c=true;
       serveset.LogStatus__c=true;
       serveset.LogWithCallout__c=false;
       serveset.Operation__c='';
       serveset.OutputClass__c='GetCustomerOutput';
       serveset.ProcessingClass__c='GetCustomerCallout';
       serveset.Strategy__c='SOAPService';
       serveset.Timeout__c=12.54;
       
       insert serveset;
        
        //GlobalSettings__c
        List<GlobalSettings__c>GlobalSettingsList=new List<GlobalSettings__c>();
        GlobalSettings__c objGlobal = new GlobalSettings__c();
        objGlobal.Name = 'ServiceStrategies'; 
        objGlobal.Value__c = 'SOAPService';
        GlobalSettingsList.add(objGlobal);
        
        insert GlobalSettingsList;
        
        //ServiceHeaders__c
        ServiceHeaders__c objServiceHeaders = new ServiceHeaders__c();
        objServiceHeaders.name = 'SOAPHTTP';
        insert objServiceHeaders;
        
        //ServiceSettings__c
        ServiceSettings__c objServiceSettings = new ServiceSettings__c();
        objServiceSettings.Name = 'UpdateCustomerData';
        objServiceSettings.EndPoint__c = 'https://services-test.entercard.com/gateway/services/CSSService?wsdl';
        objServiceSettings.HeaderName__c = 'SOAPHTTP';
        objServiceSettings.OutputClass__c = 'UpdateCustomerDataOutput';
        objServiceSettings.ProcessingClass__c = 'UpdateCustomerDatacallout'; 
        objServiceSettings.Strategy__c = 'SOAPService';
        objServiceSettings.Input_Class__c = 'UpdateCustomerDataInput'; 
        
        insert objServiceSettings;
        
        ServiceSettings__c objServiceSettings1 = new ServiceSettings__c();
        objServiceSettings1.Name = 'ViewInsurance';
        objServiceSettings1.EndPoint__c = 'https://services-test.entercard.com/gateway/services/CSSService?wsdl';
        objServiceSettings1.HeaderName__c = 'SOAPHTTP';
        objServiceSettings1.OutputClass__c = 'ViewInsuranceServiceOutput';
        objServiceSettings1.ProcessingClass__c = 'ViewInsuranceServicecallout'; 
        objServiceSettings1.Strategy__c = 'SOAPService';
        objServiceSettings1.Input_Class__c = 'ViewInsuranceServiceInput'; 
        
        insert objServiceSettings1;
        
        ServiceSettings__c objServiceSettings2 = new ServiceSettings__c();
        objServiceSettings2.Name = 'GetConsent';
        objServiceSettings2.EndPoint__c = 'https://services-test.entercard.com/gateway/services/CSSService?wsdl';
        objServiceSettings2.HeaderName__c = 'SOAPHTTP';
        objServiceSettings2.OutputClass__c = 'GetConsentServiceOutput';
        objServiceSettings2.ProcessingClass__c = 'GetConsentServicecallout'; 
        objServiceSettings2.Strategy__c = 'SOAPService';
        objServiceSettings2.Input_Class__c = 'GetConsentServiceInput'; 
        
        insert objServiceSettings2;
        
        ServiceSettings__c objServiceSettings3 = new ServiceSettings__c();
        objServiceSettings3.Name = 'UpdateConsent';
        objServiceSettings3.EndPoint__c = 'https://services-test.entercard.com/gateway/services/CSSService?wsdl';
        objServiceSettings3.HeaderName__c = 'SOAPHTTP';
        objServiceSettings3.OutputClass__c = 'UpdateConsentServiceOutput';
        objServiceSettings3.ProcessingClass__c = 'UpdateConsentServicecallout'; 
        objServiceSettings3.Strategy__c = 'SOAPService';
        objServiceSettings3.Input_Class__c = 'UpdateConsentServiceInput'; 
        
        insert objServiceSettings3;
        
        ServiceSettings__c objServiceSettings4 = new ServiceSettings__c();
        objServiceSettings4.Name = 'viewTransactionDetails';
        objServiceSettings4.EndPoint__c = 'https://services-test.entercard.com/gateway/services/CSSService?wsdl';
        objServiceSettings4.HeaderName__c = 'SOAPHTTP';
        objServiceSettings4.OutputClass__c = 'ViewTransactionsHistoryOutput';
        objServiceSettings4.ProcessingClass__c = 'ViewTransCallout'; 
        objServiceSettings4.Strategy__c = 'SOAPService';
        objServiceSettings4.Input_Class__c = 'ViewTransactionsHistoryInput';
        insert objServiceSettings4;
        
        ServiceSettings__c objServiceSettings5 = new ServiceSettings__c();
        objServiceSettings5.Name = 'PayBackCreditBalance';
        objServiceSettings5.EndPoint__c = 'https://services-test.entercard.com/gateway/services/CSSService?wsdl';
        objServiceSettings5.HeaderName__c = 'SOAPHTTP';
        objServiceSettings5.OutputClass__c = 'PayBackCreditBalanceOutput';
        objServiceSettings5.ProcessingClass__c = 'PayBackCreditBalancecallout'; 
        objServiceSettings5.Strategy__c = 'SOAPService';
        objServiceSettings5.Input_Class__c = 'PayBackCreditBalanceInput';
        insert objServiceSettings5;
        
        ServiceSettings__c objServiceSettings6 = new ServiceSettings__c();
        objServiceSettings6.Name = 'UpdateAddress';
        objServiceSettings6.EndPoint__c = 'https://services-test.entercard.com/gateway/services/CSSService?wsdl';
        objServiceSettings6.HeaderName__c = 'SOAPHTTP';
        objServiceSettings6.OutputClass__c = '';
        objServiceSettings6.ProcessingClass__c = 'UpdateCustomerAddressCallout'; 
        objServiceSettings6.Strategy__c = 'SOAPService';
        objServiceSettings6.Input_Class__c = '';
        insert objServiceSettings6;
        
        ServiceSettings__c objServiceSettings7 = new ServiceSettings__c();
        objServiceSettings7.Name = 'UpdateCustomerData';
        objServiceSettings7.EndPoint__c = 'https://services-test.entercard.com/gateway/services/CSSService?wsdl';
        objServiceSettings7.HeaderName__c = 'SOAPHTTP';
        objServiceSettings7.OutputClass__c = 'UpdateCustomerDataOutput';
        objServiceSettings7.ProcessingClass__c = 'UpdateCustomerDatacallout'; 
        objServiceSettings7.Strategy__c = 'SOAPService';
        objServiceSettings7.Input_Class__c = 'UpdateCustomerDataInput';
        insert objServiceSettings7;
        
        ServiceSettings__c objServiceSettings8 = new ServiceSettings__c();
        objServiceSettings8.Name = 'updateCustomerNameAddress';
        objServiceSettings8.EndPoint__c = 'https://services-test.entercard.com/gateway/services/CSSService?wsdl';
        objServiceSettings8.HeaderName__c = 'SOAPHTTP';
        objServiceSettings8.OutputClass__c = 'UpdateCustomerAddressOutput';
        objServiceSettings8.ProcessingClass__c = 'UpdateCustomerAddressCallout'; 
        objServiceSettings8.Strategy__c = 'SOAPService';
        objServiceSettings8.Input_Class__c = 'UpdateCustomerAddressInput';
        insert objServiceSettings8;
        
        ServiceSettings__c objServiceSettings9 = new ServiceSettings__c();
        objServiceSettings9.Name = 'GetLimitChanges';
        objServiceSettings9.EndPoint__c = 'https://services-test.entercard.com/gateway/services/CSSService?wsdl';
        objServiceSettings9.HeaderName__c = 'SOAPHTTP';
        objServiceSettings9.OutputClass__c = 'GetLimitChangesServiceOutput';
        objServiceSettings9.ProcessingClass__c = 'GetLimitChangesServiceCallout'; 
        objServiceSettings9.Strategy__c = 'SOAPService';
        objServiceSettings9.Input_Class__c = 'GetLimitChangesServiceInput';
        insert objServiceSettings9;
        
        
        ServiceSettings__c objServiceSettings10 = new ServiceSettings__c();
        objServiceSettings10.Name = 'CustomerMemoExportData';
        objServiceSettings10.EndPoint__c = 'https://services-test.entercard.com/gateway/services/CSSService?wsdl';
        objServiceSettings10.HeaderName__c = 'SOAPHTTP';
        objServiceSettings10.OutputClass__c = 'CustomerMemoExportDataInput';
        objServiceSettings10.ProcessingClass__c = 'CustomerMemoExportDataCallout'; 
        objServiceSettings10.Strategy__c = 'SOAPService';
        objServiceSettings10.Input_Class__c = 'CustomerMemoExportDataOutput';
        insert objServiceSettings10;
        
        ServiceSettings__c objServiceSettings11 = new ServiceSettings__c(); // Add by saddam 01 March 2018
        objServiceSettings11.Name = 'GetPartnerDetails';
        objServiceSettings11.EndPoint__c = 'https://midtlb1d2pub.entercard.com/CSSService/ProxyServices/CSSServicePS2?wsdl';
        objServiceSettings11.HeaderName__c = 'SOAPHTTP';
        objServiceSettings11.OutputClass__c = 'GetPartnerOutputs';
        objServiceSettings11.ProcessingClass__c = 'GetPartnerNameServiceCallout'; 
        objServiceSettings11.Strategy__c = 'SOAPService';
        objServiceSettings11.Input_Class__c = 'GetPartnerInputs';
        insert objServiceSettings11;
        
        ServiceSettings__c objServiceSettings12 = new ServiceSettings__c(); // Add by saddam 01 March 2018
        objServiceSettings12.Name = 'GetNonCustomerDetails';
        objServiceSettings12.EndPoint__c = 'https://midtlb1d2pub.entercard.com/CSSService/ProxyServices/CSSServicePS2?wsdl';
        objServiceSettings12.HeaderName__c = 'SOAPHTTP';
        objServiceSettings12.OutputClass__c = 'GetNonCustomerOutputs';
        objServiceSettings12.ProcessingClass__c = 'GetNonCustomerServiceCallout'; 
        objServiceSettings12.Strategy__c = 'SOAPService';
        objServiceSettings12.Input_Class__c = 'GetNonCustomerInputs';
        insert objServiceSettings12;
        
        ServiceSettings__c objServiceSettings13 = new ServiceSettings__c(); // Add by saddam 01 March 2018
        objServiceSettings13.Name = 'UpdateMAH';
        objServiceSettings13.EndPoint__c = 'https://midtlb4d2pub.entercard.com/gateway/services/CSSService?wsdl';
        objServiceSettings13.HeaderName__c = 'SOAPHTTP';
        objServiceSettings13.OutputClass__c = 'UpdateMAHResponse';
        objServiceSettings13.ProcessingClass__c = 'UpdateMAHCallout'; 
        objServiceSettings13.Strategy__c = 'SOAPService';
        objServiceSettings13.Input_Class__c = 'UpdateMAHRequest';
        insert objServiceSettings13;
        
        ServiceSettings__c objServiceSettings14 = new ServiceSettings__c(); // Add by saddam 01 March 2018
        objServiceSettings14.Name = 'GetStatementHistory';
        objServiceSettings14.EndPoint__c = 'https://midtlb4d2pub.entercard.com/gateway/services/CSSService?wsdl';
        objServiceSettings14.HeaderName__c = 'SOAPHTTP';
        objServiceSettings14.OutputClass__c = 'GetStatementHistoryOutput';
        objServiceSettings14.ProcessingClass__c = 'GetStatementCallout'; 
        objServiceSettings14.Strategy__c = 'SOAPService';
        objServiceSettings14.Input_Class__c = 'GetStatementHistoryInput';
        insert objServiceSettings14;
        
        //Contact Insert
        Contact objCon = new Contact();
        objCon.Lastname = 'Test';
        objCon.Firstname = 'F Test';
        objCon.SerNo__c = '12589';
        objCon.SSN__c = '7895874';
        objCon.Institution_Id__c = 2;
        objCon.Phone='123123123'; 
        objCon.Fax='98789879';
        objCon.MobilePhone='98789879'; 
        objCon.HomePhone='123123123';
        objCon.Email='testemail1@test.com';
        Insert objCon;
        
        //Address Insert
        Address__c objAddr = new Address__c();
        objAddr.Address_Id__c = '123456';
        objAddr.City__c = 'Oslo';
        objAddr.Country_Code__c = '752';
        objAddr.ZIP__c = '1471147';
        objAddr.Street_Address__c = 'Test Street Address';
        objAddr.Country__c = 'NO';
        objAddr.Location__c = 'Work';
        objAddr.People__c = objCon.id;
        
        Insert objAddr;
        
        ViewInsuranceServiceInput viewInsuranceServiceInput=new ViewInsuranceServiceInput();
        viewInsuranceServiceInput.AccountNumber='shhgbj';
        viewInsuranceServiceInput.InstitutionId=5;
        
        GetConsentServiceInput getConsentServiceInput=new GetConsentServiceInput();
        getConsentServiceInput.ConsentEntityId='hgghjkeak';
        getConsentServiceInput.ConsentEntityIdType='jankj';
        getConsentServiceInput.FromDate=date.today()-10;
        getConsentServiceInput.InstitutionId='gh';
        getConsentServiceInput.ToDate=date.today();
        
        UpdateConsentServiceInput updateConsentServiceInput=new UpdateConsentServiceInput();
        updateConsentServiceInput.InstitutionId='dghv';
        
        GetLimitChangesServiceInput objLimitChange = new GetLimitChangesServiceInput();
        objLimitChange.AccountNo = '8855221144';
        objLimitChange.From_x= Date.today();
        objLimitChange.InstitutionId = 2;
        
        GetPartnerInputs PartnerInputs = new GetPartnerInputs();
        PartnerInputs.InstitutionId = '2';
        
        GetNonCustomerInputs nonCustomerInputs = new GetNonCustomerInputs();
        nonCustomerInputs.InstitutionId = '1';
        nonCustomerInputs.FirstName = 'Testing';
    	nonCustomerInputs.LastName = 'Test';
    	nonCustomerInputs.Address1 = 'SE';
    	nonCustomerInputs.City = 'Do';
    	nonCustomerInputs.Zip = '502';
    	nonCustomerInputs.PartnerId =6;
    	nonCustomerInputs.IdentifierType = 'SSN';
        nonCustomerInputs.IdentifierValue = '245567788';
    	nonCustomerInputs.UnionName = 'Test';
    	nonCustomerInputs.DepartmentNumber = 'EC';
		
        CSSServiceHelper.getTransactionHistory(2, '12345');
        CSSServiceHelper.getCustomerDetails(2,'0000000061','Y','Y');
        CSSServiceHelper.ViewInsurance(viewInsuranceServiceInput);
        CSSServiceHelper.GetConsent(getConsentServiceInput);
        CSSServiceHelper.UpdateConsent(updateConsentServiceInput);
        CSSServiceHelper.viewTransactionDetails(2  , 'Reference', 3,2,Date.today(),Date.today() + 2,'5645454',TRUE,'5646464231358','TransactionType');
        CSSServiceHelper.updatePayBackCreditBalance(2, 'Action','Reference','55588774455','ServiceIndicator','ServiceType','LogAction',String.ValueOf(Date.today()),'456514');
        CSSServiceHelper.UpdateCustomerAddress(2, objAddr, '12589');
        CSSServiceHelper.UpdateCustomerAddress(2, objCon, '12589');
        //UpdateCustomerData(UpdateCustomerDataInput UpdateCustomerDataUserInput);
        CSSServiceHelper.GetLimitChanges(objLimitChange);
        CSSServiceHelper.getPartnerDetails(PartnerInputs); // Add by saddam 01 March 2018
        CSSServiceHelper.getNonCustomer(nonCustomerInputs); // Add by saddam 01 March 2018
        
        CSSServiceHelper.updateCustomerMemo('Add','Account','123123','type','message','2');
        CSSServiceHelper.updateCustomerMemo('Add','Account',null,'type','message','2');
        CSSServiceHelper.UpdateMAHName(2,objCon,'Test');
    }
}