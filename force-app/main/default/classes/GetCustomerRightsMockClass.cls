global class GetCustomerRightsMockClass implements WebServiceMock{
    global void doInvoke(
        Object stub,
        Object request,
        Map<String, Object> response,
        String endpoint,
        String soapAction,
        String requestName,
        String responseNS,
        String responseName,
        String responseType) 
    {
        CustomerRightsResponse.getRightToAccessDetailsResponseType customerrights = new CustomerRightsResponse.getRightToAccessDetailsResponseType();
        CustomerRightsResponse.getRightToAccessDetailsResponseBodyType bodyType = new CustomerRightsResponse.getRightToAccessDetailsResponseBodyType();
        bodyType.RightToAccessDetails = 'Tesing';
        response.put('response_x', bodyType);
    }
}