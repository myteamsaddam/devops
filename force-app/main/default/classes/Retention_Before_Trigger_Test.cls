@IsTest(seeallData=false)
public class Retention_Before_Trigger_Test{
    static testmethod void testMethod1(){
        test.startTest();  
        
        Account acc = new Account();
        acc.name='test';
        acc.Customer_Serno__c='123';
        acc.Institution_Id__c=2;
        insert acc;
        
        Contact testContact = new Contact();
            testContact.FirstName = 'Foo';
            testContact.LastName = 'Bar';
            testContact.Email ='jdoe_test_test@doe.com';
            testContact.SSN__c='Test1';
            testContact.Institution_Id__c=1;
            testContact.SerNo__c='1231';
            testContact.MobilePhone='98765432101';
            testContact.Phone='1231231231'; 
            testContact.Fax='987898791';
            testContact.HomePhone='1231231231';
            testContact.SerNo__c = 'test123';
        insert testContact;
        
        Product_Custom__c prodobj=new Product_Custom__c(
        Name='Test Product',
        Pserno__c='56789'
        );
        insert prodobj;
        
        Financial_Account__c finAccObj=new Financial_Account__c
        (
            Account_Number__c='Test5700192', 
            //Customer__c=accountObj.Id, 
            Account_Serno__c='3123123', 
            Institution_Id__c=2,
            Customer__c=acc.id,
            Customer_Serno__c=testContact.Id,
            Product__c=prodobj.Id,
            Product_Serno__c=prodobj.Pserno__c
        );
        insert finAccObj;
        
        List<Card__c> cardList = new List<Card__c>();
        Card__c cardObj =new Card__c(
        Card_Serno__c='2231236', 
        Card_Number_Truncated__c='5456654565665', 
        People__c=testContact.Id, 
        Institution_Id__c=2, 
        Financial_Account__c=finAccObj.Id,
        Financial_Account_Serno__c=finAccObj.Id, 
        People_Serno__c=testContact.Id,
        Prod_Serno__c=prodobj.Pserno__c, 
        Product__c=prodobj.Id,
        PrimaryCardFlag__c=true
        );
        
        cardList.add(cardObj);
        
        Card__c cardObj1 =new Card__c(
        Card_Serno__c='22312361', 
        Card_Number_Truncated__c='545665456', 
        People__c=testContact.Id, 
        Institution_Id__c=3, 
        Financial_Account__c=finAccObj.Id,
        Financial_Account_Serno__c=finAccObj.Id, 
        People_Serno__c=testContact.Id,
        Prod_Serno__c=prodobj.Pserno__c, 
        Product__c=prodobj.Id,
        PrimaryCardFlag__c=true
        );
        
        cardList.add(cardObj1);
        
        insert cardList;
        
        
        Retention_Response__c rr = new Retention_Response__c ();
        rr.Account_Closed__c = 'Y';
        rr.Account_Number1__c = '123';
        rr.Agent_Id__c = 'test123';
        rr.Person__c = testContact.id;
        //rr.Financial_Account__c = finAccObj.id;
        rr.First_Name1__c = 'test';
        rr.Last_Name1__c = 'test2';
        rr.Reason_for_Account_Closure__c = 'Account closure initiated by bank';
        
        insert rr;
         
         User u = new User(
         ProfileId = [SELECT Id FROM Profile WHERE Name = 'System Administrator'].Id,
         LastName = 'last',
         Email = 'puser000@amamama.com',
         Username = 'puser000@amamama.com' + System.currentTimeMillis(),
         CompanyName = 'TEST',
         Title = 'title',
         Alias = 'alias',
         TimeZoneSidKey = 'America/Los_Angeles',
         EmailEncodingKey = 'UTF-8',
         LanguageLocaleKey = 'en_US',
         LocaleSidKey = 'en_US',
         //UserRoleId = r.Id,
         FederationIdentifier= '123'
        );
        insert u;
        test.stopTest();
    }  
    
    static testmethod void testMethod2(){
        test.startTest();  
        
        Account acc = new Account();
        acc.name='test';
        acc.Customer_Serno__c='123';
        acc.Institution_Id__c=2;
        insert acc;
        
        Contact testContact = new Contact();
            testContact.FirstName = 'Foo';
            testContact.LastName = 'Bar';
            testContact.Email ='jdoe_test_test@doe.com';
            testContact.SSN__c='Test1';
            testContact.Institution_Id__c=1;
            testContact.SerNo__c='1231';
            testContact.MobilePhone='98765432101';
            testContact.Phone='1231231231'; 
            testContact.Fax='987898791';
            testContact.HomePhone='1231231231';
            testContact.SerNo__c = 'test123';
        insert testContact;
        
        Product_Custom__c prodobj=new Product_Custom__c(
        Name='Test Product',
        Pserno__c='56789'
        );
        insert prodobj;
        
        Financial_Account__c finAccObj=new Financial_Account__c
        (
            Account_Number__c='Test5700192', 
            //Customer__c=accountObj.Id, 
            Account_Serno__c='3123123', 
            Institution_Id__c=2,
            Customer__c=acc.id,
            Customer_Serno__c=testContact.Id,
            Product__c=prodobj.Id,
            Product_Serno__c=prodobj.Pserno__c
        );
        insert finAccObj;
        
        Card__c cardObj =new Card__c(
        Card_Serno__c='2231236', 
        Card_Number_Truncated__c='5456654565665', 
        People__c=testContact.Id, 
        Institution_Id__c=2, 
        Financial_Account__c=finAccObj.Id,
        Financial_Account_Serno__c=finAccObj.Id, 
        People_Serno__c=testContact.Id,
        Prod_Serno__c=prodobj.Pserno__c, 
        Product__c=prodobj.Id,
        PrimaryCardFlag__c=true
        );
        insert cardObj;
        
        Retention_Response__c rr = new Retention_Response__c ();
        rr.Account_Closed__c = 'Y';
        rr.Account_Number1__c = '123';
        rr.Agent_Id__c = 'test123';
        rr.Person__c = testContact.id;
        //rr.Financial_Account__c = finAccObj.id;
        rr.First_Name1__c = 'test';
        rr.Last_Name1__c = 'test2';
        rr.Reason_for_Account_Closure__c = 'Account closure initiated by bank';
        
        insert rr;
         
         User u = new User(
         ProfileId = [SELECT Id FROM Profile WHERE Name = 'System Administrator'].Id,
         LastName = 'last',
         Email = 'puser000@amamama.com',
         Username = 'puser000@amamama.com' + System.currentTimeMillis(),
         CompanyName = 'TEST',
         Title = 'title',
         Alias = 'alias',
         TimeZoneSidKey = 'America/Los_Angeles',
         EmailEncodingKey = 'UTF-8',
         LanguageLocaleKey = 'en_US',
         LocaleSidKey = 'en_US',
         //UserRoleId = r.Id,
         FederationIdentifier= '123'
        );
        insert u;
        test.stopTest();
    }  
    
    
    static testmethod void testMethod4(){
        test.startTest();  
        
        Account acc = new Account();
        acc.name='test';
        acc.Customer_Serno__c='123';
        acc.Institution_Id__c=2;
        insert acc;
        
        Contact testContact = new Contact();
            testContact.FirstName = 'Foo';
            testContact.LastName = 'Bar';
            testContact.Email ='jdoe_test_test@doe.com';
            testContact.SSN__c='Test1';
            testContact.Institution_Id__c=1;
            testContact.SerNo__c='1231';
            testContact.MobilePhone='98765432101';
            testContact.Phone='1231231231'; 
            testContact.Fax='987898791';
            testContact.HomePhone='1231231231';
            testContact.SerNo__c = 'test123';
        insert testContact;
        
        Product_Custom__c prodobj=new Product_Custom__c(
        Name='Test Product',
        Pserno__c='56789'
        );
        insert prodobj;
        
        Financial_Account__c finAccObj=new Financial_Account__c
        (
            Account_Number__c='Test5700192', 
            //Customer__c=accountObj.Id, 
            Account_Serno__c='3123123', 
            Institution_Id__c=2,
            Customer__c=acc.id,
            Customer_Serno__c=testContact.Id,
            Product__c=prodobj.Id,
            Product_Serno__c=prodobj.Pserno__c
        );
        insert finAccObj;
        
        Card__c cardObj =new Card__c(
        Card_Serno__c='2231236', 
        Card_Number_Truncated__c='5456654565665', 
        People__c=testContact.Id, 
        Institution_Id__c=2, 
        Financial_Account__c=finAccObj.Id,
        Financial_Account_Serno__c=finAccObj.Id, 
        People_Serno__c=testContact.Id,
        Prod_Serno__c=prodobj.Pserno__c, 
        Product__c=prodobj.Id,
        PrimaryCardFlag__c=true
        );
        insert cardObj;
        
        Retention_Response__c rr = new Retention_Response__c ();
        rr.Account_Closed__c = 'Y';
        rr.Account_Number1__c = '123';
        rr.Agent_Id__c = 'test123';
        rr.Person__c = null;
        rr.Financial_Account__c = finAccObj.id;
        rr.First_Name1__c = 'test';
        rr.Last_Name1__c = 'test2';
        rr.Reason_for_Account_Closure__c = 'Account closure initiated by bank';
        
        insert rr;
         
         User u = new User(
         ProfileId = [SELECT Id FROM Profile WHERE Name = 'System Administrator'].Id,
         LastName = 'last',
         Email = 'puser000@amamama.com',
         Username = 'puser000@amamama.com' + System.currentTimeMillis(),
         CompanyName = 'TEST',
         Title = 'title',
         Alias = 'alias',
         TimeZoneSidKey = 'America/Los_Angeles',
         EmailEncodingKey = 'UTF-8',
         LanguageLocaleKey = 'en_US',
         LocaleSidKey = 'en_US',
         //UserRoleId = r.Id,
         FederationIdentifier= '123'
        );
        insert u;
        test.stopTest();
    } 
}