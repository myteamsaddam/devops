public class OpenCaseController {
    @InvocableMethod
    Public static void updateOpencase(List<ID> Ids ){
        system.debug('-----recordId------'+Ids);
        List<Contact> conlist = [Select id,name,Open_Cases__c,(Select id,status from cases where status Not IN ('Closed','Cancelled')) from Contact where Id =: Ids];
        System.debug('-----contact----'+conlist);
        for(Contact ct : conlist){
            if(ct.cases.size() > 0){
                ct.Open_Cases__c = String.valueOf(ct.cases.size());
                //openCase = String.valueOf(ct.cases.size());
            }else{
                ct.Open_Cases__c = String.valueOf(0);
                //openCase = String.valueOf(0);   
            }
        }
        update conlist;
        system.debug('--Open_Cases--'+conlist);
    }
}