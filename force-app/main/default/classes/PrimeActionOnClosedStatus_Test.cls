@isTest
public class PrimeActionOnClosedStatus_Test {
     public static testmethod void testMethod1() {
         
         Profile p = [SELECT Id FROM Profile WHERE Name='Standard User']; 
        User u = new User(Alias = 'standt', Email='standardusersss@testorg.com', 
                          EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
                          LocaleSidKey='en_US', ProfileId = p.Id, 
                          TimeZoneSidKey='America/Los_Angeles', UserName='standuser@testorg1.com');
        insert u;
        User u1 = new User(Alias = 'standt1', Email='standarduser1@testorg.com', 
                           EmailEncodingKey='UTF-8', LastName='Testing1', LanguageLocaleKey='en_US', 
                           LocaleSidKey='en_US', ProfileId = p.Id, 
                           TimeZoneSidKey='America/Los_Angeles', UserName='standuser1@testorg2.com');
        insert u1;    
         
        // Create test account
        Account accountObj=new Account(
            Name='Test Account', 
            Institution_Id__c=2, 
            Customer_Serno__c='1123123'
        );
        insert accountObj;
       // Create test Contact  
         Contact contactObj =new Contact(
            LastName='Test Contact', 
            FirstName='First Test Contact',
            SerNo__c='2123123', 
            Institution_Id__c=2, 
            SSN__c='0000000061',
            AccountId=accountObj.Id,
            Phone='123123123', 
            Fax='98789879', 
            MobilePhone='98789879', 
            HomePhone='123123123', 
            Email='testemail@test.com'
        );
        insert contactObj;
         Case caseObj=new Case(
            ContactId=contactObj.Id, 
            AccountId=accountObj.Id, 
            //Card__c=cardObj.Id,
            //Financial_Account__c=finAccObj.Id, 
            Category__c='Account closure', 
            Origin='Web',
            status='New',
            OwnerId=u.id
        );
         insert caseObj;
         Product_Custom__c prodobj=new Product_Custom__c(
            Name='Test Product',
            Pserno__c='56789'
        );
        insert prodobj;
        
        Financial_Account__c finAccObj=new Financial_Account__c(
            Account_Number__c='12w23eeeeedd', 
            Customer__c=accountObj.Id, 
            Account_Serno__c='3123123', 
            Institution_Id__c=2,
            Customer_Serno__c=contactObj.SerNo__c, 
            Product__c=prodobj.Id,
            Product_Serno__c=prodobj.Pserno__c
        );
        insert finAccObj;
        
        Card__c cardObj=new Card__c(
            People__c=contactObj.Id,
            PrimaryCardFlag__c=true,
            Financial_Account__c=finAccObj.Id, 
            Card_Serno__c='524858519', 
            Card_Number_Truncated__c='7136785481583561', 
            Financial_Account_Serno__c=finAccObj.Id, 
            People_Serno__c=contactObj.Id,
            Prod_Serno__c=prodobj.Pserno__c, 
            Product__c=prodobj.Id,
            Institution_Id__c=2
        );
        insert cardObj;
         PrimeActionOnClosedStatus.getStatus(caseObj.id);
         PrimeActionOnClosedStatus.updateStatus(caseObj.id);
         PrimeActionOnClosedStatus.StatusUpdate(caseObj.id);
         PrimeActionOnClosedStatus.StatusUpdates(caseObj.id);
         PrimeActionOnClosedStatus.getPeople(cardObj.id);
         PrimeActionOnClosedStatus.getFAdetails(finAccObj.id);
         
     }
}