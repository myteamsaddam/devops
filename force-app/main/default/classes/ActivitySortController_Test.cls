@isTest
public class ActivitySortController_Test {
	static testMethod void testmthd1(){  
        Account accountObj=new Account(
            Name='Test Account', 
            Institution_Id__c=2, 
            Customer_Serno__c='1123123'
        );
        insert accountObj;
        
        Blob hash = Crypto.generateDigest('MD5', Blob.valueOf('0000000061'));
        string encryptSSN=EncodingUtil.convertToHex(hash);
        Contact contactObj =new Contact(
            LastName='Test Contact', 
            FirstName='First Test Contact',
            SerNo__c='2123123', 
            Institution_Id__c=2, 
            SSN__c='0000000061',
            Encrypted_SSN__c=encryptSSN,
            AccountId=accountObj.Id,
            Phone='123123123', 
            Fax='98789879', 
            MobilePhone='98789879', 
            HomePhone='123123123', 
            Email='testemail@test.com'
        );
        insert contactObj;
        
        Product_Custom__c prodobj=new Product_Custom__c(
            Name='Test Product',
            Pserno__c='56789'
        );
        insert prodobj;
        
        Financial_Account__c finAccObj=new Financial_Account__c(
            Account_Number__c='12w23eeeeedd', 
            Customer__c=accountObj.Id, 
            Account_Serno__c='3123123', 
            Institution_Id__c=2,
            Customer_Serno__c=contactObj.SerNo__c, 
            Product__c=prodobj.Id,
            Product_Serno__c=prodobj.Pserno__c
        );
        insert finAccObj;
        
        Card__c cardObj=new Card__c(
            People__c=contactObj.Id,
            PrimaryCardFlag__c=true,
            Financial_Account__c=finAccObj.Id, 
            Card_Serno__c='524858519', 
            Card_Number_Truncated__c='7136785481583561', 
            Financial_Account_Serno__c='3123123', 
            People_Serno__c='09899',
            Prod_Serno__c=prodobj.Pserno__c, 
            Product__c=prodobj.Id,
            Institution_Id__c=2
        );
        insert cardObj;
        Case cse = new Case();
        cse.ContactId = contactObj.Id;
        cse.Origin = 'Phone';
        insert cse;
        
        task taskobj =new task(whoId=contactObj.Id, 
                               subject='subject', 
                               status='Completed',Category__c='Test', type='Complaint',
                              Comments__c='', description='',CreatedById = UserInfo.getUserId(), CreatedDate = system.today(),OwnerId = UserInfo.getUserId());
        insert taskobj;

        ActivitySortController.fetchActivities(contactObj.Id);
        ActivitySortController.deleteTask(taskobj.Id);
        ActivitySortController.getCustId(cse.id);
    }
}