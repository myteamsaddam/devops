public class UpdateCustomerDataOutput{
    public HeaderType HT;
    public UpdateCustomerDataResponse UCDR;
    public class HeaderType {
        public String MsgId;
        public String CorrelationId;
        public String RequestorId;
        public String SystemId;
        public String InstitutionId;
    }
    public class UpdateCustomerDataResponse {
        public String Code;
        public String Description;
       
    }
    //To Cover Test Class 100%
    public void test()
    {
        
    }
}