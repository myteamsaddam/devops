public class GetCustomerOutput{
    public Long Serno;
    public String SSN;
    public String FirstName;
    public String LastName;
    public GetCustomerOutput.CustomerAddress [] Addresses;
    public String Mobile;
    public String Email;
    public Decimal TOTB;
    public Decimal TotalLimit;
    public Decimal TotalPossibleLimitIncrease;
    public GetCustomerOutput.CustomerFinancialAccount [] FinancialAccounts;
    
    public class CustomerAddress {
        public String AddressType;
        public String Address1;
        public String Zip;
        public String City;
        public String Country;
        public String Telephone;
    } 
    public class CustomerFinancialAccount {
        public Long Serno;
        public String AccountNo;
        //===== Status ======
        public String StGen;
        public String StGenDesc;
        public String StFin;
        public String StFinDesc;
        public String StAuth;
        public String StAuthDesc;
        //====================
        public String AccountRating;
        public Date CreateDate;
        public String RiskIndicator;
        public GetCustomerOutput.ProductType[] FinancialAccountProducts;
        public String StmtNextDate;//Date
        public Decimal OTB;
        public String CharityFlag;//SFD-651 Added by Ravi Jaroli
        public String KYC;//SFD-938 Added by Mani
        public String LPI;//SFD-249 Added by Mani
        public String NyckelKundFlag; //SFD-793 Added by Mani
        public String ProductChanged;
        public Decimal Balance;
        public Decimal Limit_x;
        public Decimal PossibleLimitIncrease;
        public String Einvoice;
        //==== Direct Debit ======
        public String DirectDebitEnabled;
        public Decimal DirectDebitAmount;
        public Decimal DirectDebitPercentage;
        public Integer DirectDebitCombination;
        public String DirectDebitClearingNo;
        public String DirectDebitAccountNo;
        //======
        public String[] Membership;
        public String PPI;
        public String PickupCode;
        public Decimal MTP;
        public GetCustomerOutput.CardType[] Cards;
    }
    public class ProductType {
        public Long PSerno;
        public Integer InstitutionId;
        public String Name;
        public String Description;
    }
    
    Public class ServiceType{
        public String AnnualFeeDate;
        public String Status;
        //=====Annual fee====== 2018 July 06: Hari Added a field for SFD - 218
    }

    public class CardType {
        public Long CardSerno;
        public String CardNo;
        public Integer WrongPinAttempts;
        //==== Card Status ==========
        public String StGen;
        public String StGenDesc;
        public String StFin;
        public String StFinDesc;
        public String StAuth;
        public String StAuthDesc;
        public String StEmb;
        public String StEmbDesc;
        //=============================
        public String LastName;
        public String FirstName;
        public GetCustomerOutput.ProductType[] CardProducts;
        Public GetCustomerOutput.ServiceType[] cardService;
        public String EmbName;
        public Date EmbDate;
        public Boolean PrimaryCard;
        public Date ExpiryDate;
        public String SecureCodeInfo;
        public String[] RememberInsurenceList;
        public String[] FreeInsurenceList;
    }
     //To Cover Test Class 100%
    public void test()
    {
        
    }
}