/*
----------------------------------------------------------------------------------------
Author    Date         Description
Shanthi   10/11/2016   Output class to implement SOA service using Integration framework.
----------------------------------------------------------------------------------------
*/
public class ViewTransactionsHistoryOutput {
        public String Number_x;
        public Long TotalNoOfTx;
        //public CSSServiceException objCSSServiceException;
        public ViewTransactionsHistoryOutput.viewTransactionType[] Transaction_x;
        
        public class viewTransactionType  {
            public Long TxSerno;
            public Integer Sequence;
            public Date PostDate;
            public String ReasonCode;
            public Decimal TxAmount;
            public Date TxDate;
            public String TxCurrency;
            public Decimal BillingAmount;
            public String BillingCurrency;
            public String Text;
            public String OrigMsgType;
            public String TruncatedCardNo;
            public Boolean CanBeSplitted;
    } 
    //To Cover Test Class 100%
    public void test()
    {
        
    }
}