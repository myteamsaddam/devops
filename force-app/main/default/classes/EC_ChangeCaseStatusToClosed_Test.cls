@isTest(SeeAllData=true)
public class EC_ChangeCaseStatusToClosed_Test {
    static testmethod void testschedule(){
        
        try{
            System.runAs(EC_UnitTestDataGenerator.adminUser) {
                // Case creation
                //Date startDate = date.newInstance(2019, 12, 15);
                Id queueId = [SELECT Id FROM Group WHERE Type = 'Queue' AND Name='SE Customer Service'].Id;
                Datetime yesterday = Datetime.now().addDays(-30);
                Case caseObj = EC_UnitTestDataGenerator.TestCase.buildInsert(new Map<String, Object>{
                    'OwnerId'=>queueId, 
                        'CreatedDate'=>yesterday
                        });
                 //Test.setCreatedDate(caseObj.Id, yesterday);
                //setting custom settings
                
                EC_SalesforceTeam__c emailAdd = EC_UnitTestDataGenerator.TestSalesforceTeam.buildInsert(new Map<String, Object>{
                });
                
                /* EC_SalesforceTeam__c emailAdd = new EC_SalesforceTeam__c();
emailAdd.Name ='Test User';
emailAdd.EC_Email__c = 'test.test@test.com';
insert emailAdd;*/
            }
            
        }catch(Exception exp){
            StatusLogHelper.logSalesforceError('Case Status Chnage to Closed', 'defaultError', 'Error in changing the status', exp, false);
        }
        
        Test.StartTest();
        EC_ChangeCaseStatusToClosed schJob = new EC_ChangeCaseStatusToClosed();      
        String schTime = '0 0 0 * * ? *'; //this will schedule for 12:00:00 AM every day
        system.schedule('ChangeCaseStatusToClosed_Test:', schTime, schJob);
        
        Test.stopTest();
    }
}