@isTest
public class EnterCardProductController_test {
    
    
@isTest static void test1()
{
    //Account
        Account account=new Account();
        account.Name='test Account';
        account.Customer_Serno__c='qwertyuiop';
        account.Institution_Id__c=5;
        insert account;
        
    //Contact
        Contact contact=new Contact();
        contact.FirstName='TestContact1';
        contact.LastName='TestContact';
        contact.AccountId=account.Id;
        contact.SSN__c='vgvavvvjq5464';
        contact.SerNo__c='qwertyuiop';
        contact.Institution_Id__c=5;
        contact.Email='xyz@abc.com';
        contact.MobilePhone='9876543210';
        contact.Phone='123123123'; 
        contact.Fax='98789879';
        contact.MobilePhone='98789879'; 
        contact.HomePhone='123123123';
        contact.Email='testemail@test.com';
        insert contact;
        System.debug(contact);
    
     //EnterCard Product
        Product_Custom__c pd=new Product_Custom__c();
        pd.Product_Description__c='dhhfygygyadyh';
        pd.Pserno__c='12312';
        insert pd;
       
        //Financial Account
        Financial_Account__c fc=new Financial_Account__c();
        fc.Customer__c=account.id;
        fc.Customer_Serno__c='gatgatsgtgyg';
        fc.Account_Serno__c='ssddddddd';
        fc.Account_Number__c='12w23eeeeedd';
        fc.Institution_Id__c=1;
        fc.Product_Serno__c='hsgdgdgfadf';
        fc.Product__c=pd.id;
        insert fc;

        //Attachment 
       Attachment at=new Attachment();
       at.Name='Product_123123';
       at.ParentId=pd.id;
       Blob bodyBlob=Blob.valueOf('Unit Test Attachment Body');
       at.Body = bodyBlob;
       insert at;
       //Attachment 
       Attachment at1=new Attachment();
       at1.Name='Product_1123123';
       at1.ParentId=pd.id;
       Blob bodyBlob1=Blob.valueOf('Unit Test Attachment Body1');
       at1.Body = bodyBlob1;
       insert at1;
        
     //Card
        Card__c c=new Card__c();
    
        c.Institution_Id__c=1;
        c.Card_Number_Truncated__c='12345bhsccccc';
        c.Prod_Serno__c='agaggdtgadtdt';
        c.Product__c=pd.id;
        c.People__c=contact.id;
        c.Financial_Account_Serno__c='gfdgtftdftdaaa';
        c.Financial_Account__c=fc.id;
        c.People_Serno__c='dahyhydyydey';
        c.Card_Serno__c='fsdtfadtrfdrdf';
       insert c;
    
       List<Attachment> st=new List<Attachment>();
        st.add(at);
    
       Map<string,List<Attachment>> productToAttachmentMap = new  Map<string,List<Attachment>>();
       productToAttachmentMap.put(string.valueOf(pd.id), st);
    
        Apexpages.StandardController sc= new ApexPages.standardController(contact);
           
        test.startTest();
        PageReference pageRef = Page.EnterCardProducts;
        pageRef.getParameters().put('Id', contact.Id);
        Test.setCurrentPage(pageRef);
        EnterCardProductController ec=new EnterCardProductController(sc);
        ec.getEnterCardProducts(contact);
        EnterCardProductController ebc=new EnterCardProductController();
    
         EnterCardProductController.ProductWrapper pw =new EnterCardProductController.ProductWrapper('gdgtdtdf','ffsfdrdfr','dtsfdrfdr');
         EnterCardProductController.ProductWrapper pwd=new EnterCardProductController.ProductWrapper();
         ebc.enterCardproductConInstance=new EnterCardProductController();
         EnterCardProductController contrl =ebc.enterCardproductConInstance;
       test.stopTest();
    
    
}
    
    

    
    
    
   

}