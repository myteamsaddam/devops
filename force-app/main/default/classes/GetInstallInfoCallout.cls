public class GetInstallInfoCallout extends SoapIO
{
    SOAPCSSServiceRequest.getInstallInfoRequestRecordType InstallInfoRecordTypeInstance;
    SOAPschemasEntercardComTypelib10.headerType InstallInfoheadertypeinstance;
    GetInstallInfoInput objgetInstallInfo;
    public  void convertInputToRequest()
    {
        objgetInstallInfo = (GetInstallInfoInput)serviceInput;
        
        InstallInfoRecordTypeInstance=new SOAPCSSServiceRequest.getInstallInfoRequestRecordType();
        InstallInfoRecordTypeInstance.Serno = objgetInstallInfo.Serno; 

        InstallInfoheadertypeinstance=new SOAPschemasEntercardComTypelib10.headerType();
        InstallInfoheadertypeinstance.MsgId = objgetInstallInfo.MsgId;
        InstallInfoheadertypeinstance.CorrelationId = Common_Settings__c.getValues('CorrelationId').Common_Value__c;
        InstallInfoheadertypeinstance.RequestorId = Common_Settings__c.getValues('RequestorId').Common_Value__c;
        InstallInfoheadertypeinstance.SystemId = Common_Settings__c.getValues('SystemId').Common_Value__c;
        InstallInfoheadertypeinstance.MsgId = objgetInstallInfo.MsgId;
        system.debug('@#@##~~~'+objgetInstallInfo);
    }
    
    public  override object invokeWebserviceCallout(SoapRequest soapRequest)
    {
        SOAPCSSServiceInvoke.x_xsoap_CSSServiceESB_CSSServicePT invokeInstance= new SOAPCSSServiceInvoke.x_xsoap_CSSServiceESB_CSSServicePT();
        invokeInstance.timeout_x=120000;
        
        ServiceSettings__c serviceObj = ServiceSettings__c.getValues('GetInstallment');
        if(serviceObj!=null && serviceObj.EndPoint__c!='' && serviceObj.EndPoint__c!=null)
        invokeInstance.endpoint_x = serviceObj.EndPoint__c;
        
        String username = serviceObj.Username__c; //'css_soa';//'evry_access';
        String password = serviceObj.Password__c;//'P1%gruzA';//'9oKuwQioQ4';
        Blob headerValue = Blob.valueOf(username + ':' + password);
        String authorizationHeader = 'BASIC ' +EncodingUtil.base64Encode(headerValue);
        
        invokeInstance.inputHttpHeaders_x = new Map<String, String>();
        invokeInstance.inputHttpHeaders_x.put('Authorization', authorizationHeader);
        
        SOAPdocsOasisOpenOrgWss200401Oasis20.UsernameToken creds=new SOAPdocsOasisOpenOrgWss200401Oasis20.UsernameToken();
        creds.Username=serviceObj.Username__c;//'css_soa';//'evry_access';
        creds.Password=serviceObj.Password__c;//'P1%gruzA';//'9oKuwQioQ4';
        SOAPdocsOasisOpenOrgWss200401Oasis20.Security_element security_ele=new SOAPdocsOasisOpenOrgWss200401Oasis20.Security_element();
        security_ele.UsernameToken=creds;
        invokeInstance.SecurityPart=security_ele;
        SOAPCSSServiceResponse.GetInstallInfoResponse_element InstallInfoResponse_elementinstance;
        InstallInfoResponse_elementinstance =  invokeInstance.getInstallmentInfo(InstallInfoheadertypeinstance,InstallInfoRecordTypeInstance);
        system.debug('InstallInfoResponse_elementinstance -->'+InstallInfoResponse_elementinstance);
        return InstallInfoResponse_elementinstance;
    }
    
    public override object convertResponseToOutput(object response, map<string, string> responseHeader, ServiceStatus serviceStatus)
    {
        if(response==null)
        {
            return null;
        }
        
        SOAPCSSServiceResponse.GetInstallInfoResponse_element InstallInfoResponse_elementinstance = (SOAPCSSServiceResponse.GetInstallInfoResponse_element)response;
        
        SOAPschemasEntercardComTypelib10.headerType InstallInfo_headerType_Instance = InstallInfoResponse_elementinstance.header;
        SOAPCSSServiceResponse.AccountsType InstallInfo_AccountsType_Instance = InstallInfoResponse_elementinstance.Accounts;
        
                
        GetInstallInfoOutput InstallInfo_InstallInfoOutputInstance = new GetInstallInfoOutput();
        if(InstallInfo_headerType_Instance!=null){
            if(InstallInfo_headerType_Instance.MsgId != null && InstallInfo_headerType_Instance.MsgId != '')
            {
                InstallInfo_InstallInfoOutputInstance.MsgId = InstallInfo_headerType_Instance.MsgId;
            }
            if(InstallInfo_headerType_Instance.CorrelationId != null && InstallInfo_headerType_Instance.CorrelationId != '')
            {
                InstallInfo_InstallInfoOutputInstance.CorrelationId = InstallInfo_headerType_Instance.CorrelationId;
            }
            if(InstallInfo_headerType_Instance.RequestorId != null && InstallInfo_headerType_Instance.RequestorId != '')
            {
                InstallInfo_InstallInfoOutputInstance.RequestorId = InstallInfo_headerType_Instance.RequestorId;
            }
            if(InstallInfo_headerType_Instance.SystemId != null && InstallInfo_headerType_Instance.SystemId != '')
            {
                InstallInfo_InstallInfoOutputInstance.SystemId = InstallInfo_headerType_Instance.SystemId;
            }
            if(InstallInfo_headerType_Instance.InstitutionId != null && InstallInfo_headerType_Instance.InstitutionId != '')
            {
                InstallInfo_InstallInfoOutputInstance.InstitutionId = InstallInfo_headerType_Instance.InstitutionId;
            }
        }
        
        if(InstallInfo_AccountsType_Instance!=null && InstallInfo_AccountsType_Instance.Account != null)
        {
            List<GetInstallInfoOutput.AccountType>  acctypeList=  new list<GetInstallInfoOutput.AccountType>();
            for(SOAPCSSServiceResponse.AccountType  eachAcctype: InstallInfo_AccountsType_Instance.Account)
            {
                GetInstallInfoOutput.AccountType temp= new GetInstallInfoOutput.AccountType();
                temp.Numberx=eachAcctype.Numberx;
                temp.Name=eachAcctype.Name;
                temp.Status=eachAcctype.Status;  
                if(eachAcctype.Installments!=null && eachAcctype.Installments.Installment!=null)
                {
                    list<GetInstallInfoOutput.InstallmentType> InstallmentTypeList=new List<GetInstallInfoOutput.InstallmentType>();
                    for(SOAPCSSServiceResponse.InstallmentType eachinstype: eachAcctype.Installments.Installment)
                    {
                        GetInstallInfoOutput.InstallmentType temp1=new GetInstallInfoOutput.InstallmentType(); 
                        temp1.Numberx1=eachinstype.Numberx1;
                        temp1.Name=eachinstype.Name;
                        temp1.Status=eachinstype.Status;
                        temp1.OutStandingBal=eachinstype.OutStandingBal;
                        if(eachinstype.Transactions!=null && eachinstype.Transactions.Transaction_x!=null){
                            List<GetInstallInfoOutput.TransactionInstallResType> TransactionInstallResTypeList=new List<GetInstallInfoOutput.TransactionInstallResType>();
                            for(SOAPCSSServiceResponse.TransactionInstallResType eachtrnx : eachinstype.Transactions.Transaction_x)
                            {
                                GetInstallInfoOutput.TransactionInstallResType temp2=new GetInstallInfoOutput.TransactionInstallResType();
                                temp2.Period=eachtrnx.Period;
                                temp2.Trxndate=eachtrnx.Trxndate;
                                temp2.Trxnmsgtype=eachtrnx.Trxnmsgtype;
                                temp2.Feereasoncode=eachtrnx.Feereasoncode;
                                temp2.Trxndescription=eachtrnx.Trxndescription;
                                temp2.Trxnamount=eachtrnx.Trxnamount;
                                temp2.Status=eachtrnx.Status;
                                temp2.Outstandingbal=eachtrnx.Outstandingbal;
                                temp2.Stgeneral=eachtrnx.Stgeneral;
                                temp2.Merchname=eachtrnx.Merchname;
                                temp2.Origcardnumber=eachtrnx.Origcardnumber;
                                temp2.Purchasedate=eachtrnx.Purchasedate;
                                TransactionInstallResTypeList.add(temp2);        
                            }
                            temp1.TransactioninstallTypes=TransactionInstallResTypeList;
                        }
                        InstallmentTypeList.add(temp1);
                    }
                    temp.InstallmentTypes=InstallmentTypeList;
                }
                acctypeList.add(temp);      
            }
            InstallInfo_InstallInfoOutputInstance.AccountTypes=acctypeList;
        }
        
        system.debug('===<(:)>==='+InstallInfo_InstallInfoOutputInstance);
        return InstallInfo_InstallInfoOutputInstance;
    }
}