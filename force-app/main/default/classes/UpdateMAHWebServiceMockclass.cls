global class UpdateMAHWebServiceMockclass implements WebServiceMock {
    global void doInvoke(
        Object stub,
        Object request,
        Map<String, Object> response,
        String endpoint,
        String soapAction,
        String requestName,
        String responseNS,
        String responseName,
        String responseType) 
        {
            SOAupdateMAHResponse.UpdateCustomerNameAddressResponse_element respon = new SOAupdateMAHResponse.UpdateCustomerNameAddressResponse_element();
            respon.UpdateCustomerNameAddressResponseRecord = new SOAupdateMAHResponse.UpdateCustomerNameAddressResponseRecordType();
            respon.UpdateCustomerNameAddressResponseRecord.result = new SOAupdateMAHResponse.Result();
            respon.UpdateCustomerNameAddressResponseRecord.result.Address = new SOAupdateMAHResponse.AddressSerno();
            respon.UpdateCustomerNameAddressResponseRecord.result.code = '0';
            respon.UpdateCustomerNameAddressResponseRecord.result.Description='success';
            respon.UpdateCustomerNameAddressResponseRecord.result.ErrorDetails = new SOAupdateMAHResponse.ArrayOfErrorResult();
            response.put('response_x',respon);
        }

}