public class ViewInsuranceServiceCallout extends SoapIO{
   
    SOAViewInsuranceRequest.ViewInsurancesRequestBodyType  viewInsurancesRequestBodyInstance;
    ViewInsuranceServiceInput input;
    
    public  void convertInputToRequest(){
    
        input = (ViewInsuranceServiceInput)serviceInput;
        viewInsurancesRequestBodyInstance = new SOAViewInsuranceRequest.ViewInsurancesRequestBodyType();
        viewInsurancesRequestBodyInstance.AccountNumber=input.AccountNumber;
        
    }
    
    public  override object invokeWebserviceCallout(SoapRequest soapRequest){
        
        SOAViewInsuranceTypelib.headerType HeadInstance=new SOAViewInsuranceTypelib.headerType();
        HeadInstance.MsgId='SFDC consuming view insurance SOA service. Time Stamp=>'+system.now();
        HeadInstance.CorrelationId=Common_Settings__c.getValues('CorrelationId').Common_Value__c;
        HeadInstance.RequestorId=Common_Settings__c.getValues('RequestorId').Common_Value__c;
        HeadInstance.SystemId=Common_Settings__c.getValues('SystemId').Common_Value__c;
        HeadInstance.InstitutionId=string.valueof(input.InstitutionId);//'2';
        SOAViewInsuranceInvoke.x_xsoap_CSSServiceESB_CSSServicePT  invokeInstance= new SOAViewInsuranceInvoke.x_xsoap_CSSServiceESB_CSSServicePT();
        invokeInstance.timeout_x=30000;
         // Added 12/13/2016
        ServiceSettings__c serviceObj = ServiceSettings__c.getValues('ViewInsurance');
        if(serviceObj!=null && serviceObj.EndPoint__c!='' && serviceObj.EndPoint__c!=null)
        invokeInstance.endpoint_x = serviceObj.EndPoint__c;
        // Added 12/13/2016
        //String username = 'evry_access';
        //String password = '9oKuwQioQ4';
        
        String username = serviceObj.Username__c; //'css_soa';
        String password = serviceObj.Password__c;//'P1%gruzA';
        
        Blob headerValue = Blob.valueOf(username + ':' + password);
        String authorizationHeader = 'BASIC ' +EncodingUtil.base64Encode(headerValue);
        
        invokeInstance.inputHttpHeaders_x = new Map<String, String>();
        invokeInstance.inputHttpHeaders_x.put('Authorization', authorizationHeader);
        
        SOAViewInsuranceSecurityHeader.UsernameToken creds=new SOAViewInsuranceSecurityHeader.UsernameToken();
        //creds.Username='evry_access';
        //creds.Password='9oKuwQioQ4';
        
        creds.Username=serviceObj.Username__c; //'css_soa';
        creds.Password=serviceObj.Password__c;//'P1%gruzA';
        
        SOAViewInsuranceSecurityHeader.Security_element security_ele=new SOAViewInsuranceSecurityHeader.Security_element();
        security_ele.UsernameToken=creds;
        invokeInstance.SecurityPart=security_ele;
        SOAViewInsuranceResponse.ViewInsurancesResponse_element viewInsurancesResponse_element;
        viewInsurancesResponse_element =  invokeInstance.viewInsurances(HeadInstance,viewInsurancesRequestBodyInstance);
        system.debug('viewInsurancesResponse_element -->'+viewInsurancesResponse_element);
        return viewInsurancesResponse_element;
     }  
        
     public override object convertResponseToOutput(object response, map<string, string> responseHeader, ServiceStatus serviceStatus){
       if(response==null)
           return null;
       
       SOAViewInsuranceResponse.ViewInsurancesResponse_element viewInsurancesResponse_elementInstance = (SOAViewInsuranceResponse.ViewInsurancesResponse_element)response;
       SOAViewInsuranceResponse.ViewInsurancesResponseRecordType  viewInsurancesResponseRecordInstance =viewInsurancesResponse_elementInstance.ViewInsurancesResponseRecord;
      
       SOAViewInsuranceResponse.SubscribedInsurance SubscribedInsuranceList = viewInsurancesResponseRecordInstance.SubscribedInsuranceList;
       SOAViewInsuranceResponse.SubscribeToInsurance SubscribeToInsuranceList=viewInsurancesResponseRecordInstance.SubscribeToInsuranceList;
       
       system.debug('***SubscribedInsuranceList***'+SubscribedInsuranceList);
       system.debug('***SubscribeToInsuranceList***'+SubscribeToInsuranceList);
       
       ViewInsuranceServiceOutput viewInsuranceServiceOutput = new ViewInsuranceServiceOutput();
      
       ViewInsuranceServiceOutput.SubscribeToInsurance subscribeToInsurance = new ViewInsuranceServiceOutput.SubscribeToInsurance();
       ViewInsuranceServiceOutput.SubscribedInsurance  subscribedInsurance  = new ViewInsuranceServiceOutput.SubscribedInsurance();
       
       subscribedInsurance.ViewInsurancesResponseBodyList=new List<ViewInsuranceServiceOutput.ViewInsurancesResponseBody>();
       subscribeToInsurance.ViewInsurancesResponseBodyList=new List<ViewInsuranceServiceOutput.ViewInsurancesResponseBody>();
      
       // Add to SubscribedInsurance
       if(SubscribedInsuranceList!=null && SubscribedInsuranceList.SubscribedInsurance!=null)
       {
           for(SOAViewInsuranceResponse.ViewInsurancesResponseBody insur : SubscribedInsuranceList.SubscribedInsurance)
           {
                ViewInsuranceServiceOutput.ViewInsurancesResponseBody  viewInsurancesResponseBody =new ViewInsuranceServiceOutput.ViewInsurancesResponseBody(); 
                viewInsurancesResponseBody.InsuranceCode=insur.InsuranceCode;
                viewInsurancesResponseBody.Description=insur.Description;
                viewInsurancesResponseBody.LogAction=insur.LogAction;
                subscribedInsurance.ViewInsurancesResponseBodyList.add(viewInsurancesResponseBody);
           }
           
           viewInsuranceServiceOutput.SubscribedInsuranceList =subscribedInsurance;
           
       }
       
       // Add to SubscribeToInsurance
       if(SubscribeToInsuranceList!=null && SubscribeToInsuranceList.SubscribeToInsurance!=null)
       {
           for(SOAViewInsuranceResponse.ViewInsurancesResponseBody insur : SubscribeToInsuranceList.SubscribeToInsurance)
           {
                ViewInsuranceServiceOutput.ViewInsurancesResponseBody  viewInsurancesResponseBody =new ViewInsuranceServiceOutput.ViewInsurancesResponseBody(); 
                viewInsurancesResponseBody.InsuranceCode=insur.InsuranceCode;
                viewInsurancesResponseBody.Description=insur.Description;
                viewInsurancesResponseBody.LogAction=insur.LogAction;
                subscribeToInsurance.ViewInsurancesResponseBodyList.add(viewInsurancesResponseBody);
           }
           
           viewInsuranceServiceOutput.SubscribeToInsuranceList=subscribeToInsurance;
       }
       
      return viewInsuranceServiceOutput;
    }        
}