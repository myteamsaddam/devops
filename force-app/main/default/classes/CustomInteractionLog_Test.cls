@isTest 
private class CustomInteractionLog_Test{
    
    @testSetup
    public static void testInsertData() 
    {
        Account accountObj=new Account(
            Name='Test Account', 
            Institution_Id__c=2, 
            Customer_Serno__c='1123123'
        );
        insert accountObj;
        
        Blob hash = Crypto.generateDigest('MD5', Blob.valueOf('0000000061'));
        string encryptSSN=EncodingUtil.convertToHex(hash);
        Contact contactObj =new Contact(
            LastName='Test Contact', 
            FirstName='First Test Contact',
            SerNo__c='2123123', 
            Institution_Id__c=2, 
            SSN__c='0000000061',
            Encrypted_SSN__c=encryptSSN,
            AccountId=accountObj.Id,
            Phone='123123123', 
            Fax='98789879', 
            MobilePhone='98789879', 
            HomePhone='123123123', 
            Email='testemail@test.com'
        );
        insert contactObj;
        
        Product_Custom__c prodobj=new Product_Custom__c(
            Name='Test Product',
            Pserno__c='56789'
        );
        insert prodobj;
        
        Financial_Account__c finAccObj=new Financial_Account__c(
            Account_Number__c='12w23eeeeedd', 
            Customer__c=accountObj.Id, 
            Account_Serno__c='3123123', 
            Institution_Id__c=2,
            Customer_Serno__c=contactObj.SerNo__c, 
            Product__c=prodobj.Id,
            Product_Serno__c=prodobj.Pserno__c
        );
        insert finAccObj;
        
        Card__c cardObj=new Card__c(
            People__c=contactObj.Id,
            PrimaryCardFlag__c=true,
            Financial_Account__c=finAccObj.Id, 
            Card_Serno__c='524858519', 
            Card_Number_Truncated__c='7136785481583561', 
            Financial_Account_Serno__c=finAccObj.Id, 
            People_Serno__c=contactObj.Id,
            Prod_Serno__c=prodobj.Pserno__c, 
            Product__c=prodobj.Id,
            Institution_Id__c=2
        );
        insert cardObj;
        
        Case caseObj=new Case(
            ContactId=contactObj.Id, 
            AccountId=accountObj.Id, 
            Card__c=cardObj.Id,
            Financial_Account__c=finAccObj.Id, 
            Category__c='Account closure', 
            Origin='Web',
            status='Closed'
        );
        insert caseObj;
        
        task taskobj =new task(whoId=contactObj.Id, 
                               whatId=caseObj.Id, 
                               subject='subject', 
                               CallObject='VOICE.33590924.1490261303651.00558000000wi0AAAQ', 
                               cnx__UniqueId__c='1978', 
                               ANI__c='1978', 
                               Call_Type__c='inbound', 
                               status='Open');
        insert taskobj;
        
        RDPC_Mapping__c rdpc =new RDPC_Mapping__c(
            Name='DK_AH_Agent123',
            Country__c='DK',
            Department__c='Application Handling',
            Profile__c='DK - Operations',
            Role__c='DK AH Agent');
        insert rdpc;
        
        Common_Settings__c commansetting =new Common_Settings__c(
            Name='CTI',
            CTI_Wrap_Time__c=0
        );
        insert commansetting;
        
    }
    
    public static testmethod void testMethod1() {
        
        test.startTest();
        Contact conObj=[SELECT Id, SerNo__c, Institution_Id__c FROM Contact limit 1];
        case caseobj=[SELECT Id, caseNumber,status,Category__c,Description,Financial_Account__c FROM case limit 1];
        /*UserRole r = new UserRole(DeveloperName = 'DK_AH_Supervisor123', Name = 'My Role');
insert r;*/
        Cache.Session.put('local.CTI.caseId',caseobj.id);
        PageReference pageRef = Page.PeopleConsentData;
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('Id',conObj.id);
        ApexPages.StandardController sc = new ApexPages.standardController(conObj);
        //CustomInteractionLog cont =new CustomInteractionLog ();
        CustomInteractionLog stdCont =new CustomInteractionLog (sc);
        //stdCont.objCase=caseobj;
        stdCont.CallType='inbound';
        stdCont.CallDurationInSeconds=1;
        stdCont.updateWhoWhatId();
        stdCont.setCallAttachedData();
        //stdCont.objCase=caseobj;
        stdCont.saveaction();
        stdCont.save();
        stdCont.getfinancialAccounts();
        
        stdCont.getCardfinancialAccounts();
        //stdCont.objCase=caseobj;
        //stdCont.objCase.status='open';
        stdCont.setCallEndData();
        
        stdCont.saveAndNew();
        test.stopTest();
    }
    
    public static testmethod void testMethod2() {
        
        test.startTest();
        Contact conObj=[SELECT Id, SerNo__c, Institution_Id__c FROM Contact limit 1];
        case caseobj=[SELECT Id, caseNumber,ContactId,Category__c,Description,Financial_Account__c FROM case limit 1];
        PageReference pageRef = Page.PeopleConsentData;
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('Id',caseobj.ContactId);
        
        ApexPages.StandardController sc = new ApexPages.standardController(caseobj);
        CustomInteractionLog stdCont =new CustomInteractionLog (sc);
        stdCont.objCase=caseobj;
        stdCont.ANI='1978';
        stdCont.CallObject='VOICE.33590924.1490261303651.00558000000wi0AAAQ';
        stdCont.CallDurationInSeconds=1;
        stdCont.CallDisposition='123';
        stdCont.CallType='inbound';
        stdCont.statusMessage='statusMessage';
        stdCont.financialVal=caseobj.Financial_Account__c;
        stdCont.updateWhoWhatId();
        stdCont.setCallAttachedData();
        stdCont.objCase=caseobj;
        stdCont.setCallEndData();   
        stdCont.getfinancialAccounts();
        stdCont.getCardfinancialAccounts();
        stdCont.loadContact();
        stdCont.caseIdChosen = caseobj.id;
        
        //stdCont.ContactId = tsk.whoid;
        stdCont.updateCase();
        StdCont.SSN = '';
        StdCont.custsName='Testing';
        StdCont.custsId='Test';
        stdCont.redirectSSN();
        
        test.stopTest();
    }
    public static testmethod void testMethod3() {
        
        Account accountObj=new Account(
            Name='Test Account', 
            Institution_Id__c=2, 
            Customer_Serno__c='11231231'
        );
        insert accountObj;
        
        Blob hash = Crypto.generateDigest('MD5', Blob.valueOf('0000000066'));
        string encryptSSN=EncodingUtil.convertToHex(hash);
        Contact contactObj =new Contact(
            LastName='Test Contact3', 
            FirstName='First Test Contact3',
            SerNo__c='21231444', 
            Institution_Id__c=2, 
            SSN__c='0000000066',
            Encrypted_SSN__c=encryptSSN,
            AccountId=accountObj.Id,
            Phone='1231231231', 
            Fax='987898791', 
            MobilePhone='987898791', 
            HomePhone='1231231231', 
            Email='testemail3@test.com'
        );
        
        insert contactObj;
        
        test.startTest();
        case caseobj=[SELECT Id, caseNumber,ContactId,Category__c,Description,Financial_Account__c FROM case limit 1];
        PageReference pageRef = Page.PeopleConsentData;
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('Id',caseobj.ContactId);
        
        ApexPages.StandardController sc = new ApexPages.standardController(caseobj);
        CustomInteractionLog stdCont =new CustomInteractionLog (sc);
        
        stdCont.ANI='1978';
        stdCont.CallObject='VOICE.33590924.1490261303651.00558000000wi0AAAQ';
        stdCont.CallDurationInSeconds=1;
        stdCont.CallDisposition='123';
        stdCont.CallType='inbound';
        stdCont.statusMessage='statusMessage';
        stdCont.updateWhoWhatId();
        stdCont.setCallAttachedData();
        stdCont.objCase=caseobj;
        stdCont.setCallEndData();
        
        /*Contact con = [Select id, SSN__c,Encrypted_SSN__c from contact where id = :contactObj.Id limit 1];
system.debug('----con----'+con.SSN__c);*/
        stdCont.SSN=contactObj.SSN__c;
        system.debug('888888888'+stdCont.SSN);
        
        stdCont.redirectSSN();
        StdCont.SSN = '9009000';
        stdCont.redirectSSN();
        
        PageReference pageRef1 = Page.CTINewCaseContact;
        pageRef1.getParameters().put('recordId', caseobj.ContactId);
        Test.setCurrentPage(pageRef1);
        stdCont.redirectName();
        
        stdCont.redirectCase();
        stdCont.displayWhatId();
        stdCont.createCase();
        
        test.stopTest();
    }
}