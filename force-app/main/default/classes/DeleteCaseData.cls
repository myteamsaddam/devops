global class DeleteCaseData implements Database.Batchable<SObject>, Database.Stateful, schedulable{ 
    global Map<Id, String> errorMap {get; set;}
    global Map<Id, SObject> IdToSObjectMap {get; set;}
    
    global DeleteCaseData(){
        errorMap = new Map<Id, String>();
        IdToSObjectMap = new Map<Id, SObject>();
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC) { 
        String clsDat = Date.today().format(); //Date.today().addmonths(120).adddays(1).format();
        system.debug('----clsdat----'+clsDat);
        String query = 'Select Id, status,Category__c,Origin, CaseNumber From Case where status = \'Closed\''; 
        if(clsDat != null){
            query = query + ' AND ' + '( X18_Months__c =: clsDat OR X120_Months__c = :clsDat) ';
            system.debug('----query-1----'+query); //X18_Months__c =: clsDat OR
        }
        return Database.getQueryLocator(query);
    } 
    
    global void execute(Database.BatchableContext BC, List<SObject> scope) { 
        List<Case> CaseList = new List<Case>();
        for(SObject s : scope){
            Case cs = (Case) s;
            //cs.Status = 'New';
            CaseList.add(cs);
        }
        
        if(CaseList.size() > 0) {
            List<Database.DeleteResult> casdele = Database.delete(CaseList, false);
            Integer index = 0;
            for(Database.DeleteResult dsr : casdele){
                if(!dsr.isSuccess()){
                    String errMsg = dsr.getErrors()[0].getMessage();
                    errorMap.put(CaseList[index].Id, errMsg);
                    IdToSObjectMap.put(CaseList[index].Id, CaseList[index]);
                }
                index++;
            }
        }
    } 
    
    global void execute(SchedulableContext sc)
    {
        DeleteCaseData d=new DeleteCaseData();
        Database.executeBatch(d, 200);
    }
    
    global void finish(Database.BatchableContext BC) { 
        //Send an email to the User after your batch completes 
        if(!errorMap.isEmpty()){
            string myid = 'ecsfdc.in@capgemini.com';
            //List<User> usrs = [Select id,email from user where profile.name = 'System Administrator'];
            AsyncApexJob a = [SELECT id, ApexClassId,JobItemsProcessed, TotalJobItems,NumberOfErrors, CreatedBy.Email FROM AsyncApexJob WHERE id = :BC.getJobId()];
            String body = 'Hi,\n \n' + 'The batch job ' + 'DeleteCaseData ' + 'has finished. \n' + 'There were ' + errorMap.size() + ' errors. Please find the error list attached. \n \n Thanks,';
            
            // Creating the CSV file
            String finalstr = 'Id, CaseNumber, Error \n';
            String subject = 'Case - Apex Batch Error List';
            String attName = 'Delete Case Errors.csv';
            for(Id id  : errorMap.keySet()){
                string err = errorMap.get(id);
                Case Cse = (Case) IdToSObjectMap.get(id);
                string recordString = '"'+id+'","'+Cse.CaseNumber+'","'+err+'"\n';
                finalstr = finalstr +recordString;
            } 
            
            // Define the email
            Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage(); 
            
            // Create the email attachment    
            Messaging.EmailFileAttachment efa = new Messaging.EmailFileAttachment();
            efa.setFileName(attName);
            efa.setBody(Blob.valueOf(finalstr));
            
            // Sets the paramaters of the email
            email.setSubject( subject );
            email.setToAddresses( new String[] {myid} );
            email.setPlainTextBody( body );
            email.setFileAttachments(new Messaging.EmailFileAttachment[] {efa});
            
            // Sends the email
            Messaging.SendEmailResult [] r = Messaging.sendEmail(new Messaging.SingleEmailMessage[] {email});   
        }
    } 
}