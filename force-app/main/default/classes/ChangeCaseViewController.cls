public class ChangeCaseViewController {
    public case caserecord{get;set;}
    public boolean isUpdatedSuccessfully{get;set;}
    pagereference returl;
    public ChangeCaseViewController(ApexPages.StandardController sc){
      if (!Test.isRunningTest())        sc.addfields(new List<String>{'Case_View__c'});
        caserecord = (Case)sc.getrecord();
        returl = sc.view();
        isUpdatedSuccessfully = false;
    }
    public pagereference saveCaseView(){
    
        isUpdatedSuccessfully = false;
        update caserecord;
        isUpdatedSuccessfully = true;
        return null;
    }
}