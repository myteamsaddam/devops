public class ActivitySortController {
    
    @AuraEnabled
    public static List<Task> fetchActivities(Id objId) {
        system.debug('--objId-'+objId);
        if(objId.getSobjectType() == Schema.Case.SObjectType){
            Case cs = [Select Id, ContactId from Case where Id =: objId];
            System.debug('----csalist--'+cs);
            objId = cs.ContactId;
        }

        List<Task> tslist = new List<Task>();
        if(objId != null){
            tslist = [SELECT Id, Subject, Owner.Name, CallType, Description, createdDate, CreatedBy.Name FROM Task where (WhoId =: objId AND Status = 'Completed') ORDER BY CreatedDate DESC LIMIT 4];
        }
        return tslist; 
    }
    
    @AuraEnabled
    Public static string getCustId(Id cseId){
        Case cs = [Select Id, ContactId from Case where Id =: cseId];
        return cs.ContactId;
    }
    
    @AuraEnabled
    public static String deleteTask(Id TskId) {
        Task tsk = [SELECT Id FROM Task where Id =: TskId];
        delete tsk;
        return null;
    }
}