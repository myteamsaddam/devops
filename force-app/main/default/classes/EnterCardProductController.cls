public with sharing class EnterCardProductController
{
    public Contact contact {get;set;}
    
    public EnterCardProductController(){}
    public List<ProductWrapper> productWrapperList {get;set;}
    
    public EnterCardProductController(ApexPages.StandardController sc){
        
        productWrapperList = new List<ProductWrapper>();
        
        this.contact =(Contact)sc.getRecord();
        getEnterCardProducts(contact);
    }
    
    //to pass the current controller instance to the component
    public EnterCardProductController enterCardproductConInstance
    {
    get{
        return this;
    }
    set;
    }
    
    public void getEnterCardProducts(Contact contact)
    {
    try
       {
         Set<string> productIdSet =new Set<string>();
         Map<string,List<Attachment>> productToAttachmentMap = new  Map<string,List<Attachment>>();
         
         List<Document> documentList= [SELECT id,name from document where name like 'No Image Available%'];
         
         
         for(Card__c  cd : [SELECT Id, Name, People__c, Product__c FROM Card__c where People__c=:contact.Id])
         {
            if(cd.Product__c!=null)
            productIdSet.add(cd.Product__c); 
                            
         }
         
         for(Attachment  attch : [SELECT Id, Name, ParentId FROM Attachment where ParentId IN :productIdSet])
         {
             
             if(productToAttachmentMap.containsKey(string.valueof(attch.ParentId)))
             {
                 List<Attachment> attchList = productToAttachmentMap.get(attch.ParentId);
                 attchList.add(attch);
                 productToAttachmentMap.put(attch.ParentId,attchList);
                 
             }
             else    
             {
                List<Attachment> attchList = new List<Attachment>();
                attchList.add(attch);
                productToAttachmentMap.put(attch.ParentId,attchList);
                 
             }
             
         }
         
         system.debug('****productToAttachmentMap****'+productToAttachmentMap);
        
         for(Product_Custom__c  prod :  [SELECT Id, Name, Pserno__c, Product_Description__c FROM Product_Custom__c where Id IN :productIdSet])
         {
             List<Attachment> productAttchments= productToAttachmentMap.get(prod.Id)!=null?productToAttachmentMap.get(prod.Id):new List<Attachment>();
             
             if(productAttchments.size()>0)
             {
                 for(Attachment  prodAttch :  productAttchments)
                 {
                     if(prodAttch.Name.startsWithIgnoreCase('Product_'+prod.Pserno__c))
                     {
                         
                         productWrapperList.add(new ProductWrapper(prod.Name,prodAttch.Id,prod.Product_Description__c));
                         break;
                     }
                 }
             }
             else
             {
                 
                 productWrapperList.add(new ProductWrapper(prod.Name,(documentList.size()>0?string.valueof(documentList[0].Id):''),prod.Product_Description__c));
             }
             
         }
         
         system.debug('****productWrapperList****'+productWrapperList);
        }
       /* catch (dmlException ex)
        { 
            StatusLogHelper.logSalesforceError('getEnterCardProducts', 'E004', 'productWrapperList: ' + productWrapperList, ex, true);
        }
        catch (calloutException ex)
        { 
            StatusLogHelper.logSalesforceError('getEnterCardProducts', 'E001', 'productWrapperList: ' + productWrapperList, ex, true);
        }*/
        catch (Exception ex)
        { 
            StatusLogHelper.logSalesforceError('getEnterCardProducts', 'E005', 'productWrapperList: ' + productWrapperList, ex, true);
        }
    }
    
    
    // wrapper class 
    public class ProductWrapper
    {
        public ProductWrapper(string productName,string productImageId, string productDescription)
        {
            this.productName=productName;
            this.productImageId=productImageId;
            this.productDescription=productDescription;
        }
        
        public ProductWrapper(){}
        
        public string productName{get;set;}
        public string productImageId{get;set;}
        public string productDescription{get;set;}
        
        
    }
    
   
}