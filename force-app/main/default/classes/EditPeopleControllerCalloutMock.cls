@isTest
global class EditPeopleControllerCalloutMock implements HttpCalloutMock{

    global HTTPResponse respond(HTTPRequest req){
       
        //System.assertEquals('POST', req.getMethod());
        HttpResponse res = new HttpResponse();
        res.setHeader('Content-Type', 'application/json');
        res.setBody('{"animals": ["majestic badger", "fluffy bunny", "scary bear", "chicken", "mighty moose"]}');
        //res.setStatusCode(202);
        res.setStatusCode(204);
        return res;
    }
    
}