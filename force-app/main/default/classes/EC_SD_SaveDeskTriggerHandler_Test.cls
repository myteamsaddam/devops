/**********************************************************************
Name: EC_SD_SaveDeskTriggerHandler_Test
=======================================================================
Purpose: This Test class is used for test coverage of the EC_SD_SaveDeskTriggerHandler.User Story -SFD-1430

======================================================================= 
History
-----------------------------------------------------------------------         
Version     Author                   Date              Detail
1.0         Saddaam Hussain         20-May-2020       Initial Version

**********************************************************************/
@isTest(seeAllData = false)
public class EC_SD_SaveDeskTriggerHandler_Test {
     /**********************************************************************
*   Author: Saddam Hussain
*   Date:     20-May-2020
*   User Story : SFD-1430
*   Param: None
*   Return: None    
*   Description: In this method we are trying to Create SaveDesk and other related data to coverage the code for the
	updateSDeskCustomerAndFAccount, updateAttemptField,updateTaskRecord,createSaveDeskListView methods.
* @Revision(s): [Date] - [Change Reference] - [Changed By] - [Description]   

**********************************************************************/
    static testMethod void updateSDeskCustomerAndFAccountTest() {
        System.runAs(EC_UnitTestDataGenerator.adminUser) {
            SaveDeskQueue__c sdObj = EC_UnitTestDataGenerator.TestSaveDesk.buildInsert(new Map<String, Object>{
                'Status__c'=>'Open',
                    'Response__c'=>'Callback Later',
                    'Calling_Attempts__c'=>'Inbound Call',
                    'Task_Count__c'=>0
                    });
            SaveDeskQueue__c sdObjs = EC_UnitTestDataGenerator.TestSaveDesk.buildInsert(new Map<String, Object>{
                'Status__c'=>'Open',
                    'Response__c'=>'Callback Later',
                    'Calling_Attempts__c'=>'Inbound Call',
                    'Task_Count__c'=>1
                    });
            SaveDeskQueue__c sdObjects = EC_UnitTestDataGenerator.TestSaveDesk.buildInsert(new Map<String, Object>{
                'Status__c'=>'Open',
                    'Response__c'=>'Callback Later',
                    'Calling_Attempts__c'=>'Inbound Call',
                    'Task_Count__c'=>2
                    });
            SaveDeskQueue__c sdObject = EC_UnitTestDataGenerator.TestSaveDesk.buildInsert(new Map<String, Object>{
                'Status__c'=>'Open',
                    'Response__c'=>'Callback Later',
                    'Calling_Attempts__c'=>'Inbound Call',
                    'Task_Count__c'=>3
                    });
            SaveDeskQueue__c sdObjec = EC_UnitTestDataGenerator.TestSaveDesk.buildInsert(new Map<String, Object>{
                'Status__c'=>'Open',
                    'Response__c'=>'Callback Later',
                    'Calling_Attempts__c'=>'Inbound Call',
                    'Task_Count__c'=>4
                    });
            List<SaveDeskQueue__c> saveDeskList = new List<SaveDeskQueue__c>();
            saveDeskList.add(sdObj);
            saveDeskList.add(sdObjs);
            saveDeskList.add(sdObjects);
            saveDeskList.add(sdObject);
            saveDeskList.add(sdObjec);
            List<String>  sDeskAttr = new List<String> {'Status__c','Response__c','Calling_Attempts__c','Task_Count__c'};
                List<SaveDeskQueue__c> saveDeskReload = EC_UnitTestDataGenerator.TestSaveDesk.reloadList(saveDeskList, sDeskAttr);
            
            Account accountObj = EC_UnitTestDataGenerator.TestAccount.buildInsert(new Map<String, Object>{
            });
            Blob hash = Crypto.generateDigest('MD5', Blob.valueOf('0000000062'));
            string encryptSSN=EncodingUtil.convertToHex(hash);
            Contact contactObj = EC_UnitTestDataGenerator.TestContact.buildInsert(new Map<String, Object>{
                'AccountId'=>accountObj.Id, 
                    'Encrypted_SSN__c'=>encryptSSN
                    });
            system.debug('-contactObj--'+contactObj.SSN__c);
            Product_Custom__c prodobj = EC_UnitTestDataGenerator.TestProductCustom.buildInsert();
            
            Financial_Account__c finAccObj = EC_UnitTestDataGenerator.TestFinancialAccount.buildInsert(new Map<String, Object>{
                'Customer__c'=>accountObj.Id,
                    'Customer_Serno__c'=>contactObj.SerNo__c, 
                    'Product__c'=>prodobj.Id,
                    'Product_Serno__c'=>prodobj.Pserno__c
                    });
            
            Card__c cardObj = EC_UnitTestDataGenerator.TestCard.buildInsert(new Map<String, Object>{
                'People__c'=>contactObj.Id,
                    'Financial_Account__c'=>finAccObj.Id, 
                    'Product__c'=>prodobj.Id,
                    'Prod_Serno__c'=>prodobj.Pserno__c
                    });
            List<Card__c> cardList = new List<Card__c>();
            cardList.add(cardObj);
            List<String>  cardAttr = new List<String> {'People__c','Financial_Account__c','Product__c','Prod_Serno__c'};
                List<Card__c> cardReload = EC_UnitTestDataGenerator.TestCard.reloadList(cardList,cardAttr);
            system.debug('--cardReload--'+cardReload);
            
            
            Task taskObj = EC_UnitTestDataGenerator.TestTask.buildInsert(new Map<String, Object>{
                'WhoId'=>contactObj.Id,
                    'CreatedById'=>UserInfo.getUserId()
                    });
            List<Task> taskList = new List<Task>();
            taskList.add(taskObj);
            List<String>  taskAttr = new List<String> {'WhoId','CreatedById'};
                List<Task> taskReload = EC_UnitTestDataGenerator.TestTask.reloadList(taskList,taskAttr);
            
            EC_SD_SaveDeskTriggerHandler.updateSDeskCustomerAndFAccount(saveDeskList);
            EC_SD_SaveDeskTriggerHandler.updateAttemptField(saveDeskList);
            EC_SD_SaveDeskTriggerHandler.updateTaskRecord(saveDeskList);
            EC_SD_SaveDeskTriggerHandler.createSaveDeskListView(saveDeskList);
            
        }
    }
    
      /**********************************************************************
*   Author: Saddam Hussain
*   Date:     20-May-2020
*   User Story : SFD-1430
*   Param: None
*   Return: None    
*   Description: In this method we are trying to Create SaveDesk if condition data to coverage the code for the
    updateAttemptField methods.
* @Revision(s): [Date] - [Change Reference] - [Changed By] - [Description]   

**********************************************************************/
    
    static testMethod void updateAttemptFieldIfTest() {
        System.runAs(EC_UnitTestDataGenerator.adminUser) {
            SaveDeskQueue__c sdObj = EC_UnitTestDataGenerator.TestSaveDesk.buildInsert(new Map<String, Object>{
                'Status__c'=>'Open',
                    'Response__c'=>'Call not answered',
                    'Task_Count__c'=>1
                    });
            List<SaveDeskQueue__c> saveDeskList = new List<SaveDeskQueue__c>();
            saveDeskList.add(sdObj);
            List<String>  sDeskAttr = new List<String> {'Status__c','Response__c','Task_Count__c'};
                List<SaveDeskQueue__c> saveDeskReload = EC_UnitTestDataGenerator.TestSaveDesk.reloadList(saveDeskList, sDeskAttr);
            
            Task taskObj = EC_UnitTestDataGenerator.TestTask.buildInsert(new Map<String, Object>{
                'CreatedById'=>UserInfo.getUserId()
                    });
            List<Task> taskList = new List<Task>();
            taskList.add(taskObj);
            List<String>  taskAttr = new List<String> {'CreatedById'};
                List<Task> taskReload = EC_UnitTestDataGenerator.TestTask.reloadList(taskList,taskAttr);
            EC_SD_SaveDeskTriggerHandler.updateAttemptField(saveDeskList);
        }
    }
    
          /**********************************************************************
*   Author: Saddam Hussain
*   Date:     20-May-2020
*   User Story : SFD-1430
*   Param: None
*   Return: None    
*   Description: In this method we are trying to Create SaveDesk else condition data to coverage the code for the
    updateAttemptField methods.
* @Revision(s): [Date] - [Change Reference] - [Changed By] - [Description]   

**********************************************************************/
    
    static testMethod void updateAttemptFieldElseTest() {
        System.runAs(EC_UnitTestDataGenerator.adminUser) {
            SaveDeskQueue__c sdObj = EC_UnitTestDataGenerator.TestSaveDesk.buildInsert(new Map<String, Object>{
                'Status__c'=>'Open',
                    'Response__c'=>'Call not answered',
                    'Task_Count__c'=>2
                    });
            List<SaveDeskQueue__c> saveDeskList = new List<SaveDeskQueue__c>();
            saveDeskList.add(sdObj);
            List<String>  sDeskAttr = new List<String> {'Status__c','Response__c','Task_Count__c'};
                List<SaveDeskQueue__c> saveDeskReload = EC_UnitTestDataGenerator.TestSaveDesk.reloadList(saveDeskList, sDeskAttr);
            
            Task taskObj = EC_UnitTestDataGenerator.TestTask.buildInsert(new Map<String, Object>{
                'Savedesk__c'=>saveDeskList[0].Id,
                    'CreatedById'=>UserInfo.getUserId()
                    });
            List<Task> taskList = new List<Task>();
            taskList.add(taskObj);
            List<String>  taskAttr = new List<String> {'Savedesk__c','CreatedById'};
                List<Task> taskReload = EC_UnitTestDataGenerator.TestTask.reloadList(taskList,taskAttr);
            EC_SD_SaveDeskTriggerHandler.updateAttemptField(saveDeskList);
            EC_SD_SaveDeskTriggerHandler.updateLatestTaskRecord(saveDeskList);
        }
    }  
}