@isTest
private with sharing class PopulateAddressDataTrigger_Test
{
    public static testmethod void TestPopulateAddressDataTrigger()
    {
       	Contact Con = new Contact();
            Con.FirstName = 'Shauryanaditya';
            Con.LastName = 'Singh';
            Con.Email ='jdoe_test_test@doe.com';
            Con.SSN__c='Test1';
            Con.Institution_Id__c=1;
            Con.SerNo__c='1231';
            Con.MobilePhone='98765432101';
            Con.Phone='1231231231'; 
            Con.Fax='987898791';
            Con.HomePhone='1231231231';
        insert Con;
        Address__c addr = new address__c();
			addr.addresstype__c='11111';
        	addr.People__c=Con.Id;
        	addr.CardPin__c=False;
        	addr.Mailer__c=True;
        	addr.Invoice__c=True;
        	addr.Other__c=False;
        	addr.Location__c='Home';
        	addr.Street_Address__c='Salesforce Street';
        	addr.ZIP__c='40079';
        	addr.City__c='Stockholm';
        	addr.Country__c='DK';
        	addr.Country_Code__c='208';
        test.startTest();   
		insert addr;
       	test.stoptest();  
       		addr.Country__c='SE';
       update addr;
		address__c addre = [select country__c from address__c where Id = :addr.Id];
		System.assertEquals('SE', addre.country__c);   
    }
}