public class UpdateMembershipDataOutput{
    public String MsgId;
    public String CorrelationId;
    public String RequestorId;
    public String SystemId;
    public String InstitutionId;
    public String Code;
    public String Description;
}