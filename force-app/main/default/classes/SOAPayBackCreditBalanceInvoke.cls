//Generated by wsdl2apex

public class SOAPayBackCreditBalanceInvoke {
    public class x_xsoap_CSSServiceESB_CSSServicePT {
        public String endpoint_x = 'http://eclvmidapp04t.ectest.local:8010/CSSService/ProxyServices/CSSServicePS';
        public Map<String,String> inputHttpHeaders_x;
        public Map<String,String> outputHttpHeaders_x;
        public String clientCertName_x;
        public String clientCert_x;
        public String clientCertPasswd_x;
        public Integer timeout_x;
        public SOAPayBackCreditBalanceSecurityHeader.Security_element SecurityPart;
        private String SecurityPart_hns = 'Security=http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd';
        private String[] ns_map_type_info = new String[]{'http://schemas.entercard.com/service/CSSService/1.0', 'SOAPayBackCreditBalanceInvoke', 'http://schemas.entercard.com/service/CSSServiceResponse/1.0', 'SOAPayBackCreditBalanceResponse', 'http://schemas.entercard.com/TYPELIB/1.0', 'SOAPayBackCreditBalanceTypelib', 'http://schemas.entercard.com/service/CSSServiceRequest/1.0', 'SOAPayBackCreditBalanceRequest', 'http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd', 'SOAPayBackCreditBalanceSecurityHeader'};
        /* public SOAPayBackCreditBalanceResponse.CustomerMemoExportDataResponseRecordType customerMemoExportData(SOAPayBackCreditBalanceRequest.CustomerMemoExportDataRequestRecordType CustomerMemoExportDataRequestRecord) {
            SOAPayBackCreditBalanceRequest.CustomerMemoExportDataRequest_element request_x = new SOAPayBackCreditBalanceRequest.CustomerMemoExportDataRequest_element();
            request_x.CustomerMemoExportDataRequestRecord = CustomerMemoExportDataRequestRecord;
            SOAPayBackCreditBalanceResponse.CustomerMemoExportDataResponse_element response_x;
            Map<String, SOAPayBackCreditBalanceResponse.CustomerMemoExportDataResponse_element> response_map_x = new Map<String, SOAPayBackCreditBalanceResponse.CustomerMemoExportDataResponse_element>();
            response_map_x.put('response_x', response_x);
            WebServiceCallout.invoke(
              this,
              request_x,
              response_map_x,
              new String[]{endpoint_x,
              'CustomerMemoExportData',
              'http://schemas.entercard.com/service/CSSServiceRequest/1.0',
              'CustomerMemoExportDataRequest',
              'http://schemas.entercard.com/service/CSSServiceResponse/1.0',
              'CustomerMemoExportDataResponse',
              'SOAPayBackCreditBalanceResponse.CustomerMemoExportDataResponse_element'}
            );
            response_x = response_map_x.get('response_x');
            return response_x.CustomerMemoExportDataResponseRecord;
        } */
        public SOAPayBackCreditBalanceResponse.PayBackCreditResponseRecordType payBackCredit(SOAPayBackCreditBalanceTypelib.headerType Header,SOAPayBackCreditBalanceRequest.PayBackCreditRequestRecordType PayBackCreditRequestRecord) {
            SOAPayBackCreditBalanceRequest.PayBackCreditRequest_element request_x = new SOAPayBackCreditBalanceRequest.PayBackCreditRequest_element();
            request_x.Header = Header;
            request_x.PayBackCreditRequestRecord = PayBackCreditRequestRecord;
            SOAPayBackCreditBalanceResponse.PayBackCreditResponse_element response_x;
            Map<String, SOAPayBackCreditBalanceResponse.PayBackCreditResponse_element> response_map_x = new Map<String, SOAPayBackCreditBalanceResponse.PayBackCreditResponse_element>();
            response_map_x.put('response_x', response_x);
            WebServiceCallout.invoke(
              this,
              request_x,
              response_map_x,
              new String[]{endpoint_x,
              'payBackCredit',
              'http://schemas.entercard.com/service/CSSServiceRequest/1.0',
              'PayBackCreditRequest',
              'http://schemas.entercard.com/service/CSSServiceResponse/1.0',
              'PayBackCreditResponse',
              'SOAPayBackCreditBalanceResponse.PayBackCreditResponse_element'}
            );
            response_x = response_map_x.get('response_x');
            return response_x.PayBackCreditResponseRecord;
        }
    }
}