public class UpdateMembershipDataInput{
    public Integer Institution_ID;
    public String PartnerName;
    public String ProductId;
    public String EntityId;
    public String UpdatedBy;
    public String DateTimeStamp;
    public String Source_System;
    public String EntityType;
    public List<memberField> memberFieldList;
        
    public class memberField{
        public String MemberFieldName;
        public String MemberFieldValues;
    }
}