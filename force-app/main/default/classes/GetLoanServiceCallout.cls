public class GetLoanServiceCallout extends SoapIO{
   
    SOALoanRequest.GetLoanAccountsInfoRequestBodyType getLoanAccountsInfoRequestInstance;
    GetLoanServiceInput input;
    
    public  void convertInputToRequest(){
    
        input = (GetLoanServiceInput)serviceInput;
        getLoanAccountsInfoRequestInstance = new SOALoanRequest.GetLoanAccountsInfoRequestBodyType();
        system.debug('------------SSN----'+input.SSN);
        getLoanAccountsInfoRequestInstance.SSN=input.SSN;
        getLoanAccountsInfoRequestInstance.AccountNumber=input.AccountNumber;

    }
    
    public  override object invokeWebserviceCallout(SoapRequest soapRequest){
        
        SOALoanTypelib.headerType HeadInstance=new SOALoanTypelib.headerType();
        HeadInstance.MsgId='SFDC consuming Get Loan Info SOA service. Time Stamp=>'+system.now();
        HeadInstance.CorrelationId=Common_Settings__c.getValues('CorrelationId').Common_Value__c;
        HeadInstance.RequestorId=Common_Settings__c.getValues('RequestorId').Common_Value__c;
        HeadInstance.SystemId=Common_Settings__c.getValues('SystemId').Common_Value__c;
        HeadInstance.InstitutionId=string.valueof(input.InstitutionId);
        SOALoanInvoke.x_xsoap_CSSServiceESB_CSSServicePT   invokeInstance= new SOALoanInvoke.x_xsoap_CSSServiceESB_CSSServicePT ();
        invokeInstance.timeout_x=30000;
         // Added 12/13/2016
        ServiceSettings__c serviceObj = ServiceSettings__c.getValues('GetLoans');
        if(serviceObj!=null && serviceObj.EndPoint__c!='' && serviceObj.EndPoint__c!=null)
        invokeInstance.endpoint_x = serviceObj.EndPoint__c;
        // Added 12/13/2016
     
        String username = serviceObj.Username__c; //'css_soa';
        String password = serviceObj.Password__c;//'P1%gruzA';
        
        Blob headerValue = Blob.valueOf(username + ':' + password);
        String authorizationHeader = 'BASIC ' +EncodingUtil.base64Encode(headerValue);
        
        invokeInstance.inputHttpHeaders_x = new Map<String, String>();
        invokeInstance.inputHttpHeaders_x.put('Authorization', authorizationHeader);
        
        SOALoanSecurityHeader.UsernameToken creds=new SOALoanSecurityHeader.UsernameToken();
        
        creds.Username=serviceObj.Username__c;//'css_soa';
        creds.Password=serviceObj.Password__c;//'P1%gruzA';
        
        SOALoanSecurityHeader.Security_element security_ele=new SOALoanSecurityHeader.Security_element();
        security_ele.UsernameToken=creds;
        invokeInstance.SecurityPart=security_ele;
        SOALoanResponse.GetLoanAccountsInfoResponse_element getLoanAccountsInfoResponse_element ;
        getLoanAccountsInfoResponse_element =  invokeInstance.getLoanAccountsInfo(HeadInstance,getLoanAccountsInfoRequestInstance);
        system.debug('getLoanAccountsInfoResponse_element -->'+getLoanAccountsInfoResponse_element);
        return getLoanAccountsInfoResponse_element;
     }  
        
     public override object convertResponseToOutput(object response, map<string, string> responseHeader, ServiceStatus serviceStatus){
       if(response==null)
           return null;
       
       SOALoanResponse.GetLoanAccountsInfoResponse_element  getLoanAccountsInfoResponse_elementInstance = (SOALoanResponse.GetLoanAccountsInfoResponse_element)response;
       SOALoanResponse.GetLoanAccountsInfoResponseBodyType  getLoanAccountsInfoResponseBodyInstance =getLoanAccountsInfoResponse_elementInstance.GetLoanAccountsInfoResponseBody;
       
       GetLoanServiceOutput getLoanServiceOutput = new GetLoanServiceOutput();
       getLoanServiceOutput.LoanAccountDetails=new List<GetLoanServiceOutput.LoanAccountDetails>();
       if(getLoanAccountsInfoResponseBodyInstance!=null)
       {
       SOALoanResponse.GetLoanAccountsInfoResponseRecordType[] getLoanAccountsInfoResponseRecordInstance=getLoanAccountsInfoResponseBodyInstance.GetLoanAccountsInfoResponseRecord;
       
           if(getLoanAccountsInfoResponseRecordInstance!=null)
           {
               for(SOALoanResponse.GetLoanAccountsInfoResponseRecordType lcs:getLoanAccountsInfoResponseRecordInstance)
               {
                   
                   //SOALoanResponse.LoanAccountDetailsType loanAccountDetailsTypeInstance = lcs.LoanAccountDetails;
                   
                   //if(loanAccountDetailsTypeInstance!=null)
                   if(lcs.LoanAccountDetails != null && lcs.LoanAccountDetails.size() > 0)
                   {
                       for(SOALoanResponse.LoanAccountDetailsType loanAccountDetailsTypeInstance : lcs.LoanAccountDetails)
                       {                   
                            GetLoanServiceOutput.LoanAccountDetails lds=new GetLoanServiceOutput.LoanAccountDetails();
                            lds.CustomerAccountNumber=loanAccountDetailsTypeInstance.CustomerAccountNumber;
                            lds.CustomerAccountSerno=loanAccountDetailsTypeInstance.CustomerAccountSerno;
                            lds.CustomerAccountStatus=loanAccountDetailsTypeInstance.CustomerAccountStatus;
                            lds.LoanAccountNumber=loanAccountDetailsTypeInstance.LoanAccountNumber;
                            lds.LoanAccountSerno=loanAccountDetailsTypeInstance.LoanAccountSerno;
                            lds.LoanProductName=loanAccountDetailsTypeInstance.LoanProductName;
                            lds.LoanAccountStatus=loanAccountDetailsTypeInstance.LoanAccountStatus;
                            lds.InitialLoanAmount=loanAccountDetailsTypeInstance.InitialLoanAmount;
                            lds.OutStandingBalance=loanAccountDetailsTypeInstance.OutStandingBalance;
                            lds.NominalInterestRate=loanAccountDetailsTypeInstance.NominalInterestRate;
                            lds.RemainingPeriod=loanAccountDetailsTypeInstance.RemainingPeriod;
                            lds.LoanSDORef=loanAccountDetailsTypeInstance.LoanSDORef;
                           
                           lds.PaymentPlanDetails = new List<GetLoanServiceOutput.PaymentPlanDetails>(); 
                           SOALoanResponse.PaymentPlanDetailsType[]  PaymentPlanDetailsTypeInstance =loanAccountDetailsTypeInstance.PaymentPlanDetails;
                           if(PaymentPlanDetailsTypeInstance!=null)
                           {
                               for(SOALoanResponse.PaymentPlanDetailsType pt:PaymentPlanDetailsTypeInstance)
                               {
                                GetLoanServiceOutput.PaymentPlanDetails pmtdtls = new GetLoanServiceOutput.PaymentPlanDetails();                           
                                pmtdtls.PaymentNumber=pt.PaymentNumber;
                                pmtdtls.PaymentDate=pt.PaymentDate;
                                pmtdtls.TransactionType=pt.TransactionType;
                                pmtdtls.TransactionDescription=pt.TransactionDescription;
                                pmtdtls.Amount=pt.Amount;
                                pmtdtls.OutstandingBalance=pt.OutstandingBalance;
                                pmtdtls.PaymentStatus=pt.PaymentStatus;
                                lds.PaymentPlanDetails.add(pmtdtls);
                               }  
                           }
                           getLoanServiceOutput.LoanAccountDetails.add(lds);
                       }
                   }             
               }
           }
       }
       
       system.debug('***getLoanServiceOutput***'+getLoanServiceOutput);
       
      return getLoanServiceOutput;
    }        
}