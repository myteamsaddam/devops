global class Email_data_fix  implements Database.Batchable<sObject>,Database.Stateful{

    global final String Query;
    //global final String emailRegex = '^[a-zA-Z0-9_|\\\\%#~`=?&/$^*!}{+-].+@[a-zA-Z0-9.-]+\\.[a-zA-Z]{2,4}$';
    //global Pattern MyPattern;
    global integer x;
    //global string REGEXsuccess;
    global string REGEXfailed;

    global Email_data_fix(){        
        Query='select id,customer_email__C, email from contact where customer_email__c!=null AND customer_email__c!=\'\'';
        //MyPattern = Pattern.compile(emailRegex);
        x=0;
        //REGEXsuccess='';
        REGEXfailed='';
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC){
        return Database.getQueryLocator(Query);
    }
    
    global void execute(Database.BatchableContext BC, List<sObject> scope){
        
        List<contact>updatelist=new List<contact>();
        for(sObject obj : scope)
        {
            contact con= (Contact)obj;
            if(con.email==null || con.email=='' || (!con.email.equalsignorecase(con.customer_email__c)))// can’t put in SOQL condition as its encrypted
            {
                /*Matcher MyMatcher = MyPattern.matcher(con.Customer_Email__c);
                if (MyMatcher.matches()&& con.Customer_Email__c.isAsciiPrintable()) 
                    //con.Email=con.Customer_Email__c;
                    REGEXsuccess+=con.Customer_Email__c+'||';
                else// if regex fail, then set email to BLANK
                    //con.Email='';
                    REGEXfailed+=con.Customer_Email__c+'||'; */
                    con.Email=con.Customer_Email__c;
                   updatelist.add(con);
                x++;
            }
        }
        Database.SaveResult[] srList = database.update(updatelist,false);
        
        for (Database.SaveResult sr : srList) {
            if (sr.isSuccess()) {
                // Operation was successful, so get the ID of the record that was processed
                System.debug('Successfully updated contact. contact ID: ' + sr.getId());
            }
            else {
                // Operation failed, so get all errors                
                for(Database.Error err : sr.getErrors()) {
                    REGEXfailed+='The following error has occurred.\n';                    
                    REGEXfailed+= err.getStatusCode() + ': ' + err.getMessage()+'\n';
                    REGEXfailed+= 'contact fields that affected this error: ' + err.getFields()+'\n ------';
                    System.debug(REGEXfailed);
                }
            }
        }
    }
    global void finish(Database.BatchableContext BC){
        system.debug('Total Email corrected number:'+x);
        Messaging.SingleEmailMessage message = new Messaging.SingleEmailMessage();
        message.toAddresses = new String[] {'subhajit.a.pal@capgemini.com','vikesh.gajula@capgemini.com'};
        message.optOutPolicy = 'FILTER';
        message.subject = 'Email Fix script complete';
        message.plainTextBody = 'Total number of record should processed:'+x+'\n\n\n\n\n\nErrormsg:='+REGEXfailed;
        Messaging.SingleEmailMessage[] messages =   new List<Messaging.SingleEmailMessage> {message};
        Messaging.SendEmailResult[] results = Messaging.sendEmail(messages);
        
        if (results[0].success) {
            System.debug('The email was sent successfully.');
        } else {
            System.debug('The email failed to send: ' + results[0].errors[0].message);
        }   
    }
}