public without sharing class ServiceStatus {
  
  public string status;
  public string errorCode;
  public string errorType;
  public string errorDescription;
  
  public string referenceId;
  public string referenceName;
  
}