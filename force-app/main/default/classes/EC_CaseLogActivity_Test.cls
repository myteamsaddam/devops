/**********************************************************************
Name: EC_SD_SaveDeskTriggerHandler_Test
=======================================================================
Purpose: This Test class is used for test coverage of the EC_CaseLogActivity.User Story -SFD-1584

======================================================================= 
History
-----------------------------------------------------------------------         
Version     Author                   Date              Detail
1.0         Saddaam Hussain         07-June-2020       Initial Version

**********************************************************************/
@isTest(seeAllData = false)
public class EC_CaseLogActivity_Test {
    /**********************************************************************
*   Author: Saddam Hussain
*   Date:     1-Jul-2020
*   User Story : SFD-1584
*   Param: None
*   Return: Map    
*   Description: In this method we cover the cover the code for case update functionality.
*   @Revision(s): [Date] - [Change Reference] - [Changed By] - [Description]   

**********************************************************************/
    static testMethod void caseUpdateTest() {
        System.runAs(EC_UnitTestDataGenerator.adminUser) {
            Account accountObj = EC_UnitTestDataGenerator.TestAccount.buildInsert(new Map<String, Object>{
            });
            Blob hash = Crypto.generateDigest('MD5', Blob.valueOf('0000000062'));
            string encryptSSN=EncodingUtil.convertToHex(hash);
            Contact contactObj = EC_UnitTestDataGenerator.TestContact.buildInsert(new Map<String, Object>{
                'AccountId'=>accountObj.Id, 
                    'Encrypted_SSN__c'=>encryptSSN
                    });
            system.debug('-contactObj--'+contactObj.SSN__c);
            Product_Custom__c prodobj = EC_UnitTestDataGenerator.TestProductCustom.buildInsert();
            
            Financial_Account__c finAccObj = EC_UnitTestDataGenerator.TestFinancialAccount.buildInsert(new Map<String, Object>{
                'Customer__c'=>accountObj.Id,
                    'Customer_Serno__c'=>contactObj.SerNo__c, 
                    'Product__c'=>prodobj.Id,
                    'Product_Serno__c'=>prodobj.Pserno__c
                    });
            
            Card__c cardObj = EC_UnitTestDataGenerator.TestCard.buildInsert(new Map<String, Object>{
                'People__c'=>contactObj.Id,
                    'Financial_Account__c'=>finAccObj.Id, 
                    'Product__c'=>prodobj.Id,
                    'Prod_Serno__c'=>prodobj.Pserno__c
                    });
            Case caseObj = EC_UnitTestDataGenerator.TestCase.buildInsert();
            System.assertNotEquals(caseObj.Category__c, null,'Please Select the Category');
            EC_CaseLogActivity.getFinancialAccounts(contactObj.Id);
            EC_CaseLogActivity.getCards(finAccObj.Id, contactObj.Id);
            EC_CaseLogActivity.doCaseUpdate(caseObj.id, contactObj.Id, 'Other', 'Other', 'Test', 
                                            EC_StaticConstant.NONE_VAL, '', '', '', '', '');
            EC_CaseLogActivity.doCaseUpdate(caseObj.id, contactObj.Id, 'Other', 'Other', 'Test', 
                                            EC_StaticConstant.CATEGORY_ACCOUNT_CLOSURE, '', '', '', '', '');
            EC_CaseLogActivity.doCaseUpdate(caseObj.id, contactObj.Id, finAccObj.Id, cardObj.Id, 'Test', 
                                            EC_StaticConstant.CATEGORY_ACCOUNT_CLOSURE, 'Test', 'Test', 'Test', 'Test', 'Test');
        }
    }
}