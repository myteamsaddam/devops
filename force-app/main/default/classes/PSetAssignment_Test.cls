/*
Created By: Srinivas D
Created Date: 17th March 2019
Created for Trigger: PSetAssignment
*/

@isTest
public class PSetAssignment_Test {
   testMethod static void assignDCMSPset() 
   {
    UserRole r = new UserRole(DeveloperName = 'DK_FC_Supervisor1', Name = 'DK FC Supervisor');
    insert r;
    Id FCRoleId=[Select id, Name from UserRole where DeveloperName = 'DK_FC_Supervisor'].Id;
    User u = new User(
     ProfileId = [SELECT Id FROM Profile WHERE Name = 'DK - Operations'].Id,
     LastName = 'last',
     Email = 'puser000@amamama.com',
     Username = 'puser000@amamama.com' + System.currentTimeMillis(),
     CompanyName = 'TEST',
     Title = 'title',
     Alias = 'alias',
     TimeZoneSidKey = 'America/Los_Angeles',
     EmailEncodingKey = 'UTF-8',
     LanguageLocaleKey = 'en_US',
     LocaleSidKey = 'en_US',
     UserRoleId = r.Id
    );
       insert u;
       u.UserroleId=FCRoleId;
       update u;
       
   }
}