@isTest
public class RetentionResponseController_test {
    
    @isTest static void test1()
    {
        //Account
        Account account=new Account();
        account.Name='test Account';
        account.Customer_Serno__c='123';
        account.Institution_Id__c=5;
        insert account;
        
        //Contact
        Contact contact=new Contact();
        contact.FirstName='TestContact1';
        contact.LastName='TestContact';
        contact.AccountId=account.Id;
        contact.SSN__c='11235464';
        contact.SerNo__c='12345';
        contact.Institution_Id__c=account.Institution_Id__c;
        contact.Email='xyz@abc.com';
        contact.MobilePhone='9876543210';
        contact.Phone='123123123'; 
        contact.Fax='98789879';
        contact.MobilePhone='98789879'; 
        contact.HomePhone='123123123';
        contact.Email='testemail@test.com';
        insert contact;
        System.debug(contact);
        
        //EnterCard Product
        Product_Custom__c pd=new Product_Custom__c();
        pd.Product_Description__c='dhhfygygyadyh';
        pd.Pserno__c='123';
        insert pd;
        
        //Financial Account
        Financial_Account__c fc=new Financial_Account__c();
        fc.Customer__c=account.id;
        fc.Customer_Serno__c=account.Customer_Serno__c;
        fc.Account_Serno__c='1234';
        fc.Account_Number__c='1234567890';
        fc.Institution_Id__c=account.Institution_Id__c;
        fc.Product_Serno__c= pd.Pserno__c;
        fc.Product__c=pd.id;
        insert fc;
        
        
        //Card
        Card__c c=new Card__c();
        
        c.Institution_Id__c=account.Institution_Id__c;
        c.Card_Number_Truncated__c='12345bhsccccc';
        c.Prod_Serno__c=pd.Pserno__c;
        c.Product__c=pd.id;
        c.People__c=contact.id;
        c.Financial_Account_Serno__c=fc.Account_Serno__c;
        c.Financial_Account__c=fc.id;
        c.People_Serno__c=contact.SerNo__c;
        c.Card_Serno__c='1234';
        insert c;
        /*   //user 
User u = new User();
u.FirstName         = 'A';
u.LastName          = 'B';
u.Email             = 'ABC@xyz.com';
u.Alias             = 'A' + '1';
u.Username          = 'A' + '2' + 'B';
u.LocaleSidKey      = 'en_US';
u.TimeZoneSidKey    = 'GMT';
u.ProfileID         = '00ei0000000rTfz';
u.LanguageLocaleKey = 'en_US';
u.EmailEncodingKey  = 'UTF-8';
insert u;
*/ 
        //        test.startTest();
        //Retention Response
        Retention_Response__c req=new Retention_Response__c();
        if (req == null)
        {               req = new Retention_Response__c ();
         req.OwnerId = UserInfo.getUserId();          
        }
        req.Financial_Account__c = fc.id;
        req.OwnerId = UserInfo.getUserId();          
        req.Person__c = contact.id;
        req.Account_Closed__c='N';
        upsert req;
        test.startTest();
        Contact ConObj =[SELECT Id  FROM contact limit 1];
        Test.setCurrentPageReference(new PageReference('Page.RetentionResponsePage')); 
        System.currentPageReference().getParameters().put('conId', ConObj.Id);
        Apexpages.StandardController sc= new ApexPages.standardController(req);
        ApexPages.currentPage().getParameters().put('id', String.valueOf(req.Id));
        RetentionResponseController  testret = new RetentionResponseController();
        List<SelectOption> selOpts=testret.getselectedaccnamefields();        
        testret.save();    
        RetentionResponseController controller = new RetentionResponseController(sc);
        system.debug(req);
        List<Financial_Account__c> result = [select id from Financial_Account__c where id= :req.Financial_Account__c limit 1];
        system.assertEquals(result[0].id, req.Financial_Account__c);
        testret.getselectedaccnamefields();
        /*       
system.debug('controller-->' + controller);
system.debug('controller result-->' + controller.result[0]);
system.debug('controller result id-->' + controller.result[0].Id);
system.debug('fc -->' + fc);
system.debug('fc id-->' + fc.Id);
{
if (fc.Id != null && contact.Id!=null && c.Id !=null)
{
controller.result[0].Id = fc.Id;
}
}
*/   
        test.stopTest();
    }
}