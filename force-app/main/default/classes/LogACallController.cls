Public class LogACallController {
    
    @AuraEnabled
    Public Static String doSave(String FinId, String CardId, Id CustId, String category, String description, String type, Boolean memchkbx, Boolean calchkbx, Boolean todoChek, String SaveAtmp, String Saved, String Reason, String Offer)
    {
        String msg = '';
        try
        {
            Task[] ctiTsklst = [Select Id, Description, WhatId, WhoId,Comments__c, CallType, Call_Type__c, Account_Serno__c,Card_Serno__c,Category__c,Customer_Serno__c,MAH_Serno__c,Type from Task where (CreatedById = :UserInfo.getUserId() AND CreatedDate = Today AND WhoId =: CustId) AND (Status = 'Completed' AND Comments__c = '' AND CallType != null) ORDER BY CreatedDate DESC Limit 1];
            system.debug('----ctiTask----'+ctiTsklst);
            Card__c[] crdlst = [Select Id, Card_Serno__c,People_Serno__c,Financial_Account_Serno__c,GeneralStatus__c, People__r.Account.Customer_Serno__c, People__r.SSN__c, People__r.Institution_Id__c from Card__c where People__c =: CustId AND Id =: CardId];
            system.debug('----crdlst---'+crdlst);
            
            If(ctiTsklst.isEmpty() && memchkbx == false && todoChek == false){
                msg = System.Label.Activity_not_available_msg;
            }
            
            system.debug('--category-'+category);
            If((memchkbx == true || calchkbx == true) && (category == '')){
                msg = System.Label.Category_not_select_msg;
            }
            
            if((memchkbx == true || calchkbx == true || todoChek == true) && (FinId == '--None--' || FinId == '')){
                msg = System.Label.Financial_Value;
            }
            
            If(category == 'Account closure' && (memchkbx == true || calchkbx == true) && (SaveAtmp == '' || Saved == '' || Reason == '' || Offer == '' || FinId == 'Other')){
                if(SaveAtmp == '')
                    msg='Please Select the Save Attempt';
                if(Saved == '')
                    msg='Please Select the Saved';
                if(Reason == '')
                    msg='Please Select the Reason';
                if(Offer == '')
                    msg = 'Please Select the Offer';
                if(FinId == 'Other')
                    msg = System.Label.Financial_Value;
                
                return msg;
            }
            
            Contact[] conlst = [select id,Institution_Id__c,SSN__c from Contact where id=:CustId];
            system.debug('-conlst--'+conlst);
            if(conlst!= null && conlst.size()>0 && conlst[0].Institution_Id__c!=null && (memchkbx == true || calchkbx == true || todoChek == false))
            {
                CustomerMemoExportDataOutput outputmemoexportdata = CSSServiceHelper.updateCustomerMemo('NewMemo','PersonSSN',conlst[0].SSN__c,'css',description,String.valueOf(conlst[0].Institution_Id__c));
            	system.debug('--outputmemoexportdata---'+outputmemoexportdata);
            }
            
            if(((crdlst!= null && crdlst.size()>0) || CardId == 'Other') && ctiTsklst.size()>0 && ctiTsklst != null && calchkbx == true && category != '')
            {   
                ctiTsklst[0].Description = description;
                if(description != null && description.length()>255){
                    ctiTsklst[0].Comments__c = description.substring(0,254);  
                } else {
                    ctiTsklst[0].Comments__c =  description;
                }
                ctiTsklst[0].Subject = 'Call'; 
                ctiTsklst[0].Type = type;
                if(ctiTsklst[0].CallType != null)
                {
                    ctiTsklst[0].Call_Type__c = ctiTsklst[0].CallType;
                }
                ctiTsklst[0].Category__c = category;
                
                if(CardId !='Other'){
                    ctiTsklst[0].Account_Serno__c = crdlst[0].Financial_Account_Serno__c;
                    ctiTsklst[0].Customer_Serno__c = crdlst[0].People_Serno__c;
                    ctiTsklst[0].Card_Serno__c = crdlst[0].Card_Serno__c;
                    ctiTsklst[0].MAH_Serno__c = crdlst[0].People__r.Account.Customer_Serno__c;
                }
                
                if(ctiTsklst[0].CallType == 'Inbound' || ctiTsklst[0].CallType == 'Outbound')
                {
                    upsert ctiTsklst;
                    msg = System.Label.Activity_update_msg;
                } 
            }
            
            if((FinId != '' && FinId != '--None--') && (memchkbx == true && description != null && category != '')) 
            {
                //This below if condition used to create the completed activity.
                if(memchkbx == true)
                {
                    Task ts = new Task();
                    ts.Subject = 'Memo'; 
                    ts.Category__c = category;
                    ts.Type = type;
                    ts.Status = 'Completed';
                    ts.Description = description;
                    if(description != null && description.length()>255){
                        ts.Comments__c = description.substring(0,254);  
                    } else {
                        ts.Comments__c =  description;
                    }
                    ts.WhoId = CustId;
                    if(FinId !='Other'){	
                        ts.Account_Serno__c = crdlst[0].Financial_Account_Serno__c;
                        ts.Customer_Serno__c = crdlst[0].People_Serno__c;
                        ts.Card_Serno__c = crdlst[0].Card_Serno__c;
                        ts.MAH_Serno__c = crdlst[0].People__r.Account.Customer_Serno__c;
                    }
                    insert ts;
                    msg = System.Label.Activity_Insert_msg;
                }
            }
            
            If((FinId != '' && FinId != '--None--') && category == 'Account closure' && (memchkbx == true || calchkbx == true) && (SaveAtmp != '' || Saved != '' || Reason != '' || Offer != '')){
                Retention_Response__c rresp = new Retention_Response__c();
                rresp.Financial_Account__c = FinId;
                rresp.Save_Attempt__c = SaveAtmp;
                rresp.Account_Closed__c = Saved;
                rresp.Reason_for_Account_Closure__c = Reason;
                rresp.Offer__c = Offer;
                
                insert rresp;
            }
            
        } catch(Exception exp){
            StatusLogHelper.logSalesforceError('LogACall', 'E005', 'Error in CTI', exp, false);
        }
        
        system.debug('----flag----'+msg);
        return msg;
    }
    
    @AuraEnabled
    Public Static Boolean doUpdate(Id WhoId, Id ConId){
        Boolean flag = false;
        Task[] tsk = [Select id, WhoId from Task where WhoId =: ConId AND (Status = 'Completed' AND Comments__c = '') ORDER BY CreatedDate DESC Limit 1];
        system.debug('-----task-----'+tsk.size());
        
        if(!tsk.isEmpty())
        {
            tsk[0].WhoId = WhoId;
            upsert tsk;
            flag = true;
        } else {
            flag = false;
        }
        system.debug('---after-update----'+tsk);
        return flag;
    }
    
    @AuraEnabled
    Public Static String getCalltype(Id CustId){
        Task[] tklst = [Select Id, CallType from Task where WhoId =: CustId AND CallType != '' AND Comments__c = '' AND (CreatedDate = Today AND CreatedById = :UserInfo.getUserId()) ORDER BY CreatedDate DESC Limit 1];
        if(!tklst.isEmpty())
        {
            return tklst[0].CallType;
        }
        return null;
    }
    
    @AuraEnabled
    Public Static Map<String, String> getFinRecs(Id CustId){
        String finId;
        Map<String, String> option =  new Map<String, String>();
        Set<Id> financialAccIdSet =new Set<Id>();
        List<Card__c> cardList = [SELECT Id, Name, People__c, Financial_Account__c FROM Card__c where People__c=:CustId and Financial_Account__c!=null ORDER BY CreatedDate DESC Limit 50000];
        for(Card__c card:cardList)
        {
            financialAccIdSet.add(card.Financial_Account__c);
        }
        
        List<Financial_Account__c> financialAccList = [SELECT Id, Name, Customer__c, Account_Number__c,Account_Serno__c,Product__c,Product__r.name FROM Financial_Account__c where Id IN:financialAccIdSet ORDER BY CreatedDate DESC Limit 50000];
        
        if(financialAccList.size() == 1){
            option.put(financialAccList[0].Id,financialAccList[0].Account_Number__c+' : '+(financialAccList[0].Product__r.name!=null?financialAccList[0].Product__r.name:'N/A'));
        }else {
            for(Financial_Account__c fa : financialAccList){
                option.put('--None--','--None--');
                option.put(fa.Id,fa.Account_Number__c+' : '+(fa.Product__r.name!=null?fa.Product__r.name:'N/A'));
            }
        }
        option.put('Other','Other');
        return option;
    }
    
    @AuraEnabled
    Public Static Map<String, String> getCardRecs(String financialVal, Id CustId){
        Map<String, String> cardMap = new Map<String, String>();
        List<Card__c> cardList = [SELECT Id, Name, People__c, Financial_Account__c,Card_Number_Truncated__c,Product__r.name,GeneralStatus__c FROM Card__c where People__c=:CustId and Financial_Account__c=:financialVal ORDER BY ExpiryDate__C DESC Limit 50000];
        system.debug('------card----'+cardList.size());        
        system.debug('---financialVal-'+financialVal);
        
        if(cardList.size() == 1){
            cardMap.put(cardList[0].Id,cardList[0].Card_Number_Truncated__c+' : '+(cardList[0].GeneralStatus__c!=null?cardList[0].GeneralStatus__c:'N/A'));
        } else {
            for(Card__c card:cardList)
            {
                cardMap.put(card.id , card.Card_Number_Truncated__c+' : '+(card.GeneralStatus__c!=null?card.GeneralStatus__c:'N/A'));
            }
        }
        if(financialVal == 'Other'){
            cardMap.put('Other','Other'); 
        }
        if(financialVal == '--None--'){
            cardMap.put('--None--','--None--');
        }
        return cardMap;
    }
    
    //public static DisableCustomerTabService service = new DisableCustomerTabService();
    /*@AuraEnabled 
    public static boolean disableCustomer(String CustId){
        Boolean flag;
        List<Task> tsklist = [Select Id,WhoId,Comments__c, CallType from Task where (CreatedById = :UserInfo.getUserId() AND CreatedDate = Today AND WhoId =: CustId) AND (Status = 'Completed' AND Comments__c = '' AND CallType != null) ORDER BY CreatedDate DESC Limit 1];
        if(!tsklist.isEmpty()){
            flag = true;
        }else{
            flag = false;
        }
        return flag;
    }*/
    
    Public String Ssn {set;get;}
    Public String CustId{set;get;}
    Public String CustName{set;get;}
    Public Case caseobj {set;get;}
    Public Task tsk {set;get;}
    Public Id TskId {set;get;}
    
    Public LogACallController(){
        caseobj = new Case();
        tsk = new Task();
    }
    
    Public Pagereference redirctcustomer()
    {
        
        Task[] tslist = [Select Id, WhoId, Comments__c, Category__c, Type from Task where CreatedById = :UserInfo.getUserId() AND CreatedDate = Today AND Comments__c = '' ORDER BY CreatedDate DESC Limit 1];
        system.debug('----tslist---'+tslist.isEmpty());
        if(Ssn != null)
        {
            Blob hash = Crypto.generateDigest('MD5', Blob.valueOf(Ssn));
            string encryptSSN=EncodingUtil.convertToHex(hash);
            Contact[] con = [select id,Name from contact where Encrypted_SSN__c = :encryptSSN];
            system.debug('----conlist----'+con.isEmpty());
            if(!con.isEmpty() && !tslist.isEmpty())
            {
                tslist[0].WhoId = con[0].id;
                upsert tslist;
                CustId = con[0].id;
                CustName = con[0].name;
            } else if(con.isEmpty()){
                CustId = System.Label.No_Customer_msg;
            } else if(!con.isEmpty() && tslist.isEmpty()){
                CustId = System.Label.Activity_not_available_msg;
            }
            //return null;
        }
        
        
        if(Ssn != '' || Ssn.isNumeric() == false) 
        {
            system.debug('--Ssn--12----'+Ssn+'-check--'+Ssn.isNumeric());
            if(!Ssn.isNumeric())
                CustId=System.Label.SSN_not_Numaric_msg;//'SSN should be Numaric value';
            
            if(Ssn == '')
                CustId=	System.Label.No_SSN_msg;//'Please enter SSN';
            
            return null;
        }
        return null;  
    }
    
    Public Pagereference updateActivity()
    {
        system.debug('-------case--'+caseobj.Type+'-----task---'+tsk.Description+'---ctgy---'+caseobj.Category__c);
        Task[] tslist = [Select Id, WhoId, Comments__c, Category__c, Type, Subject from Task where CreatedById = :UserInfo.getUserId() AND CreatedDate = Today AND Comments__c = '' AND WhoId = '' ORDER BY CreatedDate DESC Limit 1];
        system.debug('----tslist---'+tslist);
        system.debug('---cat-'+caseobj.Category__c);
        if((caseobj.Category__c == 'None' && !tslist.isEmpty()) || tslist.isEmpty())
        {
            if((caseobj.Category__c == 'None' && !tslist.isEmpty()))
                CustId=System.Label.Category_not_select_msg;
            if(tslist.isEmpty())
                CustId = System.Label.Activity_not_available_msg;
            return null;
        }
        
        if((caseobj.Category__c != 'None' || tsk.Description != null || caseobj.Type != null) && !tslist.isEmpty())
        {
            tslist[0].Category__c = caseobj.Category__c;
            tslist[0].Comments__c = tsk.Description;
            if(tsk.Description != null && tsk.Description.length()>255){
                tslist[0].Comments__c = tsk.Description.substring(0,254);  
            } else {
                tslist[0].Comments__c =  tsk.Description;
            }
            tslist[0].Type = caseobj.Type;
            tslist[0].Subject = 'Call';
            update tslist;
            TskId = tslist[0].Id;
            CustId = System.Label.Activity_update_msg;
        } 
        
        return null;
    }
}