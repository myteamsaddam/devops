public class GetStatementCallout extends SoapIO{
    SOAPCSSServiceRequest.getStatementsHistoryRequestRecordType statementRequestRecordTypeinstance;
    public string institutionId;
    public  void convertInputToRequest(){
    
    GetStatementHistoryInput ip = (GetStatementHistoryInput)serviceInput;
    statementRequestRecordTypeinstance = new SOAPCSSServiceRequest.getStatementsHistoryRequestRecordType();
    statementRequestRecordTypeinstance.Serno=ip.serno;
    institutionId = ip.institutionId;
    }
    
    public  override object invokeWebserviceCallout(SoapRequest soapRequest){
        SOAPschemasEntercardComTypelib10.headerType HeadInstance=new SOAPschemasEntercardComTypelib10.headerType();
        HeadInstance.MsgId='FinalCAll';
        HeadInstance.CorrelationId=Common_Settings__c.getValues('CorrelationId').Common_Value__c;
        HeadInstance.RequestorId=Common_Settings__c.getValues('RequestorId').Common_Value__c;
        HeadInstance.SystemId=Common_Settings__c.getValues('SystemId').Common_Value__c;
        HeadInstance.InstitutionId=institutionId;
        SOAPCSSServiceInvoke.x_xsoap_CSSServiceESB_CSSServicePT invokeInstance= new SOAPCSSServiceInvoke.x_xsoap_CSSServiceESB_CSSServicePT();
        invokeInstance.timeout_x=30000;
        // Added 12/13/2016
        ServiceSettings__c serviceObj = ServiceSettings__c.getValues('GetStatementHistory');
        if(serviceObj!=null && serviceObj.EndPoint__c!='' && serviceObj.EndPoint__c!=null)
        invokeInstance.endpoint_x = serviceObj.EndPoint__c;
        // Added 12/13/2016
        String username = serviceObj.Username__c; //'css_soa';
        String password = serviceObj.Password__c; //'P1%gruzA';
        Blob headerValue = Blob.valueOf(username + ':' + password);
        String authorizationHeader = 'BASIC ' +EncodingUtil.base64Encode(headerValue);
        
        invokeInstance.inputHttpHeaders_x = new Map<String, String>();
        invokeInstance.inputHttpHeaders_x.put('Authorization', authorizationHeader);
        
        SOAPdocsOasisOpenOrgWss200401Oasis20.UsernameToken creds=new SOAPdocsOasisOpenOrgWss200401Oasis20.UsernameToken();
        creds.Username=serviceObj.Username__c; //'css_soa';
        creds.Password=serviceObj.Password__c; //'P1%gruzA';
        SOAPdocsOasisOpenOrgWss200401Oasis20.Security_element security_ele=new SOAPdocsOasisOpenOrgWss200401Oasis20.Security_element();
        security_ele.UsernameToken=creds;
        invokeInstance.SecurityPart=security_ele;
        SOAPCSSServiceResponse.getStatementsHistoryResponseType statementResponse_elementinstance = invokeInstance.getStatementsHistory(HeadInstance,statementRequestRecordTypeinstance);
        system.debug('statementResponse_elementinstance-->'+statementResponse_elementinstance);
        return statementResponse_elementinstance;
        }  
        
     public override object convertResponseToOutput(object response, map<string, string> responseHeader, ServiceStatus serviceStatus){
       if(response==null)
       return null;
       SOAPCSSServiceResponse.getStatementsHistoryResponseType respstmt = (SOAPCSSServiceResponse.getStatementsHistoryResponseType)response; 
       SOAPCSSServiceResponse.getStatementsHistoryResponseTypeCollection[] lrc =  respstmt.getStatementsHistoryResponseTypeCollection;
       GetStatementHistoryOutput[] gsh = new List<GetStatementHistoryOutput>();
       if(lrc!=Null){ 
       for(SOAPCSSServiceResponse.getStatementsHistoryResponseTypeCollection c : lrc)
       {                    
           GetStatementHistoryOutput gs = new GetStatementHistoryOutput();
           gs.StatementSerno = c.StatementSerno;
           gs.GenerateDate = c.GenerateDate;
           gs.ClosingBalance = c.ClosingBalance;
           gs.MindueAmount = c.MindueAmount;
           gs.TotalPayments = c.TotalPayments;
           gs.OverdueAmount = c.OverdueAmount;
           gs.Overduecycles = c.Overduecycles;
           gs.Duedate = c.Duedate;
           gs.OpeningBalance = c.OpeningBalance;
           gs.TotalDebits = c.TotalDebits;
           gs.AmtTrxn = c.AmtTrxn;
           gs.TrxnDate = c.TrxnDate;
           gs.TrxnDate2 = c.TrxnDate2;
           gsh.add(gs);
           
           }
       }
          return gsh;
    }
            

}