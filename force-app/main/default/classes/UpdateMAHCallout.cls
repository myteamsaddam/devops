public class UpdateMAHCallout extends SoapIO{
    SOAupdateMAHRequest.UpdateCustomerNameAddressRequestRecordType updateMAHinstance;
    String InstitutionId;
    public  void convertInputToRequest(){
        UpdateMAHRequest inputs =  (UpdateMAHRequest)serviceInput;
        updateMAHinstance = new SOAupdateMAHRequest.UpdateCustomerNameAddressRequestRecordType();
        updateMAHinstance.Reference = inputs.Reference;
        updateMAHinstance.CustomerID = inputs.CustomerID ;
        updateMAHinstance.NewIndicator = inputs.NewIndicator;
        updateMAHinstance.SSN = inputs.SSN;
        updateMAHinstance.Customer = new SOAupdateMAHRequest.CustomerSection();
        updateMAHinstance.Customer.CustomerName = inputs.CustomerName;
        
        InstitutionId = inputs.InstitutionId;        
    }
    public  override object invokeWebserviceCallout(SoapRequest soapRequest){
        SOAupdateMAHTypelib10.headerType HeadInstance = new SOAupdateMAHTypelib10.headerType();
        HeadInstance.MsgId = 'FinalCAll';
        HeadInstance.CorrelationId='1';
        HeadInstance.RequestorId='Salesforce';
        HeadInstance.SystemId='1';
        HeadInstance.InstitutionId=InstitutionId;
        
        SOAupdateMAHCssservice.x_xsoap_CSSServiceESB_CSSServicePT invokeInstance = new SOAupdateMAHCssservice.x_xsoap_CSSServiceESB_CSSServicePT();
        invokeInstance.timeout_x=30000;
        
        ServiceSettings__c serviceObj = ServiceSettings__c.getValues('UpdateMAH');
        if(serviceObj!=null && serviceObj.EndPoint__c!='' && serviceObj.EndPoint__c!=null)
            invokeInstance.endpoint_x = serviceObj.EndPoint__c;
        
        String username = serviceObj.Username__c; 
        String password = serviceObj.Password__c;
        
        Blob headerValue = Blob.valueOf(username + ':' + password);
        String authorizationHeader = 'BASIC ' +EncodingUtil.base64Encode(headerValue);
        
        invokeInstance.inputHttpHeaders_x = new Map<String, String>();
        invokeInstance.inputHttpHeaders_x.put('Authorization', authorizationHeader);
        
        SOAupdateMAHOasis20.UsernameToken creds=new SOAupdateMAHOasis20.UsernameToken();
        creds.Username=serviceObj.Username__c; //'css_soa';//'evry_access';
        creds.Password=serviceObj.Password__c;//'P1%gruzA';//'9oKuwQioQ4';
        SOAupdateMAHOasis20.Security_element security_ele=new SOAupdateMAHOasis20.Security_element();
        security_ele.UsernameToken=creds;
        invokeInstance.SecurityPart=security_ele;
        
        SOAupdateMAHResponse.UpdateCustomerNameAddressResponseRecordType updaddcustnmResponse_elementinstance = invokeInstance.updateCustomerNameAddress(HeadInstance,updateMAHinstance);
        system.debug('updaddcustnmResponse_elementinstance-->'+updaddcustnmResponse_elementinstance);
        return updaddcustnmResponse_elementinstance;
    }
    
    public override object convertResponseToOutput(object response, map<string, string> responseHeader, ServiceStatus serviceStatus){
        if(response==null)
            return null;
        SOAupdateMAHResponse.UpdateCustomerNameAddressResponseRecordType respstmt = (SOAupdateMAHResponse.UpdateCustomerNameAddressResponseRecordType)response;
        SOAupdateMAHResponse.Result rr = respstmt.Result;
        UpdateMAHResponse op = new UpdateMAHResponse();
        
        if(rr.Address!=null)
            op.AddressSerno = Integer.valueOf(rr.Address.AddressSerno);
            op.Code = rr.Code;
        op.Description = rr.Description;
        if(rr.ErrorDetails!=null)
            op.ErrorResult = rr.ErrorDetails.ErrorResult;
        return op;
    } 
}