@isTest 
private class CTICasecreation_Test{

      public static testmethod void testMethod1() {
          
      Account accountObj=new Account(
      Name='Test Account', 
      Institution_Id__c=2, 
      Customer_Serno__c='1123123'
      );
      insert accountObj;
      
      Blob hash = Crypto.generateDigest('MD5', Blob.valueOf('0000000061'));
      string encryptSSN=EncodingUtil.convertToHex(hash);
      Contact contactObj =new Contact(
      LastName='Test Contact', 
      FirstName='First Test Contact',
      SerNo__c='2123123', 
      Institution_Id__c=2, 
      SSN__c='0000000061',
      Encrypted_SSN__c=encryptSSN,
      AccountId=accountObj.Id,
      Phone='123123123', 
      Fax='98789879', 
      MobilePhone='98789879', 
      HomePhone='123123123', 
      Email='testemail@test.com'
      );
      insert contactObj;
      
      Product_Custom__c prodobj=new Product_Custom__c(
      Name='Test Product',
      Pserno__c='56789'
      );
      insert prodobj;
      
      Financial_Account__c finAccObj=new Financial_Account__c(
      Account_Number__c='12w23eeeeedd', 
      Customer__c=accountObj.Id, 
      Account_Serno__c='3123123', 
      Institution_Id__c=2,
      Customer_Serno__c=contactObj.SerNo__c, 
      Product__c=prodobj.Id,
      Product_Serno__c=prodobj.Pserno__c
      );
      insert finAccObj;
        
      Card__c cardObj=new Card__c(
      People__c=contactObj.Id,
      PrimaryCardFlag__c=true,
      Financial_Account__c=finAccObj.Id, 
      Card_Serno__c='524858519', 
      Card_Number_Truncated__c='7136785481583561', 
      Financial_Account_Serno__c=finAccObj.Id, 
      People_Serno__c=contactObj.Id,
      Prod_Serno__c=prodobj.Pserno__c, 
      Product__c=prodobj.Id,
      Institution_Id__c=2
      );
      insert cardObj;
      
      Case caseObj=new Case(
      ContactId=contactObj.Id, 
      AccountId=accountObj.Id, 
      Card__c=cardObj.Id,
      Financial_Account__c=finAccObj.Id, 
      Category__c='Account closure', 
      Origin='Web',
      status='Closed'
      );
      insert caseObj;
      
       Common_Settings__c commansetting =new Common_Settings__c(
      Name='CTI',
      CTI_Wrap_Time__c=0
      );
      insert commansetting;
      
      task taskobj1 =new task(whoId=contactObj.Id, 
      whatId=caseObj.Id, 
      subject='subject1', 
      CallObject='VOICE.33590924.1490261303651.00558000000wi0AAAQ1', 
      cnx__UniqueId__c='19781', 
      ANI__c='19781', 
      Call_Type__c='inbound', 
      status='Completed',
      CallDurationInSeconds=121);
      insert taskobj1;
      
      task taskobj =new task(
      whatId=caseObj.Id, 
      subject='subject', 
      CallObject='VOICE.33590924.1490261303651.00558000000wi0AAAQ', 
      cnx__UniqueId__c='1978', 
      ANI__c='1978', 
      Call_Type__c='inbound', 
      status='Completed',
      CallDurationInSeconds=12);
      insert taskobj;
      
      
      
      }

}