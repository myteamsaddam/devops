public class GetCustomerRightsCallout extends SoapIO {
    CustomerRightsRequest.getRightToAccessDetailsRequestBodyType customerRightInsatnce;
    GetCustomerRightsInputs inputs;
    
    Public  void convertInputToRequest(){
        inputs = (GetCustomerRightsInputs)serviceInput;
        customerRightInsatnce = new CustomerRightsRequest.getRightToAccessDetailsRequestBodyType();
        customerRightInsatnce.AccountNumber = inputs.AccountNumber;
        customerRightInsatnce.InstitutionId = inputs.InstitutionId;
        system.debug('----customerRightInsatnce---'+customerRightInsatnce.AccountNumber);
        system.debug('----customerRightInsatnce---'+customerRightInsatnce.InstitutionId);
    }
    
    Public  override object invokeWebserviceCallout(SoapRequest soapRequest){
        CustomerRightsType.headerType HeadInstance = new CustomerRightsType.headerType();
        HeadInstance.MsgId = 'SFDC consuming Get Consent SOA service. Time Stamp=>'+system.now();
        HeadInstance.CorrelationId=Common_Settings__c.getValues('CorrelationId').Common_Value__c;
        HeadInstance.RequestorId=Common_Settings__c.getValues('RequestorId').Common_Value__c;
        HeadInstance.SystemId=Common_Settings__c.getValues('SystemId').Common_Value__c;
        HeadInstance.InstitutionId = inputs.InstitutionId;
        
        CustomerRightsService.CustomerRightsServiceBindingPort invokeInstance = new CustomerRightsService.CustomerRightsServiceBindingPort();
        invokeInstance.timeout_x = 45000;
        
        ServiceSettings__c serviceObj = ServiceSettings__c.getValues('CustomerRights');
        if(serviceObj!=null && serviceObj.EndPoint__c!='' && serviceObj.EndPoint__c!=null)
            invokeInstance.endpoint_x = serviceObj.EndPoint__c;
        String username = serviceObj.Username__c;
        String Password = serviceObj.Password__c;
        
        Blob headerValue = Blob.valueOf(username + ':' + password);
        String authorizationHeader = 'BASIC ' +EncodingUtil.base64Encode(headerValue);
        
        invokeInstance.inputHttpHeaders_x = new Map<String, String>();
        invokeInstance.inputHttpHeaders_x.put('Authorization', authorizationHeader);
        
        CustomerRightsOasis20.UsernameToken cred = new CustomerRightsOasis20.UsernameToken();
        cred.Username = serviceObj.Username__c;
        cred.Password = serviceObj.Password__c;
        
        CustomerRightsOasis20.Security_element security = new CustomerRightsOasis20.Security_element();
        security.UsernameToken = cred;
        invokeInstance.securityPart = security;
        
        system.debug('----security_ele--'+security);
        
        CustomerRightsResponse.getRightToAccessDetailsResponseType customerRightsoutput;
        customerRightsoutput = invokeInstance.getRightToAccessDetails(HeadInstance, customerRightInsatnce);
        system.debug('---customerRightsoutput-----'+customerRightsoutput);
        return customerRightsoutput;
    }
    
    Public override object convertResponseToOutput(object response, map<string, string> responseHeader, ServiceStatus serviceStatus){
        if(response == null)
            return null;
        CustomerRightsResponse.getRightToAccessDetailsResponseType customerResponse = (CustomerRightsResponse.getRightToAccessDetailsResponseType)response;
        CustomerRightsResponse.getRightToAccessDetailsResponseBodyType customerRespsonseInstance = customerResponse.GetRightToAccessDetailsResponseBody;
        System.debug('----customerRespsonseInstance---'+customerRespsonseInstance);
        GetCustomerRightsOutputs custOutputs = new GetCustomerRightsOutputs();
        if(customerRespsonseInstance != null){
            custOutputs.RightToAccessDetails = customerRespsonseInstance.RightToAccessDetails;
        }
        System.debug('----custOutputs-----'+custOutputs);
        return custOutputs;
    }
}