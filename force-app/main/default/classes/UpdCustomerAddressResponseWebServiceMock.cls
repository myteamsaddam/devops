@isTest 
global class UpdCustomerAddressResponseWebServiceMock implements WebServiceMock {
    global void doInvoke( Object stub, Object request, Map<String, Object> response, String endpoint, String soapAction, String requestName, String responseNS, String responseName, String responseType) 
    { 
     SOAPCSSServiceResponse.UpdateCustomerNameAddressResponse_element respEle =new SOAPCSSServiceResponse.UpdateCustomerNameAddressResponse_element ();
     SOAPCSSServiceResponse.UpdateCustomerNameAddressResponseRecordType  res1 = new SOAPCSSServiceResponse.UpdateCustomerNameAddressResponseRecordType ();
     res1.Header=new SOAPschemasEntercardComTypelib10.headerType();
     res1.Result=new SOAPCSSServiceResponse.Result();
     res1.Result.Code='1';
     res1.Result.Description='TestDesc'; 
     res1.Result.ErrorDetails=new SOAPCSSServiceResponse.ArrayOfErrorResult();  
     res1.Result.ErrorDetails.ErrorResult=new list<SOAPCSSServiceResponse.ErrorResult>();  
     res1.Result.Address=new SOAPCSSServiceResponse.AddressSerno();
     res1.Result.Address.AddressSerno='123';
     respEle.UpdateCustomerNameAddressResponseRecord =res1;   
     response.put('response_x', respEle); 
    } 
    }