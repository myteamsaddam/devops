@isTest
public class TestConsentDataController
{
    @testSetup
    public static void testInsertData2() 
    {
     Account accountObj=new Account(
      Name='Test Account', 
      Institution_Id__c=2, 
      Customer_Serno__c='1123123'
      );
      insert accountObj;
      
      Blob hash = Crypto.generateDigest('MD5', Blob.valueOf('0000000061'));
      string encryptSSN=EncodingUtil.convertToHex(hash);
      Contact contactObj =new Contact(
      LastName='Test Contact', 
      FirstName='First Test Contact',
      SerNo__c='2123123', 
      Institution_Id__c=2, 
      SSN__c='0000000061',
      Encrypted_SSN__c=encryptSSN,
      AccountId=accountObj.Id,
      Phone='123123123', 
      Fax='98789879', 
      MobilePhone='98789879', 
      HomePhone='123123123', 
      Email='testemail@test.com'
      );
      insert contactObj;
      
      Blob hash2 = Crypto.generateDigest('MD5', Blob.valueOf('0000000062'));
      string encryptSSN2=EncodingUtil.convertToHex(hash2);
      Contact contactObj1 =new Contact(
      LastName='1Test Contact', 
      FirstName='1First Test Contact',
      SerNo__c='12123123', 
      Institution_Id__c=2, 
      SSN__c='0000000062',
      Encrypted_SSN__c=encryptSSN2,
      AccountId=accountObj.Id,
      Phone='1123123123', 
      Fax='198789879', 
      MobilePhone='198789879', 
      HomePhone='1123123123', 
      Email='1testemail@test.com'
      );
      insert contactObj1;
        
      Product_Custom__c prodobj=new Product_Custom__c(
      Name='Test Product',
      Pserno__c='56789'
      );
      insert prodobj;
      
      Financial_Account__c finAccObj=new Financial_Account__c(
      Account_Number__c='12w23eeeeedd', 
      Customer__c=accountObj.Id, 
      Account_Serno__c='3123123', 
      Institution_Id__c=2,
      Customer_Serno__c=contactObj.SerNo__c, 
      Product__c=prodobj.Id,
      Product_Serno__c=prodobj.Pserno__c
      );
      insert finAccObj;
        
      Card__c cardObj=new Card__c(
      People__c=contactObj.Id,
      PrimaryCardFlag__c=true,
      Financial_Account__c=finAccObj.Id, 
      Card_Serno__c='524858519', 
      Card_Number_Truncated__c='7136785481583561', 
      Financial_Account_Serno__c=finAccObj.Id, 
      People_Serno__c=contactObj.Id,
      Prod_Serno__c=prodobj.Pserno__c, 
      Product__c=prodobj.Id,
      Institution_Id__c=2
      );
      insert cardObj;
      
      Case caseObj=new Case(
      ContactId=contactObj.Id, 
      AccountId=accountObj.Id, 
      Card__c=cardObj.Id,
      Financial_Account__c=finAccObj.Id, 
      Category__c='Account closure', 
      Origin='Web'
      );
      insert caseObj;
        
        //GlobalSettings__c
        List<GlobalSettings__c>GlobalSettingsList=new List<GlobalSettings__c>();
        GlobalSettings__c objGlobal = new GlobalSettings__c();
        objGlobal.Name = 'ServiceStrategies'; 
        objGlobal.Value__c = 'SOAPService';
        GlobalSettingsList.add(objGlobal);
        
        insert GlobalSettingsList;
        
         List<ServiceHeaders__c> serviceHeadersList =new List<ServiceHeaders__c>();
        //ServiceHeaders
        ServiceHeaders__c objServiceHeaders = new ServiceHeaders__c();
        objServiceHeaders.name = 'GetConsentHeader';
        serviceHeadersList.add(objServiceHeaders);
        
        ServiceHeaders__c objServiceHeaders2 = new ServiceHeaders__c();
        objServiceHeaders2.name = 'UpdateConsentHeader';
        serviceHeadersList.add(objServiceHeaders2);
       
        insert serviceHeadersList;
        
        //ServiceSettings
        List<ServiceSettings__c> serviceSettingList = new List<ServiceSettings__c>();
       
        ServiceSettings__c objService = new ServiceSettings__c();
        objService.name = 'GetConsent';
        objService.HeaderName__c = 'GetConsentHeader'; 
        objService.EndPoint__c = 'https://services-test.entercard.com/gateway/services/CSSService?wsdl';
        objService.OutputClass__c = 'GetConsentServiceOutput';
        objService.ProcessingClass__c = 'GetConsentServiceCallout';
        objService.Strategy__c = 'SOAPService';
        objService.LogRequest__c = false;
        objService.LogResponse__c = false;
        objService.LogWithCallout__c = false;
        objService.Input_Class__c = 'GetConsentServiceInput';
        objService.Username__c='Username';
        objService.Password__c='Password';
        serviceSettingList.add(objService);
        
        ServiceSettings__c objService1 = new ServiceSettings__c();
        objService1.name = 'UpdateConsent';
        objService1.HeaderName__c = 'UpdateConsentHeader'; 
        objService1.EndPoint__c = 'https://services-test.entercard.com/gateway/services/CSSService?wsdl';
        objService1.OutputClass__c = 'UpdateConsentServiceOutput';
        objService1.ProcessingClass__c = 'UpdateConsentServiceCallout';
        objService1.Strategy__c = 'SOAPService';
        objService1.LogRequest__c = false;
        objService1.LogResponse__c = false;
        objService1.LogWithCallout__c = false;
        objService1.Input_Class__c = 'UpdateConsentServiceInput';
        objService1.Username__c='Username';
        objService1.Password__c='Password';
        serviceSettingList.add(objService1);
        insert serviceSettingList;
        
        List<Common_Settings__c> commonSettingList =new List<Common_Settings__c>();
        Common_Settings__c commonSetting =new Common_Settings__c(
        Name='CorrelationId',
        Common_Value__c='1'
        );
        commonSettingList.add(commonSetting);
        Common_Settings__c commonSetting1 =new Common_Settings__c(
        Name='RequestorId',
        Common_Value__c='Salesforce'
        );
        commonSettingList.add(commonSetting1);
        Common_Settings__c commonSetting2 =new Common_Settings__c(
        Name='SystemId',
        Common_Value__c='1'
        );
        commonSettingList.add(commonSetting2);
        insert commonSettingList;
        
        ErrorCodes__c errorCodes =new ErrorCodes__c();
        errorCodes.Name='0';
        errorCodes.ErrorDescription__c='ErrorDescription';
        errorCodes.UIDisplayMessage__c='UIDisplayMessage';
        insert errorCodes;
        
        ErrorCodes__c errorCodes1 =new ErrorCodes__c();
        errorCodes1.Name='defaultError';
        errorCodes1.ErrorDescription__c='ErrorDescription';
        errorCodes1.UIDisplayMessage__c='UIDisplayMessage';
        insert errorCodes1;
        
    }
    
    public static testmethod void testmethod1()
    {
        Contact conObj=[SELECT Id, SerNo__c, Institution_Id__c FROM Contact limit 1];
        Test.setMock(WebServiceMock.class, new GetConsentWebServiceMockclass());
        PageReference pageRef = Page.PeopleConsentData;
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('Id',conObj.id);
        Test.startTest();
        ConsentDataController objConsentCon = new ConsentDataController();
        objConsentCon.isSuccess=true;
        objConsentCon.loadConsents();
        objConsentCon.getHistoricalConsents();
        
        objConsentCon.fromDate='testdate';
        objConsentCon.toDate='testdate';
        objConsentCon.getHistoricalConsents();
        
        objConsentCon.fromDate='2017-04-18';
        objConsentCon.toDate='2016-04-18';
        objConsentCon.getHistoricalConsents();
        
        objConsentCon.fromDate='2014-04-18';
        objConsentCon.toDate='2017-04-18';
        objConsentCon.getHistoricalConsents();
        
        objConsentCon.fromDate='2017-01-18';
        objConsentCon.toDate='2017-04-18';
        objConsentCon.getHistoricalConsents();
        
        Test.StopTest();
    }
    
    public static testmethod void testmethod2()
    {
        Financial_Account__c financialAcc= [SELECT Id, Account_Serno__c, Institution_Id__c FROM Financial_Account__c limit 1];
        PageReference pageRef = Page.PeopleConsentData;
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('Id',financialAcc.id);
        Test.startTest();
        ConsentDataController objConsentCon = new ConsentDataController();
        objConsentCon.caseObj =new Case();
        ApexPages.StandardController sc = new ApexPages.StandardController(financialAcc);
        ConsentDataController stdCon = new ConsentDataController(sc);
        objConsentCon.consentDataConInstance =new ConsentDataController();
        ConsentDataController contlr =objConsentCon.consentDataConInstance;
        Test.StopTest();
    }
    
    public static testmethod void testmethod3()
    {
        Financial_Account__c financialAcc= [SELECT Id, Account_Serno__c, Institution_Id__c FROM Financial_Account__c limit 1];
        Test.setMock(WebServiceMock.class, new UpdateConsentWebServiceMockclass());
        PageReference pageRef = Page.PeopleConsentData;
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('Id',financialAcc.id);
        Test.startTest();
        ConsentDataController objConsentCon = new ConsentDataController();
        objConsentCon.selectedConsentCodeSet=new set<string>();
        objConsentCon.selectedConsentCodeSet.add('Accept Nemkonto');
        objConsentCon.selectedConsentCodeSet.add('test111');
        objConsentCon.consentList=new List<string>();
        objConsentCon.consentList.add('Accept Nemkonto');
        objConsentCon.consentList.add('Accept Email');
        objConsentCon.consentList.add('Test');
        objConsentCon.updateConsentData();
        objConsentCon.selectedConsentCodeSet=new set<string>();
        objConsentCon.consentList=new List<string>();
        objConsentCon.updateConsentData();
         
        ConsentDataController.ConsentWrapper conWrapper =new ConsentDataController.ConsentWrapper('2','123','1','test','test',system.now(),'test','test');
        ConsentDataController.ConsentWrapper conWrapper1 =new ConsentDataController.ConsentWrapper('2','123','2','test','test',system.now(),'test','test');
        ConsentDataController.ConsentWrapper wrppr =new ConsentDataController.ConsentWrapper();
        Test.StopTest();
    }
}