/**********************************************************************
Name: EC_Case_CustomLogActivityController
=======================================================================
Purpose: This class is used update the case with related Financial account, customer and card data in salesforce  User Story -SFD-1584

======================================================================= 
History
-----------------------------------------------------------------------         
Version     Author                   Date              Detail
1.0         Saddaam Hussain         1-Jul-2020       Initial Version

**********************************************************************/
public with sharing class EC_CaseLogActivity {
    /**********************************************************************
*   Author: Saddam Hussain
*   Date:     1-Jul-2020
*   User Story : SFD-1584
*   Param: None
*   Return: Map    
*   Description: In this method we are trying to get the Financial Account record for customer.
*   @Revision(s): [Date] - [Change Reference] - [Changed By] - [Description]   

**********************************************************************/
    @AuraEnabled(cacheable=true)
    Public Static Map<String,String> getFinancialAccounts(Id custId){
        System.debug('-----conId---'+custId);
        Map<String, String> finOpionMap =  new Map<String, String>();
        Set<Id> financialAccIdSet = new Set<Id>();
        List<Card__c> cardList = [SELECT Id, Name, People__c, Financial_Account__c 
                                  FROM Card__c 
                                  WHERE People__c=:custId 
                                  AND Financial_Account__c!=null 
                                  ORDER BY CreatedDate DESC Limit 50000];
        for(Card__c cardObj:cardList)
        {
            financialAccIdSet.add(cardObj.Financial_Account__c);
        }
        
        List<Financial_Account__c> financialAccList = [SELECT Id, Name, Customer__c, Account_Number__c,Account_Serno__c,
                                                       Product__c,Product__r.name FROM Financial_Account__c 
                                                       WHERE Id IN:financialAccIdSet 
                                                       ORDER BY CreatedDate DESC Limit 50000];
        
        for(Financial_Account__c finObj : financialAccList){
            finOpionMap.put(finObj.Id,finObj.Account_Number__c+' : '+(finObj.Product__r.name!=null?finObj.Product__r.name:'N/A'));
            finOpionMap.put(EC_StaticConstant.OTHER_VALUE,EC_StaticConstant.OTHER_VALUE);
        }
        return finOpionMap;
    }
    
    /**********************************************************************
*   Author: Saddam Hussain
*   Date:     1-Jul-2020
*   User Story : SFD-1584
*   Param: None
*   Return: Map    
*   Description: In this method we are trying to get the card record related to Financial Account.

**********************************************************************/
    
    @AuraEnabled(cacheable=true)
    Public Static Map<String, String> getCards(String financialId, Id custId){
        Map<String, String> cardMap = new Map<String, String>();
        List<Card__c> cardList = [SELECT Id, Name, People__c, Financial_Account__c,
                                  Card_Number_Truncated__c,Product__r.name,GeneralStatus__c 
                                  FROM Card__c 
                                  WHERE People__c=:custId 
                                  AND Financial_Account__c=:financialId 
                                  ORDER BY ExpiryDate__C DESC Limit 50000];       
        system.debug('---financialVal-'+financialId);
        
        if(cardList.size() == 1){
            cardMap.put(cardList[0].Id,cardList[0].Card_Number_Truncated__c+' : '
                        +(cardList[0].GeneralStatus__c!=null?cardList[0].GeneralStatus__c:'N/A')
                       );
        } else {
            for(Card__c cardObj:cardList)
            {
                cardMap.put(cardObj.id , cardObj.Card_Number_Truncated__c+' : '
                            +(cardObj.GeneralStatus__c!=null?cardObj.GeneralStatus__c:'N/A'));
            }
        }
        if(financialId == EC_StaticConstant.OTHER_VALUE){
            cardMap.put(EC_StaticConstant.OTHER_VALUE,EC_StaticConstant.OTHER_VALUE); 
        }
        return cardMap;
    }
    
    /**********************************************************************
*   Author: Saddam Hussain
*   Date:     1-Jul-2020
*   User Story : SFD-1584
*   Param: None
*   Return: Sting    
*   Description: In this method we are trying to update the case with customer and related financial account, card
also created the task.
**********************************************************************/
    
    @AuraEnabled
    public static string doCaseUpdate(String casId, String custId, String finId, 
                                      String cardId, String commnt, String catgry,
                                      String saveAtmp, String saved, String reason, 
                                      String offer, String type) {
                                          
                                          String msg = '';
                                          try
                                          {
                                              system.debug('---'+SaveAtmp+'--'+Saved+'--'+Reason+'--'+Catgry);
                                              if((finId == EC_StaticConstant.NONE_VALUE 
                                                  || String.isBlank(finId) 
                                                  || finId == EC_StaticConstant.OTHER_VALUE 
                                                  || catgry == null 
                                                  || catgry == EC_StaticConstant.NONE_VAL)
                                                 || (catgry == EC_StaticConstant.CATEGORY_ACCOUNT_CLOSURE 
                                                     && (String.isBlank(saveAtmp) 
                                                         || String.isBlank(saved) 
                                                         || String.isBlank(reason) 
                                                         || String.isBlank(offer) 
                                                         || finId == EC_StaticConstant.OTHER_VALUE))
                                                ) {
                                                    if(finId == EC_StaticConstant.NONE_VALUE 
                                                       || String.isBlank(finId) 
                                                       || finId == EC_StaticConstant.OTHER_VALUE
                                                      ) {
                                                          msg = System.Label.Financial_Value;
                                                      }
                                                    If(catgry == null 
                                                       || catgry == EC_StaticConstant.NONE_VAL
                                                      ) {
                                                          msg = System.Label.Category_not_select_msg;
                                                      }
                                                    system.debug('---'+saveAtmp+'--'+saved+'--'+reason+'--'+offer);
                                                    if(catgry == EC_StaticConstant.CATEGORY_ACCOUNT_CLOSURE) {
                                                        if(String.isBlank(saveAtmp)) {
                                                            msg = EC_StaticConstant.RETENTION_SAVEATTEMPT;
                                                        }
                                                        if(String.isBlank(saved)) {
                                                            msg = EC_StaticConstant.RETENTION_SAVED;
                                                        }   
                                                        if(String.isBlank(reason)) {
                                                            msg= EC_StaticConstant.RETENTION_REASON;
                                                        }  
                                                        if(String.isBlank(offer)) {
                                                            msg = EC_StaticConstant.RETENTION_OFFER;
                                                        }  
                                                        if(finId == EC_StaticConstant.OTHER_VALUE) {
                                                            msg = System.Label.Financial_Value;
                                                        }
                                                        
                                                    }	
                                                    return msg;
                                                }
                                              
                                              system.debug('--FinId-'+finId+'--Catgry-'+catgry+'---CardId---'+cardId);
                                              
                                              if(finId != EC_StaticConstant.NONE_VALUE 
                                                 && finId != EC_StaticConstant.OTHER_VALUE 
                                                 && catgry != null
                                                ) {
                                                    Card__c[] cardList = [SELECT Id, Card_Serno__c,People_Serno__c,
                                                                          Financial_Account_Serno__c,GeneralStatus__c, 
                                                                          People__r.Account.Customer_Serno__c, 
                                                                          People__r.SSN__c, People__r.Institution_Id__c 
                                                                          FROM Card__c 
                                                                          WHERE People__c =: custId 
                                                                          AND Id =: cardId]; 
                                                    if((catgry != null 
                                                        || catgry != EC_StaticConstant.NONE_VAL)
                                                       &&(finId != EC_StaticConstant.NONE_VALUE 
                                                          || String.isNotBlank(finId) 
                                                          || finId != EC_StaticConstant.OTHER_VALUE)
                                                       && cardId != null
                                                      ) {
                                                          Case cse = new Case();
                                                          cse.Id = casId;
                                                          cse.Category__c = catgry;
                                                          cse.ContactId = custId;
                                                          cse.Financial_Account__c = finId;
                                                          cse.Card__c = cardId;
                                                          
                                                          update cse;
                                                          msg = EC_StaticConstant.CASE_UPDATED_MESSAGE;
                                                      }
                                                    
                                                    if(catgry != null 
                                                       && String.isNotBlank(commnt)
                                                      ) {
                                                          Task ts = new Task();
                                                          ts.Subject = EC_StaticConstant.TASK_MEMO_SUBJECT;
                                                          ts.Category__c = catgry;
                                                          ts.Type = type;
                                                          ts.Status = EC_StaticConstant.TASK_COMPLETE_STATUS;
                                                          ts.Description = commnt;
                                                          ts.WhatId = casId;
                                                          if(commnt != null 
                                                             && commnt.length()>255
                                                            ) {
                                                                ts.Comments__c = commnt.substring(0,254);  
                                                            } else {
                                                                ts.Comments__c =  commnt;
                                                            }
                                                          ts.WhoId = custId;
                                                          if(FinId != EC_StaticConstant.OTHER_VALUE){	
                                                              ts.Account_Serno__c = cardList[0].Financial_Account_Serno__c;
                                                              ts.Customer_Serno__c = cardList[0].People_Serno__c;
                                                              ts.Card_Serno__c = cardList[0].Card_Serno__c;
                                                              ts.MAH_Serno__c = cardList[0].People__r.Account.Customer_Serno__c;
                                                          }
                                                          insert ts;
                                                          msg = EC_StaticConstant.TASK_CREATED_MESSAGE;
                                                      }
                                                    
                                                    system.debug('-SaveAtmp-'+saveAtmp+'-'+saved+'-'+reason+'-'+offer);
                                                    If(catgry == EC_StaticConstant.CATEGORY_ACCOUNT_CLOSURE 
                                                       && (String.isNotBlank(saveAtmp) 
                                                           || String.isNotBlank(saved) 
                                                           || String.isNotBlank(reason) 
                                                           || String.isNotBlank(offer))
                                                      ) {
                                                          Retention_Response__c retentionObj = new Retention_Response__c();
                                                          retentionObj.Financial_Account__c = finId;
                                                          retentionObj.Person__c = custId;
                                                          retentionObj.Save_Attempt__c = saveAtmp;
                                                          retentionObj.Account_Retained__c = saved;
                                                          retentionObj.Account_Closed__c = (saved == EC_StaticConstant.YES_VALUE ? 
                                                                                            EC_StaticConstant.NO_VALUE
                                                                                            :EC_StaticConstant.YES_VALUE
                                                                                           );
                                                          retentionObj.Reason_for_Account_Closure__c = reason;
                                                          retentionObj.Offer__c = offer;
                                                          
                                                          insert retentionObj;
                                                      }
                                                    // Below method is used to make the callout to Prime to create memo.
                                                    Contact[] contactList = [SELECT id,Institution_Id__c,SSN__c 
                                                                             FROM Contact 
                                                                             WHERE id=:custId];
                                                    system.debug('-conlst--'+contactList);
                                                    if(contactList!= null 
                                                       && contactList.size()>0 
                                                       && contactList[0].Institution_Id__c!=null
                                                      ) {
                                                          CustomerMemoExportDataOutput outputmemoexportdata = 
                                                              CSSServiceHelper.updateCustomerMemo('NewMemo','PersonSSN',
                                                                                                  contactList[0].SSN__c,'css',commnt,
                                                                                                  String.valueOf(
                                                                                                      contactList[0].Institution_Id__c
                                                                                                  )
                                                                                                 );
                                                          system.debug('--outputmemoexportdata---'+outputmemoexportdata);
                                                      }
                                                }
                                              
                                          }catch(Exception exp){
                                              StatusLogHelper.logSalesforceError('EC_CaseLogActivity', 
                                                                                 'E005', 'doCaseUpdate', exp, false);
                                          }
                                          return msg;
                                      }
}