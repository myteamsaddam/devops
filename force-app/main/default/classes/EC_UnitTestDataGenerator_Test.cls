@isTest
public class EC_UnitTestDataGenerator_Test {
    
    /**********************************************************************************************
* @Author:      Priyanka Singh
* @Date:        
* @Description: test Account data generation methods
* @Revision(s): [Date] - [Change Reference] - [Changed By] - [Description]  
***********************************************************************************************/
    public static testMethod void Account_test() {
        System.runAs(EC_UnitTestDataGenerator.adminUser) {
            Account testAccount1 = EC_UnitTestDataGenerator.TestAccount.build(new Map<String, Object>{            
                'Institution_Id__c'=>2 ,
                    'Customer_Serno__c'=>'123124',
                    'Name'=>'Test  Account'
                    });
            insert testAccount1;
            
            Account testAccount2 = EC_UnitTestDataGenerator.TestAccount.buildInsert(new Map<String, Object>{            
                'Institution_Id__c'=>2 ,
                    'Customer_Serno__c'=>'113124',
                    'Name'=>'Test  Account'
                    });  
            
            Account testAccount3 = EC_UnitTestDataGenerator.TestAccount.buildInsert();      
            
            List<String> attrs = new List<String>{'Name','Institution_Id__c','Customer_Serno__c'};
                testAccount1 = EC_UnitTestDataGenerator.TestAccount.reload(testAccount1, attrs);
            
            List<Account> accountList = new List<Account>();
            accountList.add(testAccount1);
            accountList = EC_UnitTestDataGenerator.TestAccount.reloadList(accountList, attrs);
            
            List<Account> resultList = [SELECT Id
                                        FROM Account];
            System.assertEquals(3,resultList.size());
        }
    }
    /**********************************************************************************************
* @Author:      Priyanka Singh
* @Date:        
* @Description: test Contact data generation methods
* @Revision(s): [Date] - [Change Reference] - [Changed By] - [Description]  
***********************************************************************************************/
    public static testMethod void Contact_test() {
        System.runAs(EC_UnitTestDataGenerator.adminUser) {
            Contact testContact1 = EC_UnitTestDataGenerator.TestContact.build(new Map<String, Object>{
                'LastName' => 'Test Contact1',
                    'SerNo__c'=>'098991',
                    'SSN__c'=>'0000000060'
                    });
            insert testContact1;
            
            Contact testContact2 = EC_UnitTestDataGenerator.TestContact.buildInsert(new Map<String, Object>{
                'LastName' => 'Test Contact2',
                    'SerNo__c'=>'098992',
                    'SSN__c'=>'0000000062'
                    });  
            
            Contact testContact3 = EC_UnitTestDataGenerator.TestContact.buildInsert();     
            
            List<String> attrs = new List<String>{'LastName'};
                testContact1 = EC_UnitTestDataGenerator.TestContact.reload(testContact1, attrs);  
            
            List<Contact> contactList = new List<Contact>();
            contactList.add(testContact1);
            contactList = EC_UnitTestDataGenerator.TestContact.reloadList(contactList, attrs);
            
            List<Contact> resultList = [SELECT Id
                                        FROM Contact];
            System.assertEquals(3,resultList.size()); 
        }
    }
    
    /**********************************************************************************************
* @Author:      Saddam Hussain
* @Date:        
* @Description: test Product data generation methods
* @Revision(s): [Date] - [Change Reference] - [Changed By] - [Description]  
***********************************************************************************************/
    public static testMethod void Product_test() {
        System.runAs(EC_UnitTestDataGenerator.adminUser) {
            Product_Custom__c prodctObj = EC_UnitTestDataGenerator.TestProductCustom.build(new Map<String, Object>{
                'Name'=>'Test Product',
                    'Pserno__c'=>'56780'
                    });
            insert prodctObj;
            
            Product_Custom__c prodctObj1 = EC_UnitTestDataGenerator.TestProductCustom.build(new Map<String, Object>{
                'Name'=>'Test Product',
                    'Pserno__c'=>'56781'
                    });
            insert prodctObj1; 
            
            Product_Custom__c prodctObj2 = EC_UnitTestDataGenerator.TestProductCustom.buildInsert();     
            
            List<String> attrs = new List<String>{'Name','Pserno__c'};
                prodctObj = EC_UnitTestDataGenerator.TestProductCustom.reload(prodctObj, attrs);  
            
            List<Product_Custom__c> productList = new List<Product_Custom__c>();
            productList.add(prodctObj);
            productList = EC_UnitTestDataGenerator.TestProductCustom.reloadList(productList, attrs);
            
            List<Product_Custom__c> resultList = [SELECT Id
                                                  FROM Product_Custom__c];
            System.assertEquals(3,resultList.size()); 
        }
    }
    
    /**********************************************************************************************
* @Author:      Saddam Hussain
* @Date:        
* @Description: test Finance data generation methods
* @Revision(s): [Date] - [Change Reference] - [Changed By] - [Description]  
***********************************************************************************************/
    public static testMethod void Finance_test() {
        System.runAs(EC_UnitTestDataGenerator.adminUser) {
            Account accountObj = EC_UnitTestDataGenerator.TestAccount.buildInsert(new Map<String, Object>{
            });
            
            Blob hash = Crypto.generateDigest('MD5', Blob.valueOf('0000000061'));
            string encryptSSN=EncodingUtil.convertToHex(hash);
            Contact contactObj = EC_UnitTestDataGenerator.TestContact.buildInsert(new Map<String, Object>{
                'AccountId'=>accountObj.Id, 
                    'Encrypted_SSN__c'=>encryptSSN
                    });
            
            Product_Custom__c prodobj = EC_UnitTestDataGenerator.TestProductCustom.buildInsert();
            Financial_Account__c finObj = EC_UnitTestDataGenerator.TestFinancialAccount.build(new Map<String, Object>{
                'Customer__c'=>accountObj.Id,
                    'Customer_Serno__c'=>contactObj.SerNo__c, 
                    'Product__c'=>prodobj.Id,
                    'Product_Serno__c'=>prodobj.Pserno__c,
                    'Account_Serno__c' => '5219291',
                    'Institution_Id__c' => 2,
                    'Account_Number__c'=>'7100010052192910'
                    });
            insert finObj;
            
            
            Financial_Account__c finObj2 = EC_UnitTestDataGenerator.TestFinancialAccount.buildInsert(new Map<String, Object>{
                'Customer__c'=>accountObj.Id,
                    'Customer_Serno__c'=>contactObj.SerNo__c, 
                    'Product__c'=>prodobj.Id,
                    'Product_Serno__c'=>prodobj.Pserno__c
                    });     
            
            List<String> attrs = new List<String>{'Account_Serno__c','Product_Serno__c','Institution_Id__c','Account_Number__c'};
                finObj = EC_UnitTestDataGenerator.TestFinancialAccount.reload(finObj, attrs);  
            
            List<Financial_Account__c> finObjList = new List<Financial_Account__c>();
            finObjList.add(finObj);
            finObjList = EC_UnitTestDataGenerator.TestFinancialAccount.reloadList(finObjList, attrs);
            
            List<Financial_Account__c> resultList = [SELECT Id
                                                     FROM Financial_Account__c];
            System.assertEquals(2,resultList.size()); 
        }
    }
    
    /**********************************************************************************************
* @Author:      Saddam Hussain
* @Date:        
* @Description: test Finance data generation methods
* @Revision(s): [Date] - [Change Reference] - [Changed By] - [Description]  
***********************************************************************************************/
    public static testMethod void Card_test() {
        System.runAs(EC_UnitTestDataGenerator.adminUser) {
            Account accountObj = EC_UnitTestDataGenerator.TestAccount.buildInsert(new Map<String, Object>{
            });
            
            Blob hash = Crypto.generateDigest('MD5', Blob.valueOf('0000000061'));
            string encryptSSN=EncodingUtil.convertToHex(hash);
            Contact contactObj = EC_UnitTestDataGenerator.TestContact.buildInsert(new Map<String, Object>{
                'AccountId'=>accountObj.Id, 
                    'Encrypted_SSN__c'=>encryptSSN
                    });
            
            Product_Custom__c prodobj = EC_UnitTestDataGenerator.TestProductCustom.buildInsert();
            Financial_Account__c finAccObj = EC_UnitTestDataGenerator.TestFinancialAccount.buildInsert(new Map<String, Object>{
                'Customer__c'=>accountObj.Id,
                    'Customer_Serno__c'=>contactObj.SerNo__c, 
                    'Product__c'=>prodobj.Id,
                    'Product_Serno__c'=>prodobj.Pserno__c
                    });
            
            Card__c cardObjs = EC_UnitTestDataGenerator.TestCard.build(new Map<String, Object>{
                'People__c'=>contactObj.Id,
                    'Financial_Account__c'=>finAccObj.Id, 
                    'Product__c'=>prodobj.Id,
                    'Prod_Serno__c'=>prodobj.Pserno__c,
                    'PrimaryCardFlag__c'=>false, 
                    'Card_Serno__c'=>'52485851', 
                    'Card_Number_Truncated__c'=>'713678581583561', 
                    'Financial_Account_Serno__c'=>'312123',
                    'Institution_Id__c'=>2
                    });
            insert cardObjs;
            
            Card__c cardObj = EC_UnitTestDataGenerator.TestCard.buildInsert(new Map<String, Object>{
                'People__c'=>contactObj.Id,
                    'Financial_Account__c'=>finAccObj.Id, 
                    'Product__c'=>prodobj.Id,
                    'Prod_Serno__c'=>prodobj.Pserno__c
                    });
            
            List<String> attrs = new List<String>{'People__c','Financial_Account__c','Product__c','Prod_Serno__c'};
                cardObj = EC_UnitTestDataGenerator.TestCard.reload(cardObj, attrs); 
            
            List<Card__c> cardList = new List<Card__c>();
            cardList.add(cardObj);
            cardList = EC_UnitTestDataGenerator.TestCard.reloadList(cardList, attrs);
            
            List<Card__c> resultList = [SELECT Id
                                        FROM Card__c];
            System.assertEquals(2,resultList.size()); 
        }
    }
    
    /**********************************************************************************************
* @Author:      Saddam Hussain
* @Date:        
* @Description: test case data generation methods
* @Revision(s): [Date] - [Change Reference] - [Changed By] - [Description]  
***********************************************************************************************/
    public static testMethod void Case_test() {
        System.runAs(EC_UnitTestDataGenerator.adminUser) {
            
            Case caseObj = EC_UnitTestDataGenerator.TestCase.build(new Map<String, Object>{
                'EC_Subject__c'=>'Ny överföring1', 
                    'Status'=>'New', 
                    'To_Address__c'=>'tekwkorts1@entercard.com',  
                    'Case_View__c'=>'SE Fund Transfer & Bill1 Payments',
                    'Category__c'=>'FT / Bill payment1'
                    });
            insert caseObj;
            Case caseObj1 = EC_UnitTestDataGenerator.TestCase.buildInsert();
            
            List<String> attrs = new List<String>{'EC_Subject__c','Status','To_Address__c','Case_View__c','Category__c'};
                caseObj = EC_UnitTestDataGenerator.TestCase.reload(caseObj, attrs); 
            
            List<Case> caseList = new List<Case>();
            caseList.add(caseObj);
            caseList = EC_UnitTestDataGenerator.TestCase.reloadList(caseList, attrs);
            
            List<Case> resultList = [SELECT Id
                                     FROM Case];
            System.assertEquals(2,resultList.size()); 
        }
    }
    
        /**********************************************************************************************
* @Author:      Saddam Hussain
* @Date:        
* @Description: test TestSalesforceTeam data generation methods
* @Revision(s): [Date] - [Change Reference] - [Changed By] - [Description]  
***********************************************************************************************/
    public static testMethod void TestSalesforceTeam_test() {
        System.runAs(EC_UnitTestDataGenerator.adminUser) {
            
            EC_SalesforceTeam__c sfTeamObj = EC_UnitTestDataGenerator.TestSalesforceTeam.build(new Map<String, Object>{
                'Name'=>'Test User1', 
                 'EC_Email__c'=>'test1.test@test.com' 
                    });
            insert sfTeamObj;
            EC_SalesforceTeam__c sfTeamObj1 = EC_UnitTestDataGenerator.TestSalesforceTeam.buildInsert();
            
            List<String> attrs = new List<String>{'Name','EC_Email__c'};
                sfTeamObj = EC_UnitTestDataGenerator.TestSalesforceTeam.reload(sfTeamObj, attrs); 
            
            List<EC_SalesforceTeam__c> sfTeamObjList = new List<EC_SalesforceTeam__c>();
            sfTeamObjList.add(sfTeamObj);
            sfTeamObjList = EC_UnitTestDataGenerator.TestSalesforceTeam.reloadList(sfTeamObjList, attrs);
            
            List<EC_SalesforceTeam__c> resultList = [SELECT Id
                                     FROM EC_SalesforceTeam__c];
            System.assertEquals(2,resultList.size()); 
        }
    }
}