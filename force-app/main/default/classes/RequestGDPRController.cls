public with sharing class RequestGDPRController{
    Public Request_GDPR__c req {set;get;}
    Public Id parentId {set;get;}
    Public Id financialAccountId {get;set;}
    Public Card__C crd {set;get;}
    Public String RightTo {set;get;}
    Public String AgentName {set;get;}
    Public String custName {set;get;}
    Public String custAddressStreet {set;get;}
    Public String custAddressCity {set;get;}
    Public String custAddressCountry {set;get;}
    Public String custAddressZip {set;get;}
    Transient String AccessDetails {set;get;}    
    public decimal instId {set;get;}
    public String printDate{get{      
        Datetime myDT = Datetime.now();
        String myDate = (instId!=1?myDT.format('yyyy-MM-dd hh:mm a'):myDT.format('dd-MM-YYYY'));
        return myDate;
    }private set;}    
    public RequestGDPRController ()
    {
        req = new Request_GDPR__c();
        AgentName = userInfo.getName();
        financialAccountId = getParameterValue('finId');
        system.debug('---financialAccountId--'+financialAccountId);
        crd = [Select id,
               People__c,
               Financial_Account__r.Account_Number__c, 
               Financial_Account__r.Institution_Id__c 
               from card__c where 
               Financial_Account__c = :financialAccountId 
               and PrimaryCardFlag__c=true]; 
        instId = crd.Financial_Account__r.Institution_Id__c;
        Address__c[] hmaddlst = [select id,
                                 		People__r.name,
                                 		Street_Address__c,
                                 		Street_Address_2__c,
                                 		Street_Address_3__c,
                                 		Street_Address_4__c,
                                 		Street_Address_5__c,
                                 		City__c,Country__c,
                                 		Location__c,
                                 		Zip__c 
                                 		from Address__c 
                                 		where 
                                 		People__c = :crd.People__c];
        for(Address__c add : hmaddlst){
            if(add.Location__c.equalsignorecase('home')){
                custName = add.People__r.name;
        	      	if (add.Street_Address__c==null)
            	    {add.Street_Address__c	='';}
                	if (add.Street_Address_2__c==null	)
                    {add.Street_Address_2__c	='';}
                	if (add.Street_Address_3__c==null	)
                    {add.Street_Address_3__c	='';}
	                if (add.Street_Address_4__c==null	)
                    {add.Street_Address_4__c	='';}
    	            if (add.Street_Address_5__c==null	)
                    {add.Street_Address_5__c	='';}
                if (add.city__c==null)
                    {add.city__c	='';}
        		if (add.country__c==null)
                    {add.country__c	='';}    
                    if (add.Zip__c==null	)
                    {add.Zip__c	='';}    
                
                custAddressStreet 	= add.Street_Address__c		+	' '
                    				+ add.Street_Address_2__c 	+ 	' ' 
                    				+ add.Street_Address_3__c 	+ 	' ' 
                    				+ add.Street_Address_4__c 	+ 	' '
                    				+ add.Street_Address_5__c;
                 custAddressZip 		= 	add.Zip__c;
                custAddressCity          = add.City__c;
                custAddressCountry		= add.Country__c;
            }
        }
    }
    public Id getParameterValue(string key)
    {
        return apexpages.currentpage().getparameters().get(key);        
    }
    public PageReference submit()
    {
        RightTo	=	req.Right_To__c;
        system.debug('Right To'+ RightTo);
        GetCustomerRightsInputs customerinputs = new GetCustomerRightsInputs();
        if(req.Request_Via__c == 'Phone' && crd.Financial_Account__r.Account_Number__c != null){
            try{
                                customerinputs.AccountNumber = String.valueOf(crd.Financial_Account__r.Account_Number__c);
                customerinputs.InstitutionId = String.valueOf(crd.Financial_Account__r.Institution_Id__c);

                system.debug('-----customerinputs----'+customerinputs);
                GetCustomerRightsOutputs customeroutputs = CSSServiceHelper.getCustomerRights(customerinputs);
                system.debug('------customeroutputs----'+customeroutputs);
                if(customeroutputs != null){
                    AccessDetails = customeroutputs.RightToAccessDetails;
                    Attachment attach = new Attachment();
                    attach.contentType =  'application/pdf';
                    attach.name = custName + System.now() +'.pdf';
                    attach.body = EncodingUtil.base64Decode(AccessDetails);
                    req.Financial_Account__c = financialAccountId;
                    insert req;
                    attach.parentId = req.Id ;
                    insert attach;
                    PageReference pref = new PageReference('https://entercard--c.eu6.content.force.com/servlet/servlet.FileDownload?file='+attach.Id);
                    return pref;
                    
                }
            } catch(dmlException e)   {
                StatusLogHelper.logSalesforceError('CustomerRightsInputs', 'E005', 'CustomerRightsInputs: ' + customerinputs,e, true);
            }
            catch(calloutException e)   {
                StatusLogHelper.logSalesforceError('CustomerRightsInputs', 'E001', 'CustomerRightsInputs: ' + customerinputs,e, true);
            }
            catch(Exception e)   {
                StatusLogHelper.logSalesforceError('CustomerRightsInputs', 'E004', 'CustomerRightsInputs: ' + customerinputs,e, true);
            }
        }
        system.debug('---Request_Via__c-----'+req.Request_Via__c);
        if(req.Request_Via__c == 'Post' && req.Right_To__c=='Data Portability')
        {
            
                PageReference pref = Page.RightToDataAccessPostPDFDK;
                return usageissue(pref);                
        }
        else if (req.Request_Via__c == 'Post' && req.Right_To__c=='Access')
        {
            
                PageReference pref = Page.RightToDataAccessPostPDFNO;
                return usageissue(pref);                
        }
        return null;
    }
    public pagereference usageissue(pagereference pref)
    {
        pref.getParameters().put('id',parentId);
        req.Financial_Account__c = financialAccountId;
        insert req;
        Attachment attach = new Attachment();
        Blob body;
        
        try {
            body = pref.getContent();
            
        } catch (VisualforceException e) {
            body = Blob.valueOf('Some Text');
        }
        
        attach.Body = body;
        attach.contentType =  'application/pdf';
        attach.name = custName + System.now() +'.pdf';
        attach.ParentId = req.Id;
        //attach.Body = RightToDataAccessPostPDF;
        insert attach;
        return    pref;
    }
    
}