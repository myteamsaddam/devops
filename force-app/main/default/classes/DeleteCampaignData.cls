global class DeleteCampaignData implements Database.Batchable<sObject>, schedulable{

    global String query;
    
    global Database.QueryLocator start(Database.BatchableContext BC)
    {
        query = 'SELECT Id FROM Manage_Campaign__c Where Start_Date__c < LAST_N_MONTHS:'+Common_Settings__c.getInstance('CampaignDataPurgeDuration').Common_Value__c;  
      
        return Database.getQueryLocator(query);
        
    }  
    
    global void execute(Database.BatchableContext BC, List<Manage_Campaign__c> scope)
    {
        try{
                
            delete scope;
        }
            
        catch(DmlException e)
            {
                System.debug('Unexpected error occurred: ' + e.getMessage());
                StatusLogHelper.logSalesforceError('DeleteManageCampaignData', 'E005', 'scope: ' + scope, e, true);
            }
            catch(calloutException e)
            {
                System.debug('Unexpected error occurred: ' + e.getMessage());
                StatusLogHelper.logSalesforceError('DeleteManageCampaignData', 'E001', 'scope: ' + scope, e, true);
            }
            catch(Exception e)
            {
                System.debug('Unexpected error occurred: ' + e.getMessage());
                StatusLogHelper.logSalesforceError('DeleteManageCampaignData', 'E004', 'scope: ' + scope, e, true);
            }
       

    }
    
    global void execute(SchedulableContext sc)
    {
        DeleteCampaignData d=new DeleteCampaignData();
        Database.executeBatch(d, 200);
    }
    
    global void finish(Database.BatchableContext BC)
    {
       
    }
    
}