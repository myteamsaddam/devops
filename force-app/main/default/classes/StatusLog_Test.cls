@isTest
public class StatusLog_Test{
    
     public static testmethod void testMethod1() {
         RestRequest req = new RestRequest(); 
         req.requestBody =blob.valueof(JSON.serialize(new StatusLog__c()));
         RestResponse res = new RestResponse();
    
        // pass the req and resp objects to the method     
         req.requestURI = 'https://test.salesforce.com/services/apexrest/StatusLogRecord';  
         req.httpMethod = 'POST';
         RestContext.request = req;
         RestContext.response = res;
         StatusLogRecord.doPost();
     }
     
     public static testmethod void testMethod2() {
         RestRequest req = new RestRequest(); 
         req.requestBody =blob.valueof('gdhhf');
         RestResponse res = new RestResponse();
    
        // pass the req and resp objects to the method     
         req.requestURI = 'https://test.salesforce.com/services/apexrest/StatusLogRecord';  
         req.httpMethod = 'POST';
         RestContext.request = req;
         RestContext.response = res;
         StatusLogRecord.doPost();
     }
     
}