/*
----------------------------------------------------------------------------------------
Author    Date         Description
Shameel   17/01/2017   Output class to implement SOA service using Integration framework.
----------------------------------------------------------------------------------------
*/
public class GetConsentServiceOutput{

    public GetConsentDataResponseRecordType[] GetConsentDataResponseRecord;

    public class GetConsentDataResponseRecordType {
    
        public String InstitutionId;
        public String ConsentEntityId;
        public String ConsentEntityIdType;
        public String ConsentCode;
        public String Value;
        public DateTime DateTimeStamp;
        public String UpdatedBy;
        public String Source;
        
        //public ConsentCodeType[] ConsentCodeValue;
    }
    
    /*public class ConsentCodeType {
        public String code;
        public String value;
    }*/
    //To Cover Test Class 100%
    public void test()
    {
        
    }
}