public class RetentionResponseController2 
{
    public Financial_Account__c fin{set;get;}
    public Id FinancialId {get;set;}
       public Retention_Response__c req 
    {
        get {
            if (req == null)
                req = new Retention_Response__c ();
            req.OwnerId = UserInfo.getUserId();          
            return req;
        }
        set;
    }
    
    public retentionresponsecontroller2(ApexPages.StandardController controller)
    {
        req = new Retention_Response__c();
    }
    public retentionresponsecontroller2()
    {
        FinancialId = getParameterValue('finId');   
        system.debug('----financial account --id------'+FinancialId); 
        fin =[select id,
              name
              from Financial_Account__c 
              where id=:FinancialId
              limit 1];
        if(fin!=null) 
        {
            set<Id> ContactIdSet = new set<Id>();
            List<Card__c> cardList = [SELECT Id, 
                                      Name, 
                                      People__c, 
                                      Financial_Account__c 
                                      FROM Card__c
                                      where People__c!=null 
                                      and Financial_Account__c=:fin.id 
                                      Limit 1];
            req.person__c = cardlist[0].People__c;
            req.Financial_Account__c= fin.id;
        }
    }
    public Id getParameterValue(string key)
    {      
        return apexpages.currentpage().getparameters().get(key);
    }
    public PageReference save() 
    {
        upsert req;
        return null;
    }
}