@IsTest(seeallData=false)
public class EncryptSSN_Test{
 static testmethod void testMethod1(){
        test.startTest();  
        
        Account acc = new Account();
        acc.name='test';
        acc.Customer_Serno__c='123';
        acc.Institution_Id__c=2;
        insert acc;
        
        Contact testContact = new Contact();
            testContact.FirstName = 'Foo';
            testContact.LastName = 'Bar';
            testContact.Email ='jdoe_test_test@doe.com';
            testContact.Customer_Email__c = 'arnob.dey@gmail.com';
            testContact.SSN__c='545677';
            testContact.Institution_Id__c=2;
            testContact.SerNo__c='1231';
            testContact.MobilePhone='09865432101';
            testContact.Phone='1231231231'; 
            testContact.Fax='987898791';
            testContact.HomePhone='1231231231';
            testContact.SerNo__c = 'test123';
        insert testContact;    
        
        test.stopTest();
        
       }
    }