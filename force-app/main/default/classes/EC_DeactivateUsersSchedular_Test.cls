@isTest(SeeAllData=false)
private class EC_DeactivateUsersSchedular_Test{
    static testmethod void testschedule(){
        System.runAs(EC_UnitTestDataGenerator.adminUser) {
            EC_DeactivateUsersSchedular.lastLoginDate = Date.today();
            
           EC_SalesforceTeam__c emailAdd = new EC_SalesforceTeam__c();
            emailAdd.Name ='Test User';
            emailAdd.EC_Email__c = 'test.test@test.com';
            insert emailAdd;
            
            set<Id> setAccId = new Set<ID>();
            setAccId.add(EC_UnitTestDataGenerator.adminUser.id);

            Test.StartTest();
            EC_DeactivateUsersSchedular schJob = new EC_DeactivateUsersSchedular();  
            String schTime = '0 0 0 * * ? *'; //this will schedule for 12:00:00 AM every day
            system.schedule('DeactivateUsers_Test:', schTime, schJob);
            EC_DeactivateUsersSchedular.sendDeactivateUsers(setAccId);
            Test.stopTest();  
        }
    }
}