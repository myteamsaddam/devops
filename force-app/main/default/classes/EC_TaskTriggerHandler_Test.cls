/**********************************************************************
Name: EC_TaskTriggerHandler_Test
=======================================================================
Purpose: This test class is used to cove the code of EC_TaskTriggerHandler class.User Story -SFD-1363

======================================================================= 
History
-----------------------------------------------------------------------         
Version     Author                   Date              Detail
1.0         Saddaam Hussain         03-June-2020       Initial Version

**********************************************************************/
@isTest(seeAllData = false)
public class EC_TaskTriggerHandler_Test {
    /**********************************************************************
*   Author: Saddam Hussain
*   Date:     20-May-2020
*   User Story : SFD-1430
*   Param: None
*   Return: None    
*   Description: In this method we are trying to Create the list of SaveDesk records to cover the code of updateTaskCount method.
* @Revision(s): [Date] - [Change Reference] - [Changed By] - [Description]   

**********************************************************************/
    static testMethod void updateTaskCountTest() {
        System.runAs(EC_UnitTestDataGenerator.adminUser) {
            SaveDeskQueue__c sdObj = EC_UnitTestDataGenerator.TestSaveDesk.buildInsert(new Map<String, Object>{
                'Status__c'=>'Open',
                    'Response__c'=>'Test',
                    'Task_Count__c'=>2
                    });
            List<SaveDeskQueue__c> saveDeskList = new List<SaveDeskQueue__c>();
            saveDeskList.add(sdObj);
            List<String>  sDeskAttr = new List<String> {'Status__c','Response__c','Calling_Attempts__c'};
                List<SaveDeskQueue__c> saveDeskReload = EC_UnitTestDataGenerator.TestSaveDesk.reloadList(saveDeskList, sDeskAttr);
            
            Task taskObj = EC_UnitTestDataGenerator.TestTask.buildInsert(new Map<String, Object>{
                'Savedesk__c'=>saveDeskList[0].Id,
                    'CreatedById'=>UserInfo.getUserId()
                    });
            List<Task> taskList = new List<Task>();
            taskList.add(taskObj);
            List<String>  taskAttr = new List<String> {'Savedesk__c','CreatedById'};
                List<Task> taskReload = EC_UnitTestDataGenerator.TestTask.reloadList(taskList,taskAttr);
            EC_TaskTriggerHandler.updateTaskCount(taskList);
        }
    }
}