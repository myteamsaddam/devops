global class UpdateAttNODK implements Database.Batchable<sObject> , database.stateful
{
    global integer x;	
    global UpdateAttNODK()
    {
     x=0;   
    }
global Database.QueryLocator start(Database.BatchableContext bc)
    {
        String query='SELECT ContentType, Id, Name FROM Attachment WHERE ContentType = \'.tmp\''+' and parentid in (SELECT id FROM memo__C where people__r.institution_id__C in (1,3))';
        return Database.getQueryLocator(query);
    }
global void execute(Database.BatchableContext bc, List<attachment> scope)
    {
        for(attachment A: scope)
        {
A.ContentType='application/vnd.openxmlformats-officedocument.wordprocessingml.document'; 
A.name = A.name.replace('.tmp', '.doc');          
 system.debug(A);
            x++;
        }
        update scope;
    }
     
    global void finish(Database.BatchableContext bc)
    {
       List<Messaging.SingleEmailMessage> emails = new List<Messaging.SingleEmailMessage>();
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        mail.subject = 'Email NO DK Memo Attachment temp Doc Change';
		mail.plainTextBody = 'The number of memo attachments updated from temp to doc are'+x;
        mail.setToAddresses(new String[] { 'shauryanaditya.singh@capgemini.com' ,'vikesh.gajula@capgemini.com','subhajit.a.pal@capgemini.com'});
        emails.add(mail);
        Messaging.sendEmail(emails);
        Messaging.SendEmailResult[] results = Messaging.sendEmail(emails);
		if (results[0].success) 
        {
    		System.debug('The email was sent successfully.');
				}
        	else 
            	{
    		System.debug('The email failed to send: ' + results[0].errors[0].message);
		}
   }
}