Public class GetNonCustomerInputs {
    Public String InstitutionId;
    Public String FirstName;
    Public String LastName;
    Public String Address1;
    Public String City;
    Public String Zip;
    Public Integer PartnerId;
    public String IdentifierType;
    Public String IdentifierValue;
    Public String UnionName;
    Public String DepartmentNumber;
}