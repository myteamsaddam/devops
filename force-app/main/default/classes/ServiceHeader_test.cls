@isTest
public class ServiceHeader_test {
    Public static testmethod void  test()
    {
        
       ServiceHeader sh = new ServiceHeader();
        
        sh.userId='hars';
        sh.systemName='abxc';
        sh.applicationType='xyz';
        sh.messageReference='qsa';
        sh.functionName='wsx';
        sh.legalEntity='asd';
        sh.userLanguage='eng';
        sh.customerId='kent';
        sh.loopback='monk';
        sh.timeZone='bom';
       sh.test();
        test.startTest();
        System.assertEquals('hars',  sh.userId);
        System.assertEquals('abxc',  sh.systemName);
        System.assertEquals('xyz',  sh.applicationType);
        System.assertEquals('qsa',  sh.messageReference);
        System.assertEquals('wsx',  sh.functionName);
        System.assertEquals('asd',  sh.legalEntity);
        System.assertEquals('eng',  sh.userLanguage);
        System.assertEquals('kent',  sh.customerId);
         System.assertEquals('monk',  sh.loopback);
         System.assertEquals('bom',  sh.timeZone);
         test.stopTest();
        
        
    }

}