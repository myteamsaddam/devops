public without sharing abstract class BASEService implements ServiceInterface {  
  public ServiceSettings__c serviceSettings; 
  public string guid;
  
  public string serviceEndpoint;
  public map<string, string> requestHeader;
  public string certificate;
  public Object requestBody;
  
  public Map<string, string> responseHeader;
  public Object responseBody;
  
  public ServiceStatus serviceStatus = new ServiceStatus();
  
  private Object serviceOutput;
  public ServiceIO serviceIO;
  
  private datetime executionStartTime;
  private long serviceInputProcessingTime;
  private long serviceCalloutTime;
  private long serviceOutputProcessingTime;
  private datetime executionEndTime;
    
  private abstract void prepareServiceRequest();
   
    private abstract void execute();
    
    private abstract void processServiceResponse();
    
    private void getServiceEndPoint(){
      serviceEndpoint = serviceSettings.EndPoint__c;          
    }
    
    private void getRequestHeader(){
      requestHeader = ServiceHelper.getServiceHeaders(serviceSettings.HeaderName__c);    
    }
    
    private void getCertificate(){
      certificate = ServiceHelper.getCertificate(serviceSettings.Certificate__c);    
    }
    
    private void getRequestBody(Object serviceInput){
      if(serviceIO != null){ 
          requestBody = serviceIO.convertInputToRequest(serviceInput, serviceEndpoint, requestHeader, guid);  
           System.debug('requestBody :  ' + requestBody);        
          System.debug('Service Input : ' + serviceInput);
          System.debug('Service Endpoint : ' + serviceEndpoint);
          System.debug('Service Request Header : ' + requestHeader);
      }
    }
    
    private void prepareOutput(){
      if(serviceIO != null && serviceSettings.OutputClass__c != null){
          //system.debug('@@responseBody' +responseBody);
        serviceOutput = serviceIO.convertResponseToOutput(responseBody, responseHeader, serviceStatus);
          System.Debug('prepareOutput : ' + serviceOutput);   
      }     
    }    
  
  public Object performServiceCallout(ServiceSettings__c serviceSettings, Object serviceInput){
      System.Debug('Start performServiceCallout');
      System.Debug('serviceInput : ' + serviceInput);
    executionStartTime = system.now();
    
    try{
      this.serviceSettings = serviceSettings;
      if(serviceSettings.ProcessingClass__c != null)
        this.serviceIO = new ServiceIO(serviceSettings.ProcessingClass__c);
      this.serviceStatus.status = 'S';
      //this.guid = GUIDGenerator.generateGUID();
      system.debug('GUID : ' + guid);
          
      getServiceEndPoint();
      getRequestHeader();    
      getCertificate();
      getRequestBody(serviceInput);
          
      prepareServiceRequest();
    }
    catch(Exception ex){
      serviceStatus.status = 'E';
        serviceStatus.errorCode = 'Input';
        serviceStatus.errorDescription = ' Processing Error : ' + ex.getMessage();
        system.debug('@@EroorBaseService :' + ex.getMessage() + ex.getstacktracestring());      
    }
    serviceInputProcessingTime = system.now().getTime() - executionStartTime.getTime();
    
    DateTime startTime = system.now();
        try{                
            if(serviceStatus.status == 'S') execute();              
        } catch(Exception ex) {
            serviceStatus.status = 'I';
        serviceStatus.errorCode = 'Interface';
        serviceStatus.errorDescription = ex.getMessage();
        }  
        serviceCalloutTime = system.now().getTime() - startTime.getTime();
        
        startTime = system.now(); 
        try{  
          processServiceResponse();        
          prepareOutput();
        }
        catch(Exception ex){
          serviceStatus.status = 'E';
        serviceStatus.errorCode = 'Output';
        serviceStatus.errorDescription = ' Processing Error : ' + ex.getMessage();
        system.debug('@@EroorBaseService :' + ex.getMessage() + ex.getstacktracestring());      
        }
        executionEndTime = system.now();
        serviceOutputProcessingTime = executionEndTime.getTime() - startTime.getTime();        
        
        logServiceStatus();
        System.Debug('End performServiceCallout');
        
        System.Debug('serviceOutput:' +string.valueof(serviceOutput));
        return serviceOutput;
        
    }    
    
    private void logServiceStatus(){
        System.Debug('Log Service Status');
      if(serviceSettings.LogStatus__c){
          StatusLog__c statusLog = new StatusLog__c();
          statusLog.Name = serviceSettings.Name;
          statusLog.UID__c = guid;
          statusLog.Status__c = serviceStatus.status;          
          statusLog.ErrorCode__c = serviceStatus.errorCode;
          statusLog.ErrorDescription__c = serviceStatus.errorDescription;
          statusLog.StartTime__c = executionStartTime;
          statusLog.InputProcessingTime__c = serviceInputProcessingTime;
          statusLog.CalloutTime__c = serviceCalloutTime;
          statusLog.OutputProcessingTime__c = serviceOutputProcessingTime;  
          statusLog.EndTime__c = executionEndTime;
          statusLog.Log_Type__c = ServiceHelper.SERVICE_LOG;
          if(serviceSettings.LogRequest__c && requestBody != null){
            string request;
            if(requestBody instanceof string)
              request = (String) requestBody;
            else
              request = string.valueof(requestBody);
            if(request.length() > 32767)  
                request = request.substring(0,32767);      
            statusLog.RequestBody__c = request;
          }
          if(serviceSettings.LogResponse__c && responseBody != null){
            string response;
            if(responseBody instanceof string)
              response = (String) responseBody;
            else
              response = string.valueof(responseBody);
            if(response.length() > 32767)
                response = response.substring(0,32767);
            statusLog.ResponseBody__c = response;
          }
          
          if(serviceSettings.LogStatus__c)
            StatusLogHelper.logStatus(statusLog, serviceSettings.LogAtFuture__c, serviceSettings.LogWithCallout__c);    
                  
      }
    }

}