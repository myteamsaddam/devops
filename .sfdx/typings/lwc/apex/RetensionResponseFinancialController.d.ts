declare module "@salesforce/apex/RetensionResponseFinancialController.GetFinancialAccount" {
  export default function GetFinancialAccount(param: {recordId: any}): Promise<any>;
}
declare module "@salesforce/apex/RetensionResponseFinancialController.fetchUser" {
  export default function fetchUser(): Promise<any>;
}
declare module "@salesforce/apex/RetensionResponseFinancialController.ReasonforAccountClosure" {
  export default function ReasonforAccountClosure(): Promise<any>;
}
declare module "@salesforce/apex/RetensionResponseFinancialController.getselectOptions" {
  export default function getselectOptions(): Promise<any>;
}
declare module "@salesforce/apex/RetensionResponseFinancialController.GetContact" {
  export default function GetContact(param: {recordId: any}): Promise<any>;
}
declare module "@salesforce/apex/RetensionResponseFinancialController.saveRetension" {
  export default function saveRetension(param: {contactname: any, reasonforacoountclousre: any, Accountclosed: any, FinancialAccount: any, AgentID: any, Ownerid: any}): Promise<any>;
}
