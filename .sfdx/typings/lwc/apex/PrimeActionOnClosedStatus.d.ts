declare module "@salesforce/apex/PrimeActionOnClosedStatus.getStatus" {
  export default function getStatus(param: {csId: any}): Promise<any>;
}
declare module "@salesforce/apex/PrimeActionOnClosedStatus.updateStatus" {
  export default function updateStatus(param: {caseId: any}): Promise<any>;
}
declare module "@salesforce/apex/PrimeActionOnClosedStatus.ownerAssign" {
  export default function ownerAssign(param: {caseId: any}): Promise<any>;
}
declare module "@salesforce/apex/PrimeActionOnClosedStatus.StatusUpdate" {
  export default function StatusUpdate(param: {caseId: any}): Promise<any>;
}
declare module "@salesforce/apex/PrimeActionOnClosedStatus.StatusUpdates" {
  export default function StatusUpdates(param: {caseId: any}): Promise<any>;
}
declare module "@salesforce/apex/PrimeActionOnClosedStatus.getPeople" {
  export default function getPeople(param: {cardId: any}): Promise<any>;
}
declare module "@salesforce/apex/PrimeActionOnClosedStatus.getFAdetails" {
  export default function getFAdetails(param: {finId: any}): Promise<any>;
}
