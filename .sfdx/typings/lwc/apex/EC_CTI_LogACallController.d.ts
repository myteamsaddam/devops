declare module "@salesforce/apex/EC_CTI_LogACallController.updateCtiTask" {
  export default function updateCtiTask(param: {finId: any, cardId: any, custId: any, category: any, description: any, caseType: any, memoFlag: any, callFlag: any, saveAtmp: any, saved: any, reason: any, offer: any}): Promise<any>;
}
declare module "@salesforce/apex/EC_CTI_LogACallController.getCategory" {
  export default function getCategory(): Promise<any>;
}
declare module "@salesforce/apex/EC_CTI_LogACallController.getType" {
  export default function getType(): Promise<any>;
}
declare module "@salesforce/apex/EC_CTI_LogACallController.updateCustomerName" {
  export default function updateCustomerName(param: {whoId: any, conId: any}): Promise<any>;
}
declare module "@salesforce/apex/EC_CTI_LogACallController.getCallType" {
  export default function getCallType(param: {custId: any}): Promise<any>;
}
declare module "@salesforce/apex/EC_CTI_LogACallController.getFinancialDetails" {
  export default function getFinancialDetails(param: {custId: any}): Promise<any>;
}
declare module "@salesforce/apex/EC_CTI_LogACallController.getCardDetails" {
  export default function getCardDetails(param: {financialVal: any, custId: any}): Promise<any>;
}
