declare module "@salesforce/apex/FinAccountforCustomer.getFindetails" {
  export default function getFindetails(param: {PeopleId: any}): Promise<any>;
}
