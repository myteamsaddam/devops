declare module "@salesforce/apex/CustomAssignToContrl.fetchLookUpValues" {
  export default function fetchLookUpValues(param: {searchKeyWord: any, ObjectName: any}): Promise<any>;
}
