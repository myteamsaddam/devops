declare module "@salesforce/apex/RetensionResponseCustomerController.getslectedContact" {
  export default function getslectedContact(param: {recordId: any}): Promise<any>;
}
declare module "@salesforce/apex/RetensionResponseCustomerController.fetchUser" {
  export default function fetchUser(): Promise<any>;
}
declare module "@salesforce/apex/RetensionResponseCustomerController.getFinancialAccountvalues" {
  export default function getFinancialAccountvalues(param: {recordId: any}): Promise<any>;
}
declare module "@salesforce/apex/RetensionResponseCustomerController.ReasonforAccountClosure" {
  export default function ReasonforAccountClosure(): Promise<any>;
}
declare module "@salesforce/apex/RetensionResponseCustomerController.getselectOptions" {
  export default function getselectOptions(): Promise<any>;
}
declare module "@salesforce/apex/RetensionResponseCustomerController.saveRetension" {
  export default function saveRetension(param: {contactname: any, reasonforacoountclousre: any, Accountclosed: any, FinancialAccount: any, AgentID: any, Ownerid: any}): Promise<any>;
}
