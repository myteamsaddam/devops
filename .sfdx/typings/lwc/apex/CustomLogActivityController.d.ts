declare module "@salesforce/apex/CustomLogActivityController.getFinRecs" {
  export default function getFinRecs(param: {CustId: any}): Promise<any>;
}
declare module "@salesforce/apex/CustomLogActivityController.getCardRecs" {
  export default function getCardRecs(param: {financialVal: any, CustId: any}): Promise<any>;
}
declare module "@salesforce/apex/CustomLogActivityController.doCaseUpdate" {
  export default function doCaseUpdate(param: {CasId: any, custId: any, finId: any, CardId: any, commnt: any, Catgry: any, SaveAtmp: any, Saved: any, Reason: any, Offer: any, type: any}): Promise<any>;
}
