declare module "@salesforce/apex/EC_CaseLogActivity.getFinancialAccounts" {
  export default function getFinancialAccounts(param: {custId: any}): Promise<any>;
}
declare module "@salesforce/apex/EC_CaseLogActivity.getCards" {
  export default function getCards(param: {financialId: any, custId: any}): Promise<any>;
}
declare module "@salesforce/apex/EC_CaseLogActivity.doCaseUpdate" {
  export default function doCaseUpdate(param: {casId: any, custId: any, finId: any, cardId: any, commnt: any, catgry: any, saveAtmp: any, saved: any, reason: any, offer: any, type: any}): Promise<any>;
}
