declare module "@salesforce/apex/ActivitySortController.fetchActivities" {
  export default function fetchActivities(param: {objId: any}): Promise<any>;
}
declare module "@salesforce/apex/ActivitySortController.getCustId" {
  export default function getCustId(param: {cseId: any}): Promise<any>;
}
declare module "@salesforce/apex/ActivitySortController.deleteTask" {
  export default function deleteTask(param: {TskId: any}): Promise<any>;
}
