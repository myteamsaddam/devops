declare module "@salesforce/apex/TransactionDetailsContrl.getTrans" {
  export default function getTrans(param: {CasId: any}): Promise<any>;
}
declare module "@salesforce/apex/TransactionDetailsContrl.deleteTxns" {
  export default function deleteTxns(param: {lstRecordId: any}): Promise<any>;
}
declare module "@salesforce/apex/TransactionDetailsContrl.updateTxns" {
  export default function updateTxns(param: {TxnId: any, chekboxval: any}): Promise<any>;
}
declare module "@salesforce/apex/TransactionDetailsContrl.getGross" {
  export default function getGross(param: {CasId: any}): Promise<any>;
}
declare module "@salesforce/apex/TransactionDetailsContrl.getTotalRisk" {
  export default function getTotalRisk(param: {CasId: any}): Promise<any>;
}
declare module "@salesforce/apex/TransactionDetailsContrl.updateChliable" {
  export default function updateChliable(param: {caseId: any, chLiableValue: any}): Promise<any>;
}
declare module "@salesforce/apex/TransactionDetailsContrl.getCaseOwnerId" {
  export default function getCaseOwnerId(param: {CasId: any}): Promise<any>;
}
