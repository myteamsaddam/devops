declare module "@salesforce/apex/LogACallController.doSave" {
  export default function doSave(param: {FinId: any, CardId: any, CustId: any, category: any, description: any, type: any, memchkbx: any, calchkbx: any, todoChek: any, SaveAtmp: any, Saved: any, Reason: any, Offer: any}): Promise<any>;
}
declare module "@salesforce/apex/LogACallController.doUpdate" {
  export default function doUpdate(param: {WhoId: any, ConId: any}): Promise<any>;
}
declare module "@salesforce/apex/LogACallController.getCalltype" {
  export default function getCalltype(param: {CustId: any}): Promise<any>;
}
declare module "@salesforce/apex/LogACallController.getFinRecs" {
  export default function getFinRecs(param: {CustId: any}): Promise<any>;
}
declare module "@salesforce/apex/LogACallController.getCardRecs" {
  export default function getCardRecs(param: {financialVal: any, CustId: any}): Promise<any>;
}
